#Technical points

##1. Cookie sessions
We use cookie to identify the user session. A session is stored in the "session" collection of the database. Each session record can be uniquely identified by its key. The cookie value is the encrypted session key. The creation of the session, encryption and generation of the cookie are done by the session manager middleware. We use [express-session](https://www.npmjs.com/package/express-session) as session manager:  
```javascript
var session = require("express-session");
var app = require("express")();

app.use(session({  
    name: "connect.sid",  
    secret: "<secret_key>",  
    store: new MongoStore({  
        url: "<db_path>",  
        collection: "session"  
    }),  
    cookie: {  
        httpOnly: false  
    },  
    resave: true,  
    saveUninitialized: true  
}));
```  
Here is the life cycle of a cookie


### Html request
First a request is sent from the browser to the "server" instance. For every request, a session is created in the "session" collection and its key is added in the html response. Once loaded inside the browser, a cookie named "connect.sid" is associated to the web page. The value of this cookie is not exactly the key of the session but an encrypted version of the key. The encryption key is defined on server side. **All the nodejs instances which want to share connect.sid cookie must have the same encryption/decryption password.**

### API request
For every request to the "api" instance, the "connect.sid" cookie is embedded in if it exists. This cookie will be


## Social Logins
### Facebook
Go to [facebook developper interface](https://developers.facebook.com/). You have to be logged in with facebook, and to have been given the permissions to access Scoragora apps.

In the "My apps" tab, you will find 2 apps:
+ **Scoragora_local**: enables social login tests from localhost
+ **Scoragora**: production app that enable social login from www.scoragora.com

Normally, everything needed for OAuth login is in Settings > Advanced > Client OAuth Settings || Security


### Google
Visit [Visit Google Cloud Console](https://console.developers.google.com/project).
You have to be logged in with the scoragora gmail account: scoragora@gmail.com to access the scoragora google app.
+ select the scoragora project
+ go into "APIs & Auth" menu (left)
+ Select Credentials

There you will have the Client ID for the applications (production and test from localhost). You can click on "Edit settings" to configure url origins and url callbacks for oauth.


### Twitter
Visit [twitter dev platform](https://apps.twitter.com)
Log in with scoragora twitter account (login: scoragora).
There will be two apps registered: one for production, one for tests from localhost.
The menu is pretty straight forward, you will easily find what you need (much simpler than google's and fb's).


##2. Angular login redirection
The redirection should occurred when the user is not logged in and it tries to access a state or a data from the API which is not public. When not logged in, the cookie sent to the API is recognized on server side as not logged in and the reply is an error 401. On client side a receptor will listen to 401 response and automatically redirect the user to the login page. When not logged in, the user should be redirected from the state it wants to access to the login page.


# Maintenance
A maintenance page done right (with a 503 HTTP status code) has been automated in nginx conf. To trigger the automation, you just have to rename ```server/views/maintenance_off.html``` to ```server/views/maintenance_on.html``` and nginx will redirect every request on www.scoragora.com to a pretty maintenance page. BEWARE : don't forget to revert it at the end of the maintenance op ;). more info @ https://www.calazan.com/how-to-configure-nginx-so-you-can-quickly-put-your-website-into-maintenance-mode/.
