## Automation ##

### 1. Tasks  ###
Tasks are displayed in the admin interface. It is possible to trigger some of them manually but the goal is to have it completely automated. A M or A flag says if the task has been triggered respectively manually or from the automated system. Every run task has a status (running, failed, succeeded), a duration, a number of threads allocated and the parameters given in input.

### 2. Score update  ###
Every minutes a task runs. First, this task determines the matches whose score has to be updated. Basically, it selects matches whose datetime has passed, whose score is not set and whose automation field is set. Then it calls a task to retrieve the match score.
[GetMatchScore task](https://github.com/edelans/brazuca/blob/master/scheduler/algos/get_match_score.js) triggers as many scrappers as available and waits for the reply. There is an election to determine the score to take into account. The elected result is the one returned by more than 50% of the scrappers. The key point of this algorithm is the accuracy of the match date.

### 3. Fixture setup  ###
For football national championship, all the games are automatically imported from different sources provided and automation links for score update are populated. After the initial competition creation, there is a bunch of unset matches which are only associated to a competition day. The goal is to automatically fill the match data as much as possible. The algorithm works as such:  
First the source scrapper will get all the match in the competition on the different days. It will retrieve the team names, the team source urls, the day and if possible the date. Then a check is done to verify if the details provided by the scrapper matches a match in the competition. The check relies on the teams info and the day associated. If a match is found, a date and automation source url update will be done. Otherwise an unset match will be picked for the given day. This match will be initialised with teams, automation source url and date (if provided).
This algorithm highly relies on the team identification. An algorithm launched for a given source requires all the competition teams to have their automation url (and name as optional) to be set for this source. The team identification is a key part of this algorithm. Based on the team identification, the match identification is done with the hometeam and awayteam.
Once a fixture has been setup, the automation algorithm can only update the automation url links and the date. It is really important to make sure the fixture setup has been done properly. This relies on the accuracy of the automation links provided for the teams.

### 4.  Date update  ###
This part has not been implemented yet
Every day any oncoming games with automation links set should have its date updated based on the one displayed on the automation links pages. This may lead to a huge number of request at the same time so a smart solution should be found.
