When migrating the prod server to another server (performance upgrade / downgrade for instance), remember to :

- new srv: setup the architecture
  - use ansible playbook, lots of things are already automated
  - make sure NODE_ENV = production (if applicable)
  - make sure apps are "npm installed"

- old srv: set the maintenance Page by hand (so it does not got on git repo)
- old srv: make a db dump
- scp the dump to new srv
- migrate DNS

- new srv: double checks :
  - double check letsencrypt autorenew once DNS have shifted (use etc/host or custom DNS to do is just after updating the DNS zone)
  - double check backup script is OK
