#Architecture

## Version 2.0
This project is composed with 4 nodejs instances dedicated to different tasks:
- [api](https://github.com/edelans/brazuca/tree/master/api) is an API server providing/processing the data from the database
- [server](https://github.com/edelans/brazuca/tree/master/server) is a web server rendering the client web site
- [server_admin](https://github.com/edelans/brazuca/tree/master/server_admin) is a web server rendering the admin web site
- [scheduler](https://github.com/edelans/brazuca/tree/master/scheduler) is an API server managing all the manual and automated tasks

All the big algorithms are done in python and defined inside the [computer](https://github.com/edelans/brazuca/tree/master/computer) project.


### Api
The nodejs server instance is launched by running app.js. This file contains all the configuration of the server or calls files to configurate the server. Let's describe the folders:
- _locales_ contains files for the translation. The translation is used on email generation before sending.
- _models_ contains files for data schema. Even though Mongodb is schemaless, we can declare some schema for each collection so that the data is structured as we want.
- _routes_ contains files for the routing. Based on the root url defined for the router in app.js, the routing is done on the remaining part of the url.  
Example: http://api.scoragora.com/api/users/52fa0174fa7eb05a04e07b8c/leagues request will be routed on users/52fa0174fa7eb05a04e07b8c/leagues  
In the routes folder the request in routed and redirected to the correct service before building the reply. In route folder no access to the db should be done for functional purpose.
- _services_ contains files for functional work. Here we are retrieving, updating and creating data. The expected reply from the route file is always a JSON object containing a status, a code and a message or data named according to its type.  
Example:  
```javascript 
{ status: 400, code: 40001, message: 'invalid input' }
```
- _sockets_ contains code for all the socket communication. Sockets are used for the league chat. Every other communication with the client is done via usual http requests
- _test_ contains all the functional testing scenario. Around 100 tests have been built to cover the api routes.

### Computer
This project is to provide algorithms used for long time processing. Nodejs doesnt fit that kind of job. 
Algorithms are accessed in the _services_ folder.