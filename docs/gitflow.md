#Git Flow

## Development flow
###1. Create a development branch
From github website you can click on the dropdown button 'branch: master' and then type the name of your working branch _dev_.
Or you can use command line:
```shell  
#create a local branch rugby
git branch dev
#push this branch to the central repo and add it to origin
git push -u origin dev  
```  

###2. Use development branch locally
```shell  
# clone the central repo
git clone https://github.com/edelans/brazuca.git  
# checkout dev branch
git checkout -b dev origin/dev  
``` 

###3.  


## Hotfix flow
###1. Create a hotfix branch
```shell  
# create a local branch for issue-#001  
git checkout -b issue-#001 master  
``` 

###2. Implement and commit the fix
```shell  
git status  
git commit -m "fixing issue-#001" .  
```

###3. Merge it to _master_ 
```shell  
git checkout master  
git merge issue-#001  
git push origin master  
```  
