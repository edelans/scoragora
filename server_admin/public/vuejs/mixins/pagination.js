export default {
  async created() {
    await this.loadItems(0, true);
  },
  methods: {
    async updatePagination(page) {
      if (this.pagination.page !== page) {
        this.pagination.page = page;
        await this.loadItems(page);
      }
    },
    async search() {
      await this.loadItems(0, true);
    },
    async loadItems(page, withCount) {
      const res = await this.paginationLoader({
        page,
        limit: this.limit,
        count: withCount,
        search: (this.form && this.form.search) || '',
      });
      if (res.status === 200) {
        this.pagination.page = page;
        this.pagination.items = res[this.paginationItemName];
        if (withCount) {
          this.pagination.count = res.count;
        }
      }
    },
    async onCreation() {
      await this.loadItems(this.pagination.page, true);
    },
    async onDeletion() {
      await this.loadItems(this.pagination.page, true);
    },
  },
};
