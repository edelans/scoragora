import notifier from '../common/Notifier';

export default {
  methods: {
    discard() {
      this.$parent.discard();
    },
    ok() {
      this.$parent.close();
    },
    handleResponse(res) {
      if (res.status === 200) {
        this.alert = {};
        this.ok();
        notifier.notify({
          type: 'success',
          message: res.message,
        }, 3000);
      }
      this.alert = {
        type: 'danger',
        message: res.message,
      };
    },
  },
};
