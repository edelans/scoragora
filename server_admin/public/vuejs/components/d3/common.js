import * as d3 from 'd3';

class Tooltip {
  get tooltip() {
    if (!this.tt) {
      this.tt = d3.select('body').append('div')
      .attr('class', 'd3-tooltip')
      .style('opacity', 0);
    }
    return this.tt;
  }

  show(html, top, left) {
    if (this.timeout) {
      clearTimeout(this.timeout);
    }
    if (typeof html === 'object') {
      html = `<div class="d3-tt-label">${html.label}</div><div class="d3-tt-value">${html.value}</div>`;
    }
    this.tooltip.html(html)
      .style('top', `${top}px`)
      .style('left', `${left}px`);
    this.tooltip.transition()
      .duration(200)
      .style('opacity', 0.9);
  }

  hide() {
    this.tooltip.transition()
      .duration(400)
      .style('opacity', 0);
    this.timeout = setTimeout(() => {
      this.tooltip.html('')
      .style('left', '0px')
      .style('top', '0px');
    }, 410);
  }
}

const TOOLTIP = new Tooltip();

/**
 * @param data [{ value: Number, label: String }]
 * @param options { width, height }
 */
export const drawPieChart = (el, data, options) => {
  const color = d3.scaleOrdinal(d3.schemeCategory10);

  const WIDTH = options.width;
  const HEIGHT = options.height;
  const MARGIN = 20;

  const OUTER_RADIUS = (Math.max(WIDTH, HEIGHT) / 2) - MARGIN;
  const INNER_RADIUS = OUTER_RADIUS / 2;

  const pie = d3.pie()
  .value(d => d.value)
  .padAngle(0.02);

  const arc = d3.arc()
  .padRadius(OUTER_RADIUS)
  .innerRadius(INNER_RADIUS);

  const svg = d3.select(el).append('g')
  .attr('transform', `translate(${WIDTH / 2},${HEIGHT / 2})`);

  svg.selectAll('path')
  .data(pie(data))
  .enter().append('path')
  .each((d) => {
    d.percentage = Math.round((100 / 2 / Math.PI) * (d.endAngle - d.startAngle));
    d.outerRadius = OUTER_RADIUS - MARGIN;
  })
  .attr('d', arc)
  .attr('fill', d => color(d.value))
  .on('mouseover', arcTween(OUTER_RADIUS, 0))
  .on('mouseout', arcTween(OUTER_RADIUS - MARGIN, 150, true));
  svg.append('text')
  .attr('dy', '.35em')
  .style('text-anchor', 'middle')
  .style('font-size', Math.min(INNER_RADIUS/3, (INNER_RADIUS/3 - 8) / 5 * 24) + 'px')
  .attr('class', 'sc-pie-value')
  .attr('transform', 'translate(0, ' + 2*INNER_RADIUS/3 + ')')
  .text(() => '');
  svg.append('text')
  .attr('dy', '.35em')
  .style('text-anchor', 'middle')
  .style('font-size', Math.min(INNER_RADIUS / 3, (INNER_RADIUS / 3 - 8) / 4 * 24) + 'px')
  .attr('class', 'sc-pie-label')
  .attr('transform', 'translate(0, -' + 2 * INNER_RADIUS / 3 + ')')
  .text(() => '');
  svg.append('text')
  .attr('dy', '.35em')
  .style('text-anchor', 'middle')
  .style('font-size', Math.min((2 * INNER_RADIUS) / 3, ((2 * INNER_RADIUS) / 3 - 8) / 5 * 24) + 'px')
  .attr('class', 'sc-pie-percentage')
  .text(() => '');


  function arcTween(outerRadius, delay, out) {
    return function() {
      var element = d3.select(this);
      var data = element.data()[0];
      if (!out) {
        svg.select('.sc-pie-percentage')
        .text(data.percentage + '%')
        .attr('fill', color(data.value));
        svg.select('.sc-pie-label')
        .text(data.data.label)
        .attr('fill', color(data.value));
        svg.select('.sc-pie-value')
        .text(data.value)
        .attr('fill', color(data.value));
      }
      else {
        svg.select('.sc-pie-percentage').text('');
        svg.select('.sc-pie-label').text('');
        svg.select('.sc-pie-value').text('');
      }
      element.transition().delay(delay).attrTween('d', function(d) {
        var i = d3.interpolate(d.outerRadius, outerRadius);
        return function(t) { d.outerRadius = i(t); return arc(d); };
      });
    };
  }
};

/**
 * @param data [{ away: { value: Number, percentage: Number }, home: {} }] sorted by date
 * @param options { width, height, verticalLabel, position }
 */
export const drawBarChart = (el, data, options) => {
  const MARGIN = { top: 20, right: 20, bottom: 30, left: 40 };
  const WIDTH = options.width - 50;
  const HEIGHT = options.height - 60;
  const position = options.position;

  const x = d3.scaleBand()
  .rangeRound([0, WIDTH], 0.1);

  const y = d3.scaleLinear()
  .range([HEIGHT, 0]);

  const xAxis = d3.axisBottom()
  .scale(x);

  const yAxis = d3.axisLeft()
  .scale(y);

  const svg = d3.select(el)
  .attr('width', WIDTH + MARGIN.left + MARGIN.right)
  .attr('height', HEIGHT + MARGIN.top + MARGIN.bottom)
  .append('g')
  .attr('transform', 'translate(' + MARGIN.left + ',' + MARGIN.top + ')');

  x.domain(data.map(d => d.label));
  y.domain([0, d3.max(data, d => d.value)]).nice();

  svg.append('g')
  .attr('class', 'x axis')
  .attr('transform', 'translate(0,' + HEIGHT + ')')
  .call(xAxis);

  svg.append('g')
  .attr('class', 'y axis')
  .call(yAxis)
  .append('text')
  .attr('transform', 'rotate(-90)')
  .attr('y', 6)
  .attr('dy', '.71em')
  .attr('fill', '#000')
  .style('text-anchor', 'end')
  .text(options.verticalLabel || '');

  svg.selectAll('.bar')
  .data(data)
  .enter().append('rect')
  .attr('class', 'bar')
  .attr('x', function(d) { return x(d.label); })
  .attr('width', x.rangeBand())
  .attr('y', function(d) { return y(d.value); })
  .attr('height', function(d) { return HEIGHT - y(d.value); })
  .on('mouseover', (d) => {
    const left = position.left + x(d.label);
    const top = position.top + (HEIGHT * 0.7);
    TOOLTIP.show({ label: d.value, value: d.percentage + '%' }, top, left);
  })
  .on('mouseout', () => {
    TOOLTIP.hide();
  });
};

/**
 * @param data [{ away: { value: Number, percentage: Number }, home: {} }] sorted by date
 * @param options { width, height, verticalLabel, position }
 */
export const drawGroupedBarChart = (el, data, options) => {
  const margin = { top: 20, right: 20, bottom: 30, left: 40 };
  const width = options.width - 50;
  const height = options.height - 60;
  const position = options.position;

  const y = d3.scaleLinear()
    .range([height, 0]);

	const x = d3.scaleBand()
	  .range([0, width], 0.2);

	const x1 = d3.scaleBand()
		.domain(d3.range(2))
		.range([0, x.step()]);

	const xAxis = d3.axisBottom()
	    .scale(x);

	x.domain(data.map(d => d.value));

	y.domain([0, d3.max(data, function(score) {
		if (!score.home) score.home = {value: 0, percentage: 0};
		if (!score.away) score.away = {value: 0, percentage: 0};
		return Math.max(score.home.percentage, score.away.percentage);
	}) ]);

	var yAxis = d3.axisLeft()
	    .scale(y);

	const svg = d3.select(el).append('g')
	  .attr('transform', 'translate(' + margin.left + ',' + margin.top + ')');

	svg.append('g')
	  .attr('class', 'y axis')
	  .call(yAxis)
    .append('text')
    .attr('transform', 'rotate(-90)')
    .attr('y', 6)
    .attr('dy', '.71em')
    .attr('fill', '#000')
    .style('text-anchor', 'end')
    .style('font-size', '14px')
    .text(options.verticalLabel || '');

	svg.append('g')
	  .attr('class', 'x axis')
	  .attr('transform', 'translate(0,' + height + ')')
	  .call(xAxis);

	const group = svg.append('g').selectAll('g')
	    .data(data)
	  .enter();
	group.append('g')
	  .attr('transform', d => 'translate(0,0)')

	  /*  .attr('transform', function(d) { return 'translate(' + x1(1) + ',0)'; })
	  .select('rect')*/

	  .append('rect')
	  	.attr('class', 'home-bar')
	    .attr('width', x.step() * 0.5 - 4)
	    .attr('height', d => (height - y(d.home.percentage)))
	    .attr('x', d => (x(d.value) + 3))
	    .attr('y', d => y(d.home.percentage))
	  .on('mouseover', (d) => {
      const top = position.top + height*0.6;
      const left = position.left + x(d.value);
      TOOLTIP.show({ label: d.home.value, value: d.home.percentage + '%' }, top, left);
	  })
	  .on('mouseout', () => {
      TOOLTIP.hide();
	  });

	group.append('g')
	    .attr('transform', d => `translate(${(x.step() * 0.5 + 1)},0)`)
	  .append('rect')
	    .attr('class', 'away-bar')
	    .attr('width', x.step() * 0.5 - 4)
	    .attr('height', d => (height - y(d.away.percentage)))
	    .attr('x', d => x(d.value))
	    .attr('y', d => y(d.away.percentage))
	  .on('mouseover', (d) => {
      const top = position.top + height * 0.6;
      const left = position.left + x(d.value);
      TOOLTIP.show({ label: d.away.value, value: d.away.percentage + '%' }, top, left);
	  })
	  .on('mouseout', () => {
	    TOOLTIP.hide();
	  });
};

export const drawScatterPlotChart = (el, data, options) => {
  const margin = { top: 20, right: 20, bottom: 30, left: 40 };
  const width = options.width - 50;
  const height = options.height - 60;

  const x = d3.scaleLinear()
  .range([0, width]);

  const y = d3.scaleLinear()
  .range([height, 0]);

  const xAxis = d3.axisBottom()
  .scale(x);

  const yAxis = d3.axisLeft()
  .scale(y);

  const svg = d3.select(el)
  .append('g')
  .attr('transform', 'translate(' + margin.left + ',' + margin.top + ')');

  data.forEach(function(d) {
    d.processes = +d.processes;
    d.time = +d.time*0.001;
    d.count = +d.count;
  });
  x.domain([0, d3.max(data, function(d) { return d.processes; }) + 1]).nice();
  y.domain([0, d3.max(data, function(d) { return d.time; })]).nice();

  svg.append('g')
  .attr('class', 'x axis')
  .attr('transform', 'translate(0,' + height + ')')
  .call(xAxis)
  .append('text')
  .attr('class', 'label')
  .attr('x', width)
  .attr('y', -6)
  .attr('fill', '#000')
  .style('text-anchor', 'end')
  .text(options.horizontalLabel || '');

  svg.append('g')
  .attr('class', 'y axis')
  .call(yAxis)
  .append('text')
  .attr('class', 'label')
  .attr('transform', 'rotate(-90)')
  .attr('y', 6)
  .attr('dy', '.71em')
  .attr('fill', '#000')
  .style('text-anchor', 'end')
  .text(options.verticalLabel || '');

  const dot = svg.selectAll('.dot')
  .data(data)
  .enter();
  dot.append('circle')
  .attr('class', 'dot')
  .attr('r', function(d) {
    return 8 + 3*Math.sqrt(d.count);
  })
  .attr('cx', function(d) { return x(d.processes); })
  .attr('cy', function(d) { return y(d.time); })
  .style('fill', '#19BF6C');

  dot.append('text')
  .attr('class', 'label')
  .attr('transform', function(d) {
    return 'translate(' + x(d.processes) + ', ' + y(d.time) + ')';
  })
  .style('font-size',  function(d) {
    var radius = 8 + 3*Math.sqrt(d.count);
    return Math.min(radius, (radius - 8) / 3 * 24) + 'px';
  })
  .attr('fill', 'white')
  .attr('y', function(d) {
    var radius = 8 + 3 * Math.sqrt(d.count);
    return Math.min(radius / 3, (radius - 8) / 9 * 24) + 'px';
  })
  .style('text-anchor', 'middle')
  .text(function(d) {
    return Math.round(d.time*10)/10;
  });
};

/**
 * @param data [{ date: Date, value: Number }] sorted by date
 * @param options { width, height, verticalLabel }
 */
export const drawTimeLineChart = (el, data, options) => {

  const MARGIN = { top: 20, right: 20, bottom: 30, left: 50 };
  const WIDTH = options.width - MARGIN.left - MARGIN.right;
  const HEIGHT = options.height - MARGIN.top - MARGIN.bottom;

  const formatDate = d3.timeFormat('%d-%m-%Y');

  const x = d3.scaleTime()
  .range([0, WIDTH]);

  const y = d3.scaleLinear()
  .range([HEIGHT, 0]);

  const xAxis = d3.axisBottom()
  .scale(x);

  const yAxis = d3.axisLeft()
  .scale(y);

  const line = d3.line()
  .x(d => x(d.date))
  .y(d => y(d.value));

  const svg = d3.select(el).append('g')
  .attr('transform', 'translate(' + MARGIN.left + ',' + MARGIN.top + ')');

  x.domain(d3.extent(data, d => d.date));
  y.domain(d3.extent(data, d => d.value));

  svg.append('g')
  .attr('class', 'x axis')
  .attr('transform', 'translate(0,' + HEIGHT + ')')
  .call(xAxis);

  svg.append('g')
  .attr('class', 'y axis')
  .call(yAxis)
  .append('text')
  .attr('transform', 'rotate(-90)')
  .attr('y', 6)
  .attr('dy', '.71em')
  .attr('fill', '#000')
  .style('text-anchor', 'end')
  .text(options.verticalLabel || '');

  svg.append('path')
  .datum(data)
  .attr('class', 'time-line')
  .attr('d', line);

  svg.selectAll('.dot')
    .data(data)
    .enter()
    .append('circle')
    .attr('class', 'time-line-dot')
    .attr('r', 3)
    .attr('cx', (d,i) => x(d.date))
    .attr('cy', (d,i) => y(d.value))

  svg.selectAll('.dot')
    .data(data)
    .enter()
    .append('circle')
    .attr('class', 'time-line-dot-hidden')
    .attr('r', 10)
    .attr('cx', (d,i) => x(d.date))
    .attr('cy', (d,i) => y(d.value))
    .on('mouseover', function(d) {
      TOOLTIP.show({ label: formatDate(d.date), value: d.value }, d3.event.pageY - 70, d3.event.pageX - 40);
    })
    .on('mouseout', function(d) {
      TOOLTIP.hide();
    });
};
