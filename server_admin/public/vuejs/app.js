import Vue from 'vue';
import VueRouter from 'vue-router';
import BootstrapTooltip from 'bootstrap-vue/es/components/tooltip/tooltip';
import BootstrapFormRadio from 'bootstrap-vue/es/components/form-radio/form-radio';
import BootstrapFormRadioGroup from 'bootstrap-vue/es/components/form-radio/form-radio-group';

import './directives/index';
import './filters/index';
import './components/index';
import auth from './common/AuthServices';
import routes from './routes';

require('bootstrap/dist/css/bootstrap.css');

Vue.use(VueRouter);
Vue.component('b-tooltip', BootstrapTooltip);
Vue.component('b-form-radio', BootstrapFormRadio);
Vue.component('b-form-radio-group', BootstrapFormRadioGroup);


const router = new VueRouter({
  routes,
  scrollBehavior: () => ({ x: 0, y: 0 }),
});
auth.setRouter(router);
router.beforeEach((to, from, next) => {
  if (to.path !== '/login' && !auth.isAuthenticated()) {
    const dest = to.name && to.params ? { name: to.name, params: to.params } : { path: to.path };
    auth.setRedirection(dest);
    next({ path: '/login' });
    return;
  }
  next();
});

new Vue({
  router,
  template: '<div class="container"><router-view></router-view></div>',
}).$mount('#app');
