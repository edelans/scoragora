import Home from './vues/home.vue';
import Login from './vues/login.vue';
import Layout from './vues/layout.vue';

const CompetitionDetail = () => import(/* webpackChunkName: "competition" */ './vues/competitions/detail.vue');
const CompetitionJson = () => import(/* webpackChunkName: "competition" */ './vues/competitions/json.vue');
const CompetitionList = () => import(/* webpackChunkName: "competition" */ './vues/competitions/index.vue');
const CompetitionStats = () => import(/* webpackChunkName: "competition" */ './vues/competitions/stats.vue');

const LeagueDetail = () => import(/* webpackChunkName: "league" */ './vues/leagues/detail.vue');
const LeagueList = () => import(/* webpackChunkName: "league" */ './vues/leagues/index.vue');
const LeagueJson = () => import(/* webpackChunkName: "league" */ './vues/leagues/json.vue');

const LogList = () => import(/* webpackChunkName: "log" */ './vues/logs/index.vue');
const MailList = () => import(/* webpackChunkName: "mail" */ './vues/mails/index.vue');

const MatchDetail = () => import(/* webpackChunkName: "match" */ './vues/matches/detail.vue');
const MatchList = () => import(/* webpackChunkName: "match" */ './vues/matches/index.vue');
const MatchJson = () => import(/* webpackChunkName: "league" */ './vues/matches/json.vue');

const RuleList = () => import(/* webpackChunkName: "league" */ './vues/rules/index.vue');

const TaskList = () => import(/* webpackChunkName: "task" */ './vues/tasks/index.vue');

const TeamDetail = () => import(/* webpackChunkName: "team" */ './vues/teams/detail.vue');
const TeamList = () => import(/* webpackChunkName: "team" */ './vues/teams/index.vue');
const TeamJson = () => import(/* webpackChunkName: "team" */ './vues/teams/json.vue');

const UserList = () => import(/* webpackChunkName: "user" */ './vues/users/index.vue');
const UserDetail = () => import(/* webpackChunkName: "user" */ './vues/users/detail.vue');
const UserJson = () => import(/* webpackChunkName: "user" */ './vues/users/json.vue');

export default [{
  path: '/', component: Home, name: 'home',
}, {
  path: '/login',
  component: Login,
  name: 'login',
  beforeEnter(to, from, next) {
    document.title = 'Admin - Login';
    next();
  },
}, {
  path: 'abstract layout',
  component: Layout,
  beforeEnter(to, from, next) {
    document.title = 'Admin';
    next();
  },
  children: [
      { path: '/competitions', name: 'competitions', component: CompetitionList },
      { path: '/competitions/:id', name: 'competitionDetail', component: CompetitionDetail },
      { path: '/competitions/:id/json', name: 'competitionJson', component: CompetitionJson },
      { path: '/competitions/:id/stats', name: 'competitionStats', component: CompetitionStats },

      { path: '/leagues', name: 'leagues', component: LeagueList },
      { path: '/leagues/:id', name: 'leagueDetail', component: LeagueDetail },
      { path: '/leagues/:id/json', name: 'leagueJson', component: LeagueJson },

      { path: '/logs', name: 'logs', component: LogList },
      { path: '/mails', name: 'mails', component: MailList },

      { path: '/matches', name: 'matches', component: MatchList },
      { path: '/matches/:id/', name: 'matchDetail', component: MatchDetail },
      { path: '/matches/:id/json', name: 'matchJson', component: MatchJson },

      { path: '/rules', name: 'rules', component: RuleList },

      { path: '/tasks', name: 'tasks', component: TaskList },

      { path: '/teams', name: 'teams', component: TeamList },
      { path: '/teams/:id', name: 'teamDetail', component: TeamDetail },
      { path: '/teams/:id/json', name: 'teamJson', component: TeamJson },
      { path: '/users', name: 'users', component: UserList },
      { path: '/users/:id', name: 'userDetail', component: UserDetail },
      { path: '/users/:id/json', name: 'userJson', component: UserJson },
  ],
}];
