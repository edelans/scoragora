import auth from './AuthServices';

const buildUrl = (path, query) => {
  const queries = [];
  Object.keys(query)
  .filter(key => query[key] !== undefined && query[key] !== null)
  .forEach((key) => {
    if (query[key] instanceof Array) {
      query[key].forEach((value) => {
        queries.push(`${key}=${value}`);
      });
    } else {
      queries.push(`${key}=${query[key]}`);
    }
  });
  return `${path}?${queries.join('&')}`;
};

const handleResponseWithoutStatus = () => ({ status: 500, message: 'Unexpected error' });

const buildHeaders = () => ({
  Accept: 'application/json',
  'Content-Type': 'application/json',
  Authorization: `Bearer ${auth.getToken()}`,
});

class APIServices {

  constructor(root) {
    this.APIRoot = root;
  }

  get(url, query = {}) {
    return fetch(buildUrl(this.APIRoot + url, query), {
      headers: buildHeaders(),
    })
    .then(async (response) => {
      const json = await response.json();
      if (json.status === 401) {
        auth.redirectToLogin();
        return;
      }
      return json;
    })
    .catch(handleResponseWithoutStatus)
  }

  post(url, body = {}) {
    return fetch(this.APIRoot + url, {
      method: 'POST',
      headers: buildHeaders(),
      body: JSON.stringify(body),
    })
    .then(async (response) => {
      const json = await response.json();
      if (json.status === 401) {
        auth.redirectToLogin();
        return;
      }
      return json;
    })
    .catch(handleResponseWithoutStatus)
  }

  put(url, body = {}) {
    return fetch(this.APIRoot + url, {
      method: 'PUT',
      headers: buildHeaders(),
      body: JSON.stringify(body),
    })
    .then(async (response) => {
      const json = await response.json();
      if (json.status === 401) {
        auth.redirectToLogin();
        return;
      }
      return json;
    })
    .catch(handleResponseWithoutStatus)
  }

  del(url) {
    return fetch(this.APIRoot + url, {
      method: 'DELETE',
      headers: buildHeaders(),
    })
    .then(async (response) => {
      const json = await response.json();
      if (json.status === 401) {
        auth.redirectToLogin();
        return;
      }
      return json;
    })
    .catch(handleResponseWithoutStatus)
  }
}

const API = new APIServices(window.API_ROOT);

export default API;
export const api = API;
export const scheduler = new APIServices(window.SCHEDULER_API_ROOT);
