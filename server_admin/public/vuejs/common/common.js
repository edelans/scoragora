export const AUTOMATION_EXAMPLES = {
  competitions: {
    football: {
      lequipe: 'https://www.lequipe.fr/Football/ligue-1-resultats.html',
      espn: 'http://www.espnfc.co.uk/french-ligue-1/9/index',
      eurosport: '',
    },
    rugby: {
      lequipe: '',
      espn: '',
      eurosport: '',
    },
  },
  matches: {
    football: {
      lequipe: 'https://www.lequipe.fr/Football/match/407032',
      espn: 'http://www.espnfc.co.uk/match?gameId=449278',
      eurosport: 'https://www.eurosport.fr/football/ligue-1/2017-2018/dijon-fco-stade-rennais_mtc947340/live.shtml',
    },
    rugby: {
      lequipe: '',
      espn: '',
      eurosport: '',
    },
  },
  teams: {
    football: {
      lequipe: 'https://www.lequipe.fr/Football/FootballFicheClub44.html',
      espn: 'http://www.espn.co.uk/football/club/_/id/3335',
      eurosport: 'http://www.eurosport.fr/football/equipes/amiens/teamcenter.shtml',
    },
    rugby: {
      lequipe: '',
      espn: '',
      eurosport: '',
    },
  },
};

export const CRITERIAS = {
  football: ['direction', 'team_score', 'difference'],
  rugby: ['direction', 'direction_type'],
  tennis: ['direction', 'set_score'],
};

export const ROUNDS = [
  { value: 1, label: 'Final' },
  { value: 2, label: 'Semies' },
  { value: 4, label: 'Quarters' },
  { value: 8, label: '8ths' },
  { value: 16, label: '16ths' },
];

export const SOURCES = ['lequipe', 'espn', 'eurosport'];
export const SPORTS = ['football', 'rugby', 'tennis'];

export const roundToString = (round) => {
  const rnd = typeof round === 'string' ? parseInt(round, 10) : round;
  switch (rnd) {
    case 1:
      return 'finale';
    case 2:
      return 'semi finals';
    case 3:
      return 'third place';
    case 4:
      return 'quarter finals';
    case 8:
      return 'eighth finals';
    default:
      return round;
  }
};

export const idToTimestamp = id => parseInt(id.substring(0, 8), 16) * 1000;

export const debounce = (func, wait, immediate) => {
  let timeout;
  return () => {
    const later = () => {
      timeout = null;
      if (!immediate) {
        func();
      }
    };
    const callNow = immediate && !timeout;
    clearTimeout(timeout);
    timeout = setTimeout(later, wait);
    if (callNow) {
      func();
    }
  };
};
