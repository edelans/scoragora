const TOKEN_NAME = 'satellizer_token';

class AuthService {
  constructor() {
    this.tokenNotFound = false;
  }

  setRouter(router) {
    this.router = router;
  }

  setRedirection(redirection) {
    this.redirection = redirection || { name: 'home' };
  }

  setTokenAndRedirect(tokenString) {
    localStorage.setItem(TOKEN_NAME, tokenString);
    this.token = tokenString;
    const redirection = this.redirection || { name: 'home' };
    this.router.push({ name: redirection.name, params: redirection.params });
  }

  redirectToLogin() {
    this.router.push({ name: 'login' });
  }

  getToken() {
    if (!this.token && !this.tokenNotFound) {
      this.token = localStorage.getItem(TOKEN_NAME);
      if (!this.token) {
        this.tokenNotFound = true;
      }
    }
    return this.token;
  }

  isAuthenticated() {
    this.getToken();
    if (this.token) {
      if (this.token.split('.').length === 3) {
        const base64Url = this.token.split('.')[1];
        const base64 = base64Url.replace('-', '+').replace('_', '/');
        const exp = JSON.parse(window.atob(base64)).exp;
        if (exp) {
          return Math.round(new Date().getTime() / 1000) <= exp;
        }
        return true;
      }
      return true;
    }
    return false;
  }
}

export default new AuthService();
