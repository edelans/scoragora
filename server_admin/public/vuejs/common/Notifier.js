class Notifier {
  constructor() {
    this.nextId = 0;
    this.notifications = [];
  }

  notify(alert, timeout) {
    const notif = {
      id: this.nextId,
      type: alert.type,
      message: alert.message,
    };
    this.notifications.push(notif);
    this.nextId += 1;
    if (timeout) {
      setTimeout(() => {
        this.discardNotification(notif.id);
      }, timeout);
    }
  }

  discardNotification(id) {
    for (let i = this.notifications.length - 1; i >= 0; i -= 1) {
      if (this.notifications[i].id === id) {
        this.notifications.splice(i, 1);
      }
    }
  }
}

export default new Notifier();
