import Vue from 'vue';
import moment from 'moment';

import { roundToString } from '../common/common';

Vue.filter('amDateFormat', (value, format) => {
  if (!value) {
    return '';
  }
  return moment(value).format(format);
});

Vue.filter('score', (score, sport) => {
  if (sport === 'football') {
    if (!score || !score.hometeam || !score.awayteam) {
      return ' - ';
    }
    return `${score.hometeam.score} - ${score.awayteam.score}`;
  }
  if (sport === 'rugby') {
    if (!score || !score.hometeam || !score.awayteam) {
      return ' - ';
    }
    if (isNaN(score.hometeam.tries) || isNaN(score.awayteam.tries)) {
      return `${score.hometeam.score} - ${score.awayteam.score}`;
    }
    return `${score.hometeam.score}(${score.hometeam.tries}) - ${score.awayteam.score}(${score.awayteam.tries})`;
  }
  return score;
});

Vue.filter('filterGroup', (matches, groupName) => {
  result = [];
  matches.forEach((match) => {
    if (match.group === groupName) {
      result.push(match);
    }
  });
  return result;
});

Vue.filter('ucFirst', str => str.charAt(0).toUpperCase() + str.slice(1));

Vue.filter('round', roundToString);

Vue.filter('shortScore', (score, sport) => {
  if (sport === 'football') {
    if (!score || !score.hometeam || !score.awayteam) {
      return ' - ';
    }
    return `${score.hometeam.score} - ${score.awayteam.score}`;
  }
  if (sport === 'rugby') {
    if (!score || !score.hometeam || !score.awayteam) {
      return ' - ';
    }
    return `${score.hometeam.score} - ${score.awayteam.score}`;
  }
  return score;
});

const ALGO_DICO = {
  compute_ranking: 'Compute ranking',
  compute_points: 'Compute points',
  compute_points_from_scratch: 'Compute points from scratch',
  compute_crowdscoring: 'Compute crowdscoring',
};
Vue.filter('algo', algo => (ALGO_DICO[algo] || algo));
