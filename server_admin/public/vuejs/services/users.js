import { api } from '../common/APIServices';
import notifier from '../common/Notifier';

export const getDisplayEmail = (user) => {
  if (!user || !user.login) {
    return '';
  }
  if (user.login.local && user.login.local.email) {
    return user.login.local.email;
  }
  if (user.login.facebook && user.login.facebook.email) {
    return user.login.facebook.email;
  }
  if (user.login.google && user.login.google.email) {
    return user.login.google.email;
  }
  return '-';
};

export const getDisplayName = (user) => {
  if (!user || !user.login) {
    return '';
  }
  if (user.login.local && user.login.local.pseudo) {
    return user.login.local.pseudo;
  }
  if (user.login.facebook && user.login.facebook.displayName) {
    return user.login.facebook.displayName;
  }
  if (user.login.google && user.login.google.displayName) {
    return user.login.google.displayName;
  }
  if (user.login.twitter && user.login.twitter.displayName) {
    return user.login.twitter.displayName;
  }
  return '-';
};

export const getPictureURL = (user) => {
  if (!user || !user.pictureMode) {
    return null;
  }
  return user.login[user.pictureMode].pictureURL;
};

export const getLargePictureURL = (user) => {
  if (!user || !user.pictureMode) {
    return null;
  }
  const url = user.login[user.pictureMode].pictureURL;
  if (user.pictureMode === 'facebook') {
    return url.replace('/s50x50', '/s180x180');
  }
  if (user.pictureMode === 'twitter') {
    return url.replace('_normal.', '_200x200.');
  }
  return url;
};

export const getUserEmails = (user) => {
  if (!user || !user.login) {
    return [];
  }
  const emailMap = [];
  if (user.login.local && user.login.local.email) {
    emailMap[user.login.local.email] = 1;
  }
  if (user.login.facebook && user.login.facebook.email) {
    emailMap[user.login.facebook.email] = 1;
  }
  if (user.login.google && user.login.google.email) {
    emailMap[user.login.google.email] = 1;
  }
  if (user.login.twitter && user.login.twitter.email) {
    emailMap[user.login.twitter.email] = 1;
  }
  return Object.keys(emailMap);
};

export const getUsers = async (options) => {
  return api.get('/users', options);
};

export const getUser = async (userId) => {
  const res = await api.get(`/users/${userId}`);
  if (res.status === 200) {
    return res.user;
  }
  notifier.notify({
    type: 'danger',
    message: res.message,
  }, 3000);
  return null;
};

export const getUserForecasts = async (userId) => {
  const res = await api.get(`/users/${userId}/forecasts`, { limit: 100 });// all forecasts
  if (res.status === 200) {
    return res.forecasts;
  }
  notifier.notify({
    type: 'danger',
    message: res.message,
  }, 3000);
  return null;
};

export const getUsersLanguage = async () => {
  return api.get('/stats/users/language');
};

export const getUsersCreation = async (options) => {
  return api.get('/stats/users/creation', options);
};
