import { api } from '../common/APIServices';
import notifier from '../common/Notifier';

export const getTeams = async (options) => {
  return api.get('/teams', options);
};

export const getTeam = async (teamId) => {
  const res = await api.get(`/teams/${teamId}`);
  if (res.status === 200) {
    return res.team;
  }
  notifier.notify({
    type: 'danger',
    message: res.message,
  }, 3000);
  return null;
};

export const getTeamMatches = async (teamId, options) => {
  return api.get(`/teams/${teamId}/matches`, options);
};

export const createTeam = async (team) => {
  return api.post('/teams', { team });
};

export const updateTeam = async (teamId, update) => {
  return api.put(`/teams/${teamId}`, update);
};

export const scrapInfo = async (teamId, source) => {
  const res = await api.get(`/teams/${teamId}/automation`, { source });
  if (res.status === 200) {
    return res.automation;
  }
  notifier.notify({
    type: 'danger',
    message: res.message,
  }, 3000);
  return null;
};
