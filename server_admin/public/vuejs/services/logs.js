import { scheduler } from '../common/APIServices';
import notifier from '../common/Notifier';

export const getLogs = async (options) => {
  const res = await scheduler.get('/logs', options);
  if (res.status === 200) {
    return res.logs;
  }
  notifier.notify({
    type: 'danger',
    message: res.message,
  }, 5000);
  return [];
};
