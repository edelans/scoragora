import { api } from '../common/APIServices';
import notifier from '../common/Notifier';

export const getLeagues = async (options) => {
  return api.get('/leagues', options);
};

export const getLeague = async (leagueId) => {
  const res = await api.get(`/leagues/${leagueId}`);
  if (res.status === 200) {
    return res.league;
  }
  notifier.notify({
    type: 'danger',
    message: res.message,
  }, 3000);
  return null;
};

export const getLeagueUsers = async (leagueId) => {
  const res = await api.get(`/leagues/${leagueId}/users`);
  if (res.status === 200) {
    return res.users;
  }
  notifier.notify({
    type: 'danger',
    message: res.message,
  }, 3000);
  return [];
};

export const updateLeague = async (leagueId, update) => {
  return api.put(`/leagues/${leagueId}`, update);
};
