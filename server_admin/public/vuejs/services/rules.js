import { api } from '../common/APIServices';
import notifier from '../common/Notifier';

export const getRules = async (options) => {
  return api.get('/rules', options);
};

export const getRule = async (ruleId) => {
  const res = await api.get(`/rules/${ruleId}`);
  if (res.status === 200) {
    return res.rule;
  }
  notifier.notify({
    type: 'danger',
    message: res.message,
  }, 3000);
  return null;
};

export const createRule = async (rule) => {
  return api.post('/rules', { rule });
};

export const deleteRule = async (ruleId) => {
  return api.del(`/rules/${ruleId}`);
};
