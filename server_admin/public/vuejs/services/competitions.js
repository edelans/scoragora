import { api } from '../common/APIServices';
import notifier from '../common/Notifier';

export const getCompetitions = async (options) => {
  return api.get('/competitions', options);
};

export const getCompetition = async (competitionId) => {
  const res = await api.get(`/competitions/${competitionId}`);
  if (res.status === 200) {
    return res.competition;
  }
  notifier.notify({
    type: 'danger',
    message: res.message,
  }, 3000);
  return null;
};

export const getCompetitionMatches = async (competitionId) => {
  const res = await api.get(`/competitions/${competitionId}/matches`);
  if (res.status === 200) {
    return res.matches;
  }
  notifier.notify({
    type: 'danger',
    message: res.message,
  }, 3000);
  return [];
};

export const getCompetitionTeams = async (competitionId) => {
  const res = await api.get(`/competitions/${competitionId}/teams`);
  if (res.status === 200) {
    return res.teams;
  }
  notifier.notify({
    type: 'danger',
    message: res.message,
  }, 3000);
  return [];
};

export const getCompetitionRanking = async (competitionId, limit) => {
  const res = await api.get(`/competitions/${competitionId}/forecasts`, { top: limit });
  if (res.status === 200) {
    return res.users;
  }
  notifier.notify({
    type: 'danger',
    message: res.message,
  }, 3000);
  return null;
};

export const addCompetitionTeam = async (competitionId, team) => {
  return api.post(`/competitions/${competitionId}/teams`, { team });
};

export const updateCompetition = async (competitionId, update) => {
  return api.put(`/competitions/${competitionId}`, update);
};

export const updateCompetitionRules = async (competitionId, update) => {
  return api.put(`/competitions/${competitionId}/rules`, update);
};

export const createCompetition = async (competition) => {
  return api.post('/competitions', { competition });
};

export const deleteCompetition = async (competitionId) => {
  return api.del(`/competitions/${competitionId}`);
};

export const getCompetitionEmails = async (competitionId, options) => {
  return api.get(`/competitions/${competitionId}/emails`, options);
};

export const countCompetitionLeagues = async (competitionId) => {
  const res = await api.get(`/competitions/${competitionId}/leagues`, { count: true });
  if (res.status === 200) {
    return res.leagues;
  }
  notifier.notify({
    type: 'danger',
    message: res.message,
  }, 3000);
  return null;
};

export const getCompetitionLeagueCreations = async (competitionId) => {
  const res = await api.get(`/competitions/${competitionId}/leagues`, { creations: true });
  if (res.status === 200) {
    return res.count;
  }
  notifier.notify({
    type: 'danger',
    message: res.message,
  }, 3000);
  return null;
};

export const countCompetitionForecasts = async (competitionId) => {
  const res = await api.get(`/competitions/${competitionId}/forecasts`, { count: true });
  if (res.status === 200) {
    return res.count;
  }
  notifier.notify({
    type: 'danger',
    message: res.message,
  }, 3000);
  return null;
};

export const getCompetitionForecastCreations = async (competitionId) => {
  const res = await api.get(`/competitions/${competitionId}/forecasts`, { creations: true });
  if (res.status === 200) {
    return res.count;
  }
  notifier.notify({
    type: 'danger',
    message: res.message,
  }, 3000);
  return null;
};

export const scrapInfo = async (competitionId, source) => {
  return api.get(`/competitions/${competitionId}/automation`, { source });
};
