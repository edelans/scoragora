import { api } from '../common/APIServices';
import notifier from '../common/Notifier';

export const getMatches = async (options) => {
  return api.get('/matches', options);
};

export const getMatch = async (matchId) => {
  const res = await api.get(`/matches/${matchId}`);
  if (res.status === 200) {
    return res.match;
  }
  notifier.notify({
    type: 'danger',
    message: res.message,
  }, 3000);
  return null;
};

export const getMatchWithTeams = async (matchId) => {
  const res = await api.get(`/matches/${matchId}`, { teams: true });
  if (res.status === 200) {
    return res.match;
  }
  notifier.notify({
    type: 'danger',
    message: res.message,
  }, 3000);
  return null;
};

export const setMatchRule = async (matchId, ruleId) => {
  return api.put(`/matches/${matchId}`, { rule: ruleId });
};

export const setMatchDetails = async (matchId, details) => {
  return api.put(`/matches/${matchId}`, details);
};

export const setMatchResult = async (matchId, result, sport) => {
  return api.put(`/matches/${matchId}`, { result, sport });
};

export const updateMatch = async (matchId, update) => {
  return api.put(`/matches/${matchId}`, update);
};

export const scrapMatchDate = async (matchId, source) => {
  const res = await api.get(`/matches/${matchId}/automation`, { date: true, source });
  if (res.status === 200) {
    return res.result;
  }
  notifier.notify({
    type: 'danger',
    message: res.message,
  }, 3000);
  return null;
};

export const scrapMatchResult = async (matchId, source) => {
  return get(`/matches/${matchId}/automation`, { result: true, source });
};
