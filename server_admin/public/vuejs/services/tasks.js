import { scheduler } from '../common/APIServices';

export const createTask = async (task) => {
  return scheduler.post('/tasks', { task });
};

export const createTaskComputePoints = async (matchId, revert = false) => {
  return scheduler.post('/tasks', {
    task: {
      algo: 'compute_points',
      parameters: {
        match: matchId,
        revert,
      },
    },
  });
};

export const getTasks = async () => {
  return scheduler.get('/tasks');
};

export const getTaskStats = async (options) => {
  return scheduler.get('/tasks/stats', options);
};
