module.exports = {
    extends: ['airbnb', 'plugin:vue/essential'],
    parserOptions: {
        ecmaVersion: 2017
    },
    rules: {
      'no-underscore-dangle': 0
    },
};
