const express = require('express');
const morgan = require('morgan');
const bodyParser = require('body-parser');
const cookieParser = require('cookie-parser');
const methodOverride = require('method-override');
const fs = require('fs');
const url = require('url');
const winston = require('winston');
const http = require('http');
const https = require('https');
const app = express();
const cfg = require('../config');

// configure winston
winston.remove(winston.transports.Console);
winston.add(winston.transports.Console, {
  colorize: true,
  prettyPrint: true,
  timestamp: true,
});

app.set('views', __dirname + '/views');
app.set('view engine', 'pug');
app.disable('x-powered-by'); // security measure

app.use(express.static(__dirname + '/public')); // set the static files location /public/img will be /img for users
app.use(morgan('dev')); // log every request to the console
app.use(bodyParser.urlencoded({
  extended: true,
}));
app.use(bodyParser.json()); // pull information from html in POST
app.use(methodOverride()); // simulate DELETE and PUT

// required for passport
app.use(cookieParser()); // Parser cookie if needed (especially for auth)

// Declare the API base route
app.get('/', (req, res) => {
  // res.sendfile('index.html', {root: __dirname + '/public/views'});
  const env = process.env.NODE_ENV || 'dev';
  let routing = cfg.routing;
  if (env === 'dev' || env === 'local') {
    const base = `//${req.hostname}:`;
    routing = {
      api: base + cfg.api_port,
      scheduler: base + cfg.scheduler_port,
    };
  }
  res.render('index', {
    env,
    routing,
  });
});

/* var options = {
    key: fs.readFileSync('./https/server.key'),
    cert: fs.readFileSync('./https/server.crt')
}*/
http.createServer(app).listen(cfg.server_admin_port);
// https.createServer(options, app).listen(cfg.server_port);
winston.info(`Creating a server on port ${cfg.server_admin_port} in ${cfg.env} mode`);
