#!/bin/bash
# attention: needs to be exectuted as root
cd /home/scoragora/brazuca

#pull last version of brazuca
git pull origin master

#update dependencies for api
cd api/
npm install
cd ../

#update dependencies for server
cd server/
npm install
#bower install --allow-root
cd ../

#update dependencies for server_admin
cd server_admin/
npm install
#bower install --allow-root
cd ../

#update dependencies for scheduler
cd scheduler/
npm install
cd ../


#stop former forever process
sudo forever stop /home/scoragora/brazuca/api/app.js
sudo forever stop /home/scoragora/brazuca/server/app.js
sudo forever stop /home/scoragora/brazuca/server_admin/app.js
sudo forever stop /home/scoragora/brazuca/scheduler/app.js

#launch new forever monitoringprocess, with production mode
cd api
sudo NODE_ENV="production" forever start --append -l /var/log/node/scoragora-api.log /home/scoragora/brazuca/api/app.js
cd ..
sudo NODE_ENV="production" forever start --append -l /var/log/node/scoragora-server.log /home/scoragora/brazuca/server/app.js
cd scheduler
sudo NODE_ENV="production" forever start --append -l /var/log/node/scoragora-scheduler.log /home/scoragora/brazuca/scheduler/app.js
cd ..
sudo NODE_ENV="production" forever start --append -l /var/log/node/scoragora-serveradmin.log /home/scoragora/brazuca/server_admin/app.js
