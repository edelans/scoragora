#!/bin/bash
# WARNING: needs to be exectuted as root

# Make sure only root can run our script
if [[ $EUID -ne 0 ]]; then
   echo "This script must be run as root" 1>&2
   exit 1
fi

# let's go
cd /home/scoragora/brazuca

#pull last version of brazuca
git pull origin master

#update dependencies for api
cd api/
npm install
cd ../

#update dependencies for server
cd server/
npm install
cd ../

#update dependencies for server_admin
cd server_admin/
npm install
npm install --only=dev
npm run build
cd ../

#update dependencies for scheduler
cd scheduler/
npm install
cd ../


# reload all the pm2 instances
pm2 startOrRestart ecosystem.json --env production
