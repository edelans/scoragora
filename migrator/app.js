var client = require('mongodb').MongoClient;
var ObjectId = require('mongodb').ObjectID;
var cfg = require('../config');

var connectToDatabases = function(cb) {
  client.connect(cfg.mongo.uri, function(err, new_db) {
    console.log('connected to new db');
    return cb(new_db.db('api'));
  });
}

var updateFacebookProfilePicture = function(new_db) {
  var User = new_db.collection('user');
  var stream = User.find().stream();
  stream.on("data", function(user) {
    if (user.login && user.login.facebook) {
      console.log("user found " + user._id);
      User.findAndModify({_id: user._id}, {}, {
        $set: {
          'login.facebook.pictureURL' :'https://graph.facebook.com/v2.3/' + user.login.facebook.id + '/picture?type=large'
        }
      }, {}, function(err) {
        if (err) console.log(err);
        console.log('update user' + user._id);
      });
    }
  });
}

var definePictureMode = function(new_db) {
  var User = new_db.collection('user');
  User.findAndModify({pictureMode: {$exist: false}}, {}, {$set: {
    pictureMode: "local"
  }});
}

var updateFacebookProfilePicture = function(new_db) {
  var User = new_db.collection('user');
  var stream = User.find().stream();
  stream.on("data", function(user) {
    if (user.login && user.login.facebook) {
      console.log("user found " + user._id);
      User.findAndModify({_id: user._id}, {}, {
        $set: {
          'login.facebook.pictureURL' :'https://graph.facebook.com/v2.3/' + user.login.facebook.id + '/picture?type=large'
        }
      }, {}, function(err) {
        if (err) console.log(err);
        console.log('update user' + user._id);
      });
    }
  });
}


var updateCompetitionMatch = function(new_db) {
  var Match = new_db.collection('match');
  Match.update({
    _id: ObjectId("552a5d5d9103804e343fab76")
  }, {
    $set: {
      status: 'closed'
    }
  },
  {}, function(err) {
    if (err) console.log(err);
    else console.log('update done');
  });
}

var updateLeagueCanal = function(new_db) {
  var League = new_db.collection('league');
  League.update({
    _id: ObjectId("55f6d8bee92145350b380a36")
  }, {
    $set: {
      name: "Canal Rugby Pronostics",
      url: "canal.rugby.pronostics",
    }
  }, {}, function(err, r) {
    if (err) console.log(err);
    else console.log('update done');
  });
};

var addCreationDate = function(new_db) {
  var User = new_db.collection("user");
  var stream = User.find({ createdAt: { $exists: false } }).stream();
  stream.on("data", function(user) {
    if (!user.createdAt) {
      const createdAt = user._id.getTimestamp();
      User.findAndModify({_id: user._id}, {}, {
        $set: {
          createdAt,
        }
      }, {}, function(err) {
        if (err) console.log(err);
        console.log('update user' + user._id);
      });
    }
  });
};

var cleanUpUnexistingAccount = function(new_db) {
  var Forecast = new_db.collection("forecast");
  var User = new_db.collection("user");
  var League = new_db.collection("league");
  var stream = Forecast.find({}).stream();
  stream.on("data", function(forecast) {
    if (forecast.user) {
      User.findOne({_id: forecast.user}, function(err, user) {
        if (err) return console.log("error when retrieving user " + forecast.user + " : " + err);
        if (!user) {
          console.log("user " + forecast.user + " not found");
          // remove forecast
          /*Forecast.remove({ _id: forecast._id }, function(err) {
          if (err) console.log("error when removing " + forecast._id + " : " + err);
        });*/
        // remove league ref to user
        League.update({"members.user": forecast.user}, {
          $pull: {
            "members": {
              "user": forecast.user
            }
          }
        }, {multi: true}, function(err) {
          if (err) console.log("error when removing user " + forecast.user + " from league : " + err);
          else console.log("removed user " + forecast.user + " from league");
        });
        // remove league ref to user
        /*League.count({"members.user": forecast.user}, function(err, leagues) {
        if (err) console.log("error when removing user " + forecast.user + " from league : " + err);
        else console.log("removed user " + forecast.user + " from leagues : " + leagues);
      });*/
    }
  });
}
});
};

var populateTeamNameForMatches = function(new_db) {
  var Match = new_db.collection("match");
  var Team = new_db.collection("team");
  var teams = Team.find({}, function(err, teams) {
    if (err) {
      return console.log("error when retrieving all the teams : " + err);
    }
    if (!teams) return console.log("no team retrieved");

    var teamsDico = {};
    teams.each(function(err, team) {
      if (team && team._id) teamsDico[team._id] = team.name;
    });
    console.log("teamsDico set up");
    // iterate the matches
    var stream = Match.find({}).stream();
    stream.on("data", function(match) {
      console.log("match " + match._id);
      var update = {};
      var updateNeeded = false;
      if (match.hometeam && teamsDico[match.hometeam]) {
        update.hometeamname = teamsDico[match.hometeam];
        updateNeeded = true;
      }
      if (match.awayteam && teamsDico[match.awayteam]) {
        update.awayteamname = teamsDico[match.awayteam];
        updateNeeded = true;
      }
      if (updateNeeded) {
        Match.update({_id: match._id}, {$set: update}, function(err) {
          if (err) console.log("error on match update " + match._id);
          console.log("update successful for match " + match._id, update);
        });
      }
    });

  });
};

var addForecastCreationDate = function(new_db) {
  var Forecast = new_db.collection("forecast");
  var stream = Forecast.find().stream();
  stream.on("data", function(forecast) {
    if (!forecast.createdAt) {
      var createdAt = forecast._id.getTimestamp();
      Forecast.findAndModify({_id: forecast._id}, {}, {
        $set: {
          'createdAt': createdAt
        }
      }, {}, function(err) {
        if (err) console.log(err);
        console.log('update forecast' + forecast._id);
      });
    }
  });
};

var updateOneUser = function(new_db) {
	var User = new_db.collection("user");
	User.findOne({"login.facebook.id": "10153667526745098"}, function(err, user) {
		if (user) console.log("user found");
	});
	User.update({"login.facebook.id": "10153667526745098"}, {$set: {"login.local.email": "lhesnard@gmail.com"}}, function(err, n) {
		console.log("update one user done : " + err + n);
	});
}

var updateMatchDate = function(new_db) {
	var Match = new_db.collection("match");
	Match.count({datetime: {
     $lt: new Date(2013,11,17)
  }}, function(err, count) {
	  console.log(count);
  });

	/*Match.update({datetime: {
		$lt: new Date(2000,11,17)
	}}, {
		$unset: {datetime: ""}
	}, function(err, n) {
		console.log("update " + n + "");
	});*/
}

const updateMatchStatus = function(new_db) {
  const Match = new_db.collection('match');
  Match.update({
    status: 'being_computed'
  }, {
    $set: {
      status: 'closed'
    }
  },
  {}, function(err) {
    if (err) console.log(err);
    else console.log('update done');
  });
}

connectToDatabases(function(new_db) {
  //updateCompetitionMatch(new_db);
  //updateFacebookProfilePicture(new_db);
  //updateMatchBonus(new_db);
  //updateLeagueCanal(new_db);
  //addCreationDate(new_db);
  //cleanUpUnexistingAccount(new_db);
  //populateTeamNameForMatches(new_db);
  //addForecastCreationDate(new_db);
  //updateOneUser(new_db);
  //updateMatchDate(new_db);
  updateMatchStatus(new_db);
});
