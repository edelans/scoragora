#!/usr/bin/python
#
# Algorithm
# Compute points for all forecast on the competition
# Params : 
#	- competition_id
# Returns :
#	- execution_time
import sys
import os
import time
import json

CURRENT_DIR = os.path.dirname(os.path.abspath(__file__))
sys.path.append(os.path.dirname(CURRENT_DIR))

from db.db_connector import DBConnector
from common.logger import Logger
from algos.compute_points_from_scratch import ComputePointsFromScratch

if len(sys.argv) > 3:
	Logger.log({ "message": "Too many arguments are given.", "status": "400"})
if len(sys.argv) < 3:
	Logger.log({ "message": "Too few arguments are given.", "status": "400"})
else :
	start_time = time.time()

	# Get database connector
	db = DBConnector.getDB()
	
	competition_id = sys.argv[1]
	
	# set the processes number
	processes = int(sys.argv[2])
	
	result = ComputePointsFromScratch.execute(competition_id, processes)
	
	execution_time = time.time() - start_time
	result = {"status": 200, "execution_time" : execution_time, "processes": processes}
	Logger.log(result)
