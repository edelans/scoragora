#!/usr/bin/python
import sys
import os

CURRENT_DIR = os.path.dirname(os.path.abspath(__file__))
sys.path.append(os.path.dirname(CURRENT_DIR))

from algos.compute_points_from_scratch import ComputePointsFromScratch

if len(sys.argv) > 1 :
	print('{ "message": "Too many arguments are given.", "code": "500"}')
	return;