#!/usr/bin/python
#
# Algorithm
# Compute crowdscoring values for all active competition
# Params : 
# Returns :
#	- execution_time
import sys
import os
import time
import json
from bson.objectid import ObjectId

CURRENT_DIR = os.path.dirname(os.path.abspath(__file__))
sys.path.append(os.path.dirname(CURRENT_DIR))

from db.db_connector import DBConnector
from common.logger import Logger
from algos.compute_crowdscoring import ComputeCrowdscoring

if len(sys.argv) > 4:
	Logger.log({ "message": "Too many arguments are given.", "status": "400"})
if len(sys.argv) < 2:
	Logger.log({ "message": "Too few arguments are given.", "status": "400"})
else :
	start_time = time.time()
	
	# Get database connector
	db = DBConnector.getDB()
	
	processes = int(sys.argv[1])
	if len(sys.argv) == 4:
		ComputeCrowdscoring.execute(ObjectId(sys.argv[2]), sys.argv[3], processes)
	else:
		competitions = db.competition.find({"status": "public"})
		
		for competition in competitions :
			ComputeCrowdscoring.execute(competition["_id"], competition["sport"], processes)
	
	execution_time = time.time() - start_time
	result = {"status": 200, "execution_time" : execution_time, "processes": processes}
	Logger.log(result)