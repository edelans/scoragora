#!/usr/bin/python
import sys
import os
import time

CURRENT_DIR = os.path.dirname(os.path.abspath(__file__))
sys.path.append(os.path.dirname(CURRENT_DIR))

from common.logger import Logger
from bson.objectid import ObjectId
from common.lequipe_fr import LequipeFr

if len(sys.argv) != 1 :
	Logger.log({ "message": "Wrong number of argument. 1 argument is needed.", "code": "400"})
else :
	start_time = time.time()
	
	#competition_id = ObjectId(sys.argv[1]);
	sniffer = LequipeFr()
	sniffer.parseChampionship()
	
	execution_time = time.time() - start_time
	result = {"execution_time" : execution_time}
	Logger.log(result)