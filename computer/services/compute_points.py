#!/usr/bin/python
#
# Algorithm
# Compute points for all forecast on the competition
# Params : 
#	- competition_id
# Returns :
#	- execution_time
import sys
import os
import time
import json

CURRENT_DIR = os.path.dirname(os.path.abspath(__file__))
sys.path.append(os.path.dirname(CURRENT_DIR))

from db.db_connector import DBConnector
from common.logger import Logger
from algos.compute_points import ComputePoints

if len(sys.argv) > 4:
	Logger.log({ "message": "Too many arguments are given.", "status": "400"})
if len(sys.argv) < 4:
	Logger.log({ "message": "Too few arguments are given.", "status": "400"})
else :
	start_time = time.time()
	
	# set the processes number
	processes = int(sys.argv[3])
	
	# Get database connector
	db = DBConnector.getDB()
	
	match_id = sys.argv[1]
	if sys.argv[2] == 'true':
		revert = True
	else:
		revert = False

	ComputePoints.execute(match_id, revert, processes)
	
	execution_time = time.time() - start_time
	result = {"status": 200, "execution_time" : execution_time, "processes": processes}
	Logger.log(result)
