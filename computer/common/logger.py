import json
from bson.objectid import ObjectId

def formatObject(obj):
	if type(obj) is dict:
		for key in obj.keys():
			obj[key] = formatObject(obj[key])
	if type(obj) is list:
		for item in obj:
			item = formatObject(item)
	if type(obj) is ObjectId:
		obj = str(obj)
	return obj

class Logger(object):
	
	@staticmethod
	def log(obj):
		obj = formatObject(obj)
		try:
			print(json.dumps(obj))
		except TypeError:
			print(json.dumps({"message": obj}))