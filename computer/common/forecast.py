from db_connector import DBConnector
import time

class ForecastComputer :
	
	@staticmethod
	def computeRankingFromScratch(forecast_id, match_dico) :
		start_time = time.time()
		db = DBConnector.getDB()
		DBForecast = db['forecast']
		forecast = DBForecast.find_one({'_id' : forecast_id })
		s = time.time() - start_time
		print('after db')
		print(s)
		if forecast == None :
			return
		forecast['points'] = 0;
		for group in forecast['groups'] :
			for match in group['matches'] :
				if match['match'] in match_dico :
					computeMatchPoints(match, match_dico[match['match']] , forecast)
		for elimination in forecast['eliminations'] :
			for match in elimination['matches'] :
				if match['match'] in match_dico :
					computeMatchPoints(match, match_dico[match['match']] , forecast)
		for day in forecast['days'] :
			for match in day['matches'] :
				if match['match'] in match_dico :
					computeMatchPoints(match, match_dico[match['match']] , forecast)
		s = time.time() - start_time
		print('before save')
		print(s)
		DBForecast.save(forecast)
		s = time.time() - start_time
		print('fin')
		print(s)
		print('------------')
		return

def computeMatchPoints(match, match_competition, forecast) :
	"""Compare prediction and real score then compute the number of points associated.
		Update the forecast"""
	if 'rules' in match_competition and 'match' in match_competition :
		competition_match = match_competition['match']
		rules = match_competition['rules']
		points = 0
		bonus = []
		# We only consider complete forecast on a match		
		if 'hometeam_forecast' in match and 'awayteam_forecast' in match :
			if competition_match['hometeam_score'] == match['hometeam_forecast'] :
				points += rules['team_score']
				bonus.append("team_score")
			if competition_match['awayteam_score'] == match['awayteam_forecast'] :
				points += rules['team_score']
				# check if team_score is already in bonus
				# dirty code
				if len(bonus) > 0 :
					bonus = ["team_score_double"]
				else :
					bonus.append("team_score")
			if 'signum' in match :
				if match['signum'] == competition_match['signum'] :
					print('errrror')
			else :
				if sign(competition_match['hometeam_score'] - competition_match['awayteam_score']) == sign(match['hometeam_forecast'] - match['awayteam_forecast']) :
					points += rules['match_result']
					bonus.append("match_result")
				# In case the match result is good we check if there is a good goal difference
				if competition_match['hometeam_score'] - competition_match['awayteam_score'] == match['hometeam_forecast'] - match['awayteam_forecast'] :
					points += rules['goal_diff']
					bonus.append("goal_diff")
		match['bonus'] = bonus
		match['points'] = points
		forecast['points'] += points
	return

def sign(a) :
	if a > 0 :
		return 1
	if a < 0 :
		return -1
	return 0