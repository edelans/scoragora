from db.db_connector import DBConnector
from common.logger import Logger
from bson.objectid import ObjectId

# Get database connector
db = DBConnector.getDB()

def areSameSign(a, b):
	"""
		Check if a and b have the same signum
	"""
	if a == 0 :
		return b == 0
	elif a > 0 :
		return b > 0
	else :
		return b < 0

class PointComputer(object) :
	"""
		Class computing points
	"""
	
	def __init__(self, rule_id = None, rule = None): 
		"""
			Constructor base on the given rule
		"""
		if rule is not None:
			self.rule = rule
		else:
			self.rule = db.rule.find_one({"_id": rule_id})
		if self.rule == None:
			raise Exception("No rule found")
		criterias = self.rule.get("criterias")
		sport = self.rule.get("sport")
		if criterias is not None and sport is not None:
			self.sport = sport
			self.direction = None
			self.direction_type = None
			self.team_score = None
			self.difference = None
			for criteria in criterias:
				if criteria.get("name") == "direction":
					self.direction = criteria.get("points")
					continue
				if criteria.get("name") == "direction_type":
					self.direction_type = criteria.get("points")
					continue
				if criteria.get("name") == "team_score":
					self.team_score = criteria.get("points")
					continue
				if criteria.get("name") == "difference":
					self.difference = criteria.get("points")
					continue

	def computeMaxPoints(self):
		"""
			Compute the maximum number of point
		"""
		points = 0
		if self.sport == "football":
			if self.direction is not None:
				points += self.direction
			if self.team_score is not None:
				points += 2*self.team_score
			if self.difference is not None:
				points += self.difference
		if self.sport == "rugby":
			if self.direction is not None:
				points += self.direction
			if self.direction_type is not None:
				points += self.direction_type
		return points

	def computePoints(self, result, forecast):
		"""
			args: 
				- result : {awayteam: {score: 1}, hometeam: {score: 3}}
				- forecast : {awayteam: {score: 2}, hometeam: {score: 1}}
		"""
		points = 0
		bonus = []
		if self.direction is not None and not self.direction == 0:
			if self.__hasGoodDirection(result, forecast):
				#Logger.log("has good direction")
				points += self.direction
				bonus.append("direction")
		
		if self.direction_type is not None and not self.direction_type == 0:
			if self.__hasGoodDirectionType(result, forecast):
				#Logger.log("has good direction")
				points += self.direction_type
				bonus.append("direction_type")
				
		if self.team_score is not None and not self.team_score == 0:
			if self.__hasExactScore(result, forecast):
				#Logger.log("has full score")
				points += 2*self.team_score
				bonus.append("double_team_score")
			elif self.__hasGoodTeamScore(result, forecast):
				#Logger.log("has team score")
				points += self.team_score
				bonus.append("team_score")
		
		if self.difference is not None and not self.difference == 0:
			if self.__hasGoodDifference(result, forecast):
				#Logger.log("has good difference")
				points += self.difference
				bonus.append("difference")
		
		return {'bonus': bonus, 'points': points}
	
	def __hasGoodDirection(self, result, forecast):
		# direction algorithm 
		if self.sport == "football":
			return self.__hasGoodFootballDirection(result, forecast)
		if self.sport == "rugby":
			return self.__hasGoodRugbyDirection(result, forecast)
		return False
		
	def __hasGoodDirectionType(self, result, forecast):
		# direction type algorithm 
		if self.sport == "rugby":
			return self.__hasGoodRugbyDirectionType(result, forecast)
		return False
		
	def __hasExactScore(self, result, forecast):
		# exact score algorithm 
		if self.sport == "football":
			return self.__hasExactFootballScore(result, forecast)
		return False
	
	def __hasGoodTeamScore(self, result, forecast):
		# team_score algorithm 
		if self.sport == "football":
			return self.__hasGoodFootballTeamScore(result, forecast)
		return False
	
	def __hasGoodDifference(self, result, forecast):
		# difference algorithm 
		if self.sport == "football":
			return self.__hasGoodFootballDifference(result, forecast)
		return False	
	
	def __hasGoodFootballDirection(self, result, forecast):
		# forecast = {hometeam, awayteam, (direction)}
		# result = {hometeam: {score}, awayteam: {score}, (direction)}
		result_direction = result.get("direction")
		if result_direction is None:
			score_home = result.get("hometeam", {}).get("score")
			score_away = result.get("awayteam", {}).get("score")
			if score_home is not None and score_away is not None :
				result_direction = score_home - score_away
			else :
				return False
		forecast_direction = forecast.get("direction")
		if forecast_direction is None:
			score_home = forecast.get("hometeam", {}).get("score")
			score_away = forecast.get("awayteam", {}).get("score")
			if score_home is not None and score_away is not None:
				forecast_direction = score_home - score_away
			else :
				return False
		return areSameSign(forecast_direction, result_direction)
	
	def __hasGoodRugbyDirection(self, result, forecast):
		# forecast = {difference_slice, (direction)}
		# result = {hometeam: {score}, awayteam: {score}, (direction)}
		result_direction = result.get("direction")
		if result_direction is None:
			score_home = result.get("hometeam", {}).get("score")
			score_away = result.get("awayteam", {}).get("score")
			if score_home is not None and score_away is not None :
				result_direction = score_home - score_away
			else :
				return False
		forecast_direction = forecast.get("direction")
		if forecast_direction is None :
			forecast_direction = forecast.get("difference_slice")
		if forecast_direction is None :
			return False
		return areSameSign(forecast_direction, result_direction)

	def __hasExactFootballScore(self, result, forecast):
		# forecast = {hometeam, awayteam, (direction)}
		# result = {hometeam: {score}, hometeam: {score}, (direction)}
		result_home_score = result.get("hometeam", {}).get("score")
		result_away_score = result.get("awayteam", {}).get("score")
		forecast_home_score = forecast.get("hometeam", {}).get("score")
		forecast_away_score = forecast.get("awayteam", {}).get("score")
		if result_home_score is not None and result_away_score is not None and \
			forecast_home_score is not None and forecast_away_score is not None :
			return result_home_score == forecast_home_score and result_away_score == forecast_away_score
		return False
	
	def __hasGoodFootballTeamScore(self, result, forecast):
		# forecast = {hometeam, awayteam, (direction)}
		# result = {hometeam: {score}, hometeam: {score}, (direction)}
		result_home_score = result.get("hometeam", {}).get("score")
		result_away_score = result.get("awayteam", {}).get("score")
		forecast_home_score = forecast.get("hometeam", {}).get("score")
		forecast_away_score = forecast.get("awayteam", {}).get("score")
		if result_home_score is not None and result_away_score is not None and \
			forecast_home_score is not None and forecast_away_score is not None :
			return result_home_score == forecast_home_score or result_away_score == forecast_away_score
		return False

	def __hasGoodFootballDifference(self, result, forecast):
		# forecast = {hometeam, awayteam, (direction)}
		# result = {hometeam: {score}, hometeam: {score}, (direction)}
		result_home_score = result.get("hometeam", {}).get("score")
		result_away_score = result.get("awayteam", {}).get("score")
		if result_home_score is not None and result_away_score is not None:
			result_difference = result_home_score - result_away_score
		else :
			return False
		forecast_home_score = forecast.get("hometeam", {}).get("score")
		forecast_away_score = forecast.get("awayteam", {}).get("score")
		if forecast_home_score is not None and forecast_away_score is not None:
			forecast_difference = forecast_home_score - forecast_away_score
		else :
			return False
		return result_difference == forecast_difference	
	
	def __hasGoodRugbyDirectionType(self, result, forecast):
		# forecast = {difference_slice, (direction)}
		# result = {hometeam: {score}, awayteam: {score}, (direction)}
		slice_width = 10
		result_home_score = result.get("hometeam", {}).get("score")
		result_away_score = result.get("awayteam", {}).get("score")
		if result_home_score is not None and result_away_score is not None:
			result_difference = result_home_score - result_away_score
		else :
			return False
		forecast_difference_slice = forecast.get("difference_slice")
		if forecast_difference_slice is None :
			return False
		if result_difference == 0 and forecast_difference_slice == 0:
			return True
		if result_difference < 0 and forecast_difference_slice < 0 and \
				result_difference >= slice_width*forecast_difference_slice and result_difference < slice_width*(forecast_difference_slice+1):
			return True
		if result_difference < -25 and forecast_difference_slice == -3:
			return True
		if result_difference > 0 and forecast_difference_slice > 0 and \
				result_difference <= slice_width*forecast_difference_slice and result_difference > slice_width*(forecast_difference_slice-1):
			return True
		if result_difference > 25 and forecast_difference_slice == 3:
			return True
		return False
				