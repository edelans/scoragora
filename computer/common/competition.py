import multiprocessing as mp
from db_connector import DBConnector
from forecast import ForecastComputer

class ORMCompetition :
	
	def __init__(self, id) :
		dico = DBConnector.getDB()['competition'].find_one({'_id': id})
		if dico == None :
			return
		self.id = dico['_id']
		self.name = dico['name']
		self.year = dico['year']
		self.start_date = dico['start_date']
		self.status = dico['status']
		self.groups = dico['groups']
		self.eliminations = dico['eliminations']
		self.days = dico['days']
		# Create a dico for rules
		self.match_dico = dict()	
		for group in self.groups :
			rules = group['rules']
			for match in group['matches'] :
				self.match_dico[match] = {'rules' : rules}
		for elimination in self.eliminations :
			rules = elimination['rules']
			for match in elimination['matches'] :
				self.match_dico[match] = {'rules': rules}
		for day in self.days :
			rules = day['rules']
			for match in group['matches'] :
				self.match_dico[match] = {'rules': rules}
			
	def rankFromScratch(self) :
		"""Compute point and ranking for every account betting on this competition"""
		if self.id == None :
			return

		# Find ids for matches defined for this competition
		matches = DBConnector.getDB()['match'].find({
				'competition': self.id, 
				'hometeam_score': {'$ne': None}, 
				'awayteam_score': {'$ne': None}
			})
		
		# this dico avoids too many populate
		match_dico = dict()
		for match in matches :
			if match['_id'] in self.match_dico :
				self.match_dico[match['_id']]['match'] = match
		
		# Get ids for all forecasts on this competition
		# limited to 100 for testing
		forecast_ids = DBConnector.getDB()['forecast'].find({ 'competition' : self.id }, { '_id' : 1 }).limit(100)
		
		#pool = mp.Pool(processes=4)
		#forecasts = []
		#for forecast_id in forecast_ids :
		#	pool.apply(doComputePointsFromScratch, args=[forecast_id['_id'], self.match_dico]) 
			#forecasts.append(forecast_id['_id'])
			
		#[pool.apply(doComputePointsFromScratch, args=[(x,), self.match_dico]) for x in forecasts]
		for x in forecast_ids :
			ForecastComputer.computeRankingFromScratch(x['_id'], self.match_dico)
		return
	
def doComputePointsFromScratch(forecast_id, rules) :
	ForecastComputer.computeRankingFromScratch(forecast_id, rules)