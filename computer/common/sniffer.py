from common.logger import Logger

class Sniffer:
	"""Class defining method for sniffer"""
	
	def __init__(self, match):
		if "automation" not in match.keys():
			Logger.log("automation missing")
			return
		self.automation = match["automation"]
	
	def parseMatchLiveScore(self):
		Logger.log("parseMatchScore is not defined.")
	
	def parseMatchFinalScore(self):
		Logger.log("parseMatchFinalScore is not defined.")
	
	def parseChampionship(self):
		Logger.log("parseChampionship is not defined.")
