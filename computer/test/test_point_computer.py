from common.point_computer import PointComputer
import unittest

class PointComputerFootballMaxPointsTestCase(unittest.TestCase):
	
	def test_no_criteria(self):
		computer = PointComputer(rule = {
			"sport": "football", 
			"criterias": []
		})
		computation = computer.computeMaxPoints()
		self.assertEqual(computation, 0)
		 
	def test_direction(self):
		computer = PointComputer(rule = {
			"sport": "football", 
			"criterias": [{
				"name": "direction",
				"points": 12
			}]
		})
		computation = computer.computeMaxPoints()
		self.assertEqual(computation, 12)
	
	def test_team_score(self):
		computer = PointComputer(rule = {
			"sport": "football", 
			"criterias": [{
				"name": "team_score",
				"points": 6
			}]
		})
		computation = computer.computeMaxPoints()
		self.assertEqual(computation, 12)

	def test_difference(self):
		computer = PointComputer(rule = {
			"sport": "football", 
			"criterias": [{
				"name": "difference",
				"points": 12
			}]
		})
		computation = computer.computeMaxPoints()
		self.assertEqual(computation, 12)

	def test_all_criterias(self):
		computer = PointComputer(rule = {
			"sport": "football", 
			"criterias": [{
				"name": "direction",
				"points": 12
			}, {
				"name": "team_score",
				"points": 6
			}, {
				"name": "difference",
				"points": 12
			}]
		})
		computation = computer.computeMaxPoints()
		self.assertEqual(computation, 36)
		
class PointComputerRugbyMaxPointsTestCase(unittest.TestCase):
	
	def test_no_criteria(self):
		computer = PointComputer(rule = {
			"sport": "rugby", 
			"criterias": []
		})
		computation = computer.computeMaxPoints()
		self.assertEqual(computation, 0)
		 
	def test_direction(self):
		computer = PointComputer(rule = {
			"sport": "rugby", 
			"criterias": [{
				"name": "direction",
				"points": 12
			}]
		})
		computation = computer.computeMaxPoints()
		self.assertEqual(computation, 12)
	
	def test_direction_type(self):
		computer = PointComputer(rule = {
			"sport": "rugby", 
			"criterias": [{
				"name": "direction_type",
				"points": 12
			}]
		})
		computation = computer.computeMaxPoints()
		self.assertEqual(computation, 12)

	def test_all_criterias(self):
		computer = PointComputer(rule = {
			"sport": "rugby", 
			"criterias": [{
				"name": "direction",
				"points": 12
			}, {
				"name": "direction_type",
				"points": 12
			}]
		})
		computation = computer.computeMaxPoints()
		self.assertEqual(computation, 24)

class PointComputerFootballDirectionTestCase(unittest.TestCase):
	
	def setUp(self):
		self.computer = PointComputer(rule = {
			"sport": "football", 
			"criterias": [{
				"name": "direction",
				"points": 12
			}]
		})
		
		self.result_victory = {
			"hometeam": {"score" : 2},
			"awayteam": {"score" : 1}
		}
		
		self.result_draw = {
			"hometeam": {"score" : 2},
			"awayteam": {"score" : 2}
		}
		
		self.result_defeat = {
			"hometeam": {"score" : 0},
			"awayteam": {"score" : 1}
		}
		
		self.forecast_defeat = {
			"hometeam": {
				"score": 1
			},
			"awayteam": {
				"score": 3
			}
		}
		
		self.forecast_draw = {
			"hometeam": {
				"score": 1
			},
			"awayteam": {
				"score": 1
			}
		}

	def test_direction(self):
		 self.assertEqual(self.computer.direction, 12)
		 
	def test_no_forecast(self):
		computation = self.computer.computePoints(self.result_defeat, {})
		self.assertEqual(computation["points"], 0)
		self.assertFalse("direction" in computation["bonus"])
	
	def test_no_result(self):
		computation = self.computer.computePoints({}, self.forecast_defeat)
		self.assertEqual(computation["points"], 0)
		self.assertFalse("direction" in computation["bonus"])
	
	def test_different_direction(self):
		computation = self.computer.computePoints(self.result_victory, self.forecast_defeat)
		self.assertEqual(computation["points"], 0)
		self.assertFalse("direction" in computation["bonus"])
	
	def test_same_direction(self):
		computation = self.computer.computePoints(self.result_defeat, self.forecast_defeat)
		self.assertEqual(computation["points"], 12)
		self.assertTrue("direction" in computation["bonus"])
	
	def test_same_draw(self):
		computation = self.computer.computePoints(self.result_draw, self.forecast_defeat)
		self.assertEqual(computation["points"], 0)
		self.assertFalse("direction" in computation["bonus"])
		
	def test_same_draw(self):
		computation = self.computer.computePoints(self.result_draw, self.forecast_draw)
		self.assertEqual(computation["points"], 12)
		self.assertTrue("direction" in computation["bonus"])

class PointComputerRugbyDirectionTestCase(unittest.TestCase):
	
	def setUp(self):
		self.computer = PointComputer(rule = {
			"sport": "rugby", 
			"criterias": [{
				"name": "direction",
				"points": 12
			}]
		})
		
		self.result_victory = {
			"hometeam": {"score" : 23},
			"awayteam": {"score" : 10}
		}
		
		self.result_draw = {
			"hometeam": {"score" : 21},
			"awayteam": {"score" : 21}
		}
		
		self.result_defeat = {
			"hometeam": {"score" : 9},
			"awayteam": {"score" : 14}
		}
		
		self.forecast_defeat = {
			"difference_slice": -2
		}
		
		self.forecast_draw = {
			"difference_slice": 0
		}

	def test_direction(self):
		 self.assertEqual(self.computer.direction, 12)
		 
	def test_no_forecast(self):
		computation = self.computer.computePoints(self.result_defeat, {})
		self.assertEqual(computation["points"], 0)
		self.assertFalse("direction" in computation["bonus"])
	
	def test_no_result(self):
		computation = self.computer.computePoints({}, self.forecast_defeat)
		self.assertEqual(computation["points"], 0)
		self.assertFalse("direction" in computation["bonus"])
	
	def test_different_direction(self):
		computation = self.computer.computePoints(self.result_victory, self.forecast_defeat)
		self.assertEqual(computation["points"], 0)
		self.assertFalse("direction" in computation["bonus"])
	
	def test_same_direction(self):
		computation = self.computer.computePoints(self.result_defeat, self.forecast_defeat)
		self.assertEqual(computation["points"], 12)
		self.assertTrue("direction" in computation["bonus"])
	
	def test_different_draw(self):
		computation = self.computer.computePoints(self.result_draw, self.forecast_defeat)
		self.assertEqual(computation["points"], 0)
		self.assertFalse("direction" in computation["bonus"])
		
	def test_same_draw(self):
		computation = self.computer.computePoints(self.result_draw, self.forecast_draw)
		self.assertEqual(computation["points"], 12)
		self.assertTrue("direction" in computation["bonus"])
		
class PointComputerRugbyDirectionTypeTestCase(unittest.TestCase):
	
	def setUp(self):
		self.computer = PointComputer(rule = {
			"sport": "rugby", 
			"criterias": [{
				"name": "direction_type",
				"points": 12
			}]
		})
		
		self.result_victory = {
			"hometeam": {"score" : 33},
			"awayteam": {"score" : 10}
		}
		
		self.result_large_victory = {
			"hometeam": {"score" : 50},
			"awayteam": {"score" : 7}
		}
		
		self.result_draw = {
			"hometeam": {"score" : 21},
			"awayteam": {"score" : 21}
		}
		
		self.result_defeat = {
			"hometeam": {"score" : 9},
			"awayteam": {"score" : 14}
		}
		
		self.forecast_defeat = {
			"difference_slice": -2
		}
		
		self.forecast_draw = {
			"difference_slice": 0
		}
		
		self.forecast_victory = {
			"difference_slice": 3
		}

	def test_direction(self):
		 self.assertEqual(self.computer.direction_type, 12)
		 
	def test_no_forecast(self):
		computation = self.computer.computePoints(self.result_defeat, {})
		self.assertEqual(computation["points"], 0)
		self.assertFalse("direction_type" in computation["bonus"])
	
	def test_no_result(self):
		computation = self.computer.computePoints({}, self.forecast_defeat)
		self.assertEqual(computation["points"], 0)
		self.assertFalse("direction_type" in computation["bonus"])
	
	def test_different_direction(self):
		computation = self.computer.computePoints(self.result_victory, self.forecast_defeat)
		self.assertEqual(computation["points"], 0)
		self.assertFalse("direction_type" in computation["bonus"])
	
	def test_same_slice(self):
		computation = self.computer.computePoints(self.result_victory, self.forecast_victory)
		self.assertEqual(computation["points"], 12)
		self.assertTrue("direction_type" in computation["bonus"])
	
	def test_different_slice(self):
		computation = self.computer.computePoints(self.result_defeat, self.forecast_defeat)
		self.assertEqual(computation["points"], 0)
		self.assertFalse("direction_type" in computation["bonus"])
		
	def test_same_draw(self):
		computation = self.computer.computePoints(self.result_draw, self.forecast_draw)
		self.assertEqual(computation["points"], 12)
		self.assertTrue("direction_type" in computation["bonus"])
		
	def test_large_victory(self):
		computation = self.computer.computePoints(self.result_large_victory, self.forecast_victory)
		self.assertEqual(computation["points"], 12)
		self.assertTrue("direction_type" in computation["bonus"])

class TestPointComputerFootballTeamScore(unittest.TestCase):
	
	def setUp(self):
		self.computer = PointComputer(rule = {
			"sport": "football", 
			"criterias": [{
				"name": "team_score",
				"points": 6
			}]
		})
		self.forecast = {
			"hometeam": {
				"score": 1
			},
			"awayteam": {
				"score": 3
			}
		}

	def test_team_score(self):
		 self.assertEqual(self.computer.team_score, 6)
	
	def test_no_forecast(self):
		computation = self.computer.computePoints({"hometeam": {"score": 2}, "awayteam": {"score": 4}}, {})
		self.assertEqual(computation["points"], 0)
		self.assertFalse("team_score" in computation["bonus"])
		self.assertFalse("double_team_score" in computation["bonus"])
	
	def test_no_result(self):
		computation = self.computer.computePoints({}, self.forecast)
		self.assertEqual(computation["points"], 0)
		self.assertFalse("team_score" in computation["bonus"])
		self.assertFalse("double_team_score" in computation["bonus"])
	
	def test_different_scores(self):
		computation = self.computer.computePoints({"hometeam": {"score": 2}, "awayteam": {"score": 4}}, self.forecast)
		self.assertEqual(computation["points"], 0)
		self.assertFalse("team_score" in computation["bonus"])
		self.assertFalse("double_team_score" in computation["bonus"])
	
	def test_same_awayscore(self):
		computation = self.computer.computePoints({"hometeam": {"score": 0}, "awayteam": {"score": 3}}, self.forecast)
		self.assertEqual(computation["points"], 6)
		self.assertTrue("team_score" in computation["bonus"])
		self.assertFalse("double_team_score" in computation["bonus"])
	
	def test_same_homescore(self):
		computation = self.computer.computePoints({"hometeam": {"score": 1}, "awayteam": {"score": 4}}, self.forecast)
		self.assertEqual(computation["points"], 6)
		self.assertTrue("team_score" in computation["bonus"])
		self.assertFalse("double_team_score" in computation["bonus"])
	
	def test_same_full_score(self):
		computation = self.computer.computePoints({"hometeam": {"score": 1}, "awayteam": {"score": 3}}, self.forecast)
		self.assertEqual(computation["points"], 12)
		self.assertFalse("team_score" in computation["bonus"])
		self.assertTrue("double_team_score" in computation["bonus"])


class TestPointComputerFootballDifference(unittest.TestCase):
	
	def setUp(self):
		self.computer = PointComputer(rule = {
			"sport": "football", 
			"criterias": [{
				"name": "difference",
				"points": 12
			}]
		})
		self.result = {
			"hometeam": {"score" : 1},
			"awayteam": {"score" : 2}
		}
		self.forecast = {
			"hometeam": {
				"score": 1
			},
			"awayteam": {
				"score": 3
			}
		}

	def test_difference(self):
		 self.assertEqual(self.computer.difference, 12)
	
	def test_no_forecast(self):
		computation = self.computer.computePoints(self.result, {})
		self.assertEqual(computation["points"], 0)
		self.assertFalse("difference" in computation["bonus"])
	
	def test_no_result(self):
		computation = self.computer.computePoints({}, self.forecast)
		self.assertEqual(computation["points"], 0)
		self.assertFalse("difference" in computation["bonus"])
	
	def test_different_difference(self):
		computation = self.computer.computePoints(self.result, self.forecast)
		self.assertEqual(computation["points"], 0)
		self.assertFalse("difference" in computation["bonus"])
	
	def test_same_difference(self):
		computation = self.computer.computePoints({"hometeam": {"score": 1}, "awayteam": {"score": 3}}, self.forecast)
		self.assertEqual(computation["points"], 12)
		self.assertTrue("difference" in computation["bonus"])
	

if __name__ == "__main__" : unittest.main()