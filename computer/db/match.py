from db_connector import DBConnector

db = DBConnector.getDB()

class Team(object):
	
	@staticmethod
	def retrieveMatchFromTeamsAndCompetition(competition, hometeam, awayteam):
		return db.match.find_one({'competition': competition_id, 'hometeam': hometeam, 'awayteam': awayteam})
	
	@staticmethod
	def insertMatchFromLequipe(competition, hometeam, awayteam, lequipe):
		match = {
			'hometeam': hometeam,
			'awayteam': awayteam,
			'competition': competition,
			'sources': {
				'lequipe': {
					'id': match_id
				}
			}
		}
		db.match.insert(match)
	
	@staticmethod
	def updateLequipeFields(match_id, field_id):
		db.match.update({
			'_id': match_id
		}, {
			'$set': {
				'sources.lequipe.id': field_id
			}
		})