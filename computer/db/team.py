from db_connector import DBConnector

db = DBConnector.getDB()

class Team(object):
	
	@staticmethod
	def retrieveTeamFromLequipe(name):
		return db.team.find_one({'names.lequipe': name})

	@staticmethod
	def createTeamFromLequipe(name):
		team = {
			'name': name,
			'names': {
				'lequipe': name
			}
		}
		db.team.insert(team)