from pymongo import MongoClient

class DBConnector(object) :
	"""Class connecting to the database"""
	# Hard-coded connection
	# Get database details
	mongo_uri = 'mongodb://localhost:27017/api'
	#'mongodb://scoragora:brazuca2014@ds047020.mongolab.com:47020/api'
	
	# Connect to the datavase
	client = None
		
	@staticmethod
	def getDB() :
		"""Get the current database"""
		#if DBConnector.client == None:
		DBConnector.client = MongoClient(DBConnector.mongo_uri)
		return DBConnector.client['api']
	