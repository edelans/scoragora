import time

from db.db_connector import DBConnector
from common.logger import Logger
from bson.objectid import ObjectId
from common.lequipe_fr import LequipeFr

# Get database connector
db = DBConnector.getDB()

class GetMatchFinalScore(object):
	
	@staticmethod
	def execute(match_id):
		
		# get match
		match = db.match.find_one({"_id": ObjectId(match_id)})
		sniffer = LequipeFr(match)
		#Logger.log(sniffer.parseChampionship())
		sniffer.parse
		Logger.log(sniffer.parseMatchFinalScore())
		
		