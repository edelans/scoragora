import multiprocessing as mp

from common.point_computer import PointComputer

from algos.compute_progress import ComputeProgress
from db.db_connector import DBConnector
from common.logger import Logger
from bson.objectid import ObjectId

# Get database connector
db = DBConnector.getDB()

def update_league(league_id, members):
	db_thread = DBConnector.getDB()
	db_thread.league.update({
		"_id": league_id
	}, {
		"$set": {
			"members": members,
			"ranked" : True
		}
	})

class ComputeRanking(object) :
	"""
		Class computing the points for every forecast on a given competition.
	"""
	
	@staticmethod
	def execute(competition_id, processes):
		"""
			Parameter: 
				- competition_id
		"""
		
		# retrieve the leagues
		leagues = db.league.find({
			"competition": ObjectId(competition_id)
		})
		count = leagues.count()
		
		pool = mp.Pool(processes)
		#aggregation
		for league in leagues:
			#iterate leagues
			if "members" not in league.keys():
				Logger.log("no members")
				continue
			members = league["members"]
			# sort members
			def getKey(item):
				if "new_points" in item.keys():
					return item["points"]
				elif "points" in item.keys():
					if "inc_points" in item.keys():
						return item["points"] + item["inc_points"]
					else :
						return item["points"]
				return None
			
			members.sort(key=getKey, reverse=True)
			
			# set the rank and replace old points and new points
			index = 0
			previous_rank = 0
			previous_points = -1
			for member in members:
				index += 1
				if "new_points" in member.keys():
					# override points
					member["points"] = member["new_points"]
					# delete new_point
					member.pop("new_points", None)
				if "inc_points" in member.keys():
					# increment points
					#Logger.log(member["points"])
					member["points"] += member["inc_points"]
					#Logger.log(member["points"])
					# reset inc_points
					member.pop("inc_points", None)
				if "points" in member.keys():
					points = member["points"]
					if not points == previous_points:
						previous_rank = index
						previous_points = points
					member["rank"] = previous_rank
			
			#update_league(league["_id"], members)
			pool.apply_async(update_league, (league["_id"], members))
		# Wait for the end of all the updates
		pool.close()
		pool.join()
		return {"processes": processes, "complexity": {"leagues": count}}