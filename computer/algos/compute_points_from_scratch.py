import multiprocessing as mp

from common.point_computer import PointComputer

from algos.compute_progress import ComputeProgress
from algos.compute_ranking import ComputeRanking
from db.db_connector import DBConnector
from common.logger import Logger
from bson.objectid import ObjectId

# Get database connector
db = DBConnector.getDB()

def update_forecast(forecast_id, matches, points, competition_id, user_id):
	# Get database connector
	db_thread = DBConnector.getDB()
	
	db_thread.forecast.update({
		"_id": forecast_id
	}, {
		"$set": {
			"matches": matches,
			"points": points 
		}
	})
	# Update user leagues
	result = db_thread.league.update({
		"members": {
			"$elemMatch": {
				"user": user_id
			}
		},
		"competition": competition_id
	}, {
		"$set" : {
			"members.$.new_points" : points,
			"ranked" : False
		}
	}, upsert=False, multi=True)

class ComputePointsFromScratch(object) :
	"""
		Class computing the points for every forecast on a given competition.
	"""
	
	@staticmethod
	def execute(competition_id, processes=16):
		"""
			Parameter: 
				- competition_id
		"""
		# retrieve all the matches from the competition
		matches = db.match.find({
			"competition": ObjectId(competition_id)
		})
		
		# create match dico
		match_dico = {}
		for match in matches:
			if "result" in match.keys() and "rule" in match.keys():
				if str(match["_id"]) not in match_dico.keys():
					match_dico[str(match["_id"])] = match
		
		# get all the rules from the competition
		rules = matches.distinct('rule')
		
		# create all the points computer
		computer_dico = {}
		for rule in rules:
			computer_dico[str(rule)] = PointComputer(rule)
		
		pool = mp.Pool(processes)
		results = []
		# iterate forecasts
		forecasts = db.forecast.find({"competition" : ObjectId(competition_id)})
		for forecast in forecasts:
			#iterate matches
			if "matches" not in forecast.keys() or "user" not in forecast.keys():
				continue
			points = 0
			for match in forecast["matches"]:
				if "match" not in match.keys() or "forecast" not in match.keys():
					continue
				match_id = str(match["match"])
				if match_id in match_dico.keys():
					#Logger.log({"in": "inside"})
					rule = str(match_dico[match_id]["rule"])
					if rule in computer_dico.keys():
						match_result = match_dico[match_id]["result"]
						match_forecast = match["forecast"]
						computation = computer_dico[rule].computePoints(match_result, match_forecast)
						points += computation["points"]
						match["points"] = computation["points"]
						match["bonus"] = computation["bonus"]
						##if match["points"] is not None and not match["points"] == computation["points"]:
						#	Logger.log({"mpoints": match["points"], "points": computation["points"]})
			#if not forecast["points"] == points and not forecast["points"] - 150 == points and not forecast["points"] == 0:
			#	Logger.log({"fpoints": forecast["points"], "points": points})
			#update_forecast(forecast["_id"], forecast["matches"], points, ObjectId(competition_id), forecast["user"])
			pool.apply_async(update_forecast, (forecast["_id"], forecast["matches"], points, ObjectId(competition_id), forecast["user"]))
		# Wait for the end of all the updates
		pool.close()
		pool.join()
		
		Logger.log("start of progress")
		ComputeProgress.execute(competition_id)
		Logger.log("start of ranking")
		ComputeRanking.execute(competition_id, processes)
			