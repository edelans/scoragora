import datetime
from common.point_computer import PointComputer

from db.db_connector import DBConnector
from common.logger import Logger
from bson.objectid import ObjectId

# Get database connector
db = DBConnector.getDB()

class ComputeProgress(object) :
	"""
		Class computing the progress for a given competition.
	"""
	
	@staticmethod
	def execute(competition_id):
		"""
			Parameter: 
				- competition_id
		"""
		matches = db.match.find({
			"competition": ObjectId(competition_id)
		})
		
		count_dico = {}
		for match in matches:
			if "rule" in match.keys():
				if "status" in match.keys() and match["status"] == "closed":
					if str(match["rule"]) in count_dico.keys():
						count_dico[str(match["rule"])]["past"] += 1
					else :
						count_dico[str(match["rule"])] = {"past": 1, "future": 0}
				else :
					if str(match["rule"]) in count_dico.keys():
						count_dico[str(match["rule"])]["future"] += 1
					else :
						count_dico[str(match["rule"])] = {"past": 0, "future": 1}
		past_points = 0
		future_points = 0
		past_matches = 0
		future_matches = 0
		for rule_id in count_dico.keys():
			computer = PointComputer(ObjectId(rule_id))
			max_points = computer.computeMaxPoints()
			past_points += count_dico[rule_id]["past"]*max_points
			future_points += count_dico[rule_id]["future"]*max_points
			past_matches += count_dico[rule_id]["past"]
			future_matches += count_dico[rule_id]["future"]
		db.competition.update({
			"_id": ObjectId(competition_id)
		}, {
			"$set": {
				"progress": {
					"matches": {
						"previous": past_matches,
						"coming": future_matches
					},
					"total": past_points + future_points,
					"now": past_points
				}
			}
		})