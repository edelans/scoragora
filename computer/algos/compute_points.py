import multiprocessing as mp

from common.point_computer import PointComputer

from algos.compute_progress import ComputeProgress
from algos.compute_ranking import ComputeRanking
from db.db_connector import DBConnector
from common.logger import Logger
from bson.objectid import ObjectId

# Get database connector
db = DBConnector.getDB()

def update_forecast(forecast_id, match_id, user_id, competition_id, computation, revert = False):
	db_thread = DBConnector.getDB()
	bonus = computation.get("bonus")
	points = computation.get("points")
	if bonus is None or points is None:
		# In this particular case the computation failed and we shouldn't update anything
		return
	if revert is True :
		inc = -int(points)
		db_thread.forecast.update({
			"_id": forecast_id,
			"matches": {
				"$elemMatch": {
					"match": match_id
				}
			}
		}, {
			"$inc": {
				"points": inc
			},
			"$set": {
				"matches.$.bonus": [],
				"matches.$.points": 0
			}
		})
	else:
		inc = int(points)
		db_thread.forecast.update({
			"_id": forecast_id,
			"matches": {
				"$elemMatch": {
					"match": match_id
				}
			}
		}, {
			"$inc": {
				"points": inc
			},
			"$set": {
				"matches.$.bonus": bonus,
				"matches.$.points": points
			}
		})
	# Update user leagues
	db_thread.league.update({
		"members": {
			"$elemMatch": {
				"user": user_id
			}
		},
		"competition": competition_id
	}, {
		"$inc" : {
			"members.$.inc_points" : inc
		},
		"$set": {
			"ranked" : False
		}
	}, upsert=False, multi=True)


class ComputePoints(object) :
	"""
		Class computing the points for every forecast on a given competition.
	"""
	
	@staticmethod
	def execute(match_id, revert, processes=8):
		"""
			Parameter: 
				- match_id
		"""
		# status required
		if revert :
			status = "closed"
			new_status = "active"
		else:
			status = "to_be_computed"
			new_status = "being_computed"
		# retrieve the match
		match = db.match.find_one({
			"_id": ObjectId(match_id),
			"status": status
		})
		
		if match is None:
			# no match found
			Logger.log('no match found')
			return
		
		# update the match status to avoid other modifications
		db.match.update({
			"_id": ObjectId(match_id)
		}, {
			"$set": {
				"status": new_status
			}
		})
		
		rule_id = match.get("rule")
		match_result = match.get("result")
		competition_id = match.get("competition")
		if rule_id is None or match_result is None or competition_id is None:
			# incomplete record
			Logger.log('incomplete record')
			return	
		
		# create computer
		computer = PointComputer(rule_id)
		
		pool = mp.Pool(processes)
		results = []
		# iterate forecasts
		forecasts = db.forecast.find({
			"competition" : match["competition"],
		}, {
			"matches": {
				"$elemMatch": {
					"match": ObjectId(match_id)
				}
			},
			"user": 1,
			"competition": 1
			#"points": 1
		})
		for forecast in forecasts:
			#iterate forecasts
			forecast_id = forecast.get("_id")
			matches = forecast.get("matches", [])
			if not matches:
				Logger.log("no matches")
				continue
			user_id = forecast.get("user")
			if not user_id:
				Logger.log("no user linked")
				continue
			#if "points" not in forecast.keys():
			#	Logger.log({"keys": forecast.keys()})
			#points = forecast["points"]
			match_forecast = matches[0].get("forecast")
			
			if not match_forecast:
				#Logger.log("no forecast in match")
				continue
			
			computation = computer.computePoints(match_result, match_forecast)
			#if not computation["points"] == 0:
			#	break
			pool.apply_async(update_forecast, (forecast_id, match.get("_id"), user_id, forecast.get("competition"), computation, revert))
		# Wait for the end of all the updates
		pool.close()
		pool.join()
		
		#forecast = db.forecast.find_one({"_id": forecast_id})
		#if "matches" not in forecast.keys():
		#	return
		#Logger.log({"matches" : len(forecast["matches"])})
		#for match in forecast["matches"]:
		#	if str(match["match"]) == match_id:
		#		Logger.log({"cpoints": computation["points"], "mpoints": match["points"]})
		#		Logger.log({"fpoints": points, "new_fpoints": forecast["points"]})
		#		if computation["points"] == match["points"]:
		#			Logger.log('OK')
		#		else:
		#			Logger.log('KO')
		#[p.get() for p in results]
		
		# Update match
		if revert :
			db.match.update({
				"_id": ObjectId(match_id)
			}, {
				"$set": {
					"status": "active",
					"result": {}
				}
			})
		else :
			db.match.update({
				"_id": ObjectId(match_id)
			}, {
				"$set": {
					"status": "closed"
				}
			})
		
		Logger.log("start of progress")
		ComputeProgress.execute(str(competition_id))
		Logger.log("start of ranking")
		ComputeRanking.execute(str(competition_id), processes)