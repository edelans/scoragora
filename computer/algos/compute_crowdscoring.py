import time
import datetime
import multiprocessing as mp

from db.db_connector import DBConnector
from common.logger import Logger
from bson.objectid import ObjectId

# Get database connector
db = DBConnector.getDB()

def update_match_hometeam(match_id, hometeam_forecast):
	db_thread = DBConnector.getDB()
	db_thread.match.update({
			"_id": match_id
		}, {
			"$set": {
				"forecast.hometeam": hometeam_forecast 
			}
		})
		
def update_match_awayteam(match_id, awayteam_forecast):
	db_thread = DBConnector.getDB()
	db_thread.match.update({
			"_id": match_id
		}, {
			"$set": {
				"forecast.awayteam": awayteam_forecast 
			}
		})

def update_match_difference_slice(match_id, difference_slice):
	db_thread = DBConnector.getDB()
	db_thread.match.update({
			"_id": match_id
		}, {
			"$set": {
				"forecast.slices": difference_slice 
			}
		})

class ComputeCrowdscoring(object):
	
	@staticmethod
	def execute(competition_id, sport, processes=8):
		start_time = time.time()
		if sport == "football":
			aggregation = db.forecast.aggregate([{
					"$match": {
						"competition": competition_id
					}
				},{
					"$project": {
						"matches": 1
					}
				},{
					"$unwind": "$matches"
				},{
					"$group": {
						"_id" : {
							"score" : "$matches.forecast.hometeam.score",
							"match" : "$matches.match"
						},
						"count" : {
							"$sum" : 1
						}
					}
				}, {
					"$group" : {
						"_id" : {
							"match" : "$_id.match"
						},
						"scores" : {
							"$push": {"count": "$count", "score" : "$_id.score" }
						}
					}
				}
			])
		
			# Update match statistics
			home_pool = mp.Pool(processes)
			home_results = [home_pool.apply_async(update_match_hometeam, (match["_id"]["match"], match['scores'])) for match in aggregation]
			# Wait for the end of all the updates
			[p.get() for p in home_results]
			
			aggregation = db.forecast.aggregate([{
					"$match": {
						"competition": competition_id
					}
				},{
					"$project": {
						"matches": 1
					}
				},{
					"$unwind": "$matches"
				},{
					"$group": {
						"_id" : {
							"score" : "$matches.forecast.awayteam.score",
							"match" : "$matches.match"
						},
						"count" : {
							"$sum" : 1
						}
					}
				}, {
					"$group" : {
						"_id" : {
							"match" : "$_id.match"
						},
						"scores" : {
							"$push": {"count": "$count", "score" : "$_id.score" }
						}
					}
				}
			])
	
			# Update match statistics
			away_pool = mp.Pool(processes)
			away_results = [away_pool.apply_async(update_match_awayteam, (match["_id"]["match"], match['scores'])) for match in aggregation]
			# Wait for the end of all the updates
			[p.get() for p in away_results]

		if sport == "rugby": 
			aggregation = db.forecast.aggregate([{
					"$match": {
						"competition": competition_id
					}
				},{
					"$project": {
						"matches": 1
					}
				},{
					"$unwind": "$matches"
				},{
					"$group": {
						"_id" : {
							"difference_slice" : "$matches.forecast.difference_slice",
							"match" : "$matches.match"
						},
						"count" : {
							"$sum" : 1
						}
					}
				}, {
					"$group" : {
						"_id" : {
							"match" : "$_id.match"
						},
						"slices" : {
							"$push": {"count": "$count", "difference_slice" : "$_id.difference_slice" }
						}
					}
				}
			])
			
			# Update match statistics
			rugby_pool = mp.Pool(processes)
			rugby_results = [rugby_pool.apply_async(update_match_difference_slice, (match["_id"]["match"], match["slices"])) for match in aggregation]
			# Wait for the end of all the updates
			[p.get() for p in rugby_results]
		
		# compute the execution time
		execution_time = time.time() - start_time
		Logger.log({"status": 200, "competition": str(competition_id), "execution_time": execution_time, "processes": processes})