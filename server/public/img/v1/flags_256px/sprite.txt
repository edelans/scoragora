############################
           CSS
############################
.sprite { background: url('sprite.png') no-repeat top left; width: 256px; height: 256px;  }
.sprite.Uruguay { background-position: 0px 0px;  }
.sprite.USA { background-position: 0px -266px;  }
.sprite.England { background-position: 0px -532px;  }
.sprite.Switzerland { background-position: 0px -798px;  }
.sprite.Spain { background-position: 0px -1064px;  }
.sprite.Russia { background-position: 0px -1330px;  }
.sprite.Portugal { background-position: 0px -1596px;  }
.sprite.Nigeria { background-position: 0px -1862px;  }
.sprite.Netherlands { background-position: 0px -2128px;  }
.sprite.Mexico { background-position: 0px -2394px;  }
.sprite.KoreaRepublic { background-position: 0px -2660px;  }
.sprite.Japan { background-position: 0px -2926px;  }
.sprite.Italy { background-position: 0px -3192px;  }
.sprite.Iran { background-position: 0px -3458px;  }
.sprite.Honduras { background-position: 0px -3724px;  }
.sprite.Greece { background-position: 0px -3990px;  }
.sprite.Ghana { background-position: 0px -4256px;  }
.sprite.Germany { background-position: 0px -4522px;  }
.sprite.France { background-position: 0px -4788px;  }
.sprite.Ecuador { background-position: 0px -5054px;  }
.sprite.Croatia { background-position: 0px -5320px;  }
.sprite.IvoryCoast { background-position: 0px -5586px;  }
.sprite.CostaRica { background-position: 0px -5852px;  }
.sprite.Colombia { background-position: 0px -6118px;  }
.sprite.Chile { background-position: 0px -6384px;  }
.sprite.Cameroon { background-position: 0px -6650px;  }
.sprite.Brazil { background-position: 0px -6916px;  }
.sprite.BosniaandHerzegovina { background-position: 0px -7182px;  }
.sprite.Belgium { background-position: 0px -7448px;  }
.sprite.Australia { background-position: 0px -7714px;  }
.sprite.Argentina { background-position: 0px -7980px;  }
.sprite.Algeria { background-position: 0px -8246px;  }


############################
           HTML
############################
<div class='sprite Uruguay'></div>
<div class='sprite USA'></div>
<div class='sprite England'></div>
<div class='sprite Switzerland'></div>
<div class='sprite Spain'></div>
<div class='sprite Russia'></div>
<div class='sprite Portugal'></div>
<div class='sprite Nigeria'></div>
<div class='sprite Netherlands'></div>
<div class='sprite Mexico'></div>
<div class='sprite KoreaRepublic'></div>
<div class='sprite Japan'></div>
<div class='sprite Italy'></div>
<div class='sprite Iran'></div>
<div class='sprite Honduras'></div>
<div class='sprite Greece'></div>
<div class='sprite Ghana'></div>
<div class='sprite Germany'></div>
<div class='sprite France'></div>
<div class='sprite Ecuador'></div>
<div class='sprite Croatia'></div>
<div class='sprite IvoryCoast'></div>
<div class='sprite CostaRica'></div>
<div class='sprite Colombia'></div>
<div class='sprite Chile'></div>
<div class='sprite Cameroon'></div>
<div class='sprite Brazil'></div>
<div class='sprite BosniaandHerzegovina'></div>
<div class='sprite Belgium'></div>
<div class='sprite Australia'></div>
<div class='sprite Argentina'></div>
<div class='sprite Algeria'></div>