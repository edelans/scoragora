angular.module('scoragora.controllers.prediction', [])
.controller('ForecastCtrl', function(forecastModel, forecastService, messageService, competitionService, pageInfo, $scope, $uibModal, $anchorScroll, $location, $stateParams, $analytics, $i18next) {
  pageInfo.title = $i18next("title.forecast.competition");
  var forecast_id = $stateParams.id;
  $anchorScroll.yOffset = 70;

  $scope.scrollTo = function(id) {
    $location.hash(id);
    $anchorScroll();
  };

  $scope.alert = {};

  $scope.saveAll = function() {
    if ($scope.forecast && $scope.forecast.matches) {
      $scope.forecast.matches.forEach(function(match) {
        if ($scope.match_dico[match.match]) {
          match.forecast = $scope.match_dico[match.match].match_forecast;
        }
      });
      forecastService.updateForecast($scope.forecast._id, $scope.forecast.matches)
      .success(function(data) {
        $scope.alert = {
          type: 'success',
          message: data.message
        };

        // emit event track for google analytics
        $analytics.eventTrack('setForecast', {
          category: 'Forecast',
          label: $scope.user._id
        });
      })
      .error(function(data) {
        messageService.error($scope.alert, data);
      });
    }
  };

  $scope.saveGroup = function(group) {
    if ($scope.forecast && $scope.forecast.matches) {
      $scope.forecast.matches.forEach(function(match) {
        if ($scope.match_dico[match.match] && $scope.match_dico[match.match].group == group) {
          match.forecast = $scope.match_dico[match.match].match_forecast;
        }
      });
      forecastService.updateForecast($scope.forecast._id, $scope.forecast.matches)
      .success(function(data) {
        $scope.alert = {
          type: 'success',
          message: data.message
        };

        // emit event track for google analytics
        $analytics.eventTrack('setForecast', {
          category: 'Forecast',
          label: $scope.user._id
        });
      })
      .error(function(data) {
        messageService.error($scope.alert, data);
        console.log($scope.alert);
      });
    }
  };

  $scope.saveRound = function(round) {
    if ($scope.forecast && $scope.forecast.matches) {
      $scope.forecast.matches.forEach(function(match) {
        if ($scope.match_dico[match.match] && $scope.match_dico[match.match].round == round) {
          match.forecast = $scope.match_dico[match.match].match_forecast;
        }
      });
      forecastService.updateForecast($scope.forecast._id, $scope.forecast.matches)
      .success(function(data) {
        $scope.alert = {
          type: 'success',
          message: data.message
        };

        // emit event track for google analytics
        $analytics.eventTrack('setForecast', {
          category: 'Forecast',
          label: $scope.user._id
        });
      })
      .error(function(data) {
        messageService.error($scope.alert, data);
        console.log($scope.alert);
      });
    }
  };

  $scope.openRulesModal = function() {
    var modalInstance = $uibModal.open({
      templateUrl: 'views/league_rules.html',
      controller: 'LeagueRulesCtrl',
      resolve: {
        rules: function() {
          return $scope.competition.rules;
        },
        config: function() {
          return $scope.competition.config;
        }
      }
    });
  };

  function getForecast(forecast_id, next) {
    if (forecastModel.forecast && forecastModel.forecast._id == forecast_id) {
      $scope.prediction = forecastModel.prediction;
      next();
    }
    else {
      forecastService.getForecast(forecast_id)
      .success(function(data) {
        $scope.forecast = data.forecast;
        next();
      });
    }
  }

  function isValidMatch(match) {
    return match.datetime && match.hometeam && match.awayteam;
  }

  function isActiveMatch(match) {
    return match.status != "closed";
  }

  function buildData() {
    if ($scope.forecast && $scope.forecast.matches && $scope.competition && $scope.matches) {
      var stats = {
        points: 0,// TODO $scope.forecast.points,
        bet: {
          matches: 0
        },
        competition: {
          matches: 0
        }
      };
      // Store progression
      if ($scope.competition.progress) {
        stats.competition.progress = Math.floor(100*$scope.competition.progress.now / $scope.competition.progress.total);
        stats.competition.points = $scope.competition.progress.total;
        // if ($scope.competition.progress.matches) {
        //   stats.competition.matches = $scope.competition.progress.matches.coming + $scope.competition.progress.matches.previous;
        //   stats.bet.matches = $scope.competition.progress.matches.previous;
        // }
      }
      var match_dico = {};
      $scope.matches.forEach(function(match) {
        // increment total number of match
        stats.competition.matches++;
        if (moment(match.datetime).isBefore(moment())) {
          match.closed = true;
        }
        if (match.status == "closed") {
          stats.bet.matches++;
        }
        match_dico[match._id] = match;
      });
      var direction = 0;
      var perfect = 0;
      $scope.forecast.matches.forEach(function(match) {
        if (match_dico[match.match]) {
          match_dico[match.match].match_forecast = match.forecast;
        }
        if (match.bonus && match.bonus.length > 0) {
          match.bonus.forEach(function(item) {
            if (item == "direction") {
              direction++;
            }
            // Check if perfect
            if ($scope.competition.sport == "rugby" && item == "direction_type") {
              perfect++;
            }
            else if ($scope.competition.sport == "football" && item == "double_team_score") {
              perfect++;
            }
          });
        }
        if (match.points) {
          stats.points += match.points;
        }
      });
      stats.bet.direction = Math.floor(100*direction / stats.bet.matches);
      stats.bet.perfect = Math.floor(100*perfect / stats.bet.matches);
      $scope.match_dico = match_dico;

      $scope.rounds = [];
      $scope.group_names = [];
      $scope.days = [];
      $scope.groupInfo = {};
      $scope.roundInfo = {};
      $scope.dayInfo = {};
      $scope.day = Infinity;
      $scope.competition.matches.forEach(function(match) {
        if (isValidMatch(match_dico[match.match])) {
          // Check if it s a new group name
          if (!!match.group && $scope.group_names.indexOf(match.group) == -1) {
            $scope.group_names.push(match.group);
          }
          // Check if it s an active group game
          if (!!match.group && isActiveMatch(match_dico[match.match])) {
            $scope.groupInfo[match.group] = {active: true};
          }
          // Check if it s a new round number
          if (!!match.round && $scope.rounds.indexOf(match.round) == -1) {
            $scope.rounds.push(match.round);
          }
          if (!!match.round && isActiveMatch(match_dico[match.match])) {
            $scope.roundInfo[match.round] = {active: true};
          }
          // Check if it s a new day number
          if (!isNaN(match.day) && !match.group && !match.round && $scope.days.indexOf(match.day) == -1) {
            $scope.days.push(match.day);
          }
          if (!isNaN(match.day) && !match.group && !match.round && isActiveMatch(match_dico[match.match])) {
            $scope.dayInfo[match.day] = {active: true};
            var datetime = match_dico[match.match].datetime;
            if (moment(datetime).isAfter(moment()) && match.day < $scope.day) {
	          $scope.day = match.day;
            }
          }
          match.valid = true;
        }
      });
      $scope.stats = stats;
      $scope.rounds.sort(function(a, b) {
        if (b == 3) return 1.5 - a;
        if (a == 3) return b - 1.5;
        return b - a;
      });
      $scope.group_names.sort();
      $scope.days.sort();
      $scope.day = Math.min($scope.day, $scope.days.length - 1);
    }
  }

  function getCompetitionForecast(competition_id, next) {
    if (forecastModel.competition && forecastModel.competition._id == competition_id) {
      $scope.competition = forecastModel.competition;
      next();
    }
    else {
      competitionService.getCompetition(competition_id)
      .success(function(data) {
        $scope.competition = forecastModel.competition = data.competition;
        next();
      });
    }
  }

  function getCompetitionMatches(competition_id, next) {
    if (forecastModel.matches && forecastModel.matches.length > 0 && forecastModel.matches[0].competition == competition_id) {
      $scope.matches = forecastModel.matches;
      next();
    }
    else {
      competitionService.getCompetitionMatches(competition_id, {})
      .success(function(data) {
        $scope.matches = forecastModel.matches = data.matches;
        next();
      });
    }
  }

  getForecast(forecast_id, function() {
    getCompetitionForecast($scope.forecast.competition, buildData);
    getCompetitionMatches($scope.forecast.competition, buildData);
  });
});
