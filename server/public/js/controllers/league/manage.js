angular.module('scoragora.controllers.league.manage', [])
.controller('LeagueManageCtrl', function(userModel, leagueModel, leagueService, userInfo, pageInfo, $scope, $state, $stateParams, $i18next) {
  pageInfo.title = $i18next("title.league.invitation");

  $scope.user_id = userInfo.userId;

  // Retrieve the league
  leagueService.getLeaguesByUrlName($stateParams.name)
  .success(function(data) {
  	if (!data.leagues || data.leagues.length === 0) {
	  // empty league list in the model
	  if (userModel.user) delete userModel.user.user_leagues;
	  $scope.leagueNotFound = true;	
  	}
  	
    $scope.league = data.leagues[0];
    $scope.link = "www.scoragora.com/#/join/" + $scope.league.token;
    if ($scope.league.captains) {
	  $scope.league.captains.forEach(function(captain) {
		if (captain == $scope.user_id) $scope.isCaptain = true;  
	  });
    }
    if (!$scope.isCaptain) {
	  $state.go("league.invite", {name: $scope.league.name});
    }
    
    // retrieve the members
    leagueService.getAllLeagueMembers($scope.league._id)
    .success(function(data) {
	  if (data.members) {
        var dico_members = {};
        var dico_flags = {};
        var flags = [];
        data.members.forEach(function(member) {
          dico_members[member._id] = member;
        });
        $scope.members = $scope.league.members;
        $scope.members.forEach(function(member) {
	      if (dico_members[member.user]) member.user = dico_members[member.user];
	      if (member.flag) {
		    if (!dico_flags[member.flag]) {
			  dico_flags[member.flag] = 0;
			  flags.push({flag: member.flag});
			}
		    dico_flags[member.flag] += 1;
	      } 
        });
        // Sort members by email
        $scope.members.sort(function(a, b) {
	      if (a.user.displayName) {
	      	return a.user.displayName.localeCompare(b.user.displayName);
	      }
	      return -1;
        });
        
        // List flags
        flags.forEach(function(flag) {
	      flag.count = dico_flags[flag.flag];
        });
        $scope.flags = flags;
      }
    })
    .error(function(data) {
	  if (data && data.status == 403) $state.go("league.invite", {name: $stateParams.name});
    });
  })
  .error(function(data) {
  	if (data && data.code) {
	  if (data.code == 40012) {
        // empty league list in the model
        if (userModel.user) delete userModel.user.user_leagues;
        $scope.leagueNotFound = true;
      }
      else if (data.code == 40013) {
        // empty league list in the model
        if (userModel.user) delete userModel.user.user_leagues;
        $scope.notMember = true;
      }
    }
  });
  
  $scope.updateMemberFlag = function(userId) {
	return function(flag) {
	  delete leagueModel.league;
	  leagueService.updateLeagueMember($scope.league._id, userId, {
		flag: {
		  value: flag
		}
	  })
	  .success(function(data) {
	    var dico_flags = {};
	    var flags = [];
	    $scope.members.forEach(function(member) {
	      if (member.user._id == userId) {
		    member.flag = flag;
	      }
	      if (member.flag) {
		    if (!dico_flags[member.flag]) {
			  dico_flags[member.flag] = 0;
			  flags.push({flag: member.flag});
			}
		    dico_flags[member.flag] += 1;
	      } 
        });
        
        // List flags
        flags.forEach(function(flag) {
	      flag.count = dico_flags[flag.flag];
        });
        $scope.flags = flags;
	  });	
	};
  };

});