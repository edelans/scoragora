angular.module("scoragora.controllers.league.stats", [])
.controller('LeagueStatsCtrl', function(leagueService, competitionService, leagueTool, userInfo, userModel, pageInfo, $scope, $stateParams, $i18next) {
  pageInfo.title = $i18next("title.league.stats");
  $scope.user_id = userInfo.userId;

  $scope.select = function(member, event) {
    event.preventDefault();
    // d3js hack
    var selection = d3.selectAll('.sc-evolution-section svg[sc-multi-line] path[lineId="' + member.user + '"], .sc-evolution-section svg[sc-multi-line] circle[lineId="' + member.user + '"]');
    if (selection && selection[0].length) selection.classed("hidden", member.checked);
    else {
      // complex selector doesnt work on some browsers
      d3.selectAll('.sc-evolution-section svg[sc-multi-line] path.line').each(function(path) {
        if (path.lineId == member.user) {
          var line = d3.select(this);
          line.classed("hidden", member.checked);
        }
      });
      d3.selectAll('.sc-evolution-section svg[sc-multi-line] .point-value').each(function(point) {
        if (point.lineId == member.user) {
          var line = d3.select(this);
          line.classed("hidden", member.checked);
        }
      });
    }
    member.checked = !member.checked;
  };
  
  $scope.ranking_options = [{
	  value: "from",
	  label: $i18next('league.stats.from')
  }, {
	  value: "till",
	  label: $i18next('league.stats.till')
  }];
  
  $scope.phase_options = [{
	  value: "group",
	  label: $i18next('forecast.qualifications')
  }];
  
  $scope.customForm = {
	  phase: 3,
	  ranking: "till"
  };
  
  $scope.computeCustomCupRanking = function() {
	  if (!$scope.competition.matches) return;
	  if (!$scope.customForm.phase || !$scope.customForm.ranking) return;
	  // get all the match to take into account
	  var matches = _.filter($scope.competition.matches, function(match) {
		  if ($scope.customForm.ranking == "from") {
			  if ($scope.customForm.phase == "group") return true;
			  if ($scope.customForm.phase == 3) return match.round <= 3 && match.round != 2;
			  if ($scope.customForm.phase == 2) return match.round <= 3;
			  return match.round <= $scope.customForm.phase;
		  }
		  if ($scope.customForm.ranking == "till") {
			  if (!!match.group) return true;
			  if ($scope.customForm.phase == 3) return match.round >= 2;
			  if ($scope.customForm.phase == 2) return match.round >= 2 && match.round != 3;
			  return match.round >= $scope.customForm.phase;
		  }
	  });
	  console.log(matches.length);
  };


  // Retrieve the league
  leagueTool.getLeague($stateParams.name, function(err, league) {
    if (err) {
      if (err.code == 40012) {
        // empty league list in the model
        if (userModel.user) delete userModel.user.user_leagues;
        $scope.leagueNotFound = true;
      }
      else if (err.code == 40013) {
        // empty league list in the model
        if (userModel.user) delete userModel.user.user_leagues;
        $scope.notMember = true;
      }
      return; // TODO log it the server
    }
    // add the checked flag to 10 first members
    if (league && league.members) {
      var countDown = 9;
      league.members.forEach(function(member) {
        if (member.user == $scope.user_id) {
          member.checked = true;
        }
        else if (countDown > 0) {
          member.checked = true;
          countDown--;
        }
        else {
          member.checked = false;
        }
      });
    }
    $scope.league = league;

    competitionService.getCompetitionMatches($scope.league.competition, {status: 'past'})
    .success(function(data) {
      if (data.matches) {
        // create a match dico
        $scope.matches = data.matches;
        $scope.matchesAsDico = _.indexBy(data.matches, function(item) {
          return item._id;
        });
        buildMultiLineData();
      }
    });

    leagueService.getLeagueForecasts($scope.league._id)
    .success(function(data) {
      if (data.forecasts) {
        $scope.forecasts = data.forecasts;
        buildMultiLineData();
      }
    });

    leagueTool.getLeagueMembersAsDico($scope.league._id, function(err, members) {
      if (err) return; // TODO sthg
      $scope.membersAsDico = members;
      buildMultiLineData();
    });
    
    /*competitionService.getCompetition($scope.league.competition)
    .success(function(data) {
      $scope.competition = data.competition;
      $scope.computeCustomCupRanking();
    });*/
  });

  /* This function is a mess !!!! TODO: refactor */
  function buildMultiLineData() {
    if ($scope.league && $scope.league.members && $scope.forecasts && $scope.membersAsDico && $scope.matchesAsDico) {

      // set the color set
      var color = d3.scale.category10();
      color.domain(_.map($scope.membersAsDico, function(item, key) { return item._id + item.displayName; }));
      _.each($scope.league.members, function(item) {
        item.style = {background: color(item.user + $scope.membersAsDico[item.user].displayName) };
      });

      // Populate data in each forecast
      var linesPerMatch = {};
      var linesPerDate = {};
      _.each($scope.forecasts, function(item) {
        var member = _.findWhere($scope.league.members, {user: item.user});
        var user = $scope.membersAsDico[item.user];
        var match = $scope.matchesAsDico[item.match];
        if (match && member && user) {
          if (!linesPerMatch[user._id]) {
            linesPerMatch[user._id] = {
              hidden: !member.checked,
              lineId: user._id,
              lineLabel: user.displayName,
              lineColor: color(user._id + user.displayName),
              points: []
            };
          }
          if (!linesPerDate[user._id]) {
            linesPerDate[user._id] = {
              hidden: !member.checked,
              lineId: user._id,
              lineLabel: user.displayName,
              lineColor: color(user._id + user.displayName),
              points: []
            };
          }
          linesPerMatch[user._id].points.push({
            match: match,
            user: member,
            date: moment(match.datetime).unix(),//.subtract(5, "hours").startOf("day").unix(),
            value: item.points,
            lineId: user._id
          });
          linesPerDate[user._id].points.push({
            match: match,
            user: member,
            date: moment(match.datetime).subtract(5, "hours").startOf("day").unix(),
            value: item.points,
            lineId: user._id
          });
        }
      });

      // Per match

      linesPerMatch = _.map(linesPerMatch, function(item, key) {
        return item;
      });

      _.each(linesPerMatch, function(line) {
        line.points.sort(function(a, b) {
	        if (a.date < b.date) return -1;
	        if (a.date > b.date) return 1;
	        return a.match._id.localeCompare(b.match._id);
        });
        var points = 0;
        line.points = _.each(line.points, function(item) {
          points += item.value;
          item.value = points;
          item.columnId = item.match._id;
        });
        line.points.unshift({
          lineId: line.lineId,
          value: 0,
          columnId: ""
        });
      });

      $scope.statsPerMatch = linesPerMatch; //groupPerUserArray;
      $scope.statsPerMatchOptions = {
        yTitle: $i18next("league.stats.charts_points"),
        noXTickLabel: true,
        height: 400,
        margin: {
          bottom: 30
        }
      };
      $scope.options = {
      	mode : "match"
      };

      // Per date

      linesPerDate = _.map(linesPerDate, function(item, key) {
        return item;
      });

      _.each(linesPerDate, function(line) {
        var groupedPointsPerDate = {};
        _.each(line.points, function(points) {
          if (!groupedPointsPerDate[points.date]) {
            groupedPointsPerDate[points.date] = points;
          }
          else {
            groupedPointsPerDate[points.date].value += points.value;
          }
        });

        groupedPointsPerDate = _.map(groupedPointsPerDate, function(item, key) {
          return item;
        });

        var sortedPoints = _.sortBy(groupedPointsPerDate, "date");
        var points = 0;
        line.points = _.each(sortedPoints, function(item) {
          points += item.value;
          item.value = points;
          item.columnId = moment.unix(item.date).format("DD/MM");
        });
        line.points.unshift({
          lineId: line.lineId,
          value: 0,
          columnId: ""
        });
      });

      $scope.statsPerDate = linesPerDate;
      $scope.statsPerDateOptions = {
        yTitle: $i18next("league.stats.charts_points"),
        height: 400,
        margin: {
          bottom: 50
        }
      };
      // [{points: [{value, key}], lineId, lineColorId}]
    }
  }
});