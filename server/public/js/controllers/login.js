angular.module('scoragora.controllers.login', [])
.controller('LoginCtrl', function(userInfo, userModel, userService, messageService, pageInfo, $scope, $i18next, $auth) {
  pageInfo.title = $i18next('title.login');

  $scope.login = "";
  $scope.password = "";

  $scope.alert = {};

  $scope.submit = function() {
    $scope.alert = {
      type: 'info',
      message: $i18next('messages:code.10001')
    };
    var redirect = $auth.loginRedirect;
    $auth.login({
      email: $scope.login,
      password: $scope.password
    }, redirect)
    .then(function() {
      getUser();
    })
    .catch(function(response) {
      messageService.error($scope.alert, response.data);
    });
  };

  $scope.authenticate = function(provider) {
    var redirect = $auth.loginRedirect;
    $auth.authenticate(provider, redirect)
    .then(function() {
      getUser();
    })
    .catch(function(response) {
      if (response.error) return;
      messageService.error($scope.alert, response.data);
    });
  };

  function getUser() {
    var userId = userInfo.getUserId();
    userService.getUser(userId)
    .success(function(data) {
      userModel.user = userService.populateUser(data.user);
      if (data.user) {
        userInfo.displayName = data.user.displayName;
        userInfo.pictureURL = data.user.pictureURL;
      }
      // set language
      if (data.user.lng) {
        $i18next.options.lng = data.user.lng;
      }
    });
  }
})
.controller('LoginForgottenCtrl', function(loginService, pageInfo, $scope, $i18next) {
  pageInfo.title = $i18next("title.forgotten_password");

  $scope.token = {
    email : ''
  };

  $scope.submit = function() {
    $scope.disabled = true;
    loginService.createRecoveryToken({
      email: $scope.token.email
    })
    .success(function(data) {
      $scope.email_sent = true;
    })
    .error(function(data) {
      $scope.disabled = false;
      $scope.alert = {
        type: 'danger',
        message: data && data.message ? data.message : 'internal server error'
      };
    });
  };
})
.controller('LoginResetCtrl', function(loginService, pageInfo, $scope, $location, $i18next) {
  pageInfo.title = $i18next("title.reset_password");

  $scope.form = {};

  var token = $location.search().token;
  var email = $location.search().email;

  $scope.submit = function() {
    $scope.disabled = true;
    loginService.updateUserPassword({
      email: email,
      token: token,
      password: $scope.form.password,
      password_confirmation: $scope.form.password_confirmation
    })
    .success(function(data) {
      $scope.alert = {
        type: 'success',
        message: data.message
      };
    })
    .error(function(data) {
      $scope.disabled = false;
      $scope.alert = {
        type: 'danger',
        message: data && data.message ? data.message : 'internal server error'
      };
    });
  };
})
.controller('RegisterCtrl', function(userService, messageService, pageInfo, $scope, $i18next) {
  pageInfo.title = $i18next("title.register");

  $scope.registered = false;

  $scope.alert = {};
  $scope.user = {
	lng: $i18next.options.lng
  };
  
  $scope.register = function() {
    $scope.alert = {
      type: 'info',
      message: $i18next('messages:code.10002')
    };
    userService.createUser($scope.user)
    .success(function(data) {
      $scope.email = data.email;
      $scope.registered = true;
    })
    .error(function(data) {
      messageService.error($scope.alert, data);
    });
  };
})
.controller('RegisterConfirmationCtrl', function(userService, messageService, userModel, pageInfo, $scope, $stateParams, $state, $i18next) {
  pageInfo.title = $i18next("title.register_validate");

  var token = $stateParams.token;

  $scope.alert = {
    type: 'info',
    message: 'register.alert.validation_ongoing'
  };

  userService.validateUser(token)
  .success(function(data) {
    if (data.user) userModel.user = userService.populateUser(data.user);
    userModel.first_time = true;
    $state.go('user');
  })
  .error(function(data) {
    messageService.error($scope.alert, data);
  });
});
