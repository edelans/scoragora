angular.module('scoragora.controllers', [
  'scoragora.controllers.login',
  'scoragora.controllers.league',
  'scoragora.controllers.prediction',
  'scoragora.controllers.user'
])
.controller('Ctrl', function(pageInfo, $scope) {
  $scope.page = pageInfo;
})

.controller('RuleCtrl', function(pageInfo, $scope, $i18next) {
  pageInfo.title = $i18next("title.rules");
})

.controller('FaqCtrl', function(pageInfo, $scope, $i18next) {
  pageInfo.title = $i18next("title.faq");

  $scope.isCreateAccountCollapsed = true;
  $scope.isCreateLeagueCollapsed = true;
  $scope.isCreateAccountCollapsed = true;
  $scope.isHowToInviteCollapsed = true;
  $scope.toto = true;
  $scope.isHowToCompleteForecastCollapsed = true;
  $scope.isCrowdscoringCollapsed = true;
  $scope.isMultipleLeagueCollapsed = true;
  $scope.isLateAccountCreationCollapsed = true;
  $scope.isLateLeagueJoinCollapsed = true;
  $scope.isCompleteAllForecastCollapsed = true;
  $scope.isSocialLoginCollapsed = true;
  $scope.isNoSPAMCollapsed = true;
  $scope.isPrivacyCollapsed = true;
  $scope.isDeleteAccountCollapsed = true;
  $scope.isPointsCalculationCollapsed = true;
  $scope.isPointsEvolutionCollapsed = true;
  $scope.isOthersForecastsCollapsed = true;
  $scope.isAvatarChangeCollapsed = true;
  $scope.isPersonalProfilPicCollapsed = true;
  $scope.isChangeLanguageCollapsed = true;
  $scope.isChangePasswordCollapsed = true;
  $scope.isRevenueModelCollapsed = true;
})

.controller('HomeCtrl', function($auth, userInfo, userModel, userService, pageInfo, $scope, $state, $i18next, amMoment) {
  pageInfo.title = $i18next('title.index');

  $scope.authenticate = function(provider) {
    $auth.authenticate(provider)
    .then(function() {
      getUser();
    })
    .catch(function(response) {
      $state.go('login');
    });
  };

  function getUser() {
    var userId = userInfo.getUserId();
    userService.getUser(userId)
    .success(function(data) {
      userModel.user = userService.populateUser(data.user);
      if (data.user) {
        userInfo.displayName = data.user.displayName;
        userInfo.pictureURL = data.user.pictureURL;
      }
      // set language
      if (data.user.lng) {
        $i18next.options.lng = data.user.lng;
        amMoment.changeLocale(data.user.lng);
      }
    });
  }
})


.controller('LayoutCtrl', function($auth, userInfo, userModel, userService, loginService, $scope, $state, $i18next, amMoment) {

  $scope.userInfo = userInfo;
  var user_id = userInfo.getUserId();
  if (!user_id) {
    setLocaleFromBrowser();
    $scope.ready = true;
  }
  else {
    getUser(user_id);
  }

  function getUser(userId) {
    userService.getUser(userId)
    .success(function(data) {
      $scope.user = userModel.user = userService.populateUser(data.user);
      if (data.user) {
        userInfo.displayName = data.user.displayName;
        userInfo.pictureURL = data.user.pictureURL;
      }
      // set language
      if ($scope.user.lng) {
        $i18next.options.lng = $scope.user.lng;
        amMoment.changeLocale($scope.user.lng);
      }
      else {
        setLocaleFromBrowser();
      }
      $scope.ready = true;
    });
  }

  function setLocaleFromBrowser() {
    if (navigator && navigator.language) $i18next.options.lng = navigator.language;
    if (navigator && navigator.userLanguage) $i18next.options.lng = navigator.userLanguage;
  }

  $scope.logout = function() {
    delete userInfo.userId;
    delete userInfo.displayName;
    delete userInfo.pictureURL;
    $auth.logout('/login');
  };
});
