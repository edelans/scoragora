angular.module('scoragora.controllers.user.leagues', [])
.controller('UserLeaguesCtrl', function (userInfo, userService, leagueService, messageService, userModel, pageInfo, $scope, $uibModal, $i18next){
  pageInfo.title = $i18next("title.user.leagues");
  $scope.options = {
  mode: "current"
  };

  var user_id = userInfo.getUserId();
  if (userModel.user && userModel.user.user_leagues && userModel.user._id === user_id) {
    $scope.leagues = userModel.user.user_leagues;
    buildCurrentAndPastLeagues();
  }
  else {
    userService.getUserLeagues(user_id, {order:'activity'})
    .success(function(data) {
      $scope.leagues = data.leagues;
      if (userModel.user) userModel.user.user_leagues = data.leagues;
      buildCurrentAndPastLeagues();
    });
  }

  $scope.closeAlert = function() {
  delete $scope.alert;
  };

  $scope.askLeaveConfirmation = function (league) {
    var modalInstance = $uibModal.open({
      templateUrl: 'myModalContent.html',
      controller: 'LeaveLeagueCtrl',
      resolve: {
        league: function(){
          return league;
        }
      }
    });

    modalInstance.result.then(function (data) {
      $scope.alert = {};
      messageService.alert($scope.alert, data.data);

      // delete userModel.user.user_leagues so that it will be reloaded without the left league
      delete userModel.user.user_leagues;
      userService.getUserLeagues(user_id, {order:'activity'})
      .success(function(data) {
        $scope.leagues = userModel.user.user_leagues = data.leagues;
        buildCurrentAndPastLeagues();
      });
    });
  };

  $scope.$watch(function() {
  return $scope.options ? $scope.options.mode : null;
  }, function(value) {
  $scope.list = value == 'current' ? $scope.currentLeagues : $scope.pastLeagues;
  });

  function buildCurrentAndPastLeagues() {
  $scope.currentLeagues = [];
  $scope.pastLeagues = [];
  if ($scope.leagues && $scope.leagues.length > 0) {
    $scope.leagues.forEach(function(league) {
    if (league.competition && league.competition.status == "public") {
      $scope.currentLeagues.push(league);
    }
    else {
      $scope.pastLeagues.push(league);
    }
    });
  }
  if ($scope.pastLeagues.length === 0) $scope.options.mode = 'current';
  $scope.list = $scope.options && $scope.options.mode == 'current' ? $scope.currentLeagues : $scope.pastLeagues;
  }
})
.controller('LeaveLeagueCtrl', function ($scope, userModel, $uibModalInstance, leagueService, league) {

  $scope.leagueToLeave=league;

  $scope.leave = function() {
    leagueService.removeUserFromLeague(league._id)
    .success(function(data) {
      $uibModalInstance.close({type:'success', data: data});
    }).error(function(data) {
      $uibModalInstance.close({type: 'error', data:data});
    });
  };

  $scope.cancel = function () {
    $uibModalInstance.dismiss('cancel');
  };
});
