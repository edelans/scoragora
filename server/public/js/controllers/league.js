angular.module('scoragora.controllers.league', [
	'scoragora.controllers.league.manage',
	'scoragora.controllers.league.stats'
])
.controller('LeagueCtrl', function(leagueModel, leagueService, leagueTool, userModel, userService, competitionService, userInfo, socketIO, pageInfo, $scope, $stateParams, $uibModal, $i18next) {
  var message_buckets = 3;

  $scope.user_id = userInfo.userId;
  $scope.oldestMessagedDate = moment().valueOf();
  $scope.areAllMatchesLoaded = false;
  $scope.message = {};
  $scope.messages = [];

  function loadCompetitionMatches(options) {
    competitionService.getCompetitionMatches($scope.league.competition, options)
    .success(function(data) {
      if (data.matches) {
        $scope.matches = leagueModel.leagueCompetitionMatches = data.matches;
      }
    });
  }

  function loadMessages() {
    leagueService.getLeagueMessages($scope.league._id, {limit: message_buckets, timestamp: $scope.oldestMessagedDate})
    .success(function(data) {
      if (data.messages && data.messages.length > 0) {
        $scope.oldestMessagedDate = moment(data.messages[0].datetime).valueOf();
        $scope.messages = leagueModel.leagueMessages = data.messages.concat($scope.messages);
        buildMessages();
      }
      else {
        $scope.areAllMessagesLoaded = leagueModel.areAllLeagueMessagesLoaded = true;
      }
    });
  }

  function buildMessages() {
    $scope.messageDays = _.groupBy($scope.messages, function(item) {
      return moment(item.datetime).startOf('day').unix();
    });
  }

  function loadForecast() {
    var user_id = userInfo.getUserId();
    function getForecastFromModel() {
      if (userModel.user && userModel.user.user_forecasts && userModel.user._id === user_id) {
        userModel.user.user_forecasts.forEach(function(forecast) {
          if ($scope.league.competition == forecast.competition._id) {
            $scope.forecast = forecast._id;
            leagueModel.forecast = $scope.forecast;
          }
        });
      }
    }

    getForecastFromModel();
    if (!$scope.forecast) {
      userService.getUserForecasts(user_id, {})
      .success(function(data) {
        if (!userModel.user) userModel.user = {};
        userModel.user.user_forecasts = data.forecasts;
        getForecastFromModel();
      });
    }
  }

  // create a rank_label for each member so that the rank is displayed nicely
  function rankLeague() {
    if ($scope.league && $scope.league.members) {
      var points = -1;
      $scope.league.members.forEach(function(member) {
        if (member.points == points) {
          member.rank_label = ' - ';
        }
        else {
          member.rank_label = '#' + member.rank;
          points = member.points;
        }
      });
    }
  }

  // filter members
  function filterLeagueMembers() {
  if ($scope.league && $scope.league.premium && $scope.league.premium.validated) {
    var members = [];
    $scope.league.members.forEach(function(item) {
    if (!item.status || item.status == "accepted") members.push(item);
    });
    $scope.league.members = members;
  }
  }

  // populate captain flag
  function populateCaptainFlag() {
    $scope.league.captains.forEach(function(captain) {
     if (captain == $scope.user_id) $scope.isCaptain = true;
    });
  }

  function populatePremiumFlag() {
    $scope.isPremium = $scope.league.premium && $scope.league.premium.validated;
  }

  // Retrieve the league
  leagueTool.getLeague($stateParams.name, function(err, league) {
    if (err) {
      if (err.code == 40012) {
        // empty league list in the model
        if (userModel.user) delete userModel.user.user_leagues;
        $scope.leagueNotFound = true;
      }
      else if (err.code == 40013) {
        // empty league list in the model
        if (userModel.user) delete userModel.user.user_leagues;
        $scope.notMember = true;
      }
      return; // TODO log it the server
    }
    $scope.league = league;
    pageInfo.title = $i18next("title.league.index", {league_name: $scope.league.name});
    populatePremiumFlag();

    // In case of premium league, filter out unaccepted members and determine if captain
    if ($scope.isPremium) {
      populateCaptainFlag();
      filterLeagueMembers();
    }
    rankLeague();

    leagueTool.getLeagueCompetition($scope.league.competition, function(err, competition) {
      if (err) return; // TODO sthg
      $scope.competition = competition;
    });

    loadCompetitionMatches({limit: 5, status: 'past'});

    /*leagueTool.getLeagueCompetitionTeamsAsDico($scope.league.competition, function(err, teams) {
    if (err) return; // TODO sthg
    $scope.dico_teams = teams;
  });*/

  loadForecast();

  leagueTool.getLeagueMembersAsDico($scope.league._id, function(err, members) {
    if (err) return; // TODO sthg
    $scope.dico_members = members;
  });

  if (leagueModel.leagueMessages) {
    $scope.messages = leagueModel.leagueMessages;
    $scope.oldestMessagedDate = moment($scope.messages[0].datetime).valueOf();
    $scope.areAllMessagesLoaded = leagueModel.areAllLeagueMessagesLoaded;
    buildMessages();
  }
  else {
    loadMessages();
  }
});

$scope.getMoreMatches = function() {
  loadCompetitionMatches({status: 'past'});
  $scope.areAllMatchesLoaded = true;
};

$scope.loadPreviousMessages = function() {
  loadMessages();
};

$scope.sendMessage = function() {
  socketIO.emit('send_message', {
    sender: $scope.user_id,
    content: $scope.message.content,
    datetime: new Date(),
    league: $scope.league._id
  });
  $scope.message.content = '';
};

// SOCKET
socketIO.on('disconnect', function(data) {
  $scope.alert = {
    type: 'info',
    message: "Chat connection lost"
  };
});

socketIO.emit('join_league', {league: $stateParams.name});

socketIO.on('receive_message', function(data) {
  if (data.league == $scope.league._id) {
    $scope.messages.push(data);
    buildMessages();
  }
});

$scope.openRulesModal = function() {
  var modalInstance = $uibModal.open({
    templateUrl: 'views/league_rules.html',
    controller: 'LeagueRulesCtrl',
    resolve: {
      rules: function() {
        return $scope.competition.rules;
      },
      config: function() {
        return $scope.competition.config;
      }
    }
  });
};

$scope.downloadRanking = function() {
  var modalInstance = $uibModal.open({
    templateUrl: 'views/league/download_ranking.html',
    controller: 'LeagueDownloadCtrl',
    resolve: {
      league: function() {
        return $scope.league;
      }
    }
  });
};

$scope.doCheckout = function(token) {
  leagueService.upgradeLeague($scope.league._id, token)
  .success(function(data){
    // make the league appear premium
    $scope.league.premium = {validated: true};
    populatePremiumFlag();
    // display payment success message
    // data = {status: 200, message: "league status successfully updated"}
    $scope.alert = {
      type: 'success',
      message: $i18next('premium.payment_success')
    };
  })
  .error(function(err){
    // display error message
    // err = {status: 500, message: "internal server error"}
    $scope.alert = {
      type: 'danger',
      message: err.message
    };
  });
};

$scope.removeAlert = function() {
  delete $scope.alert;
};

})
.controller('LeagueDownloadCtrl', function($scope, leagueService, messageService, $i18next, $timeout, $uibModalInstance, league) {
  $scope.predownload = true;

  $scope.download = function() {
    $scope.predownload = false;
    $scope.alert = {
      type: "info",
      message: $i18next('league.download.generating')
    };
    leagueService.downloadRanking(league._id)
    .success(function(data) {
      $scope.alert = {
        type: "info",
        message: $i18next('league.download.downloading')
      };
      var filename = league.url + "_" + $i18next('league.download.ranking') + ".pdf";
      var blob = new Blob([data], {type: "application/octet-stream"});
      saveAs(blob, filename);

      // wait 2s before closing the modal
      $timeout(function() {
        $uibModalInstance.close();
      }, 2000);
    })
    .error(function(data) {
      // convert the buffer to string
      var str = String.fromCharCode.apply(null, new Uint8Array(data));
      var obj;
      try {
        obj = JSON.parse(str);
      }
      catch(e) {
        console.log("Unable to parse the server reply");
        obj = {code: 50000};
      }
      messageService.error($scope.alert, obj);
    });
  };

  $scope.cancel = function() {
    $uibModalInstance.dismiss();
  };
})
.controller('LeagueRulesCtrl', function($scope, $uibModalInstance, rules, config) {
  $scope.criterias = [];

  // transform rules
  if (rules.group && rules.group.rule) {
    var total = 0;
    rules.group.rule.forEach(function(item){
      if (item.name == 'team_score') {
        total += 2*item.points;
      }
      else {
        total += item.points;
      }
      if ($scope.criterias.indexOf(item.name) == -1) {
        $scope.criterias.push(item.name);
      }
    });
    $scope.group = rules.group;
    $scope.group.total = total;
  }
  if (rules.day && rules.day.rule) {
    var dayTotal = 0;
    rules.day.rule.forEach(function(item){
      if (item.name == 'team_score') {
        dayTotal += 2*item.points;
      }
      else {
        dayTotal += item.points;
      }
      if ($scope.criterias.indexOf(item.name) == -1) {
        $scope.criterias.push(item.name);
      }
    });
    $scope.day = rules.day;
    $scope.day.total = dayTotal;
  }
  if (rules.round) {
    var keys = Object.keys(rules.round);
    if (keys.indexOf('rule') != -1) {
      // Same rule for all the rounds
    }
    if (rules.round.rule) {
      console.log("Unique rule for all the rounds is not supported");
    }
    else {
      $scope.rounds = [];


      keys.forEach(function(key) {
        var rule = rules.round[key].rule;
        var total = 0;
        rule.forEach(function(item){
          if (item.name == 'team_score') {
            total += 2*item.points;
          }
          else {
            total += item.points;
          }
          if ($scope.criterias.indexOf(item.name) == -1) {
            $scope.criterias.push(item.name);
          }
        });
        $scope.rounds.push({
          round: key,
          rule: rule,
          total: total
        });
      });
      $scope.rounds.sort(function(a, b) {
        if (b.round == 3) return 1.5 - a.round;
        if (a.round == 3) return b.round - 1.5;
        return b.round - a.round;
      });
    }
  }

  $scope.rules = rules;
  $scope.config = config;

  $scope.cancel = function() {
    $uibModalInstance.dismiss();
  };
})
.controller('LeagueCreationCtrl', function (competitionService, leagueService, userModel, pageInfo, $scope, $state, $stateParams, $i18next) {
  pageInfo.title = $i18next("title.user.new_league");

  $scope.league = {};
  $scope.icon = '';
  $scope.isCreationDisabled = true;
  $scope.disablingLeagueName = true;
  $scope.disablingCompetitionId = true;
  $scope.defaultValue = {
    label: '-'
  };

  // populate dropdown with competitions
  competitionService.getAllPublicCompetitions({ status: 'public' })
  .success(function(data){
    $scope.competitions = [];
    var option = {};
    data.competitions.forEach(function(competition) {
      var label = $i18next('competition.' + competition.name + '.name_sport', {sport: competition.sport}) + ' ';
      if (competition.config && competition.config.type == 'championship') label += (competition.year-1) + '/';
      label += competition.year;
      if (competition.status == 'ready') return;
      option = {
        value: competition._id,
        label: label
      };
      $scope.competitions.push(option);
    });
  });

  $scope.$watch('league.competition_id', function(nv){
    if (!nv) return;
    // nv has the value of league.competition_id. You can do whatever you want and this code
    // would be called each time value of 'competition_id' change
    $scope.disablingCompetitionId=false;
    $scope.isCreationDisabled=$scope.disablingLeagueName || $scope.disablingCompetitionId;
  });

  $scope.checkLeagueName = function(league_name){

    // don't do unecessary check
    if (!league_name || league_name ==='') {
      $scope.icon='';
      $scope.disablingLeagueName=true;
      return;
    }

    // show spinner
    $scope.icon='repeat rotating';

    // send request to server
    leagueService.checkLeagueName(league_name)
    .success(function(data) {
      if (data.available) {
        $scope.icon='ok icon-success';
        $scope.disablingLeagueName=false;
        $scope.isCreationDisabled=$scope.disablingLeagueName || $scope.disablingCompetitionId;
      }else {
        $scope.icon='remove icon-danger';
        $scope.disablingLeagueName=true;
        $scope.isCreationDisabled=$scope.disablingLeagueName || $scope.disablingCompetitionId;
      }
    })
    .error(function(data) {
      //probable invalid league name
      $scope.icon='remove icon-danger';
      $scope.disablingLeagueName=true;
      $scope.isCreationDisabled=$scope.disablingLeagueName || $scope.disablingCompetitionId;
    });
  };


  $scope.submit = function() {
    leagueService.createLeague($scope.league.competition_id, $scope.league.name, $stateParams.sponsor)
    .success(function(data){
      // delete userModel.user.user_leagues so that it will be reloaded with the new league
      delete userModel.user.user_leagues;
      $state.go('league',{name: data.league.url});
    })
    .error(function(err){
      //display data.message
      $scope.alert = {
        type: 'danger',
        message: err.message
      };
    });
  };
})
.controller('LeagueInviteCtrl', function(leagueTool, userInfo, pageInfo, $scope, $stateParams, $i18next) {
  pageInfo.title = $i18next("title.league.invitation");

  $scope.user_id = userInfo.userId;

  // Retrieve the league
  leagueTool.getLeague($stateParams.name, function(err, league) {
    if (err) return; // TODO sthg
    $scope.league = league;
    $scope.link = "www.scoragora.com/#/join/" + $scope.league.token;
  });
})
.controller('LeagueJoinCtrl', function(leagueService, userService, messageService, userInfo, pageInfo, leagueModel, $scope, $stateParams, $state, $auth, $timeout, $i18next) {
  pageInfo.title = $i18next("title.league.join");

  var user_id = userInfo.userId;

  function populatePremiumFlag() {
  $scope.isPremium = $scope.league.premium && $scope.league.premium.validated;
  }

  function getUserLeagues(user_id, next) {
    userService.getUserLeagues(user_id, {})
    .success(function(data) {
      next(data.leagues);
    });
  }

  function isMember(leagues,league_id){
    if (!leagues) return false;
    for (i = leagues.length - 1; i >= 0; i--) {
      if (leagues[i]._id == league_id) {
        return true;
      }
    }
    return false;
  }

  leagueService.getLeaguesByToken($stateParams.token)
  .success(function(data) {
    if (!data.leagues || data.leagues.length === 0) {
      $scope.noLeagueFound = true;
      return;
    }
    $scope.league = data.leagues[0];
    populatePremiumFlag();
    // TODO remove this code
    if (!$scope.isPremium && $scope.league.members.length > 50) {
      $scope.isMaxSizeExceeded = true;
      return;
    }

    getUserLeagues(user_id, function(leagues){
      $scope.isAlreadyMember = isMember(leagues, $scope.league._id);
      $scope.joinEnabled = !$scope.noLeagueFound && !$scope.isAlreadyMember && !$scope.isMaxSizeExceeded;
    });
  });

  $scope.switchAccount = function() {
    // set the post login redirection
    $auth.loginRedirect = {state: $state.current, params: $state.params};
    delete userInfo.userId;
    delete userInfo.displayName;
    delete userInfo.pictureURL;
    $auth.logout('/login');
  };

  $scope.joinLeague = function() {
    $scope.alert = {
      type : 'info',
      message: 'Sending request ...'
    };
    leagueService.addUserToLeague($scope.league._id)
    .success(function(data) {
      $scope.alert = {
        type : 'success',
        message: $i18next('league.join.success_message', {league_name: $scope.league.name})
      };
      delete leagueModel.league;
      $timeout(function() {
        $state.go('league', { name: $scope.league.url });
      }, 3000);
    }).error(function(data) {
      if (data) data.league_name = $scope.league.name;
      messageService.error($scope.alert, data);
    });
  };
})
.controller('LeagueMatchCtrl', function(leagueModel, leagueService, leagueTool, matchService, pageInfo, $scope, $stateParams, $i18next) {
  pageInfo.title = $i18next("title.league.match");

  // Retrieve the league
  leagueTool.getLeague($stateParams.name, function(err, league) {
    if (err) return; // TODO sthg
    $scope.league = league;

    leagueTool.getLeagueMembersAsDico($scope.league._id, function(err, members) {
      if (err) return; // TODO sthg
      $scope.dico_members = members;
    });

    matchService.getMatch($stateParams.matchId)
    .success(function(data) {
      $scope.match = data.match;
      $scope.formattedDate = moment(data.match.datetime).valueOf();
    });

    // get forecasts
    leagueService.getLeagueMatchForectasts($scope.league._id, $stateParams.matchId)
    .success(function(data) {
      var dico_forecasts = {};
      data.forecasts.forEach(function(forecast) {
        dico_forecasts[forecast.user] = forecast;
      });
      $scope.dico_forecasts = dico_forecasts;
    });
  });
});
