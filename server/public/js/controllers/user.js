angular.module('scoragora.controllers.user', [
  'scoragora.controllers.user.leagues'
])
.controller('UserCtrl', function(userInfo, userService, userModel, pageInfo, $scope, $i18next) {

  function getUserLeagues(user_id) {
    if (userModel.user && userModel.user.userTopLeagues && userModel.user._id === user_id) {
      $scope.leagues = userModel.user.userTopLeagues;
    }
    else {
      userService.getUserLeagues(user_id, {limit: 5, order: 'activity'})
      .success(function(data) {
        $scope.leagues = data.leagues;
        if (userModel.user) userModel.user.userTopLeagues = data.leagues;
      });
    }
  }

  function getUserMatches(user_id) {
    userService.getUserMatches(user_id, {limit:5, status:'past'})
    .success(function(data) {
      $scope.lastResults = data.matches;
    });
  }

  function getUserMatchforecasts(user_id) {
    userService.getUserMatchforecasts(user_id, {limit:5})
    .success(function(data) {
      $scope.nextForecasts = data.matches;
    });
  }

  var user_id = userInfo.getUserId();
  if (user_id) {
    pageInfo.title = $i18next("title.user.index", {pseudo: userInfo.displayName});
    getUserLeagues(user_id);
    getUserMatches(user_id);
    getUserMatchforecasts(user_id);
  }
  if (userModel.user && userModel.first_time) {
    $scope.alert = {
      type: 'success',
      message: $i18next('common.welcome', {pseudo: userModel.user.displayName})
    };
  }

})
.controller('UserForecastsCtrl', function(userInfo, userService, userModel, pageInfo, $scope, $i18next) {
  pageInfo.title = $i18next("title.forecast.index");
  var user_id = userInfo.getUserId();
  if (userModel.user && userModel.user.user_forecasts && userModel.user._id === user_id) {
    $scope.forecasts = userModel.user.user_forecasts;
  }
  else {
    userService.getUserForecasts(user_id, {})
    .success(function(data) {
      $scope.forecasts = userModel.user.user_forecasts = data.forecasts;
    });
  }
})
.controller('UserSettingsCtrl', function ($scope, pageInfo, userInfo, userModel, userService, $uibModal, $i18next, amMoment) {
  pageInfo.title = $i18next("title.user.settings");

  $scope.lngs=[{
    value:'en',
    label:'English (EN)'
  },{
    value:'fr',
    label:'Français (FR)'
  }];

  if (userModel.user) {
    $scope.user = userModel.user;
    $scope.defaultValue = {
      label: $scope.user.lng || '-'
    };
  }

  // for alert closing:
  $scope.closeAlert = function(index) {
    delete $scope.alert;
  };

  $scope.updateUserLng = function(){
    if ($scope.user) {
      userService.updateUser($scope.user._id, {lng:$scope.user.lng})
      .success(function(data) {
        $i18next.options.lng = $scope.user.lng;
        amMoment.changeLocale($scope.user.lng);
        $scope.alert = {
          type: 'success',
          message: $i18next('messages:code.' + data.code)
        };
      }).error(function(data) {
        messageService.error($scope.alert, data);
      });
    }
  };

  $scope.$watch('user.lng', function(newValue, oldValue){
    if(newValue === oldValue){
      return;
    }
    $scope.updateUserLng();
  });

  $scope.updateEmailSettings = function(){
    console.log($scope.user.email_settings);
    userService.updateUser(user_id, {emailSettings:$scope.user.email_settings})
    .success(function(data) {
      $scope.alert = {
        type: 'success',
        message: "Your modifications have been taken into account"
      };
      // delete userModel.user.emailSettings so that it will be reloaded
      delete userModel.user.emailSettings;
      userService.getUser(user_id)
      .success(function(data) {
        userModel.user = data.user;
      });
    }).error(function(data) {
      $scope.alert = {
        type: 'danger',
        message: "There was an error processing your change"
      };
    });
  };

  // Change Picture modal
  $scope.changePicture = function () {
    var changePhotoModalInstance = $uibModal.open({
      templateUrl: 'changePhotoModalContent.html',
      controller: 'ChangePhotoModalInstanceCtrl',
      resolve: {
        user: function() {
          return $scope.user;
        }
      }
    });

    changePhotoModalInstance.result.then(function(data) {
      if (data.alert) {
        $scope.alert = data.alert;
      }
      if (data.pictureMode) {
        $scope.user.pictureMode = data.pictureMode;
      }
      if (data.pictureURL) {
        $scope.user.pictureURL = userInfo.pictureURL = data.pictureURL;
        if (data.pictureMode == 'local' && $scope.user.login && $scope.user.login.local) {
          $scope.user.login.local.pictureURL = data.pictureURL;
        }
      }
      // save user in userModel
      userModel.user = $scope.user;
    });
  };


  // Change Pseudo modal
  $scope.changePseudo = function () {
    var changePseudoModalInstance = $uibModal.open({
      templateUrl: 'changePseudoModalContent.html',
      controller: 'ChangePseudoModalInstanceCtrl'
    });

    changePseudoModalInstance.result.then(function (data) {
      $scope.user.login.local.pseudo = $scope.user.displayName = userModel.user.displayName = userModel.user.login.local.pseudo = data.newPseudo;
      console.log($scope.user);
      $scope.alert = {
        type : data.type,
        message: data.data.message
      };
    }, function () {
      //todo: a quoi sert cette fonction ?
    });
  };


  // Account deletion modal
  $scope.askDeleteConfirmation = function () {
    var deleteModalInstance = $uibModal.open({
      templateUrl: 'DeleteModalContent.html',
      controller: 'DeleteAccountModalInstanceCtrl'
    });

    deleteModalInstance.result.then(function (data) {
      $scope.alert = {
        type : data.type,
        message: data.data.message
      };
    }, function () {
      //todo: a quoi sert cette fonction ?
    });
  };
})
.controller('ChangePseudoModalInstanceCtrl', function ($scope, $uibModalInstance, userModel, userService) {
  var user_id = userModel.user._id;

  $scope.updatePseudo = function (){
    //console.log('update pseudo in progress');
    userService.updateUser(user_id, {pseudo:$scope.pseudo})
    .success(function(data) {
      $uibModalInstance.close({
        type:'success',
        data:data,
        newPseudo:$scope.pseudo
      });
    }).error(function(data) {
      $uibModalInstance.close({
        type:'danger',
        data:data
      });
    });
  };

  $scope.cancel = function () {
    $uibModalInstance.dismiss('cancel');
  };
})
.controller('ChangePhotoModalInstanceCtrl', function ($scope, $uibModalInstance, userModel, userService, user) {
  $scope.pictureMode = user.pictureMode;
  $scope.login = user.login;
  if (user && user.login && user.login.local) {
    $scope.localPictureURL = user.login.local.pictureURL;
  }

  $scope.localURLs = [
    "/img/profile_picture/willy66.jpg",
    "/img/profile_picture/juanito70.jpg",
    "/img/profile_picture/argentina78.jpg",
    "/img/profile_picture/striker94.jpg",
    "/img/profile_picture/footix98.jpg",
    "/img/profile_picture/trix_flix08.jpg",
    "/img/profile_picture/zakumi10.jpg",
    "/img/profile_picture/fuleco14.jpg",
    "/img/profile_picture/victor16.jpg",
    "/img/profile_picture/poulpe.jpg"
  ];

  $scope.$watch('pictureMode', function(value) {
    if (value == "local" && user.login.local) {
      $scope.pictureURL = $scope.localPictureURL;
    }
    if (user && user.login) {
      if (value == "facebook" && user.login.facebook) {
        $scope.pictureURL = user.login.facebook.pictureURL;
      }
      if (value == "google" && user.login.google) {
        $scope.pictureURL = user.login.google.pictureURL;
      }
      if (value == "twitter" && user.login.twitter) {
        $scope.pictureURL = user.login.twitter.pictureURL;
      }
    }
  });

  $scope.chooseLocalPicture = function(url){
    $scope.localPictureURL = url;
    $scope.pictureURL = url;
  };

  $scope.save = function (){
    userService.updateUser(user._id, {
      pictureMode: $scope.pictureMode,
      pictureURL: $scope.pictureURL
    })
    .success(function(data) {
      $uibModalInstance.close({
        message: {
          type:'success',
          message:data
        },
        pictureMode: $scope.pictureMode,
        pictureURL: $scope.pictureURL
      });
    }).error(function(data) {
      $uibModalInstance.close({
        type:'danger',
        data:data
      });
    });
  };

  $scope.cancel = function () {
    $uibModalInstance.dismiss('cancel');
  };
})
.controller('DeleteAccountModalInstanceCtrl', function ($scope, $uibModalInstance, userModel) {
  $scope.cancel = function () {
    $uibModalInstance.dismiss('cancel');
  };
});
