angular.module('scoragora.filters', [])
.filter('filterGroup', function() {
  return function(matches, group_name) {
    result = [];
    matches.forEach(function(match) {
      if (match.group == group_name) {
        result.push(match);
      }
    });
    return result;
  };
})
.filter('filterDay', function() {
  return function(matches, day) {
    result = [];
    matches.forEach(function(match) {
      if (match.day == day) {
        result.push(match);
      }
    });
    result.sort(function(a, b) {
	    return moment(a.datetime).isBefore(moment(b.datetime)) ? 1 : -1;
    });
    return result;
  };
})
.filter('score', function() {
  return function(score, sport) {
    if (sport == 'football' || sport == 'rugby') {
      if (!score || !score.hometeam || !score.awayteam) return ' - ';
      return (score.hometeam.score !== null ?  score.hometeam.score : '' ) + ' - ' + (score.awayteam.score !== null ?  score.awayteam.score : '' );
    }
      return score;
  };
})
.filter("amUnixFormat", function() {
  return function(unix, format) {
    return moment.unix(unix).format(format);
  };
});
