angular.module('scoragora.directives', [
  "scoragora.directives.score",
  "scoragora.directives.charts"
])
.directive('scDropdownSelect', function() {
  return {
    restrict: 'E',
    transclude: true,
    scope: {
      options: "=",
      defaultValue: "=",
      ngModel: "="
    },
    require: 'ngModel',
    link: function($scope, $selement, $attrs) {
      $scope.$watch('options', function() {
        $scope.selects = [];

        if (!$scope.options || typeof $scope.options !== "object") return;
        $scope.options.forEach(function(option) {
          if (typeof option != 'object') {
            option = {
              value: option,
              label: option
            };
          }
          $scope.selects.push(option);
        });

        if ($scope.options.length == 1) {
          $scope.selected_option = $scope.options[0];
          $scope.ngModel = $scope.selected_option.value;
        }
      });

      $scope.open = false;
      $scope.toggle = function() {
        $scope.open = !$scope.open;
      };

      $scope.select = function(item) {
        $scope.selected_option = item;
        $scope.ngModel = $scope.selected_option.value;
        $scope.open = false;
      };

      if ($scope.ngModel === null) {
        $scope.selected_option = {
          label: ' - '
        };
      }
      else if (typeof $scope.ngModel != 'object') {
        $scope.selected_option = {
          value: $scope.ngModel,
          label: $scope.ngModel
        };
      }

      if ($scope.defaultValue) {
        if (typeof $scope.defaultValue != 'object') {
          $scope.selected_option = {
            value: $scope.defaultValue,
            label: $scope.defaultValue
          };
        }
        else {
          $scope.selected_option = $scope.defaultValue;
        }
      }

      $scope.btnClass = $attrs.btnClass;
    },
    template: '<div class="btn-group" ng-class="{\'open\': open}">'
    + '<button type="button" class="btn btn-default dropdown-toggle" ng-class="btnClass" ng-click="toggle()">'
    + '<span>{{selected_option.label}}</span>'
    + '<span>&nbsp;</span>'
    + '<span class="caret"></span>'
    + '</button>'
    + '<ul class="dropdown-menu" role="menu">'
    + '<li ng-class="{\'disabled\': item.value == selected_option.value}" ng-repeat="item in selects">'
    + '<a href ng-click="select(item)" href="#" ng-class="{\'disabled\': item.value == selected_option.value}">{{item.label | i18next}}</a>'
    + '</li>'
    + '</ul>'
    + '</div>'
  };
})
.directive("scAffix", function($window, $document) {
  return {
    restrict: "A",
    controller: function($scope, $element, $attrs) {
      var offsetTop = parseInt($attrs.offsetTop);
      if (isNaN(offsetTop)) {offsetTop = 0;}
      angular.element($window).bind("scroll", function() {
        if (offsetTop && this.pageYOffset >= offsetTop) {
          $element.attr("style", "position: fixed; top: " + offsetTop + "px;");
        } else {
          $element.attr("style", "");
        }
        $scope.$apply();
      });
    }
  };
})
.directive("scCopyButton", function($i18next, $timeout) {
  return {
    restrict: "E",
    transclude: true,
    scope: {
      scCopyText: "="
    },
    link: function($scope, $element) {
      $scope.ngClass = $element.attr("class");
      $element.attr("class", "");
      $scope.isOpen = false;

      $scope.success = function(e) {
        $scope.message = $i18next('common.copied');
        $timeout(function() {
          $scope.isOpen = true;
        }, 1);
      };

      $scope.error = function(e) {
        $scope.message = $i18next('common.copy_not_supported');
        $timeout(function() {
          $scope.isOpen = true;
        }, 1);
      };

      $scope.leave = function() {
        $scope.isOpen = false;
      };
    },
    template: '<button ng-class="ngClass" ngclipboard ngclipboard-success="success(e)" ngclipboard-error="error(e)" data-clipboard-text="{{scCopyText}}" ng-mouseleave="leave()" tooltip-placement="bottom" tooltip-is-open="isOpen" tooltip-trigger="none" uib-tooltip="{{message}}">'
      + '<ng-transclude>'
      + '</ng-transclude>'
      + '</button>'
  };
})
.directive('ngEnter', function() {
  return function(scope, element, attrs) {
    element.bind("keydown keypress", function(event) {
      if(event.which === 13) {
        scope.$apply(function(){
          scope.$eval(attrs.ngEnter, {'event': event});
        });
        event.preventDefault();
      }
    });
  };
})
.directive('smartSavedInput', function($timeout) {
  return {
	restrict: "E",
	transclude: true,
	scope: {
	  ngSubmit: "&",
	  ngModel: "="
	},
	link: function($scope, $element, $attrs) {
	  $scope.value = $scope.ngModel;
	  $scope.status = "idle";
	  
	  $scope.$watch(function() {
		return $scope.ngModel;
	  }, function() {
		if ($scope.ngModel != $scope.value) {
		  $scope.status = "unsaved";
		}
		else {
		  $scope.status = "saved";
		  $timeout(function() {
			$scope.status = "idle";
		  }, 3000);
		}
	  });
	  
	  $scope.$watch(function() {
		return $scope.value;
	  }, function() {
		if ($scope.ngModel == $scope.value) {
	      $scope.status = "idle";
	    }
	    else $scope.status = "unsaved";
	  });
	  
	  $scope.save = function() {
		if ($scope.value == $scope.ngModel) return;
		$scope.ngSubmit({value: $scope.value});
	  };
	  
	  $scope.placeholder = $attrs.placeholder;
	  $scope.inputClass = $attrs.inputClass;
	},
	template: '<div class="form-group has-feedback" ng-class="{\'has-warning\': status == \'unsaved\', \'has-success\' : status == \'saved\'}">'
			+ '<label class="control-label sr-only" for="savedInput">Hidden label</label>'
			+ '<input autocorrect="off" autocapitalize="off" autocomplete="off" spellcheck="false" id="savedInput" type="text" class="form-control" ng-class="inputClass" ng-model="value" placeholder="{{placeholder}}" ng-enter="save()" style="padding-right: 20px;">'
      		+ '<span class="form-control-feedback" ng-if="status == \'unsaved\'">&nbsp;*</span>'
      		+ '<span class="form-control-feedback" ng-if="status == \'saved\'"><i class="fa fa-check"></i></span>'
      		+ '<span id="savedInput" class="sr-only">(unsaved)</span>'
      		+ '</div>'
  };
})
.directive('scPagination', function() {
  return {
    restrict: 'E',
    transclude: true,
    scope: {
      page: "=",
      pages: "=",
      width: "=",
      /*update: "=",*/
    },
    link: function($scope) {
	    
	    if (!$scope.update) $scope.update = function() {};
	    
      var center_range = 2;
      var border_range = 2;
      // to be computed based on the given width
      var total_range = 2*center_range + 2*border_range + 3;
      var first_range;
      var second_range;
      $scope.indexes = [];

      var update_page = function(page) {
        $scope.page = page;
        if ($scope.pages > total_range) {
          $scope.indexes = [];
          if ($scope.page < center_range + border_range + 2) {
            for (i = 0; i < border_range + 2*center_range + 2; i++) {
              $scope.indexes.push({
                i : i
              });
            }
            $scope.indexes.push({
              i : '..',
              Nan: true
            });
            for (i = $scope.pages - border_range; i < $scope.pages; i++) {
              $scope.indexes.push({
                i : i
              });
            }
          } else if ($scope.page > $scope.pages - center_range - border_range - 2) {
            for (i = 0; i < border_range; i++) {
              $scope.indexes.push({
                i : i
              });
            }
            $scope.indexes.push({
              i : '..',
              Nan: true
            });
            for (i = $scope.pages - 2*center_range - border_range - 2; i < $scope.pages; i++) {
              $scope.indexes.push({
                i : i
              });
            }
          } else {
            for (i = 0; i < border_range; i++) {
              $scope.indexes.push({
                i : i
              });
            }
            $scope.indexes.push({
              i : '..',
              Nan: true
            });
            for (i = $scope.page - center_range; i < $scope.page + center_range + 1; i++) {
              $scope.indexes.push({
                i : i
              });
            }
            $scope.indexes.push({
              i : '..',
              Nan: true
            });
            for (i = $scope.pages - border_range; i < $scope.pages; i++) {
              $scope.indexes.push({
                i : i
              });
            }
          }
        }
        else {
          $scope.indexes = [];
          for (i = 0; i < $scope.pages; i++) {
            $scope.indexes.push({
              i : i
            });
          }
        }
      };

      var update_range = function() {
        if ($scope.width) {
          border_range = Math.min(Math.floor(($scope.width - 3) / 4), 3);
          center_range = Math.floor(($scope.width - 3 - 2*border_range)/2);
          total_range = 2*center_range + 2*border_range + 3;
        }
      };

      // go to the next page
      $scope.next = function(e) {
        e.preventDefault();
        if ($scope.page < $scope.pages - 1) {
          $scope.page++;
          $scope.update($scope.page);
          update_page($scope.page);
        }
      };

      // go to the next page
      $scope.previous = function(e) {
        e.preventDefault();
        if ($scope.page > 0) {
          $scope.page -= 1;
          $scope.update($scope.page);
          update_page($scope.page);
        }
      };

      // go to the next page
      $scope.to = function(page, e) {
        e.preventDefault();
        $scope.page = page;
        $scope.update($scope.page);
        update_page(page);
      };

      $scope.goTo = function(new_page) {
        $scope.page = new_page - 1;
        $scope.update($scope.page);
        update_page($scope.page);
      };

      // Bind pages
      $scope.$watch('pages', function(value) {
        update_page($scope.page);
      });

      // Bind page
      $scope.$watch('page', function(value) {
        update_page(value);
      });

      // Bind width
      $scope.$watch('width', function(value) {
        update_range();
        update_page($scope.page);
      });

      update_range();
      update_page($scope.page);
    },
    templateUrl: 'views/pagination.html'
  };
});
