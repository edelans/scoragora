angular.module("scoragora.directives.score", [])
.directive('scRugbyForecastInput', function() {
  return {
    restrict: 'E',
    transclude: true,
    scope: {
      ngModel: "=",
      open: "="
    },
    require: "ngModel",
    link: function($scope) {
      $scope.select = function(value) {
        $scope.ngModel = value;
      };
    },
    template: '<div class="rugby-forecast-input">'
    + '<button class="value3" ng-click="select(3)" ng-class="{\'selected\': ngModel == 3}" tooltip-placement="bottom" uib-tooltip="{{\'forecast.tooltip.home_win_huge\' | i18next}}">&nbsp;</button>'
    + '<button class="value2" ng-click="select(2)" ng-class="{\'selected\': ngModel == 2}" tooltip-placement="bottom" uib-tooltip="{{\'forecast.tooltip.home_win_medium\' | i18next}}">&nbsp;</button>'
    + '<button class="value1" ng-click="select(1)" ng-class="{\'selected\': ngModel == 1}" tooltip-placement="bottom" uib-tooltip="{{\'forecast.tooltip.home_win_short\' | i18next}}">&nbsp;</button>'
    + '<button class="value0" ng-click="select(0)" ng-class="{\'selected\': ngModel == 0}" tooltip-placement="bottom" uib-tooltip="{{\'forecast.tooltip.draw\' | i18next}}">&nbsp;</button>'
    + '<button class="value-1" ng-click="select(-1)" ng-class="{\'selected\': ngModel == -1}" tooltip-placement="bottom" uib-tooltip="{{\'forecast.tooltip.away_win_short\' | i18next}}">&nbsp;</button>'
    + '<button class="value-2" ng-click="select(-2)" ng-class="{\'selected\': ngModel == -2}" tooltip-placement="bottom" uib-tooltip="{{\'forecast.tooltip.away_win_medium\' | i18next}}">&nbsp;</button>'
    + '<button class="value-3" ng-click="select(-3)" ng-class="{\'selected\': ngModel == -3}" tooltip-placement="bottom" uib-tooltip="{{\'forecast.tooltip.away_win_huge\' | i18next}}">&nbsp;</button>'
    + '</div>'
  };
})
.directive('scRugbyForecast', function() {
  return {
    restrict: 'E',
    transclude: true,
    scope: {
      forecast: "=",
      result: "="
    },
    link: function($scope) {
      function buildData() {
        var slice;
        if ($scope.result && $scope.result.hometeam && $scope.result.awayteam) {
          var diff = $scope.result.hometeam.score - $scope.result.awayteam.score;
          if (diff < -20) slice = -3;
          else if (diff < -10) slice = -2;
          else if (diff < 0) slice = -1;
          else if (diff === 0) slice = 0;
          else if (diff > 20) slice = 3;
          else if (diff > 10) slice = 2;
          else if (diff > 0) slice = 1;
        }
        $scope.options = [];
        for (i = -3; i <= 3; i++) {
          if (slice == -i && $scope.forecast && $scope.forecast.difference_slice == -i) {
            $scope.options.push({
              full: true,
              divClass: "highlighted",
              tooltip: 'forecast.tooltip.perfect_forecast'
            });
          }
          else if (slice == -i) {
            $scope.options.push({
              full: true,
              divClass: "selected",
              tooltip: 'forecast.tooltip.correct_forecast'
            });
          }
          else if ($scope.forecast && $scope.forecast.difference_slice == -i) {
            $scope.options.push({
              full: true,
              tooltip: 'forecast.tooltip.forecast'
            });
          }
          else {
            $scope.options.push({
              full: false
            });
          }
        }
      }

      $scope.$watch("forecast", function(data) {
        buildData();
      });

      $scope.$watch("result", function(data) {
        buildData();
      });
    },
    template: '<div class="rugby-forecast">'
    + '<div ng-class="option.divClass" ng-repeat="option in options">'
    + '<i class="fa fa-circle" uib-tooltip="{{option.tooltip | i18next}}" tooltip-append-to-body="true" tooltip-placement="bottom" ng-if="option.full"></i>'
    + '<i class="fa fa-circle-o" ng-if="!option.full"></i>'
    + '</div>'
    + '</div>'
  };
});
