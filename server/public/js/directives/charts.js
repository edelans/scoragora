function drawMultiLineChart(selector, data, options) {
  var defaultTo = function(a, b) {
    if (!!a) return a;
    return b;
  };

  // data: [{points: [{value, columnId}], lineId, lineColorId}]
  var margin = {top: 20, right: 30, bottom: options.margin.bottom, left: 50};
  var width = options.width - margin.left - margin.right;
  var height = options.height - margin.top - margin.bottom;

  var x = d3.scale.ordinal()
  .domain(d3.set(data[0].points.map(function (d) { return d.columnId; })).values())
  .rangePoints([0, width], 0.5);

  var line = d3.svg.line()
  //.interpolate("basis")
  .x(function(d) { return x(d.columnId); })
  .y(function(d) { return y(d.value); });

  var y = d3.scale.linear()
  .range([height, 0]);

  var xAxis = d3.svg.axis()
  .scale(x)
  .orient("bottom")
  .tickFormat(function(d, i){
    if (options.noXTickLabel) return "";
    return d;
  });

  var yAxis = d3.svg.axis()
  .scale(y)
  .orient("left")
  .ticks(10);

  var svg = d3.select(selector)
  .append("g")
  .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

  var color = d3.scale.category10();
  color.domain(data.filter(function(item) { return item.lineId; }));

  y.domain([
    d3.min(data, function(col) { return d3.min(col.points, function(v) { return v.value; }); }),
    d3.max(data, function(col) { return d3.max(col.points, function(v) { return v.value; }); })
  ]);

  svg.append("g")
  .attr("class", "x axis")
  .attr("transform", "translate(0," + height + ")")
  .call(xAxis)
  .selectAll("text")
  .style("text-anchor", "end")
  .attr("dx", "-.5em")
  .attr("dy", ".3em")
  .attr("transform", function(d) {
    return "rotate(-65)";
  });

  var verticalAxis = svg.append("g")
  .attr("class", "y axis")
  .call(yAxis);
  /*.append("text")
  .attr("transform", "rotate(-90)")
  .attr("y", 6)
  .attr("dy", ".71em")
  .style("text-anchor", "end");
  .text(options.yTitle);*/

  var yAxisGrid = yAxis.ticks(10)
  .tickSize(width, 0)
  .tickFormat("")
  .orient("right");

  /*var xAxisGrid = xAxis.ticks(data.length)
  .tickSize(-height, 0)
  .tickFormat("")
  .orient("top");*/

  svg.append("g")
  .classed('y', true)
  .classed('grid', true)
  .call(yAxisGrid);

  verticalAxis
  .append("text")
  .attr("transform", "rotate(-90)")
  .attr("y", 6)
  .attr("dy", ".71em")
  .style("text-anchor", "end")
  .text(options.yTitle);

  var city = svg.selectAll(".city")
  .data(data)
  .enter().append("g")
  .attr("class", "account");

  city.append("path")
  .attr("class", function(d) { return "line line-value" + (d.hidden ?  " hidden" : ""); })
  .attr("d", function(d) { return line(d.points); })
  .attr("lineId", function(d) { return d.lineId; })
  .style("stroke", function(d, i) { return defaultTo(d.lineColor, color(d.lineId)); });


  city.selectAll(".dot")
  .data(function (d) { return d.points; })
  .enter().append("svg:circle")
  .attr("class", function(d) { return "point-value" + (this.parentNode.__data__.hidden ?  " hidden" : ""); })
  .attr("lineId", function(d, i) { return this.parentNode.__data__.lineId; })
  .attr("fill", function(d, i) { return this.parentNode.__data__.lineColor; })
  .attr("cx", function(d, i) { return x(d.columnId); })
  .attr("cy", function(d, i) { return y(d.value); })
  .attr("r", function(d, i) { return 2.5; });

}

angular.module("scoragora.directives.charts", [])
.directive("scMultiLine", function() {
  return {
    restrict: 'A',
    scope: {
      data: "=",
      options: "=?"
    },
    controller: function($scope, $element, $attrs) {
      var width = $element.parent().prop("offsetWidth") - 20;
      var height = width/2;

      // build options
      if (!$scope.options) $scope.options = {};
      $scope.options.width = $scope.options.width || width;
      $scope.options.height = $scope.options.height || height;
      if (!$scope.options.yTitle) $scope.options.yTitle = "Points";
      if (!$scope.options.margin) {
        $scope.options.margin = {
          bottom: 40
        };
      }

      $scope.id = Math.floor(Math.random() * 1000000001);
      $element.attr("id","multi-line-"+$scope.id);
      $element.attr("width", $scope.options.width);
      $element.attr("height", $scope.options.height);
    },
    link: function(scope, element, attrs){
      scope.$watch('data', function(data){
        if(data && data.length > 0){
          drawMultiLineChart("#multi-line-"+scope.id, scope.data, scope.options);
          return;
        }
      }, (attrs.objectequality === undefined ? false : (attrs.objectequality === 'true')));
    }
  };
});
