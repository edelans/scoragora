var app = angular.module('scoragora', [
  // libs
  'ngCookies',
  'ngSanitize',
  'ui.bootstrap',
  'ui.router',
  'angular-loading-bar',
  'jm.i18next',
  'satellizer',
  'monospaced.elastic',
  'btford.socket-io',
  'angularMoment',
  'luegg.directives',
  'ngclipboard',
  'angulartics',
  'angulartics.google.analytics',
  'stripe.checkout',
    // Controllers
  'scoragora.controllers',
  'scoragora.factories',
  'scoragora.services',
  'scoragora.directives',
  'scoragora.filters'
]);

angular.module('jm.i18next').config(['$i18nextProvider', function ($i18nextProvider) {
  $i18nextProvider.options = {
    lng: 'en',
    useCookie: false,
    useLocalStorage: true,
    cache: true,
    localStorageExpirationTime: 300000, // 5 min in ms, default 1 week
    fallbackLng: 'en',
    resGetPath: '../locales/__lng__/__ns__.json',
    ns: {
      namespaces: ['translation', 'teams', 'messages'],
      defaultNs: 'translation'
    },
    defaultLoadingValue: '' // ng-i18next option, *NOT* directly supported by i18next
  };
}]);

app.provider('$authProvider', function() {
  this.$get = function($auth) {
    return $auth;
  };
});

app.config(function($stateProvider, $urlRouterProvider, $httpProvider, $provide, cfpLoadingBarProvider, $authProvider) {

  //ngClipProvider.setPath("js/ZeroClipboard.swf");

  var routing = document.getElementById("routing");

  var googleClientId = routing.getAttribute('google-client');
  var facebookClientId = routing.getAttribute('facebook-client');
  var apiRoot = routing.getAttribute('api') + "/api";
  $provide.value('apiRoot', apiRoot);

  cfpLoadingBarProvider.includeSpinner = false;
  cfpLoadingBarProvider.latencyThreshold = 300;

  $urlRouterProvider.otherwise('/');

  // Satellizer config
  $authProvider.loginRedirect = '/user';
  $authProvider.loginUrl = apiRoot + '/login/local';
  $authProvider.signupUrl = apiRoot + '/login/local';
  $authProvider.loginRoute = '/login';

  $authProvider.facebook({
    url: apiRoot + '/login/facebook',
    clientId: facebookClientId
  });

  $authProvider.google({
    url: apiRoot + '/login/google',
    clientId: googleClientId
  });

  $authProvider.twitter({
    url: apiRoot + '/login/twitter'
  });

  $httpProvider.defaults.withCredentials = true;

  $stateProvider
  .state('layout', {
    abstract: true,
    templateUrl: 'views/layout.html',
    controller: 'LayoutCtrl'
  })
  .state('home', {
    url: '/',
    templateUrl: '/views/home.html',
    controller: 'HomeCtrl',
    parent: 'layout'
  })
  .state('login', {
    url: '/login',
    templateUrl: '/views/login.html',
    controller: 'LoginCtrl',
    parent: 'layout'
  })
  .state('login.forgotten', {
    url: '/login/forgotten',
    templateUrl: '/views/login_forgotten.html',
    controller: 'LoginForgottenCtrl',
    parent: 'layout'
  })
  .state('login.reset', {
    url: '/passwordreset',
    templateUrl: '/views/login_reset.html',
    controller: 'LoginResetCtrl',
    parent: 'layout'
  })
  .state('register', {
    url: '/registration',
    templateUrl: '/views/register.html',
    controller: 'RegisterCtrl',
    parent: 'layout'
  })
  .state('register.confirm', {
    url: '/registration/confirm/:token',
    templateUrl: '/views/register_confirmation.html',
    controller: 'RegisterConfirmationCtrl',
    parent: 'layout'
  })
  .state('faq', {
    url: '/faq',
    templateUrl: '/views/faq.html',
    parent: 'layout',
    controller: 'FaqCtrl'
  })
  .state('about', {
    url: '/about',
    templateUrl: '/views/about.html',
    parent: 'layout'
  })
  .state('rules', {
    url: '/rules',
    templateUrl: '/views/rules.html',
    parent: 'layout',
    controller: 'RuleCtrl'
  })
  .state('user', {
    url: '/user',
    templateUrl: '/views/user.html',
    controller: 'UserCtrl',
    parent: 'layout'
  })
  .state('userLeagues', {
    url: '/user/leagues',
    templateUrl: '/views/leagues.html',
    controller: 'UserLeaguesCtrl',
    parent: 'layout'
  })
  .state('leagueCreation', {
    url: '/newLeague',
    templateUrl: '/views/leagueCreation.html',
    controller: 'LeagueCreationCtrl',
    parent: 'layout'
  })
  .state('leagueCreationWithSponsor', {
    url: '/newLeague/:sponsor',
    templateUrl: '/views/leagueCreation.html',
    controller: 'LeagueCreationCtrl',
    parent: 'layout'
  })
  .state('league', {
    url: '/league/:name',
    templateUrl: '/views/league/index.html',
    controller: 'LeagueCtrl',
    parent: 'layout'
  })
  .state('league.match', {
    url: '/league/:name/match/:matchId',
    templateUrl: '/views/league/match.html',
    controller: 'LeagueMatchCtrl',
    parent: 'layout'
  })
  .state('league.invite', {
    url: '/league/:name/invite',
    templateUrl: '/views/league/invite.html',
    controller: 'LeagueInviteCtrl',
    parent: 'layout'
  })
  .state('league.manage', {
    url: '/league/:name/manage',
    templateUrl: '/views/league/manage.html',
    controller: 'LeagueManageCtrl',
    parent: 'layout'
  })
  .state('league.join', {
    url: '/join/:token',
    templateUrl: '/views/join.html',
    controller: 'LeagueJoinCtrl',
    parent: 'layout'
  })
  .state('league.stats', {
    url: '/league/:name/stats',
    templateUrl: '/views/league/stats.html',
    controller: 'LeagueStatsCtrl',
    parent: 'layout'
  })
  .state('forecasts', {
    url: '/forecasts',
    templateUrl: '/views/competitions.html',
    controller: 'UserForecastsCtrl',
    parent: 'layout'
  })
  .state('forecast', {
    url: '/forecasts/:id',
    templateUrl: '/views/forecast.html',
    controller: 'ForecastCtrl',
    parent: 'layout'
  })
  .state('settings', {
    url: '/settings',
    templateUrl: '/views/settings.html',
    controller: 'UserSettingsCtrl',
    parent: 'layout'
  });

})
.run(['$rootScope', '$state', '$auth', function($rootScope, $state, $auth) {
  $rootScope._ = window._;
  $rootScope.Utils = {
    keys : Object.keys
  };

  // enumerate routes that don't need authentication
  var routesThatDontRequireAuth = ['/login', '/registration', '/registration/confirm/:token', '/passwordreset', '/faq', '/rules', '/about'];
  var routesUnavailableForAuthUsers = ['/login', '/registration'];

  // introduce startswith for route auth checking
  if (typeof String.prototype.startsWith != 'function') {
    String.prototype.startsWith = function (str){
      return this.slice(0, str.length) == str;
    };
  }

  // check if route requires auth
  var isPublic = function (route) {
    // exception for index '/'
    if (route === '/') return true;
    // check against public route list
    return _.find(routesThatDontRequireAuth, function (noAuthRoute) {
      return route.startsWith(noAuthRoute);
    });
  };

  $rootScope.$on('$stateChangeStart', function(ev, toState, toParams){
    // if route requires auth
    if (!isPublic(toState.url)) {
      if (!$auth.isAuthenticated()) {
        $auth.loginRedirect = {state: toState, params: toParams};
        ev.preventDefault();
        $state.go('login');
      }
    }
    // if route is not available to authenticated users
    else if (routesUnavailableForAuthUsers.indexOf(toState.url) != -1 && $auth.isAuthenticated()) {
      ev.preventDefault();
      $state.go('user');
    }
  });
}]);
