angular.module('scoragora.factories', [])
.factory('socketIO', function(apiRoot, socketFactory, $auth) {
  var socket = io(apiRoot, {
    query: "token=" + $auth.getToken()
  });

  socket = socketFactory({
    ioSocket: socket
  });

  return socket;
})

.factory('userInfo', function($auth) {
  this.getUserId = function() {
    if (this.userId) return this.userId;
    if ($auth.isAuthenticated() && $auth.getPayload()) {
      this.userId = $auth.getPayload().sub;
    }
    return this.userId;
  };

  return this;
})

.factory('userModel', function() {
  return this;
})

.factory('leagueModel', function() {
  return this;
})

.factory('forecastModel', function() {
  return this;
})

.factory('pageInfo', function() {
  this.title = 'Scoragora';
  return this;
})

.factory('_', function() {
  return window._; // assumes underscore has already been loaded on the page
});
