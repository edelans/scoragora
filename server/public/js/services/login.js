angular.module('scoragora.services.login', [])
.service('loginService', function(apiRoot, toolService, $http) {

  this.login = function(credentials) {
    return $http.post(apiRoot + '/login/local', credentials);
  };

  this.logout = function() {
    return $http.delete(apiRoot + '/login');
  };

  this.createRecoveryToken = function(info) {
    return $http.post(apiRoot + '/tokens', info);
  };

  this.updateUserPassword = function(info) {
    return $http.put(apiRoot + '/login/local', info);
  };
});
