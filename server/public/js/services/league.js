angular.module('scoragora.services.league', [])
.service('leagueService', function(apiRoot, toolService, $http) {

  this.getLeaguesByUrlName = function(urlname) {
    var url = toolService.buildUrl(apiRoot + '/leagues?', {
      url: urlname
    });
    return $http.get(url);
  };

  this.getLeaguesByToken = function(token) {
    var url = toolService.buildUrl(apiRoot + '/leagues?', {
      token: token
    });
    return $http.get(url);
  };

  this.getLeagueMembers = function(league_id, all) {
    return $http.get(apiRoot + '/leagues/' + league_id + '/members');
  };

  this.getAllLeagueMembers = function(league_id, all) {
    return $http.get(apiRoot + '/premium/leagues/' + league_id + '/members');
  };

  this.getLeagueMessages = function(league_id, options) {
    var url = toolService.buildUrl(apiRoot + '/leagues/' + league_id + '/messages?', options);
    return $http.get(url);
  };

  this.getLeagueMatchForectasts = function(league_id, match_id) {
    return $http.get(apiRoot + '/leagues/' + league_id + '/matches/' + match_id + '/forecasts');
  };

  this.checkLeagueName = function(league_name) {
    return  $http.get(apiRoot+'/leagues', {
      params:{
        checkavailability:true,
        name: league_name
      }
    });
  };

  this.createLeague = function(competition_id, league_name, sponsor) {
    return  $http.post(apiRoot + '/leagues', {
      competition: competition_id,
      name: league_name,
      sponsor: sponsor
    });
  };

  this.addUserToLeague = function(league_id) {
    //user_id will be fetched from req
    return  $http.put(apiRoot+'/leagues/'+league_id+'/members');
  };

  this.removeUserFromLeague = function(league_id) {
    //user_id will be fetched from req
    return  $http.delete(apiRoot+'/leagues/'+league_id+'/members');
  };

  this.getLeagueForecasts = function(league_id) {
    return $http.get(apiRoot + "/leagues/" + league_id + "/forecasts");
  };

  this.downloadRanking = function(league_id) {
    return $http.get(apiRoot + "/premium/leagues/" + league_id + "/ranking", {
      responseType: "arraybuffer"
    });
  };

  this.updateLeagueMember = function(league_id, user_id, update) {
    return $http.put(apiRoot+'/premium/leagues/'+league_id+'/members/' + user_id, update);
  };

  this.upgradeLeague = function(league_id, stripeToken) {
    return  $http.post(apiRoot+'/leagues/'+league_id+'/stripe', {
      stripeToken:stripeToken,
    });
  };
})
.service("leagueTool", function(leagueModel, leagueService, competitionService) {
  this.getLeague = function(url, next) {
    if (leagueModel.league && leagueModel.league.url == url) {
      return next(null, leagueModel.league);
    }
    delete leagueModel.competition;
    delete leagueModel.competitionTeamsAsDico;
    delete leagueModel.leagueMembers;
    delete leagueModel.leagueMessages;
    leagueService.getLeaguesByUrlName(url)
    .success(function(data) {
      if (!data.leagues || data.leagues.length < 1) return next({status: 404, message: "League not found"});
      leagueModel.league = data.leagues[0];
      return next(null, data.leagues[0]);
    })
    .error(function(data) {
      return next(data);
    });
  };

  this.getLeagueCompetition = function(competitionId, next) {
    if (leagueModel.league && leagueModel.competition && leagueModel.competition.id == competitionId && leagueModel.league.competition == competitionId) {
      return next(null, leagueModel.competition);
    }
    delete leagueModel.competition;
    competitionService.getCompetition(competitionId)
    .success(function(data) {
      if (data.competition.progress) {
        data.competition.progress.value = Math.floor(100*data.competition.progress.now / data.competition.progress.total);
      }
      leagueModel.competition = data.competition;
      return next(null, leagueModel.competition);
    });
  };

  this.getLeagueCompetitionTeamsAsDico = function(competitionId, next) {
    if (leagueModel.competitionTeamsAsDico && leagueModel.competition && leagueModel.league && leagueModel.competition.id == competitionId && leagueModel.league.competition == competitionId) {
      return next(null, leagueModel.competitionTeamsAsDico);
    }
    competitionService.getCompetitionTeams(competitionId)
    .success(function(data) {
      if (data.teams) {
        var dico_teams = {};
        data.teams.forEach(function(team) {
          dico_teams[team.team._id] = team.team;
        });
        leagueModel.competitionTeamsAsDico = dico_teams;
        return next(null, dico_teams);
      }
      return next("No team retrieved");
    });
  };

  this.getLeagueMembersAsDico = function(leagueId, next) {
    if (leagueModel.membersAsDico && leagueModel.league && leagueModel.league.id == leagueId) {
      return next(null, leagueModel.membersAsDico);
    }
    leagueService.getLeagueMembers(leagueId)
    .success(function(data) {
      if (data.members) {
        var dico_members = {};
        data.members.forEach(function(member) {
          dico_members[member._id] = member;
        });
        leagueModel.membersAsDico = dico_members;
        return next(null, dico_members);
      }
      return next("No members retrieved");
    });
  };
});
