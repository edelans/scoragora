angular.module('scoragora.services.user', [])
.service('userService', function(apiRoot, toolService, $http) {

  this.populateUser = function(user) {
    // displayEmail
    if (user.login && user.login.local && user.login.local.email) user.displayEmail = user.login.local.email;
    else if (user.login && user.login.facebook && user.login.facebook.email) user.displayEmail = user.login.facebook.email;
    else if (user.login && user.login.google && user.login.google.email) user.displayEmail = user.login.google.email;
    else if (user.login && user.login.twitter) user.displayEmail = '-';

    // displayName
    if (user.login && user.login.local && user.login.local.pseudo) user.displayName = user.login.local.pseudo;
    else if (user.login && user.login.facebook && user.login.facebook.displayName) user.displayName = user.login.facebook.displayName;
    else if (user.login && user.login.google && user.login.google.displayName) user.displayName = user.login.google.displayName;
    else if (user.login && user.login.twitter && user.login.twitter.displayName) user.displayName = user.login.twitter.displayName;

    // pictureMode
    if (user.pictureMode) {
      user.pictureURL = user.login[user.pictureMode].pictureURL;
    }
    else if (user.login && user.login.local && user.login.local.pictureURL) {
      user.pictureURL = user.login.local.pictureURL;
    }
    else if (user.login && user.login.facebook && user.login.facebook.pictureURL) {
      user.pictureURL = user.login.facebook.pictureURL;
    }
    else if (user.login && user.login.twitter && user.login.twitter.pictureURL) {
      user.pictureURL = user.login.twitter.pictureURL;
    }
    else if (user.login && user.login.google && user.login.google.pictureURL) {
      user.pictureURL = user.login.google.pictureURL;
    }
    return user;
  };

  this.createUser = function(user) {
    return $http.post(apiRoot + '/users', {user : user});
  };

  this.validateUser = function(token) {
    var url = toolService.buildUrl(apiRoot + '/users?', {token: token});
    return $http.get(url);
  };

  this.getUser = function(user_id) {
    return $http.get(apiRoot + '/users/' + user_id);
  };

  this.updateUser = function(user_id, data) {
    /* Update user details
     * @data:
     *   - pictureMode: [local, twitter, facebook, google]
     *  - pictureURL: String
     *  - pseudo: String
     *  - passwordForm: {
     *    password: String,
     *    oldPassword: String,
     *    passwordConfirmation: String
     *  }
     *  - emailSettings: {
     *    ranking_updates: 'no_updates' || 'daily' || 'weekly',
     *    forecast_management: {
     *      reminder_24h: Boolean [true],
     *      new_matches: Boolean [true]
     *     }
     *  }
     */
    return $http.put(apiRoot + '/users/' + user_id, data);
  };


  this.getUserLeagues = function(user_id, options) {
    /* Get user league list
     * @querystring:
     *  - limit: Number of items
     *  - page: Number of the page
     *  - order: [activity]
     *  - unreadmsg: Boolean
     */
    var url = toolService.buildUrl(apiRoot + '/users/' + user_id + '/leagues?', options);
    return $http.get(url);
  };

  this.getUserMatches = function(user_id, options) {
    /* Get user match list
     * @querystring:
     *  - limit: Number of items
     *  - page: Number of the page
     *  - status: [past]
     */
    var url = toolService.buildUrl(apiRoot + '/users/' + user_id + '/matches?', options);
    return $http.get(url);
  };

  this.getUserForecasts = function(user_id, options) {
    // Get user forecast list sorted on forecast status and _id(creation date)
    // @querystring:
    //  - limit: Number of items
    //  - page: Number of the page
    var url = toolService.buildUrl(apiRoot + '/users/' + user_id + '/forecasts?', options);
    return $http.get(url);
  };

  this.getUserMatchforecasts = function(user_id, options) {
    // Get user match forecast list
    // @querystring:
    //  - limit: Number of items
    //  - page: Number of the page
    //  - status: [past]
    var url = toolService.buildUrl(apiRoot + '/users/' + user_id + '/matchforecasts?', options);
    return $http.get(url);
  };


  this.getUserMatchforecasts = function(user_id, options) {
    // Get user match forecast list
    // @querystring:
    //  - limit: Number of items
    //  - page: Number of the page
    //  - status: [past]
    var url = toolService.buildUrl(apiRoot + '/users/' + user_id + '/matchforecasts?', options);
    return $http.get(url);
  };

});
