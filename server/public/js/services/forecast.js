angular.module('scoragora.services.forecast', [])
.service('forecastService', function(apiRoot, toolService, $http) {

  this.getForecast = function(forecast_id) {
    return $http.get(apiRoot + '/forecasts/' + forecast_id);
  };

  this.updateForecast = function(forecast_id, matches){
    /**
     *  Update forecast bet
     *  @data:
     *    - matches: [{match: String, forecast: Object}]
     *    - bonus: [{name: String, value: String}]
     */
    return $http.put(apiRoot + '/forecasts/' + forecast_id,{matches: matches});
  };

});
