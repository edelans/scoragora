angular.module('scoragora.services.competition', [])
.service('competitionService', function(apiRoot, toolService, $http) {


  this.getAllPublicCompetitions = function(options) {
    /* @querystring:
    *	- limit: Number of items
    *	- page: Number of the page
    *  - status: public | ?
    */
    var url = toolService.buildUrl(apiRoot + '/competitions?', options);
    return $http.get(url);
  };

  this.getCompetition = function(competition_id) {
    return $http.get(apiRoot + '/competitions/' + competition_id);
  };

  this.getCompetitionMatches = function(competition_id, options) {
    var url = toolService.buildUrl(apiRoot + '/competitions/' + competition_id + '/matches?', options);
    return $http.get(url);
  };
});
