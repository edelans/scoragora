angular.module('scoragora.services', [
  'scoragora.services.login',
  'scoragora.services.league',
  'scoragora.services.user',
  'scoragora.services.competition',
  'scoragora.services.forecast',
  'scoragora.services.match'
])
.service('toolService', function() {
  this.buildUrl = function(root, options) {
    var url = root;
    var separator = '';
    Object.keys(options).forEach(function(key) {
      if (options[key] !== null && options[key] !== undefined) {
        url += separator + key + "=" + options[key];
        separator = '&';
      }
    });
    return url;
  };
})
.service('messageService', function($i18next) {
  this.error = function(item, data, options) {
    if (!options) {
      options = {};
    }
    if (!data || typeof data != "object" || !data.code) {
      data = {
        code: 50000
      };
    }

    item.message = $i18next('messages:code.' + data.code, data);
    item.type = 'danger';

    if (options.timeout && !isNaN(options.timeout)) {
      setTimeout(function() {
        delete item.type;
        delete item.message;
      }, options.timeout);
    }
  };
  
  this.alert = function(item, data, options) {
    if (!options) {
      options = {};
    }
    if (!data || typeof data != "object" || !data.code) {
      data = {
        code: 50000
      };
    }

    item.message = $i18next('messages:code.' + data.code, data);
    if (data.status == 200) item.type = 'success';
    else item.type = 'danger';

    if (options.timeout && !isNaN(options.timeout)) {
      setTimeout(function() {
        delete item.type;
        delete item.message;
      }, options.timeout);
    }
  };
});
