angular.module('scoragora.services.match', [])
.service('matchService', function(apiRoot, toolService, $http) {

  this.getMatch = function(match_id) {
    return $http.get(apiRoot + '/matches/' + match_id);
  };

});
