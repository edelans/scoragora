module.exports = function(grunt) {
  grunt.initConfig({
    jshint: {
      options: {
        laxbreak: true,
        reporterOutput: ''
      },
      dist: ['public/js/**/*.js', 'public/js/*.js', '!public/js/lib.js', '!**/*.min.js', '!**/min.js','!public/js/v1/*'],
      lib: ['public/js/angular.loading-bar.min.js']
    },
    less: {
      bootstrap: {
        options: {
          paths: ["public/less/bootstrap/mixin"]
        },
        files: {
          "public/css/bootstrap.css": "public/less/bootstrap/bootstrap.less"
        }
      },
      dist: {
        files: {
          "public/css/index_less.css": "public/less/import.less"
        }
      }
    },
    uglify: {
      options: {
        mangle: false,
      },
      dist: {
        src : ['public/js/app.js', 'public/js/app.*.js', '!public/js/v1/*.js', 'public/js/**/*.js', '!public/js/lib.js', '!**/*.min.js', '!**/min.js'],
        dest: 'public/js/min.js'
      }
    },
    concat: {
      options: {
        separator: ';'
      },
      libjs: {
        src: [
          'bower_components/angular/angular.min.js',
          'bower_components/angular-cookies/angular-cookies.js',
          'bower_components/angular-sanitize/angular-sanitize.min.js',
          'bower_components/angular-bootstrap/ui-bootstrap-tpls.min.js',
          'bower_components/angular-ui-router/release/angular-ui-router.min.js',
          'bower_components/angular-loading-bar/build/loading-bar.min.js',
          'bower_components/i18next/i18next.min.js',
          'bower_components/angular-scroll-glue/src/scrollglue.js',
          'bower_components/ng-i18next/dist/ng-i18next.min.js',
          'bower_components/angular-socket-io/socket.js', // min doesnt work
          'public/js/scoragora_satellizer.js', // should be minified
          'public/js/FileSaver.min.js',
          'bower_components/angular-elastic/elastic.js',
          'bower_components/moment/min/moment.min.js',
          'bower_components/moment/locale/en-gb.js',
          'bower_components/moment/locale/fr.js',
          'bower_components/d3/d3.min.js',
          'bower_components/angular-moment/angular-moment.js',
          'bower_components/angulartics/dist/angulartics.min.js',
          'bower_components/angulartics/dist/angulartics-ga.min.js',
          'bower_components/clipboard/dist/clipboard.min.js',
          'bower_components/ngclipboard/dist/ngclipboard.min.js',
          'bower_components/underscore/underscore.js',
          'bower_components/underscore.string/dist/underscore.string.min.js'
        ],
        dest: 'public/js/lib.js'
      },
      js: {
        src: [
          'public/js/lib.js',
          'public/js/min.js'
        ],
        dest: 'public/js/min.js'
      }
    },
    jadeFilerevUsemin: {

    },
    watch: {
      js: {
        files: ['public/js/**/*.js', 'public/js/*.js', '!**/*.min.js', '!**/min.js'],
        tasks: ['jshint:dist', 'uglify:dist', 'concat:js']
      },
      less: {
        files: ["public/less/**/*.less"],
        tasks: ["less:dist"]
      }
    }
  });

  grunt.loadNpmTasks('grunt-contrib-concat');
  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-contrib-jshint');
  grunt.loadNpmTasks('grunt-contrib-less');
  grunt.loadNpmTasks('grunt-jade-filerev-usemin');
  grunt.loadNpmTasks('grunt-contrib-watch');

  grunt.registerTask('default', ['jshint:dist', 'uglify:dist', 'concat:libjs', 'concat:js']);

  grunt.registerTask("dev-css", ["less:dist", "watch:less"]);
  grunt.registerTask("dev-js", ["concat:libjs", "jshint:dist", "uglify:dist", "concat:js", "watch:js"]);
};
