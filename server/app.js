var pmx            = require('pmx').init();
var express        = require('express');
var morgan         = require('morgan');
var bodyParser     = require('body-parser');
var cookieParser   = require('cookie-parser');
var methodOverride = require('method-override');
var fs             = require('fs');
var winston        = require('winston');
var http           = require('http');
var https          = require('https');
var app            = exports.app = express();
var server;
var cfg	           = require('../config');

// configure winston
winston.remove(winston.transports.Console);
winston.add(winston.transports.Console, {
  colorize: true,
  prettyPrint: true,
  timestamp: true
});

app.set('views', __dirname + '/views');
app.set('view engine', 'jade');
app.disable('x-powered-by'); // security measure


app.use(express.static(__dirname + '/public'));   // set the static files location /public/img will be /img for users
app.use(morgan('dev'));                // log every request to the console
app.use(bodyParser.urlencoded({
  extended: true
}));
app.use(bodyParser.json());            // pull information from html in POST
app.use(methodOverride());             // simulate DELETE and PUT

// required for passport
app.use(cookieParser());               // Parser cookie if needed (especially for auth)

// Declare the API base route
app.get('/', function(req, res) {
  //res.sendfile('index.html', {root: __dirname + '/public/views'});
  res.render('index', {
    env: process.env.NODE_ENV || 'dev',
    routing: cfg.routing,
    google: {
      client: cfg.google.clientID
    },
    facebook: {
      client: cfg.facebook.clientID
    }
  });
});

/*var options = {
key: fs.readFileSync('./https/server.key'),
cert: fs.readFileSync('./https/server.crt')
}*/
http.createServer(app).listen(cfg.server_port);
//https.createServer(options, app).listen(cfg.server_port);
winston.info('Creating a server on port ' + cfg.server_port + ' in ' + cfg.env + ' mode');
