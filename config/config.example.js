var config = {};

// port
config.api_port = 3001;
config.server_port = 3000;
config.server_admin_port = 3002;
config.scheduler_port = 8081;

// token secret key
config.secret = '<your secret for token>';

// admin
config.admin = {
	user: "admin",
	password: "brazuca2014"
};

// mongo database
config.mongo = {
  uri: '<mongo uri>'
};

// Winston logs
config.winstonLogs = {
  collection: "log",
  db: "<mongo uri for logs>",
  capped: true,
  cappedSize: 100000000, // 100Mo
  includeIds: false,
  level: "info"
};

// Log level
config.log_level = "info";

// Routing
config.routing = {
  api: '<api url, example : https://api.scoragora.com>',
  scheduler: '<scheduler api, example: https://scheduler.scoragora.com'
};

// social accounts authorization credentials with local callback url
config.google = {
  clientID: '<client id>',
  secret: '<secret>',
  realm: '<url, example: http://www.scoragora.com/>',
  callbackURL: '<callback url, example: /api/login/google/callback>'
};

config.facebook = {
  clientID: '<client id>',
  secret: '<secret>',
  callbackURL: "<callback url>"
};

config.twitter = {
  consumerKey: '<client key>',
  consumerSecret: '<secret>',
  callbackURL: "<callback url>"
};

config.sendgrid = {
    apiKey: '<api key>'
};

config.stripe = {
    secretKey: '<secret key>',
    publishableKey: '<public key>'
};

module.exports = config;
