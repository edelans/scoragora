var config = {};

// port
config.api_port = 3001;
config.server_port = 3000;
config.server_admin_port = 3002;
config.scheduler_port = 8081;

config.secret = 'XOtitaSt5RXC  ';

// admin
config.admin = {
	user: "admin",
	password: "brazuca2014"
};

// mongo database
config.mongo = {
  uri: 'mongodb://localhost:27017/api'
};

// Winston logs
config.winstonLogs = {
  collection: "log",
  db: "mongodb://localhost:27017/logs",
  capped: true,
  cappedSize: 100000000, // 100Mo
  includeIds: false,
  level: "info"
};

// Log level
config.log_level = "info";

// Routing
config.routing = {
  api: 'https://staging-api.scoragora.com',
  scheduler: 'https://staging-scheduler.scoragora.com'
};

config.facebook = {
  clientID: '531509203717415',
  secret: 'bc61c4771609784e4076f0c50ac79671',
  callbackURL: "/api/login/facebook/callback"
};

// social accounts authorization credentials with local callback url
config.google = {
  clientID: '976002710436-67uq3ikf1u0gq1hgk7s7pufuu86dsug8.apps.googleusercontent.com',
  secret: 'w6D51rp1BiNM2AlW9zyQg9sa',
  realm: 'http://www.scoragora.com/',
  callbackURL: '/api/login/google/callback'
};

config.facebook = {
  clientID: '218368321698173',
  secret: 'a1538b40962c381dd33f26cf5aac9f29',
  callbackURL: "/api/login/facebook/callback"
};

config.twitter = {
  consumerKey: 'jJwUMT7NFdHOfqTR273HQvZza',
  consumerSecret: 'yM4goJAw83ZwK8ELQfyFaPjDoAH3r2lyV2r0wHlmTBEH0UYjPR',
  callbackURL: '/api/login/twitter/callback'
};

config.sendgrid = {
    apiKey: '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'
};

config.stripe = {
    secretKey: 'sk_test_AejfDcuLDLp6wv4ukjQZT2ga',
    publishableKey: 'pk_test_BgfAstxwpwlFtzafWcYJYPqa'
};

module.exports = config;
