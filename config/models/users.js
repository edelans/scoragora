const LoginSchema = {
	local: {
		email: {
			type: String,
			match: /^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-.]+\.[a-zA-Z0-9-]+$/,
			lowercase: true,
			trim: true,
		},
		password: {
			type: String
		},
		pseudo: String,
		pictureURL: String
	},
	google: {
		id: String,
		token: String,
		email: String,
		displayName: String,
		name: {
      familyName: String,
      middleName: String,
      givenName: String
    },
    pictureURL: String
  },
  facebook: {
    id: String,
  	token: String,
    email: String,
    displayName: String,
    name: {
    	familyName: String,
      middleName: String,
      givenName: String
    },
    gender: String,
    pictureURL: String,
  },
  twitter: {
    id: String,
    token: String,
    email: String,
    displayName: String,
    username: String,
    pictureURL: String
  }
};

const UserSchema = {
	// Handle compte creation
	expiry : {
		type: Date,
		expires: 60*60
	},
	createdAt: Date,
	deleted: Boolean,
	login : LoginSchema,
	lng: String,
	pictureMode: String,
	leagues: [{
		league: {
			type: "objectid",
			ref: 'League'
		},
		ranking: Number
	}],
	forecasts: [{
		forecast: {
			type: "objectid",
			ref: 'Forecast'
		}
	}],
    email_settings: {
        ranking_updates: {
            type: String,
            enum: ['no_updates', 'daily', 'weekly']
        },
        forecast_management: {
            reminder_24h: {
                type: Boolean,
                default: true
            },
            new_matches: {
                type: Boolean,
                default: true
            }
        }
    }
};

module.exports = UserSchema;