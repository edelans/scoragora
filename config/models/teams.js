var TeamSchema = {
	label: String, // ex: borussia_dortmund --> useful for any link or css
	name: String, // ex: Borussia Dortmund --> just for admin display
	logo: String, // Path to display the logo
	automation: {
		lequipe: {
			url: String, // team web page
			name: String // name of the team on lequipe.fr !important
		},
		espn: {
			url: String,
			name: String
		},
		eurosport: {
			url: String,
			name: String
		}	
	},
	sport: {
		type: String,
		enum: ['football', 'rugby', 'basket']
	}
};

module.exports = TeamSchema;