var LeagueSchema = {
    createdAt: Date,
    lastMessageAt: Date,
    // enterprise store all the data for the enterprise
    // could be sht like {validated: true}
    premium: {
    	validated: Boolean
    },
    sponsor: {
      type: String
    },
    name: {
        type: String
    },
    url: {
        type: String,
        unique: true
    },
    token: {
        type: String
    },
    captains: [{
        type: "objectid",
        ref: 'User'
    }],
    competition: {
        type: "objectid",
        ref: 'Competition'
    },
    members: [{
      	user: {
            type: "objectid",
            ref: 'User'
        },
        points: {
            type: Number,
            default: 0
        },
        rank: Number,
        lastConnectedAt: Date,
        status: {
	        type: String,
	        enum: ["banned", "accepted"]
        },
        flag: String
    }],
    max_size: {
        type: Number,
        default: 50
    }
};


// à discuter et à faire évoluer
var EnterpriseSchema = {
    enabled: {
        type: Boolean
    },
    name: {
        type: String
    },
    admins: [{
        type: "objectid",
        ref: 'User'
    }],
    logo: {
        type: String
    }
};


module.exports = LeagueSchema;
