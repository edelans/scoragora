var TokenSchema = {
    datetime: {
	    type: Date,
	    expires: 60*60
    },
    token: String,
    user: {
	    type: "objectid",
	    ref: 'User'
    }
};

module.exports = TokenSchema;