var MatchSchema = {
	competition : {
		type: "objectid",
        ref: 'Competition'	
	},
	hometeam : {
		type: "objectid",
        ref: 'Team'
	},
	hometeamname: String,
	awayteam: {
		type: "objectid",
        ref: 'Team'
	},
	awayteamname: String,
	datetime: Date,
	live: Boolean,
	result: "mixed", // { sets: [], hometeam_score: 2, final_direction: 'away', hometeam_tries: 2, awayteam_tries: 1}
	forecast: "mixed", // { hometeam: [{score: 1, count: 128}], awayteam: []} 
	rule: {
		type: "objectid",
        ref: 'Rule'
	},
	sport: {
		type: String,
		enum: ['football','rugby', 'tennis'] 
	},
	status: {
		type: String,
		enum: ['inactive', 'active', 'to_be_computed', 'being_computed', 'closed']
	},
	automation: {
		lequipe: {
			url: String
		},
		espn: {
			url: String
		},
		eurosport: {
			url: String
		}
	}
};

module.exports = MatchSchema;