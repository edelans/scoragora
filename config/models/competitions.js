var CompetitionSchema = {
    name: {
        type: String,
        match: /[a-z0-9_-]{3,50}/i
    },
    year: {
        type: String
    },
    start_date: Date,
    // open to public
    status: {
        type: String,
        enum: ["private", "ready", "public", "archived"],
        default: "private"
    },
    sport: {
	    type: String,
	    enum: ['football', 'rugby', 'tennis']
    },
    progress: {
        matches: {
		    previous: Number,
		    coming: Number,
	    },
        total: {
            type: Number,
            default: 1000
        },
        now: {
            type: Number,
            default: 1
        }
    },
    config: {
        type: {
            type: String,
            enum: ["cup", "championship"]
        },
        cup: {
            group: {
                number: Number,
                capacity: Number,
                two_legged: Boolean, // say if it is a two legged opposition
                qualified_number: {
                    type: Number
                } // represent the number of team qualified
            },
            elimination: {
                round_max: Number,
                two_legged: Boolean,
                third_place: Boolean // say if there is a match for the third place
            }
        },
        championship: {
            day: {
                capacity: Number, // number of teams
                two_legged: Boolean,
            },
            elimination: {

            }
        }
    },
    // table points with detailled rules
    rules: {
	    type: "mixed" // {group: { rule: <rule content>}}}
    },
    // team list
    teams: [ {
    	team: {
	        type: "objectid",
	        ref: 'Team'
	    }
	}],
    matches: [{
	 	match: {
		 	type: "objectid",
		 	ref: 'Match'
	 	},
	 	group: String,
	 	day: Number,
	 	round: Number  // maybe move to enum final, semi, eighths, third_place
    }],
    automation: {
		lequipe: {
			url: String, // competition web page
			name: String // name of the team on lequipe.fr !important
		},
		espn: {
			url: String,
			name: String
		},
		eurosport: {
			url: String,
			name: String
		}	
	}
};

module.exports = CompetitionSchema;