var RuleSchema = {
    name: String,
    sport: {
    	type: String,
    	enum: ['football', 'rugby', 'tennis']
    },
    criterias: [{
    	name: String,
    	points: Number
    }]
};

module.exports = RuleSchema;