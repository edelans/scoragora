var TaskSchema = {
    type: {
		type: String,
		enum: ['auto', 'manual']
	},
	// parameters {competition: "adb78563553dbc35646", processes: 16}
	parameters: "mixed",
	datetime: Date,
	algo: {
		type: String,
		enum: [
			'compute_points', 
			'compute_points_from_scratch', 
			'compute_crowdscoring', 
			'compute_ranking'
		]
	},
	processes: Number,
	// complexity {matches: 64, forecasts: 7460}
	complexity: "mixed",
	execution_time: Number,
	status: {
		type: String,
		enum: ['planned', 'running', 'failed', 'succeeded']
	}
};

module.exports = TaskSchema;