var env = process.env.NODE_ENV || 'dev',
  cfg = require('./config.' + env);

cfg.env = env;

module.exports = cfg;
