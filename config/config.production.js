var config = {};

// port
config.api_port = 3001;
config.server_port = 3000;
config.server_admin_port = 3002;
config.scheduler_port = 8081;

config.secret = '$hfre69ftcb50$devhsr1df3frefej34';

// admin
config.admin = {
	user: "admin",
	password: "brazuca2014"
};

// mongo database
config.mongo = {
  uri: 'mongodb://localhost:27017/api'
};

// Winston logs
config.winstonLogs = {
  collection: "log",
  db: "mongodb://scoragora:brazuca2014@ds011231.mlab.com:11231/scoragora-logs",
  capped: true,
  cappedSize: 100000000, // 100Mo
  includeIds: false,
  level: "error"
};

// Log level
config.log_level = "error";

// Routing
config.routing = {
  api: 'https://api.scoragora.com',
  scheduler: 'https://scheduler.scoragora.com'
};

// social accounts authorization credentials with local callback url
config.google = {
  clientID: '976002710436-67uq3ikf1u0gq1hgk7s7pufuu86dsug8.apps.googleusercontent.com',
  secret: 'w6D51rp1BiNM2AlW9zyQg9sa',
  realm: 'http://www.scoragora.com/',
  callbackURL: '/api/login/google/callback'
};

config.facebook = {
  clientID: '218368321698173',
  secret: 'a1538b40962c381dd33f26cf5aac9f29',
  callbackURL: "/api/login/facebook/callback"
};

config.twitter = {
  consumerKey: 'jJwUMT7NFdHOfqTR273HQvZza',
  consumerSecret: 'yM4goJAw83ZwK8ELQfyFaPjDoAH3r2lyV2r0wHlmTBEH0UYjPR',
  callbackURL: '/api/login/twitter/callback'
};

config.sendgrid = {
    apiKey: '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'
};

config.stripe = {
    secretKey: 'sk_live_J1Iep6hm0syiqUCErCPH6Axg',
    publishableKey: 'pk_live_x7yobEQd8OEauU0uNAnOQUBH'
};

module.exports = config;
