# -*- mode: ruby -*-
# vi: set ft=ruby :

VAGRANTFILE_API_VERSION = "2"

Vagrant.configure(VAGRANTFILE_API_VERSION) do |config|
  config.vm.box = "ubuntu/trusty64"
  config.vm.network :private_network, ip: "33.33.33.11"
  config.vm.define "vagrant"

  config.hostsupdater.aliases = ["www.scoragora.com", "api.scoragora.com", "admin.scoragora.com", "scoragora.com"]

  # Share an additional folder to the guest VM. The first argument is
  # the path on the host to the actual folder. The second argument is
  # the path on the guest to mount the folder. And the optional third
  # argument is a set of non-required options.

  config.vm.synced_folder "./", "/var/www/scoragora"

  config.vm.provider "virtualbox" do |v|
    v.customize ["modifyvm", :id, "--cpuexecutioncap", "100"]
    v.customize ["modifyvm", :id, "--memory", 1024]
    v.customize ["modifyvm", :id, "--cpus", 1]
  end

  config.ssh.forward_agent = true

  config.vm.provision :ansible do |ansible|
    ansible.inventory_path = "provisioning/hosts/vagrant"
    ansible.playbook = "provisioning/playbook.yml"
    ansible.limit = "vagrant"
    ansible.sudo = true
    ansible.verbose = "vvv"
    ansible.host_key_checking = false
  end
end
