#Dev config

##Initial Setup
You have to install
+ Virtualbox `$ sudo apt-get install virtualbox`
+ Vagrant `$ sudo apt-get install vagrant` and `$ sudo apt-get install virtualbox-dkms`

For NFS sync : Install nfs server on your machine:
`$ sudo apt-get install nfs-kernel-server`

beware: nfs cannot work on crypted partitions, but default vagrant folder sync is ok as a fallbak.

##Load the target linux box

    vagrant box add ubuntu/trusty64

It will download the ubuntu image to your filesystem.

##Launch vagrant
In your root repository (where the `Vagrantfile` is, launch `vagrant up`)



#Execute the playbook

```
ansible-playbook playbook.yml --extra-vars="hosts=<HOSTNAME>"
```

For testing purposes:
```
ansible-playbook playbook.yml --extra-vars="hosts=rozanoff"  --check
```
