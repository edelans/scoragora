# enrise.mongodb

This is an Ansible task for installing and configuring MongoDB, as well as creating users and databases.

## Requirements

- Tested on Ansible 1.6
- Tested on Ubuntu 14.04 (trusty), but it should work on any modern Debian based system.

## Dependencies

None.

## Example playbook

To use this role, build a vars file (vars/mongodb.yml, for example) which you include in your playbook,
which contains something like the following:

    mongodb_users:
     - { username: username, password: password, database: dbname }

Next, you can include the role in your playbook:

    - hosts: all
      sudo: yes
      vars_files:
        - vars/mongodb.yml
      roles:
        - enrise.mongodb



## Feedback? Found a bug? Requests?

Let us have it! http://github.com/Enrise/ansible-role-mongodb/issues


# Configuration for systemd
To make a long story, short, Ubuntu shifted from upstart to systemd. One common solution to the problem is to write systemd script that will start MongodDB.


```nano /lib/systemd/system/mongodb.service```

```
[Unit]
Description=MongoDB Database Service
Wants=network.target
After=network.target

[Service]
ExecStart=/usr/bin/mongod --config /etc/mongod.conf
ExecReload=/bin/kill -HUP $MAINPID
Restart=always
User=mongodb
Group=mongodb
StandardOutput=syslog
StandardError=syslog

[Install]
WantedBy=multi-user.target
```

To enable mongodb on startup

    sudo systemctl enable mongodb.service
