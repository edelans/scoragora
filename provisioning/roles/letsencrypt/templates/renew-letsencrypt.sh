#!/bin/sh

cd /etc/letsencrypt/
letsencrypt --config /etc/letsencrypt/configs/scoragora.conf certonly

if [ $? -ne 0 ]
 then
        ERRORLOG=`tail /var/log/letsencrypt/cron.log`
        echo -e "The Let's Encrypt cert has not been renewed! \n \n" \
                 $ERRORLOG
 else
        nginx -s reload
fi

exit 0
