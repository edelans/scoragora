Example Playbook
-------------------------

    - hosts: servers
      roles:
        - role: nvm
          nvm:
            user: deploy
            version: v0.4.0
            node_version: '0.10'
