# define caching
proxy_cache_path /data/nginx/cache levels=1:2 keys_zone=mycache:10m inactive=60m;
proxy_cache_key "$scheme$request_method$host$request_uri$cookie_user";
proxy_cache_valid any      10m;



# connexion sans HTTPS : procedure de vlidation certbot + redirection en https
server {
    listen 80;
    server_name  scoragora.com  www.scoragora.com  admin.scoragora.com  api.scoragora.com  scheduler.scoragora.com;

    location /nginx_stub_status {
       stub_status on;
       access_log   off;
       allow 54.38.182.60;
       allow 127.0.0.1;
       deny all;
    }

    include snippets/letsencrypt.conf;
    return 301 https://$host$request_uri;
}


# redrection no www vers www
server {
    listen 443 ssl http2;
    server_name scoragora.com;

    ssl_certificate /etc/letsencrypt/live/scoragora.com/fullchain.pem;
    ssl_certificate_key /etc/letsencrypt/live/scoragora.com/privkey.pem;
    ssl_trusted_certificate /etc/letsencrypt/live/scoragora.com/chain.pem;
    include snippets/ssl.conf;

    return 301 https://www.scoragora.com$request_uri;
}

# main app
server {
   listen 443 ssl;
   server_name www.scoragora.com;
   access_log off;
   error_log  /var/log/nginx/scoragora.app.error.log;
   location /public {
       root /var/www/scoragora/source/server/public;
       expires 3d;
       add_header Cache-Control "public";
   }
   # Media: images, icons, video, audio, HTC
   location ~* /(css|fonts|img|js|locales)/.* {
      root /var/www/scoragora/source/server/public;
      expires 3d;
      add_header Cache-Control "public";
   }
   location / {
       if (-f /var/www/scoragora/current/server/views/error/maintenance_on.html) {
          return 503;
       }
       proxy_set_header Upgrade $http_upgrade;
       proxy_set_header Connection "upgrade";
       proxy_http_version 1.1;
       proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
       proxy_set_header Host $host;
       proxy_set_header X-Real-IP $remote_addr;
       proxy_set_header X-NginX-Proxy true;
       proxy_pass http://127.0.0.1:3000;
   }
   location /nginx_stub_status {
       stub_status on;
       access_log   off;
       allow 54.38.182.60;
       allow 127.0.0.1;
       deny all;
   }

   proxy_intercept_errors on;
   error_page 403 /error/403.html;
   error_page 404 /error/404.html;
   error_page 405 /error/405.html;
   error_page 503 /error/maintenance_on.html;
   error_page 500 501 502 504 /error/5xx.html;
   location ^~ /error/ {
       internal;
       root /var/www/scoragora/current/server/views/;
   }
   ssl_certificate /etc/letsencrypt/live/scoragora.com/fullchain.pem;
   ssl_certificate_key /etc/letsencrypt/live/scoragora.com/privkey.pem;
   ssl_trusted_certificate /etc/letsencrypt/live/scoragora.com/chain.pem;
   include snippets/ssl.conf;
}

# admin
server {
   listen 443 ssl http2;
   server_name admin.scoragora.com;

   access_log off;
   error_log  /var/log/nginx/scoragora.admin.error.log;
   location /public {
       root /var/www/scoragora/source/server_admin/public;
       expires 1d;
   }
   location / {
       proxy_set_header Upgrade $http_upgrade;
       proxy_set_header Connection "upgrade";
       proxy_http_version 1.1;
       proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
       proxy_set_header Host $host;
       proxy_pass http://127.0.0.1:3002;

   }
   ssl_certificate /etc/letsencrypt/live/scoragora.com/fullchain.pem;
   ssl_certificate_key /etc/letsencrypt/live/scoragora.com/privkey.pem;
   ssl_trusted_certificate /etc/letsencrypt/live/scoragora.com/chain.pem;
   include snippets/ssl.conf;
}

# api
server {
   listen 443 ssl;
   server_name api.scoragora.com;
   access_log off;
   error_log  /var/log/nginx/scoragora.api.error.log;
   location / {
       proxy_pass http://127.0.0.1:3001;
       proxy_http_version 1.1;
       proxy_set_header Upgrade $http_upgrade;
       proxy_set_header Connection 'upgrade';
       proxy_set_header Host $host;
       proxy_cache_bypass $http_upgrade;
   }
   ssl_certificate /etc/letsencrypt/live/scoragora.com/fullchain.pem;
   ssl_certificate_key /etc/letsencrypt/live/scoragora.com/privkey.pem;
   ssl_trusted_certificate /etc/letsencrypt/live/scoragora.com/chain.pem;
   include snippets/ssl.conf;
}

# scheduler
server {
   listen 443 ssl http2;
   server_name "scheduler.scoragora.com";
   access_log off;
   error_log  /var/log/nginx/scoragora.scheduler.error.log;
   location /public {
       root /var/www/scoragora/source/scheduler/public;
       expires 1d;
   }
   location / {
       proxy_set_header Upgrade $http_upgrade;
       proxy_set_header Connection "upgrade";
       proxy_http_version 1.1;
       proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
       proxy_set_header Host $host;
       proxy_pass http://127.0.0.1:8081;
       proxy_read_timeout 120s;

   }
   ssl_certificate /etc/letsencrypt/live/scoragora.com/fullchain.pem;
   ssl_certificate_key /etc/letsencrypt/live/scoragora.com/privkey.pem;
   ssl_trusted_certificate /etc/letsencrypt/live/scoragora.com/chain.pem;
   include snippets/ssl.conf;
}
