
#!/bin/bash

# clean files from previous restore
rm -rf /data/backups/restore/*
echo "\n**** cleaned restore dir ****\n"

# get latest restore
latest="$(ls -r /data/backups/rozanoff/scoragora-*.tgz | head -1)"
if test -z "$latest"; then
  # there's no logs yet, bail out
  exit
fi
echo "\n**** latest archive is $latest ****\n"

#untar the archive:
tar -C /data/backups/restore -zxvf $latest

echo "\n**** about to drop db ****\n"
#drop local api db
mongo api --eval "db.dropDatabase()"
echo "\n**** droped db ****\n"

echo "\n**** about to restore ****\n"
# restore db
DIR_PATH=$(echo ${latest%.tgz})
DIR_NAME=$(echo ${DIR_PATH##*/})
echo $DIR_NAME
mongorestore /data/backups/restore/$DIR_NAME

echo "\n****  DONE !! Good job. ****\n"
