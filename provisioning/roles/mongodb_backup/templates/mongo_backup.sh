#!/bin/bash
THISHOST=$(hostname)
BACKUP_SERVER_IP=91.134.142.135

MONGO_DATABASE="api"
APP_NAME="scoragora"

MONGO_HOST="127.0.0.1"
MONGO_PORT="27017"
TIMESTAMP=`date +%F-%H%M`
MONGODUMP_PATH="/usr/bin/mongodump"
BACKUPS_DIR="/data/backups/$THISHOST"
BACKUP_NAME="$APP_NAME-$TIMESTAMP"
MAX_DUMP_AGE_IN_DAYS="15"

mongo admin --eval "printjson(db.fsyncLock())"
$MONGODUMP_PATH -h $MONGO_HOST:$MONGO_PORT -d $MONGO_DATABASE
mongo admin --eval "printjson(db.fsyncUnlock())"


# organize dumps
mkdir -p $BACKUPS_DIR
mv dump $BACKUP_NAME
tar -zcvf $BACKUPS_DIR/$BACKUP_NAME.tgz $BACKUP_NAME
rm -rf $BACKUP_NAME

echo "\n\nAbout to send file to $BACKUP_SERVER_IP:/data/backups/$THISHOST... "
# uncomment following line to send dump to other server
#scp $BACKUPS_DIR/$BACKUP_NAME.tgz mongobackup@$BACKUP_SERVER_IP:/data/backups/$THISHOST

# reminder to make the auto transfert right :
# create backup user on remote host and enable passwordless login:
#     sudo adduser --home /data/backups --disabled-password mongobackup
# generate rsa key pair on local host :
#     ssh-keygen
# copy id_rsa.pub to remote host
#     scp to /data/backups/.ssh/authorized_keys


# cleaning up dumps older than MAX_DUMP_AGE_IN_DAYS
find $BACKUPS_DIR -name "$APP_NAME-*" -a -mtime +${MAX_DUMP_AGE_IN_DAYS} -delete
echo


#How to restore a backup:
#untar the archive:
#    tar -zxvf scoragora-2016-05-20-1817.tgz to extract the file to the current directory
#restore the backup:
#    mongorestore scoragora-2016-05-20-1817
