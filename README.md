brazuca
=======

[ ![Codeship Status for edelans/brazuca](https://codeship.com/projects/fd1c0700-8ac8-0132-d7b1-460e9fe922d7/status?branch=v2)](https://codeship.com/projects/60125)


# Architecture

## config
Central folder to store all variables for all apps and environments.

## server
Frontend (angular app) for users.

## server_admin
Front end admin (admin angular app) apiRoot dans app.js defines the api URL ... to be improved when we find another more suitable method to define api root.

## api
Backend (api)

## computer
CPU/memory intensive scripts not suited for a node execution which is single threaded (for instance ranking calculation, points computation...).

## scheduler
Node instance that manage scripts in computer folder.


# Dependencies
We use:
* *npm* to manage server-side dependencies
* *bower* to manage client-side dependencies
* *grunt* for automating some tasks (see `gruntfile`s for more information), including merging dependencies into one minified file.

All different apps have (when needed) their own independant `gruntfle`, `package.json`/`node_modules` and `bower.json`/`bower_components`.

Library files ( `/node_modules/*` and `/bower_components/*` for each app) are not commited on the repo as those libs can be heavy and above all it adds a lot of noise to every commits pushed with an update of a lib. So when one of us adds a new library to an app, we all have to make a `bower install` (if it's a client side lib) or `npm install` (if it's a server side lib) at the root of the app to download the files from public repos, and then  execute `grunt` to compile those assets on single files (lib.js, min.js) (not commited either for same reasons), it makes page requests to the server more efficient.


# Installation

## Manually
You will have to install:
* git
    sudo apt-get install git
* nodejs and npm
    sudo apt-get install nodejs nodejs-dev npm
* [bower](http://bower.io/)
    npm install -g bower
* [mongodb](http://docs.mongodb.org/manual/administration/install-on-linux/)
* python 2.7


## Provisionning with Vagrant and Ansible
/!\ work in progress
see 'provisionning branch' (unfortunately this branch is mangled with work in progress on social logins...)
see https://github.com/victorkane/simple-mean-vm

## Build

When every piece of software you need to launch the app are installed and configured, to launch the app you will have to:

* download external librairies
    sudo npm install
    bower install
for the different apps (api, server, server_admin, scheduler)

* build assets
    grunt
for the different apps (api, server, server_admin, scheduler)

* Launch the different instances in the right order
    node api/app.js
    node server/app.js
    node server_admin/app.js
    node scheduler/app.js

NB:
* the default environment is dev. If you want to instantiate the app with others parameters, you can use another `NODE_ENV` and launch the app with this environment : `NODE_ENV=production node app.js`
* api must be launched before server and server_admin.



## Deploy and launch with PM2
[PM2](https://github.com/Unitech/pm2) is a production process manager for Node.js applications. It allows you to :
* keep applications alive forever,
* reload them without downtime,
* load balance multiple instances of the same app
* send monitoring metrics to a very [neat dashboard](https://app.keymetrics.io/#/login)  
* and facilitate common system admin tasks, like the very useful `deploy` command
* find more useful stuff on [github project page](https://github.com/Unitech/pm2).

### It needs a global installation

    sudo npm install -g pm2

### Development mode

    # Start your application in development mode
    # = Print the logs and restart on file change
    $ pm2-dev run my-app.js


### Deployment one-liner
In the future, we will be able to deploy on production from your local machine with this simple one liner:

    pm2 deploy ecosystem.json production


For now, we need to log on the server to launch those two commands:

    sudo su
    sh /home/scoragora/brazuca/deploy_scoragora.pm2.sh

It is very important to execute them as the root user (hence `sudo su`).

### Monitoring server & app metrics
See [Scoragora's neat dashboard](https://app.keymetrics.io/#/login) on Keymetrics.



## Testing
We use [mocha](http://mochajs.org/) to run our test. For convenience, the command to execute the tests are icorporated in the `package.json` of each app so that a simple

    npm test

can launch the full test suite for this app.

So far we try to implement tests for critical backend operations (api app only). Of course, more tests are always welcome...

It would be nice to implement some coverage tests (with (istambul)[https://gotwarlost.github.io/istanbul/] ?) in the future.  

Another refinement woud be to implement end to end tests for the `server` app. [Protractor](https://angular.github.io/protractor/#/) seems to be the common standard for angular apps.



## Continuous integration ('CI') with  Codeship
Scoragora has an account on codeship used for automated testing. It is conceivable to evolve to continuous deployment.



# Workflow
## Git flow
We will try and use the very standard [githb flow](https://guides.github.com/introduction/flow/), with 4 types of branches:
* *master*: always stable, and deployed on production.
* *dev* :  branch that is regularly merge on master when everything looks ok. This is the branch that agglomerate all the work of the feature branches until next merge on master.
* *feature branches*: as soon as you work on a new experiment/feature/fix ... etc : you start with a new branch (usually from the dev branch), you push your commits, and as soon as you want to discuss your work with another member of the team, your trigger a *pull request*. It can be done very early to troubleshoot work in progress with team members. Github provides a very neat interface to do that, and it's a fantstic discipline to foster mutual improvement.
* *hotfix branches*: when a fix should be done on production (embarrassing typo, error 500....) you can then create a branch from master, do your commits and merge it back on master.


## Report issues on Github issues tool


# Style guide
We found a pretty neat stye guide on [airbnb's javascript repo](https://github.com/airbnb/javascript). It woud be useful for everybodys efficiency's sake that we could write js the same way.

## Linters

In order to enforce this coding style and improve code quality with automatic tools, this repo is configured to use [eslint](http://eslint.org/docs/user-guide/getting-started). To use it, you first have to install it (global installations provides easier integration) :

```
    npm install -g eslint
```

Eslint will check the js files against a set of rules that we can customize as we want (configure your editor to lint on the fly to have immediate feedback when you code). Those rules are defined in files named `.eslintrc` at the root of every module (all our modules doesn't need the same rules.).

One of the most famous style guide is [airbnb's](https://github.com/airbnb/javascript). Our `.eslintrc` file has been imported from this ruleset (for es5).

## Editors to the rescue
Tweaking the config of your text editor can make the adoption of multiple practices totally painless.
To share some of these configs efficiently across editors, there is a convenient standard named [editorconfig](http://editorconfig.org/) used to specify the conventions for the project (tabs vs spaces, indent size, trim trailing whitespaces... ). That's the reason why there is a `.editorconfig` file at the root of the project, which can (and should) be coupled with [plugins](http://editorconfig.org/#download) on all major editors to import those settings.

You should also configure your text editor to use your linter on the fly, it's awesome (lot of ressources on the web about it. Google it!).




# Troubleshooting

## Strategy

- check the monitoring services (google analytics, pm2...) but don't trust them
- check that all services are running (nginx, nodejs apps, mongo)
- then check their logs
- hunt for the bottlenecks



## Big picture

```
          +--------------------------------------------------------------------------------------------------+
          |UBUNTU BOX                                                                                        |
          |                                                                          +-------------------+   |
          |                        +-------+---------------------------+             |                   |   |
          |                 +------>NODEJS - "API"                     +-------------+ MONGO - database  |   |
          |                 |      |backend for the front-end apps     |             |                   |   |
          |                 |      +-----------------------------------+             |                   |   |
          |                 |                                                        +-------------------+   |
          |                 |                                                                                |
          | +-------+-------+      +-------+---------------------------+                                     |
          |F|       |              |NODEJS - "server"                  |                                     |
          |I|       +--------------+sends the angular files            |                                     |
          |R|       |              |to the clients                     |                                     |
          |E|       |              +-----------------------------------+                                     |
          | |       |                                                                                        |
WWW +-------> NGINX |                                                                                        |
          | |       |              +-------+---------------------------+                                     |
          |W|       +--------------+NODEJS - "scheduler"               |                                     |
          |A|       |              |orchestrate background calculations|                                     |
          |L|       |              |(points, rankings...)              |                                     |
          |L|       |              +-----------------------------------+                                     |
          | +-------+--------+                                                                               |
          |                  |                                                                               |
          |                  |     +-------+---------------------------+                                     |
          |                  +----->NODEJS - "server_admin"            |                                     |
          |                        |sends the angular files            |                                     |
          |                        |to the clients                     |                                     |
          |                        +-----------------------------------+                                     |
          |                                                                                                  |
          |                                                                                                  |
          +--------------------------------------------------------------------------------------------------+
```

(use http://asciiflow.com/ for updates)


## OS

### CPU and RAM

use `htop`

![image](/uploads/987ec6a034297761b63195eea6261c69/image.png)



### Disk usage

use `ncdu` to investigate.

        sudo apt install ncdu

![image](/uploads/8d5649e4ed0368b8cafe816282ebe595/image.png)

The application can be run from any directory, including the root directory. The output will tell you the disk usage of each file and directory, with the ability to drill down into any listed directory.


## Nginx

- check the config file with `sudo nginx -t`
- check the certificates are ok (letsencrypt auto renew should be ON)
-

## NODEJS

- check apps are running : `pm2 list` should display green buttons  :

![image](/uploads/da36cf8347462fa533722711ce09b4f4/image.png)

- check realtime logs : `pm2 logs`
