// empty all collections except system ones
var collections = db.getCollectionNames();
for(var i in collections) {
  var collection = collections[i];
  if (!collection.match(/^system.(.*)$/)) {
    db[collection].remove({});
  }
}

/***************************************************************************
****
****								USERS
****
**************************************************************************/

// insert user Etienne
db.user.insert({
  _id: ObjectId("53668055a5746578545df2d8"),
  createdAt: ISODate("2014-05-04T18:00:53.000Z"),
  forecasts: [{
    forecast: ObjectId("53000da4d94a6de820acf005")
  }],
  leagues: [{
    league: ObjectId("5300a28935b003712ba1adac")
  }],
  lng: "en",
  login: {
    local: {
      email: "etienne@scoragora.com",
      password: "$2a$10$d4GMqC6TDfEiY2eJB2ffv.JfOMINOuVZKu9nKxVToxhkQ3pgtIpzS",
      pictureURL: "/images/profile_picture/trix_flix08.jpg",
      pseudo: "etienne"
    },
    google: {
      id: "111372465408075365270",
      token: "ya29.LgA7DvfgDJNiqB8AAAD9lPVaI7ZL0u5nBrHpCvKWWaCIzuFNIh0Mgu2FRfJosQ",
      displayName: "Fabrisio K",
      email: "koutcho.fabrice@gmail.com",
      pictureURL: "https://lh5.googleusercontent.com/-sstGsFFvOZw/AAAAAAAAAAI/AAAAAAAAAfI/KTAwJX5G7Ds/photo.jpg"
    }
  },
  pictureMode: "local",
  points: 0,
  email_settings: { forecast_management: { new_matches: true, reminder_24h: true } }
});

// insert user Edouard
db.user.insert({
  _id: ObjectId("536e20d65ce44a661fde090a"),
  createdAt: ISODate("2014-05-10T12:51:34.000Z"),
  forecasts: [],
  leagues: [],
  lng: "en",
  login: {
    local: {
      email: "edouard@scoragora.com",
      password: "$2a$10$4aUCK09kaCjLRIxekFJpyuFT704J0jWoKASF5aRED67GArmcfJYDe",
      pictureURL: "/images/profile_picture/trix_flix08.jpg",
      pseudo: "edouard"
    }
  },
  pictureMode: "local",
  points: 0,
  email_settings: { forecast_management: { new_matches: true, reminder_24h: true } }
});

// insert user Evil Eve (external)
db.user.insert({
  _id: ObjectId("53c10d0bb3263bdf3a79e09a"),
  created_at: ISODate("2014-07-12T10:25:15.843Z"),
  forecasts: [],
  leagues: [],
  lng: "en",
  login: {
    local: {
      email: "other@scoragora.com",
      password: "$2a$10$4aUCK09kaCjLRIxekFJpyuFT704J0jWoKASF5aRED67GArmcfJYDe",
      pictureURL: "/images/profile_picture/trix_flix08.jpg",
      pseudo: "other"
    }
  },
  pictureMode: "local",
  points: 0,
  email_settings: { forecast_management: { new_matches: true, reminder_24h: true } }
});

/***************************************************************************
****
****							COMPETITIONS
****
**************************************************************************/
db.competition.insert({
  _id: ObjectId("52ea654cf7073b5b2f42bc27"),
  config: {
    "type": "cup",
    "cup": {
      "elimination": {
        "round_max": 8,
        "two_legged": false,
        "third_place": true
      },
      "group": {
        "number": 8,
        "capacity": 4,
        "two_legged": false,
        "qualified_number": 2
      }
    }
  },
  matches: [{
    match: ObjectId("52ea654cf7073b5b2f42bc50"),
    group: "A",
    day: 1
  },{
    match: ObjectId("53025e2e632b0d1402a3cb15"),
  },{
    match: ObjectId("52fd4eda6b891e6702ea3490"),
  },{
    match: ObjectId("53025e2e632b0d1402a3caf7"),
  }],
  name: "world_cup",
  progress: {
    now: 700,
    total: 2590
  },
  start_date: new Date(),
  status: "public",
  sport: "football",
  teams: [{
    team: ObjectId("5499a23357465b41c09d497d")
  }, {
    team: ObjectId("5499a23357465b41c09d4982")
  }, {
    team: ObjectId("5499a23357465b41c09d498c")
  }, {
    team: ObjectId("5499a23357465b41c09d497e")
  }],
  year: "2014"
});

/***************************************************************************
****
****								MATCHES
****
**************************************************************************/

// fullfil Matches
db.match.insert({
  _id: ObjectId("52fd4eda6b891e6702ea3490"),
  awayteam: ObjectId("5499a23357465b41c09d4982"),
  competition: ObjectId("52ea654cf7073b5b2f42bc27"),
  datetime: ISODate("2014-04-29T18:45:00.000Z"),
  hometeam: ObjectId("5499a23357465b41c09d497d"),
  result: {
    hometeam_score: 0,
    awayteam_score: 1
  },
  forecast: {

  },
  automation: {
    eurosport: "link"
  },
  status: "closed"
});
db.match.insert({
  _id: ObjectId("53025e2e632b0d1402a3caf7"),
  awayteam: ObjectId("545e24bf51f8a75922e34a1d"),
  competition: ObjectId("52ea654cf7073b5b2f42bc27"),
  datetime: ISODate("2014-06-19T19:00:00.000Z"),
  hometeam: ObjectId("545e24bf51f8a75922e34a29"),
  result: {

  },
  forecast: {

  },
  automation: {
    eurosport: "link"
  },
  status: "closed"
});

// future match
var current_date = new Date();
var d2 = new Date(current_date);
d2.setHours(current_date.getHours() + 1);
db.match.insert({
  _id: ObjectId("53025e2e632b0d1402a3cb15"),
  sport: "football",
  awayteam: ObjectId("545e24bf51f8a75922e34a25"),
  competition: ObjectId("52ea654cf7073b5b2f42bc27"),
  datetime: d2,
  hometeam: ObjectId("545e24bf51f8a75922e34a14"),
  result: {
    hometeam_score: 0,
    awayteam_score: 1
  },
  forecast: {

  },
  automation: {
    eurosport: "link"
  },
  status: "active"
});

/***************************************************************************
****
****								TEAMS
****
**************************************************************************/
db.team.insert({
  _id: ObjectId("5499a23357465b41c09d497d"),
  label: "Chelsea"
});
db.team.insert({
  _id: ObjectId("5499a23357465b41c09d4982"),
  label: "FC Barcelona"
});
db.team.insert({
  _id: ObjectId("5499a23357465b41c09d498c"),
  label: "Brazil"
});
db.team.insert({
  _id: ObjectId("5499a23357465b41c09d497e"),
  label: "Bayern Munchen"
});

/***************************************************************************
****
****								LEAGUES
****
**************************************************************************/
// a league without etienne and edouard, just evil eve
db.league.insert({
  _id: ObjectId("53bcf56ab3263bdf3a79daeb"),
  name:"the other league",
  url: "the.other.league",
  token:"test",
  enterprise:"BigCo Inc.",
  competition:ObjectId("52ea654cf7073b5b2f42bc27"),
  createdAt:ISODate("2014-07-09T07:55:22.418Z"),
  captains:[ObjectId("53c10d0bb3263bdf3a79e09a")],
  members:[
    {
      user: ObjectId("53c10d0bb3263bdf3a79e09a"),
      points: 810,
      rank: 1
    }
  ]
});

// another league without etienne and edouard, just evil eve
db.league.insert({
  _id: ObjectId("53bcf56ab3263bdf3a79da34"),
  name:"the other league 2",
  url: "the.other.league.2",
  token:"test",
  competition:ObjectId("52ea654cf7073b5b2f42bc27"),
  createdAt:ISODate("2014-07-09T07:55:22.418Z"),
  captains:[ObjectId("53c10d0bb3263bdf3a79e09a")],
  members:[
    {
      user: ObjectId("53c10d0bb3263bdf3a79e09a"),
      points: 810,
      rank: 1
    }
  ]
});

// a league with etienne and edouard (captain),
db.league.insert({
  _id: ObjectId("5300a28935b003712ba1adac"),
  name:"my funny league",
  url: "my.funny.league",
  competition:ObjectId("52ea654cf7073b5b2f42bc27"),
  createdAt:ISODate("2014-02-16T11:35:37.000Z"),
  captains:[ObjectId("536e20d65ce44a661fde090a")],
  members:[
    {
      user: ObjectId("536e20d65ce44a661fde090a"),
      points: 810,
      rank: 1
    },
    {
      user: ObjectId("53668055a5746578545df2d8"),
      points: 727,
      rank: 2
    }
  ]
});


// a league with etienne (captain) and edouard
db.league.insert({
  _id: ObjectId("7777a28935b003712ba1bbbb"),
  name:"my even funnier league",
  url: "my.even.funnier.league",
  competition:ObjectId("52ea654cf7073b5b2f42bc27"),
  createdAt:ISODate("2014-02-16T11:35:37.000Z"),
  captains:[ObjectId("53668055a5746578545df2d8")],
  members:[
    {
      user: ObjectId("536e20d65ce44a661fde090a"),
      points: 810,
      rank: 1
    },
    {
      user: ObjectId("53668055a5746578545df2d8"),
      points: 727,
      rank: 2
    }
  ]
});

/***************************************************************************
****
****								FORECASTS
****
**************************************************************************/
// insert forecast for Etienne
db.forecast.insert({
  _id: ObjectId("53000da4d94a6de820acf005"),
  competition: ObjectId("52ea654cf7073b5b2f42bc27"),
  user: ObjectId("53668055a5746578545df2d8"),
  points: 727,
  matches: [{
    match: ObjectId("52fd4eda6b891e6702ea3490"),
    points: 0,
    bonus: [],
    forecast: {
      hometeam: {
        score: 1
      },
      awayteam: {
        score: 2
      }
    }
  }, {
    match: ObjectId("53025e2e632b0d1402a3caf7"),
    points: 0,
    bonus: [],
    forecast: {
      hometeam: {
        score: 1
      },
      awayteam: {
        score: 0
      }
    }
  }, {
    match: ObjectId("53025e2e632b0d1402a3cb15"),
    points: 0,
    bonus: [],
    forecast: {
      hometeam: {
        score: 2
      },
      awayteam: {
        score: 3
      }
    }
  }],
  status: "active"
});

// insert forecast for Edouard
db.forecast.insert({
  _id: ObjectId("535c563330ecdaa03335e361"),
  competition: ObjectId("52ea654cf7073b5b2f42bc27"),
  user: ObjectId("536e20d65ce44a661fde090a"),
  points: 827,
  matches: [{
    match: ObjectId("52fd4eda6b891e6702ea3490"),
    points: 0,
    bonus: [],
    forecast: {
      hometeam: {
        score: 1
      },
      awayteam: {
        score: 2
      }
    }
  }, {
    match: ObjectId("53025e2e632b0d1402a3caf7"),
    points: 0,
    bonus: [],
    forecast: {
      hometeam: {
        score: 1
      },
      awayteam: {
        score: 0
      }
    }
  }, {
    match: ObjectId("53025e2e632b0d1402a3cb15"),
    points: 0,
    bonus: [],
    forecast: {
      hometeam: {
        score: 2
      },
      awayteam: {
        score: 3
      }
    }
  }],
  status: "active"
});



/***************************************************************************
****
****                               MESSAGES
****
**************************************************************************/

// insert forecast for Edouard
db.messageBucket.insert({
  _id: ObjectId("535c563330ecdaa03335e362"),
  league: ObjectId("5300a28935b003712ba1adac"),
  count: 1,
  messages: [
    {
      sender: ObjectId("536e20d65ce44a661fde090a"),
      datetime: ISODate("2014-02-16T10:35:37.000Z"),
      content: "Hello world !"
    }
  ],
  updatedAt: ISODate("2014-02-16T11:35:37.000Z")
});

db.messageBucket.insert({
  _id: ObjectId("535c563330ecdaa03335e363"),
  league: ObjectId("5300a28935b003712ba1adac"),
  count: 1,
  messages: [
    {
      sender: ObjectId("53668055a5746578545df2d8"),
      datetime: ISODate("2014-02-16T10:36:37.000Z"),
      content: "Hello world too !"
    }
  ],
  updatedAt: ISODate("2014-02-16T11:36:37.000Z")
});

db.messageBucket.insert({
  _id: ObjectId("535c563330ecdaa03335e364"),
  league: ObjectId("5300a28935b003712ba1adac"),
  count: 1,
  messages: [
    {
      sender: ObjectId("536e20d65ce44a661fde090a"),
      datetime: ISODate("2014-02-16T11:37:37.000Z"),
      content: "Scoragora is sooo cooool"
    }
  ],
  updatedAt: ISODate("2014-02-16T11:37:37.000Z")
});

db.messageBucket.insert({
  _id: ObjectId("535c563330ecdaa03335e366"),
  league: ObjectId("5300a28935b003712ba1adac"),
  count: 1,
  messages: [
    {
      sender: ObjectId("53668055a5746578545df2d8"),
      datetime: ISODate("2014-02-16T11:38:37.000Z"),
      content: "Hell Yeah ! Lorem ipsum dolor sit amet jsqd qd azeazpe"
    }
  ],
  updatedAt: ISODate("2014-02-16T11:38:37.000Z")
});
