var mongoose = require('mongoose');

var translator = require("../tools/translator.js");
var schema = require("../../config/models/tokens.js");

var TokenSchema = translator.translate(schema, mongoose);

module.exports = mongoose.model('Token', TokenSchema, 'token');
