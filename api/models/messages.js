var mongoose = require('mongoose');

var MessageSchema = new mongoose.Schema({
  sender: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'User'
  },
  datetime: Date,
  content: String
});

var MessageBucketSchema = new mongoose.Schema({
  league: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'League'
  },
  count: Number,
  messages: [MessageSchema],
  updatedAt: Date
});

module.exports = mongoose.model('MessageBucket', MessageBucketSchema, 'messageBucket');
