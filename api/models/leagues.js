const mongoose = require('mongoose');

const translator = require('../tools/translator.js');
const schema = require('../../config/models/leagues.js');

const LeagueSchema = translator.translate(schema, mongoose);

LeagueSchema.methods.isPremium = function () {
  return this.premium && this.premium.validated;
};

module.exports = mongoose.model('League', LeagueSchema, 'league');
