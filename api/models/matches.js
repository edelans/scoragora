var mongoose = require("mongoose");

var translator = require("../tools/translator.js");
var schema = require("../../config/models/matches.js");

var MatchSchema = translator.translate(schema, mongoose);

module.exports = mongoose.model('Match', MatchSchema, 'match');
