const mongoose = require('mongoose');
const bcrypt = require('bcrypt-nodejs');

const translator = require('../tools/translator.js');
const schema = require('../../config/models/users.js');

const UserSchema = translator.translate(schema, mongoose);

const SALT_WORK_FACTOR = 10;

UserSchema.pre('save', function (next) {
  const user = this;

  // only hash the password if it has been modified (or is new)
  if (!user.isModified('login.local.password')) {
    return next();
  }

  // generate a salt
  try {
    const salt = bcrypt.genSaltSync(SALT_WORK_FACTOR);

    // hash the password using our new salt
    const hash = bcrypt.hashSync(user.login.local.password, salt);
    // override the cleartext password with the hashed one
    user.login.local.password = hash;
    return next();
  } catch (e) {
    return next(e);
  }
});

UserSchema.methods.validPassword = function (candidatePassword) {
  try {
    return bcrypt.compareSync(candidatePassword, this.login.local.password);
  } catch (e) {
    return false;
  }
};

UserSchema.methods.displayName = function () {
  return this.login.local.pseudo || this.login.facebook.displayName
  || this.login.google.displayName || this.login.twitter.displayName;
};

UserSchema.methods.displayEmail = function () {
  return this.login.local.email || this.login.facebook.email
  || this.login.google.email || this.login.twitter.email;
};

UserSchema.methods.pictureURL = function () {
  switch (this.pictureMode) {
    case 'local':
      if (this.login.local && this.login.local.pictureURL) {
        return this.login.local.pictureURL;
      }
      break;
    case 'facebook':
      if (this.login.facebook) {
        return this.login.facebook.pictureURL;
      }
      break;
    case 'google':
      if (this.login.google) {
        return this.login.google.pictureURL;
      }
      break;
    case 'twitter':
      if (this.login.twitter) {
        return this.login.twitter.pictureURL;
      }
      break;
    default:
      break;
  }
  return '/img/profile_picture/footix98.jpg';
};

module.exports = mongoose.model('User', UserSchema, 'user');
