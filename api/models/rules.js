var mongoose = require('mongoose');

var translator = require("../tools/translator.js");
var schema = require("../../config/models/rules.js");

var RuleSchema = translator.translate(schema, mongoose);

module.exports = mongoose.model('Rule', RuleSchema, 'rule');
