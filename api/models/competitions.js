var mongoose = require("mongoose");

var translator = require("../tools/translator.js");
var schema = require("../../config/models/competitions.js");

var CompetitionSchema = translator.translate(schema, mongoose);

module.exports = mongoose.model('Competition', CompetitionSchema, 'competition');
