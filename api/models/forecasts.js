var mongoose = require('mongoose');

var MatchForecastSchema = new mongoose.Schema({
  match: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Match'
  },
  points: Number,
  bonus: [String], // goal_diff | match_result | team_score | team_score_double
  forecast: mongoose.Schema.Types.Mixed // {hometeam_score: 2, awayteam_score: 2, final_direction: 'away'}
});

var ForecastSchema = new mongoose.Schema({
  createdAt: Date,
  competition: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Competition'
  },
  user: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'User'
  },
  points: {
    type: Number,
    default: 0
  },
  matches: [MatchForecastSchema],
  status: {
    type: String,
    enum: ["active", "archived"] // active when you can still update it. archived otherwise (=the match has started)
  }
});

module.exports = mongoose.model('Forecast', ForecastSchema, 'forecast');
