const winston = require('winston');
const jwt = require('jwt-simple');
const moment = require('moment');
const socketIo = require('socket.io');

const cfg = require('../../config');

const checker = require('../routes/checker.js');
const SvcMessage = require('../services/svc_message.js');

let io;

const authorize = () => {
  return (socket, next) => {
    if (!socket || !socket.request) {
      winston.error('Socket IO | socket request should be provided');
      return;
    }
    const req = socket.request;
    if (!req._query || !req._query.token) {
      return;
    }
    const token = req._query.token;

    let payload = null;
    try {
      payload = jwt.decode(token, cfg.secret);
    } catch (err) {
      winston.error('Checker | error catched when decoding token : ' + err);
      return;
    }
    if (payload.exp <= moment().unix()) {
      return;
    }
    req.user = payload.sub;
    next();
  };
};

/*
* Important notice:
* This socket is not secured. it should only be used to send data from server to client.
* Not the opposite way
*/

exports.initSocketIO = (httpInstance) => {
  io = socketIo.listen(httpInstance).of('/api');

  process.env.DEBUG = 'socket.io:socket';

  io.use(authorize());

  io.on('connection', (socket) => {
    winston.info('socket IO | socket connection');

    // socket.emit('league_request');

    socket.on('join_league', (data) => {
      winston.info('Socket IO | receiving join league ' + data.league);
      checker.checkSocketLeagueMembership(socket, { url: data.league }, (user, league) => {
        socket.room = league.id;
        socket.join(socket.room);
      });
    });

    socket.on('send_message', (data) => {
      checker.checkSocketLeagueMembership(socket, { id: data.league }, async (user, league) => {
        await SvcMessage.insertMessage(league.id, data);
        winston.info('Socket IO | Broadcasting message to room ' + data.league);
        io.to(data.league).emit('receive_message', data);
      });
    });

    socket.on('disconnect', () => {
      checker.checkSocketLeagueMembership(socket, { id: socket.room }, () => {
        // Here we can store the information
        winston.info('Socket IO | deconnection from ' + socket.room);
      });
    });
  });
};
