exports.pmap = async (array, task, concurrency = Infinity) => {
  if (array.length === 0) {
    return;
  }
  await new Promise((resolve, reject) => {
    let index = 0;
    let completed = 0;
    const limit = Math.min(array.length, concurrency);
    const handleNext = async () => {
      if (index < array.length) {
        index += 1;
        await task(array[index - 1], index - 1);
        completed += 1;
        await handleNext();
      } else if (completed === array.length) {
        resolve();
      }
    };
    while (index < limit) {
      handleNext().catch(reject);
    }
  });
};
