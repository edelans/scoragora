const convertRec = (mongoose, schema) => {
	if (schema === null) {
		return null;
	}
	// String ''
	if (typeof schema == 'string') {
		if (schema == 'objectid') {
			return mongoose.Schema.Types.ObjectId;
		}
		if (schema === 'mixed') {
			return mongoose.Schema.Types.Mixed;
		}
		return schema;
	}
	//
	if (typeof schema !== 'object') {
		return schema;
	}
	// Array []
	if (schema.constructor === Array) {
		const result = [];
		schema.forEach((item) => {
			result.push(convertRec(mongoose, item));
		});
		return result;
	}
	// Object {}
	if (schema instanceof RegExp) {
		return schema;
	}
	const result = {};
	Object.keys(schema).forEach((key) => {
		result[key] = convertRec(mongoose, schema[key]);
	});
	return result;
};

exports.convertRec = convertRec;

/**
 * Transform a js schema into a schema for mongoose model
 * @mongoose connection
 * @schema js schema
 * returns mongoose schema
 */
exports.translate = (schema, mongoose) => {
  return mongoose.Schema(convertRec(mongoose, schema));
};
