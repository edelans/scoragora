API Brazuca
===========

## some help for mongo shell queries 
Simple usecase: extract all emails for a given competition and a given language.

### fr
copy those lines in a file called `rwc15_fr_emails.js`
	var userIds = db.forecast.distinct("user", {"competition": ObjectId("55cbda012fbb96f443928bfa")});

	var frenchLocalEmails = db.user.distinct("login.local.email", {_id: {$in: userIds}, lng: "fr"}); 
	var frenchFBEmails = db.user.distinct("login.facebook.email", {_id: {$in: userIds}, lng: "fr"}); 
	var frenchGEmails = db.user.distinct("login.google.email", {_id: {$in: userIds}, lng: "fr"}); 

	print(frenchLocalEmails);
	print(frenchFBEmails);
	print(frenchGEmails);

execute script on api db

	mongo api rwc15_fr_emails.js > rwc15_fr_emails.csv


#### en
copy those lines in a file called `rwc15_en_emails.js`
	var userIds = db.forecast.distinct("user", {"competition": ObjectId("55cbda012fbb96f443928bfa")});

	var englishLocalEmails = db.user.distinct("login.local.email", {_id: {$in: userIds}, lng: {$ne: "fr"}}); 
	var englishFBEmails = db.user.distinct("login.facebook.email", {_id: {$in: userIds}, lng: {$ne: "fr"}}); 
	var englishGEmails = db.user.distinct("login.google.email", {_id: {$in: userIds}, lng: {$ne: "fr"}});

	print(englishLocalEmails);
	print(englishFBEmails);
	print(englishGEmails);

execute script on api db

	mongo api rwc15_en_emails.js > rwc15_en_emails.csv


## documentation


Ajax :
	
POST : connexion via le formulaire, envoi du formulaire (login/password encrypté)
GET : 	connexion via Facebook, création de compte 	
GET : 	connexion via Google+, création de compte
GET : 	connexion via Twitter, création de compte

	
Register

Image
L’écran de création de compte permet à un utilisateur de se créer un compte via login/password. Il possède un formulaire pour le pseudo, le login, le mot de passe. Un bouton permet d’envoyer le formulaire. Un lien permet de retourner à la page de login.

Ajax :

POST : register via le formulaire, envoi du formulaire 	(pseudo/login/password)

API login
	GET /login : indique si l’utilisateur est connecté
	DELETE /login : déconnecte un utilisateur
	GET /login/facebook : lance une connexion via facebook
GET /login/facebook/callback : finalise la connexion via facebook
GET /login/facebook/link : link l’utilisateur courant à un compte facebook
DELETE /login/facebook/link : unlink l’utilisateur courant de son compte facebook
	GET /login/google : connecte l’utilisateur via google
	GET /login/twitter : connecte l’utilisateur via twitter
	GET /login/local?login=toto&password=$pwd : connecte l’utilisateur via local
	PUT /login/local : met à jour le mot de passe ou l’adresse email
POST /login/local : crée un compte pour l’utilisateur

DELETE /login : supprime le compte de l’utilisateur

	
Accueil (page User)
GET /users/:userID?profile=summary -- OK
retourne uniquement le username et l’avatar (lien)
GET /users/:userID/matches?limit=5&status=past  -- OK
récupérer les 5 derniers matchs qui ont eu lieu, et sur lesquels il pouvait parier
GET /users/:userID/matchforecast?limit=5&order=mostrecentfirst -- OK
récupérer les 5 prochains matchs qui vont avoir lieu, et sur lesquels il peut parier
GET /users/:userID/leagues?limit=5&order=lastactivefirst&unreadmsg=true -- OK sans unreadmsg
récupérer les 5 dernieres ligues actives, avec nom, lien vers la ligue, classement du joueur
GET /users/:userID/forecasts?status=active&summary=true -- OK
renvoie la liste des forecasts en cours (ex: un pour la CDM, un pour la Ligua, …)




Mes Ligues
GET /users/:userID/leagues
renvoie la liste des ligues du user, avec son classement au sein de la ligue, liste des capitaines

DELETE /users/:userID/leagues/:leagueID
si le user est bien capitaine, on ne fait rien.
si le user n’est pas capitaine, on l'enleve de la ligue

DELETE /leagues/:leagueID
si le user est capitaine, on delete la ligue -> on retire l’association vers tous les membres de la ligue et on supprime l’objet ligue.



Page de création d’une ligue
GET /competitions?status=open&summary=true
retourne id, nom, year, lien vers image ?

GET /leagues?checkavailability=true&name=<name>
retourne l’id de ligue si elle existe, et un array vide si non

POST /leagues
en donnée: un json avec le nom, l’id de la compet.
retourne: success/fail



Page d’une ligue
GET /leagues/:leagueID
récupere le nom, le ranking (id, rang, nb de pts), 

GET /leagues/:leagueID/members
retourne le username de tous les users, leurs avatars, etc.

GET /competitions/:competitionID?summary=true
retourne la progression, le nom, la date, points calculation… tout sauf les références aux autres objets

GET /competitions/:competitionID/matches?limit=5&status=past 
récupérer les 5 derniers matchs qui ont eu lieu, et sur lesquels il pouvait parier, populer les teams

GET /leagues/leagueID/messages?limit=2&skip=0[&timestamp=555555]
recupere les 2 derniers buckets de messages, en en passant zero.

pour les stats:
traitements coté client:
GET /leagues/leagueID/forecasts


pour un traitement coté serveur:
GET /leagues/leagueID/history?type=[ranking|points]&abscissa=[date|phase|day]&limit=[date|days]

GET /leagues/:leagueID/matches/matchID/forecasts
voir les forecast des autres joueurs de sa ligue, dispo qu’après le début du match.

GET /matches/matchID
recupere equipes, date, lieu ? , score, … populer les équipes.

GET /matches/matchID?filter=score
pour faire du live update

GET /rules/rulesID
a voir si on l’inclue ds la récupération du match ou pas...



Mes pronostics
GET /users/:userID/forecasts?status=active --OK


Page d’un prono
GET /forecasts/:forecastID -- OK
récupère les paris du joueur sur une competition

GET /competitions/competitionID/matches
récupère les matchs d’une competition (utilisée dans ligue)

PUT /forecasts/forecastID --OK
{[{match_id, score}]}



Settings
GET users/userID -- OK

PUT users/userID
mise a jour de:
picture mode
pseudo
password
picture
pour tout ce qui est préférences de mailing (reminders / rankings)

DELETE login/local
si le user est bien le bon user, on delete son profil après avoir vérifié qu’il ne laissait pas de ligue sans capitaine.

Link et unlinking: cf specs login

