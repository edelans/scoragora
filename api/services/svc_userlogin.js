var crypto = require('crypto');
var winston = require('winston');

var User = require('../models/users.js');

var SvcEmail = require('./svc_email.js');

const key = 'abkvruv8fegrf56nNhferJkgtrgoirt3';
let decipher;

/**
* Create user
* pseudo, email and password need to be given
* email has to be a valid email and must not be in database
*
*/
exports.createUser = async (user) => {
  winston.info('Svc User Login | Create new user with locale ' + user.lng);
  // Validate input
  if (!user.pseudo || user.pseudo === '') {
    // Pseudo missing
    return { status: 400, code: 40004, message: 'pseudo missing' };
  }
  if (!user.email || user.email === '') {
    // Email missing
    return { status: 400, code: 40005, message: 'email missing' };
  }
  if (!user.password || user.password === '') {
    // Password missing
    return { status: 400, code: 40006, message: 'password missing' };
  }
  // check lng
  if (user.lng && user.lng.match(/^fr/)) {
    user.lng = 'fr';
  } else {
    user.lng = 'en';
  }

  // Create user
  const existingAccount = await User.findOne({
    'login.local.email': user.email.toLowerCase()
  }).exec();
  if (existingAccount) {
    // An account with the same email already exist
    return { status: 400, code: 40009, message: 'email already used' };
  }
  const account = new User({
    expiry: new Date(),
    createdAt: new Date(),
    lng: user.lng,
    pictureMode: 'local',
    login: {
      local: {
        email: user.email.toLowerCase(),
        password: user.password,
        pseudo: user.pseudo
      }
    }
  });
  let savedAccount;
  try {
    savedAccount = await account.save();
  } catch (err) {
    if (err.errors && err.errors['login.local.email']) {
      return { status: 400, code: 40007, message: 'invalid email' };
    }
    if (err.errors && err.errors['login.local.pseudo']) {
      return { status: 400, code: 40008, message: 'invalid pseudo' };
    }
    winston.error('Svc User Login | error when creating user : ' + err);
    return { status: 500, code: 50000, message: 'internal server error' };
  }
  // Prepare token
  const cipher = crypto.createCipher('aes-256-cbc', key);
  let ciphered = cipher.update(JSON.stringify(savedAccount._id), 'utf8', 'hex');
  ciphered += cipher.final('hex');
  const link = 'http://www.scoragora.com/#/registration/confirm/' + ciphered;
  winston.info('Svc User Login | Sending email to ' + user.email);
  try {
    await SvcEmail.sendRegistrationValidation(user, link, { lng: savedAccount.lng });
    return { status: 200, message: 'account successfully created', email: user.email };
  } catch (e) {
    // remove the user
    await User.remove({ _id: savedAccount._id }).exec();
    return { status: 500, code: 50000, message: 'internal server error' };
  }
};

exports.getUserAndValidateRegistration = async (token) => {
  winston.info('Svc User Login | Validation registration for token ' + token);
  let userId;
  try {
    decipher = crypto.createDecipher('aes-256-cbc', key);
    let deciphered = decipher.update(token, 'hex', 'utf8');
    deciphered += decipher.final('utf8');
    userId = JSON.parse(deciphered);
  } catch (err) {
    winston.error('Svc User Login | error when deciphering token : ' + err);
    return { status: 400, code: 40019, message: 'invalid token' };
  }
  const user = await User.findOneAndUpdate({
    _id: userId,
    expiry: {
      $ne: null
    }
  }, {
    $unset: {
      expiry: ''
    }
  }).exec();
  if (user) {
    return { status: 200, message: 'registration successfully validated', user };
  }
  return { status: 400, code: 40018, message: 'no registration needed anymore' };
};
