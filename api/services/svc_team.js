const winston = require('winston');

const Team = require('../models/teams.js');
const Match = require('../models/matches.js');

/**
* Provide team details from the DB
*/
exports.getTeams = async (options) => {
  winston.info('Service Team | team detail retrieved for teams matching ' + options.search);
  const selector = {};
  if (options.search && options.search !== '') {
    selector.$or = [{
      label: new RegExp(options.search, 'i')
    }, {
      name: new RegExp(options.search, 'i')
    }];
  }
  if (options.sport && options.sport !== '') {
    selector.sport = options.sport;
  }
  const teams = await Team
  .find(selector)
  .skip(options.page * options.limit)
  .limit(options.limit)
  .sort({ name: 1 })
  .exec();
  if (options.count) {
    const count = await Team.count(selector).exec();
    return { status: 200, teams, count };
  }
  return teams;
};

exports.getTeam = async (teamId) => {
  winston.info('Service Team | team detail retrieved for id ' + teamId);
  return Team
  .findById(teamId)
  .exec();
};

/**
* Find team matches
*/
exports.getTeamMatches = async (teamId, options) => {
  winston.info('Service Team | team matches retrieved for id ' + teamId);
  const selector = {
    $or: [{
      awayteam: teamId
    }, {
      hometeam: teamId
    }]
  };
  const matches = await Match
  .find(selector)
  .skip(options.page * options.limit)
  .limit(options.limit)
  .sort({ datetime: -1 })
  .populate('hometeam awayteam')
  .exec();
  if (options.count) {
    const count = await Match.count(selector).exec();
    return { status: 200, matches, count };
  }
  return matches;
};

/**
* Create team
*/
exports.createTeam = async (team) => {
  winston.info('Service Team | team creation');
  const newTeam = new Team({
    label: team.label,
    name: team.name,
    sport: team.sport
  });

  await newTeam.save();
  return { status: 200, message: 'New team has been succesfully created' };
};

/**
* Update team info
*/
exports.updateTeamInfo = async (teamId, team) => {
  winston.info('Service Team | team info updated for id ' + teamId);
  await Team
  .update({
    _id: teamId
  }, {
    $set: {
      label: team.label,
      name: team.name,
      sport: team.sport
    }
  })
  .exec();
  // update the matches with this team
  winston.info('Service Team | updating matches for team ' + teamId);
  const updateReportHome = await Match.update({ hometeam: teamId }, {
    $set: { hometeamname: team.name }
  }, { multi: true }).exec();
  winston.debug('Service Team | ' + updateReportHome + ' home games updated');
  const updateReportAway = await Match.update({ awayteam: teamId }, {
    $set: { awayteamname: team.name }
  }, { multi: true }).exec();
  winston.debug('Service Team | ' + updateReportAway + ' away games updated');
  return { status: 200, message: 'Team info has been updated' };
};

/**
* Update team automation
*/
exports.updateTeamAutomation = async (teamId, automation) => {
  winston.info('Service Team | team automation updated for id ' + teamId);
  await Team
  .update({
    _id: teamId
  }, {
    $set: {
      automation: automation
    }
  })
  .exec();
  return { status: 200, message: 'Team automation has been updated' };
};
