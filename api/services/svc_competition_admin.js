/**
* This file contains methods only called from an admin route
*/

const winston = require('winston');
const mongoose = require('mongoose');

const APIException = require('../routes/APIException.js');
const iterator = require('../tools/iterator.js');

const Competition = require('../models/competitions.js');
const Match = require('../models/matches.js');
const League = require('../models/leagues.js');
const Rule = require('../models/rules.js');
const User = require('../models/users.js');
const Forecast = require('../models/forecasts.js');

const CompetitionCache = require('./cache/competition.js');


/**
* Provide competition record
*/
exports.getCompetitionRecord = async (competitionId) => {
  winston.info('Service Competition Admin | Competition record retrieved for competition :' + competitionId);
  // then target the db
  const competition = await Competition
  .findById(competitionId)
  .exec();
  if (competition) {
    return competition;
  }
  winston.info('Service Competition Admin | no competition found for id ' + competitionId);
  throw new APIException('Competition not found', 400);
};

/**
* Provide competition matches details from the DB
* @competition_id competition id
* @next callback
*/
exports.getCompetitionMatchRecords = async (competitionId) => {
  winston.info('Service Competition Admin | Matches retrieved for competition :' + competitionId);
  const selector = { competition: competitionId };
  const matches = await Match
  .find(selector)
  .exec();
  if (matches.length === 0) {
    // check if the competition exists
    const competition = await Competition
    .findById(competitionId)
    .exec();
    if (!competition) {
      throw new APIException('Competition not found', 400);
    }
  }
  return matches;
};

/**
* Provide competition teams details from the DB
* @competition_id competition id
* @next callback
*/
exports.getCompetitionTeamRecords = async (competitionId) => {
  winston.info('Service Competition Admin | retrieve teams for competition ' + competitionId);
  const competition = await Competition
  .findById(competitionId)
  .populate('teams.team')
  .exec();
  if (competition) {
    return competition.teams;
  }
  winston.info('Service Competition Admin | no competition found for id ' + competitionId);
  throw new APIException('Competition not found', 400);
};

/**
* Update competition status
*/
exports.updateCompetitionStatus = async (competitionId, status) => {
  winston.info('Service Competition Admin | Updating status to ' + status + ' for competition ' + competitionId);
  if (status !== 'private' && status !== 'public' && status !== 'archived') {
    return { status: 400, message: 'invalid status' };
  }
  const updateReport = await Competition
  .update({ _id: competitionId }, {
    $set: { status }
  }).exec();
  if (updateReport) {
    // Remove competition from the cache
    CompetitionCache.flush(competitionId);
    return { status: 200, message: 'competition status has been successfully updated' };
  }
  return { status: 400, message: 'Competition not found' };
};

/**
* Update competition automation
*/
exports.updateCompetitionAutomation = async (competitionId, automation) => {
  winston.info('Service Competition Admin | Updating automation for competition ' + competitionId);
  // TODO Check the automation
  const updateReport = await Competition
  .update({
    _id: competitionId
  }, { $set: { automation } })
  .exec();
  if (updateReport) {
    // Competition automation is not cached so no need to update the cache
    return { status: 200, message: 'competition automation has been successfully updated' };
  }
  return { status: 200, message: 'no competition found' };
};

/**
* Add competition team
*/
exports.addCompetitionTeam = async (competitionId, team) => {
  winston.info('Service Competition Admin | Addition of team ' + team + ' for competition ' + competitionId);
  const competition = await Competition
  .findOne({ _id: competitionId, 'teams.team': team })
  .exec();
  if (competition) {
    return { status: 200, message: 'no update needed' };
  }
  await Competition
  .update({ _id: competitionId }, {
    $push: { teams: { team } }
  }).exec();
  // No competition cache update as teams are not cached
  return { status: 200, message: 'competition team has been added' };
};

/**
* Delete a competition
* This is only possible if no league uses it
*/
exports.deleteCompetition = async (competitionId) => {
  winston.info('Service Competition Admin | Deleting competition ' + competitionId);
  // check if a league is linked to this competition
  const league = await League.findOne({
    competition: competitionId
  }).exec();
  if (league) {
    return { status: 400, message: 'impossible to delete competition' };
  }
  // remove all competition matches
  await Match.remove({
    competition: competitionId
  }).exec();
  await Competition.remove({
    _id: competitionId
  }).exec();
  // Remove the competition from the cache
  CompetitionCache.flush(competitionId);
  return { status: 200, message: 'competition has been successfully deleted' };
};

/**
* Create a competition
*/
exports.createCompetition = async (competition) => {
  winston.info('Service Competition Admin | Creating new competition');
  // Check the input  TODO: improvement needed
  if (!competition.name) {
    return { status: 400, message: 'name missing' };
  }
  if (competition.year == null) {
    return { status: 400, message: 'year missing' };
  }
  if (!competition.sport) {
    return { status: 400, message: 'sport missing' };
  }
  if (!competition.config) {
    return { status: 400, message: 'config missing' };
  }
  if (!competition.config.type) {
    return { status: 400, message: 'type missing' };
  }
  if (competition.config.type === 'cup') {
    if (!competition.config.cup) {
      return { status: 400, message: 'cup missing' };
    }
  } else if (competition.config.type === 'championship') {
    if (!competition.config.championship) {
      return { status: 400, message: 'championship missing' };
    }
  } else {
    return { status: 400, message: 'invalid type' };
  }
  // generate matches
  const GROUP_NAME = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
  competition.matches = [];
  if (competition.config.type === 'cup') {
    const group = competition.config.cup.group;
    if (group && group.number > 0 && group.capacity > 0) {
      const dayNumber = (group.two_legged ? 2 : 1) * ((2 * Math.ceil(group.capacity / 2)) - 1);
      const matchesPerDay = Math.floor(group.capacity / 2);
      for (let i = 0; i < group.number; i += 1) {
        for (let day = 0; day < dayNumber; day += 1) {
          for (let j = matchesPerDay; j > 0; j -= 1) {
            competition.matches.push({ group: GROUP_NAME.charAt(i), day });
          }
        }
      }
    }
    const elimination = competition.config.cup.elimination;
    if (elimination && elimination.round_max > 0) {
      competition.matches.push({ round: 1 });
      if (elimination.third_place) {
        competition.matches.push({ round: 3 });
      }
      for (let round = 2; round <= elimination.round_max; round *= 2) {
        for (let i = round; i > 0; i -= 1) {
          if (elimination.two_legged) {
            competition.matches.push({ round, day: 1 });
            competition.matches.push({ round, day: 2 });
          } else {
            competition.matches.push({ round });
          }
        }
      }
    }
  }
  if (competition.config.type === 'championship') {
    console.log(competition.config);
    const day = competition.config.championship.day;
    if (day && day.capacity > 0) {
      const dayNumber = (day.two_legged ? 2 : 1) * ((2 * Math.ceil(day.capacity / 2)) - 1);
      const matchesPerDay = Math.floor(day.capacity / 2);
      for (let dayIndex = 0; dayIndex < dayNumber; dayIndex += 1) {
        for (let j = matchesPerDay; j > 0; j -= 1) {
          competition.matches.push({ day: dayIndex });
        }
      }
    }
  }

  // save the competition
  competition.status = 'private';
  const newCompetition = new Competition(competition);
  await iterator.pmap(newCompetition.matches, async (match) => {
    const newMatch = Match({
      status: 'active',
      competition: newCompetition.id,
      sport: newCompetition.sport
    });
    match.match = newMatch.id;
    await newMatch.save();
  });
  await newCompetition.save();
  return { status: 200, message: 'Competition successfully created' };
};

/**
* Compute and update the points table
*/
const computeCompetitionRules = async (competitionId) => {
  winston.info('Service Competition Admin | compute rules for competition ' + competitionId);
  const competition = await Competition
  .findById(competitionId)
  .populate('matches.match')
  .exec();
  if (!competition) {
    return { status: 400, message: 'Competition not found' };
  }
  if (!competition.matches || competition.matches.length === 0) {
    return { status: 200, message: 'nothing to compute' };
  }
  // Create the tree
  const tree = {
    groups: {},
    groupCount: 0,
    days: {},
    dayCount: 0,
    rounds: {},
    roundCount: 0
  };
  const ruleIds = [];
  competition.matches.forEach((match) => {
    if (match.match.rule) {
      if (match.group) {
        if (!tree.groups[match.group]) {
          tree.groups[match.group] = { days: {}, rules: {}, ruleCount: 0 };
          tree.groupCount += 1;
        }
        if (match.day !== null) {
          if (!tree.groups[match.group].days[match.day]) {
            tree.groups[match.group].days[match.day] = { rules: {}, ruleCount: 0 };
          }
          if (!tree.groups[match.group].days[match.day].rules[match.match.rule]) {
            tree.groups[match.group].days[match.day].rules[match.match.rule] = [];
            tree.groups[match.group].days[match.day].ruleCount += 1;
          }
          tree.groups[match.group].days[match.day].rules[match.match.rule].push(match.match._id);
        } else {
          if (!tree.groups[match.group].rules[match.match.rule]) {
            tree.groups[match.group].rules[match.match.rule] = [];
            tree.groups[match.group].ruleCount += 1;
          }
          tree.groups[match.group].rules[match.match.rule].push(match.match._id);
        }
      } else if (match.round) {
        if (!tree.rounds[match.round]) {
          tree.rounds[match.round] = { days: {}, rules: {}, ruleCount: 0 };
          tree.roundCount += 1;
        }
        if (match.day !== null) {
          if (!tree.rounds[match.round].days[match.day]) {
            tree.rounds[match.round].days[match.day] = { rules: {}, ruleCount: 0 };
          }
          if (!tree.rounds[match.round].days[match.day].rules[match.match.rule]) {
            tree.rounds[match.round].days[match.day].rules[match.match.rule] = [];
            tree.rounds[match.round].days[match.day].ruleCount += 1;
          }
          tree.rounds[match.round].days[match.day].rules[match.match.rule].push(match.match._id);
        } else {
          if (!tree.rounds[match.round].rules[match.match.rule]) {
            tree.rounds[match.round].rules[match.match.rule] = [];
            tree.rounds[match.round].ruleCount += 1;
          }
          tree.rounds[match.round].rules[match.match.rule].push(match.match._id);
        }
      } else if (match.day) {
        if (!tree.days[match.day]) {
          tree.days[match.day] = { rules: {}, ruleCount: 0 };
          tree.dayCount += 1;
        }
        if (!tree.days[match.day].rules[match.match.rule]) {
          tree.days[match.day].rules[match.match.rule] = [];
          tree.days[match.day].ruleCount += 1;
        }
        tree.days[match.day].rules[match.match.rule].push(match.match._id);
      }
      // add rule to rule_ids
      if (match.match.rule && ruleIds.indexOf(match.match.rule) === -1) {
        ruleIds.push(match.match.rule);
      }
    }
  });
  // populate rules
  const ruleRecords = await Rule
  .find({ _id: { $in: ruleIds } })
  .exec();
  // Create a dico
  const rulesDico = {};
  ruleRecords.forEach((rule) => {
    rulesDico[rule._id] = rule.criterias;
  });
  // Shrink the tree
  const shrink = (dico, allSame) => {
    const rules = {};
    const keys = Object.keys(dico);
    let error = false;
    let count = 0;
    keys.forEach((key) => {
      if (dico[key].ruleCount === 1) {
        const rule = Object.keys(dico[key].rules)[0];
        if (!rules[rule]) {
          rules[rule] = [];
          count += 1;
        }
        rules[rule].push(key);
      } else if (allSame) {
        error = true;
        winston.error('Service Competition Admin | For now all games in a set should have the same rule.');
      }
    });
    if (error) {
      return { error: true };
    }
    return { rules, count };
  };
  const table = {};
  if (tree.groupCount > 0) {
    const groupKeys = Object.keys(tree.groups);
    table.group = {};
    console.log(tree.groups.A);
    // shrink group days
    for (let i = 0; i < groupKeys.length; i += 1) {
      if (Object.keys(tree.groups[groupKeys[i]].days).length > 0) {
        const result = shrink(tree.groups[groupKeys[i]].days, true);
        if (result.error) {
          winston.error('Service Competition Admin | For now all group days have the same rule');
          return { status: 500, code: 50000, message: 'internal server error' };
        }
        tree.groups[groupKeys[i]].rules = result.rules;
        tree.groups[groupKeys[i]].ruleCount = result.count;
      }
    }
    // Shrink rounds
    const result = shrink(tree.groups);
    const keys = Object.keys(result.rules);
    if (keys.length === 1) {
      table.group = { rule: rulesDico[keys[0]] };
    } else {
      keys.forEach((key) => {
        result.rules[key].forEach((group) => {
          table.group[group] = { rule: rulesDico[key] };
        });
      });
    }
  }
  if (tree.roundCount > 0) {
    const roundKeys = Object.keys(tree.rounds);
    table.round = {};
    console.log(tree.rounds[1]);
    // shrink round days
    for (let i = 0; i < roundKeys.length; i += 1) {
      if (Object.keys(tree.rounds[roundKeys[i]].days).length > 0) {
        const result = shrink(tree.rounds[roundKeys[i]].days, true);
        if (result.error) {
          winston.error('Service Competition Admin | For now all round days have the same rule');
          return { status: 500, code: 50000, message: 'internal server error' };
        }
        tree.rounds[roundKeys[i]].rules = result.rules;
        tree.rounds[roundKeys[i]].ruleCount = result.count;
      }
    }
    // Shrink rounds
    const result = shrink(tree.rounds);
    const keys = Object.keys(result.rules);
    if (keys.length === 1) {
      table.round = { rule: rulesDico[keys[0]] };
    } else {
      keys.forEach((key) => {
        result.rules[key].forEach((round) => {
          table.round[round] = { rule: rulesDico[key] };
        });
      });
    }
  }
  if (tree.dayCount > 0) {
    const result = shrink(tree.days, true);
    if (result.error) {
      return { status: 500, code: 50000, message: 'internal server error' };
    }
    const keys = Object.keys(result.rules);
    if (keys.length === 1) {
      table.day = { rule: rulesDico[keys[0]] };
    } else {
      winston.error('Service Competition Admin | For now all days should have the same rule.');
      return { status: 500, code: 50000, message: 'internal server error' };
    }
  }
  // update competition
  await Competition
  .update({ _id: competitionId }, { rules: table })
  .exec();
  // Remove competition from cache
  CompetitionCache.flush(competitionId);
  return { status: 200, message: 'Competition rules successfully updated' };
};
exports.computeCompetitionRules = computeCompetitionRules;

/**
* Update rules for a bunch of matches in the competition
* Only one type of competition match is allowed (day, round, group)
*/
exports.updateCompetitionRules = async (competitionId, input) => {
  winston.info('Service Competition Admin | update rules for competition ' + competitionId);
  // check the input
  if (!input.rule) {
    return { status: 400, message: 'missing rule' };
  }
  const update = {};
  let isValid = false;
  if (input.day && String(input.day).match(/^\*$|^[0-9]+$/i)) {
    update.day = input.day;
    isValid = true;
  } else if (input.round && String(input.round).match(/^\*$|^[0-9]+$/i)) {
    update.round = input.round;
    isValid = true;
  } else if (input.group && String(input.group).match(/^\*$|^[A-Z]+$/i)) {
    update.group = input.group;
    isValid = true;
  }
  if (!isValid) {
    return { status: 400, message: 'invalid update' };
  }
  winston.debug(update);
  const competition = await exports.getCompetitionRecord(competitionId);
  // Select all the matching matches
  const selectedMatchIds = [];
  competition.matches.forEach((match) => {
    if (update.group && match.group && (update.group === '*' || match.group === update.group)) {
      selectedMatchIds.push(match.match);
    } else if (update.round && !isNaN(match.round) && (update.round === '*' || match.round === update.round)) {
      selectedMatchIds.push(match.match);
    } else if (update.day && !isNaN(match.day) && (update.day === '*' || match.day === update.day)) {
      selectedMatchIds.push(match.match);
    }
  });
  winston.info('Svc Competition Admin | ' + selectedMatchIds.length + ' matches selected for rule update');
  await Match.update({ _id: { $in: selectedMatchIds } }, {
    $set: { rule: input.rule }
  }, { multi: true }).exec();
  return computeCompetitionRules(competitionId);
};

/**
* Count the number of forecasts
* @competition competition id
* @next callback
*/
exports.countCompetitionForecasts = async (competitionId) => {
  winston.info('Service Competition Admin | Count competition forecasts');
  return Forecast.count({ competition: competitionId }).exec();
};

/**
* Count the number of forecasts
* @competition competition id
* @next callback
*/
exports.countCompetitionLeagues = async (competitionId) => {
  winston.info('Service Competition Admin | Count competition leagues');
  return League.count({ competition: competitionId }).exec();
};

/**
* Aggregate the leagues by creation date
* @competition competition id
* @next callback
*/
exports.getCompetitionLeagueCreations = async (competitionIdAsString) => {
  winston.info('Service Competition Admin | Aggregate competition league creation');
  const result = await League.aggregate([{
    $match: {
      competition: new mongoose.Types.ObjectId(competitionIdAsString)
    }
  }, {
    $group: {
      _id: {
        year: { $year: '$createdAt' },
        dayOfYear: { $dayOfYear: '$createdAt' }
      },
      count: {
        $sum: 1
      }
    }
  }]).exec();
  const aggregation = [];
  if (result && result.length) {
    result.forEach((item) => {
      if (item._id) {
        aggregation.push({
          dayOfYear: item._id.dayOfYear,
          year: item._id.year,
          count: item.count
        });
      }
    });
  }
  return aggregation;
};

/**
* Aggregate the forecasts by creation date
* @competition competition id
* @next callback
*/
exports.getCompetitionForecastCreations = async (competitionIdAsString) => {
  winston.info('Service Competition Admin | Aggregate competition forecast creation');
  const result = await Forecast.aggregate([{
    $match: {
      competition: new mongoose.Types.ObjectId(competitionIdAsString)
    }
  }, {
    $group: {
      _id: {
        year: { $year: '$createdAt' },
        dayOfYear: { $dayOfYear: '$createdAt' }
      },
      count: { $sum: 1 }
    }
  }]).exec();
  const aggregation = [];
  if (result && result.length) {
    result.forEach((item) => {
      if (item._id) {
        aggregation.push({
          dayOfYear: item._id.dayOfYear,
          year: item._id.year,
          count: item.count
        });
      }
    });
  }
  return aggregation;
};

exports.getCompetitionEmails = async (competitionId, options) => {
  winston.info('Service Competition Admin | Retrieve emails in ' + options.lng + ' for competition ' + competitionId);

  const getSelector = async () => {
    const selector = { competition: competitionId };
    if (!options.reminder) {
      return selector;
    }
    const matches = await Match.distinct('_id', {
      status: 'active',
      competition: competitionId,
      datetime: { $exists: true, $ne: null },
      hometeam: { $exists: true, $ne: null },
      awayteam: { $exists: true, $ne: null }
    }).exec();
    if (!matches || matches.length === 0) {
      winston.debug('Service Competition Admin | no match retrieved');
      throw new Error('no match retrieved');
    }
    winston.info(`Service Competition Admin | retrieved ${matches.length} distinct matches`);
    selector.matches = {
      $elemMatch: {
        match: { $in: matches },
        'forecast.hometeam.score': { $exists: false },
        'forecast.awayteam.score': { $exists: false },
        'forecast.difference_slice': { $exists: false }
      }
    };
    return selector;
  };

  let selector;
  try {
    selector = await getSelector();
  } catch (e) {
    if (e.message === 'no match retrieved') {
      return [];
    }
    throw e;
  }
  winston.debug('Service Competition Admin | selector : ', selector);
  const users = await Forecast.distinct('user', selector).exec();
  winston.info('Service Competition Admin | retrieved', users.length, ' distinct forecasts : ');
  let lng = options.lng;
  if (options.lng === 'en') {
    lng = { $ne: 'fr' }; // TODO need to be improved
  }
  let emails = await User.distinct('login.local.email', {
    _id: { $in: users }, lng
  }).exec();
  emails.concat(await User.distinct('login.google.email', {
    _id: { $in: users }, lng, 'login.local.email': { $exists: false }
  }).exec());
  emails.concat(await User.distinct('login.facebook.email', {
    _id: { $in: users },
    lng,
    'login.local.email': { $exists: false },
    'login.google.email': { $exists: false }
  }).exec());
  return emails;
};
