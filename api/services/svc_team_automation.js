const winston = require('winston');

const Team = require('../models/teams.js');

const LequipeTeamScrapper = require('../../scheduler/algos/scrappers_team/lequipe.js');
const EurosportTeamScrapper = require('../../scheduler/algos/scrappers_team/eurosport.js');
const EspnTeamScrapper = require('../../scheduler/algos/scrappers_team/espn.js');

/**
* Compute name from the source provided
*/
exports.computeName = async (teamId, source) => {
  winston.info('Service Team Automation | compute name from scrapping for team ' + teamId);
  const team = await Team.findOne({ _id: teamId }).exec();
  if (team.automation && team.automation[source] && team.automation[source].url) {
    // Iterate the sources
    // lequipe.fr
    if (source === 'lequipe') {
      return LequipeTeamScrapper.scrapTeamName(team.automation[source].url);
    }
    if (source === 'eurosport') {
      return EurosportTeamScrapper.scrapTeamName(team.automation[source].url, team.sport);
    }
    if (source === 'espn') {
      return EspnTeamScrapper.scrapTeamName(team.automation[source].url, team.sport);
    }
    return { status: 400, message: 'source not supported for team name scrapping' };
  }
  return { status: 400, message: 'source not defined' };
};
