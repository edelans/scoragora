const winston = require('winston');

const APIException = require('../routes/APIException.js');

var User = require('../models/users.js');
var League = require('../models/leagues.js');
var Forecast = require('../models/forecasts.js');
var Match = require('../models/matches.js');

var SvcToken = require('./svc_token.js');

/**
* Provide user details from the DB
*/
exports.getUser = async (userId) => {
  winston.info('Service User | user detail retrieved for user :' + userId);
  const user = await User.findById(userId)
  .select({ forecasts: 0, leagues: 0, 'login.local.password': 0 })
  .exec();
  if (user) {
    return user;
  }
  winston.info('Service User | no user found for id ' + userId);
  throw new APIException('no user found', 400);
};

/**
* Provide user summary from the DB
*/
exports.getUserSummary = async (userId) => {
  winston.info('Service User | user summary retrieved for user :' + userId);
  const user = await User.findById(userId).exec();
  if (!user) {
    winston.info('Service User | no user found for id ' + userId);
    throw new APIException('no user found', 400);
  }
  return {
    _id: user._id,
    displayName: user.displayName(),
    pictureUrl: user.pictureURL()
  };
};

/**
* Provide user league summaries from the DB
*/
exports.getUserLeagueSummaries = async (userId, options) => {
  winston.info('Service User | user league summaries retrieved for user :' + userId);
  const sort = {};
  if (options.order === 'activity') {
    sort.lastMessageAt = -1;
  } else {
    sort._id = -1;
  }
  const selector = {
    'members.user': userId
  };
  const leagues = await League
  .find(selector, { _id: 1, name: 1, url: 1, captains: 1, members: 1, competition: 1 })
  .sort(sort)
  .skip(options.page * options.limit)
  .limit(options.limit)
  .populate('competition', 'status name year config.type')
  .exec();
  // let's alter the leagues array to add leagueSize and userRank for all leagues without communicating members
  const leaguesSummary = [];
  for (let i = leagues.length - 1; i >= 0; i--) {
    // we make a copy of the league
    // otherwise we won't be able to modify it,
    // as we are dealing with reference to the same object all the time.
    const league = JSON.parse(JSON.stringify(leagues[i]));
    league.leagueSize = league.members.length;
    for (let k = league.members.length - 1; k >= 0; k--) {
      if (league.members[k].user === userId) {
        league.userRank = league.members[k].rank;
        break;
      }
    }
    delete league.members;
    leaguesSummary.push(league);
  }
  return leaguesSummary;
};

/**
* Provide user league summaries with number of unread messages from the DB
*/
exports.getUserLeagueSummariesWithUnreadMessages = async (userId, options) => {
  winston.info('Service User | user league summaries with unread messages retrieved for user :' + userId);
  const sort = {};
  if (options.order === 'activity') {
    sort.lastMessageAt = -1;
  } else {
    sort._id = -1;
  }
  const selector = {
    'members.user': userId
  };
  return League
  .find(selector, { _id: 1, name: 1, url: 1, captains: 1 })
  .sort(sort)
  .skip(options.page * options.limit)
  .limit(options.limit)
  .exec();
};

/**
* Provide past user matches from the DB
*/
exports.getPastUserMatches = async (userId, options) => {
  winston.info('Service User | past user match details retrieved for user :' + userId);
  const competitions = await Forecast
  .find({ user: userId })
  .distinct('competition')
  .exec();
  winston.info('competitions : ' + competitions);
  const matches = await Match
  .find({ competition: { $in: competitions }, datetime: { $lte: new Date() } })
  .sort({ datetime: -1 })
  .limit(options.limit)
  .skip(options.page * options.limit)
  .select({ automation: 0, forecast: 0 })
  .exec();
  return matches;
};


// TODO documenter le parametre options
exports.getUserMatchForecasts = async (userId, options) => {
  winston.info('Service User | user match forecast retrieved for user : ' + userId);
  const forecasts = await Forecast
  .find({ user: userId, status: 'active' })
  .exec();
  const competitions = [];
  const forecastDico = {};
  forecasts.forEach((forecast) => {
    competitions.push(forecast.competition);
    forecastDico[forecast.competition] = forecast;
  });
  const matches = await Match
  .find({ competition: { $in: competitions }, datetime: { $gte: new Date() } }, { forecast: 0 })
  .sort({ datetime: 1 })
  .limit(options.limit)
  .skip(options.page * options.limit)
  .populate('hometeam awayteam', 'name -_id')
  .exec();
  const result = [];
  matches.forEach((match) => {
    // look for the forecast
    const forecast = forecastDico[match.competition];
    const userForecast = {
      forecast: forecast.id,
      competition: match.competition,
      match: match.id,
      hometeam: match.hometeam,
      awayteam: match.awayteam,
      datetime: match.datetime
    };
    if (forecast && forecast.matches) {
      forecast.matches.forEach((matchForecast) => {
        if (match._id.equals(matchForecast.match)) {
          userForecast.matchForecast = matchForecast.forecast;
        }
      });
    }
    result.push(userForecast);
  });
  // console.log('getUserMatchForecasts: %j',result);
  return result;
};

exports.getUserForecastSummaries = (userId, options) => {
  winston.info('Service User | forecast summaries retrieved for user :' + userId);
  return Forecast
  .find({ user: userId }, { _id: 1, competition: 1, points: 1 })
  .limit(options.limit)
  .skip(options.page * options.limit)
  .sort({ status: 1, _id: -1 })
  .populate('competition', 'name year config.type')
  .exec();
};

/**
* Update user picture mode if the mode is valid and available
* Twitter mode can be used only if a twitter account is linked to the user
*/
exports.updateUserPictureMode = async (userId, info) => {
  winston.info('Service User | picture mode update to ' + info.pictureMode + ' for user : ' + userId);
  const criteria = {
    _id: userId
  };
  const change = {
    pictureMode: info.pictureMode
  };
  let isValid = false;
  if (info.pictureMode === 'twitter') {
    criteria['login.twitter.pictureURL'] = { $ne: null };
    isValid = true;
  }
  if (info.pictureMode === 'google') {
    criteria['login.google.pictureURL'] = { $ne: null };
    isValid = true;
  }
  if (info.pictureMode === 'facebook') {
    criteria['login.facebook.pictureURL'] = { $ne: null };
    isValid = true;
  }
  if (info.pictureMode === 'local') {
    isValid = true;
    if (info.pictureURL && info.pictureURL.match(/^\//i)) {
      change['login.local.pictureURL'] = info.pictureURL;
    }
  }
  if (!isValid) {
    return { status: 400, message: 'Invalid input' };
  }
  const updateReport = await User
  .update(criteria, change).exec();
  if (updateReport.n) {
    return { status: 200, message: 'User picture mode has been updated' };
  }
  return { status: 400, message: 'User can\'t be updated' };
};

exports.updateUserPictureURL = async (userId, pictureURL) => {
  winston.info('Service User | updating pictureURL to ' + pictureURL + ' for user : ' + userId);
  const updateReport = await User
  .update({ _id: userId }, { 'login.local.pictureURL': pictureURL }).exec();
  if (updateReport.n) {
    winston.info('Service User | successfully updated picture for user : ' + userId);
    return { status: 200, message: 'User picture has been updated' };
  }
  winston.info('Service User | picture could not be updated for user : ' + userId);
  return { status: 400, message: 'User can\'t be updated' };
};

exports.updateUserPseudo = async (userId, pseudo) => {
  winston.info('Service User | trying pseudo update to ' + pseudo + ' for user : ' + userId);
  if (pseudo.length > 50) {
    return { status: 400, message: 'Invalid input : pseudo exceeding 50 chars' };
  }
  const updateReport = await User
  .update({ _id: userId }, { 'login.local.pseudo': pseudo }).exec();
  if (updateReport.n) {
    winston.info('Service User | successfully updated pseudo to ' + pseudo + ' for user : ' + userId);
    return { status: 200, message: 'User pseudo has been updated' };
  }
  winston.info('Service User | pseudo could not be updated for user : ' + userId);
  return { status: 400, message: 'User can\'t be updated' };
};


exports.updateUserMailSettings = async (userId, mailsettings) => {
  winston.info('Service User | updating mail settings for user : ' + userId);
  const updateReport = await User
  .update({ _id: userId }, { email_settings: mailsettings }).exec();
  if (updateReport.n) {
    winston.info('Service User | mail settings updated for user : ' + userId);
    return { status: 200, message: 'User email_settings has been updated' };
  }
  winston.info('Service User | mail settings could not be updated for user : ' + userId);
  return { status: 400, message: 'User can\'t be updated' };
};


exports.updateUserLng = async (userId, lng) => {
  winston.info('Service User | updating lng to ' + lng + ' for user : ' + userId);
  if (lng !== 'fr' && lng !== 'en') {
    // invalid lng
    winston.info('Service User | user ' + userId + ' cant update lng to ' + lng);
    return { status: 400, code: 40016, message: 'Invalid language' };
  }
  // valid lng
  const updateReport = await User
  .update({ _id: userId }, { lng }).exec();
  if (updateReport.n) {
    winston.info('Service User | successfully updated lng for user : ' + userId);
    return { status: 200, code: 20001, message: 'User lng has been updated' };
  }
  winston.info('Service User | lng could not be updated for user : ' + userId);
  return { status: 400, code: 40015, message: 'User can\'t be updated' };
};

exports.updateUserPassword = async (userId, form) => {
  winston.info('Service User | Update password for user : ' + userId);
  // check form
  if (!form.oldPassword) {
    return { status: 400, message: 'old password missing' };
  }
  if (!form.password) {
    return { status: 400, message: 'password missing' };
  }
  if (!form.passwordConfirmation) {
    return { status: 400, message: 'password confirmation missing' };
  }
  if (form.passwordConfirmation !== form.password) {
    return { status: 400, message: 'password and password confirmation dont match' };
  }
  const user = await User.findById(userId).exec();
  if (!user) {
    winston.info('Service User | can\'t find user ' + userId + ' for password update');
    return { status: 400, message: 'no user found' };
  }
  if (!user.validPassword(form.oldPassword)) {
    return { status: 400, message: 'wrong old password' };
  }
  user.login.local.password = form.password;
  await user.save();
  return { status: 200, message: 'User password has been updated' };
};

exports.updateUserPasswordFromEmail = async (info) => {
  winston.info('Service User | Update password for user with email : ' + info.email);
  // check input
  if (!info.token) {
    return { status: 400, message: 'token missing' };
  }
  if (!info.email) {
    return { status: 400, message: 'email missing' };
  }
  if (typeof info.email !== 'string') {
    return { status: 400, message: 'invalid email' };
  }
  if (!info.password) {
    return { status: 400, message: 'password missing' };
  }
  if (!info.password_confirmation) {
    return { status: 400, message: 'password confirmation missing' };
  }
  if (info.password_confirmation !== info.password) {
    return { status: 400, message: 'password and password confirmation dont match' };
  }
  const token = await SvcToken.getToken(info.token);
  if (!token) {
    return { status: 400, message: 'token not found' };
  }
  if (!token.user) {
    winston.error('Service User | invalid data ' + token);
    return { status: 500, code: 50000, message: 'internal server error' };
  }
  const user = await User.findOne({ _id: token.user, 'login.local.email': info.email }).exec();
  if (!user) {
    winston.info('Service User | no user found for email ' + info.email);
    return { status: 400, message: 'user not found' };
  }
  user.login.local.password = info.password;
  await user.save();
  return { status: 200, message: 'User password has been updated' };
};

exports.removeUser = async (userId) => {
  winston.info('Service User | remove user ' + userId);
  await User.update({ _id: userId }, {
    $set: {
      deleted: true,
      login: {}
    }
  });
  return { status: 200, message: 'User successfully removed' };
};

exports.getUserRecord = async (userId) => {
  winston.info('Service User | user detail retrieved for user ' + userId);
  const user = await User
  .findById(userId).exec();
  if (!user) {
    winston.info('Service User | no user found for id ' + userId);
    throw new APIException('no user found', 400);
  }
  return user;
};

/**
* Provide user details from the DB
*/
exports.getUsers = async (options) => {
  winston.info('Service User | user detail retrieved for users matching ' + options.search);
  const selector = {};
  if (options.search && options.search !== '' && typeof options.search === 'string') {
    selector.$or = [{
      'login.local.pseudo': new RegExp(options.search, 'i')
    }, {
      'login.local.email': new RegExp(options.search, 'i')
    }, {
      'login.google.email': new RegExp(options.search, 'i')
    }, {
      'login.facebook.email': new RegExp(options.search, 'i')
    }, {
      'login.twitter.displayName': new RegExp(options.search, 'i')
    }, {
      'login.google.displayName': new RegExp(options.search, 'i')
    }, {
      'login.facebook.displayName': new RegExp(options.search, 'i')
    }];
  }
  const users = await User
  .find(selector)
  .skip(options.page * options.limit)
  .limit(options.limit)
  .sort({ _id: 1 })
  .exec();
  if (options.count === 'true') {
    const count = await User.count(selector).exec();
    return { status: 200, users, count };
  }
  return { status: 200, users };
};
