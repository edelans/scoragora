const winston = require('winston');
const mongoose = require('mongoose');

const MessageBucket = require('../models/messages.js');

/**
* Provide messages for a league
*/
exports.getMessageBuckets = async (leagueId, options) => {
  winston.info('Service Message | retrieve message bucket for league :' + leagueId);
  const result = await MessageBucket
  .aggregate([{
    $match: {
      league: new mongoose.Types.ObjectId(leagueId),
      updatedAt: {
        $lt: new Date(options.timestamp)
      }
    }
  }, {
    $sort: {
      updatedAt: -1
    }
  }, {
    $limit: options.limit
  }, {
    $project: {
      messages: 1
    }
  }, {
    $unwind: '$messages'
  }, {
    $sort: {
      'messages.datetime': 1
    }
  }, {
    $group: {
      _id: {

      },
      messages: {
        $push: {
          sender: '$messages.sender',
          content: '$messages.content',
          datetime: '$messages.datetime'
        }
      }
    }
  }])
  .exec();
  if (!result || !result[0] || !result[0].messages || result[0].messages.length === 0) {
    winston.info('Service Message | no messageBucket found for league id ' + leagueId);
    return { status: 200, message: 'No message found' };
  }
  return { status: 200, messages: result[0].messages };
};

exports.insertMessage = async (leagueId, message) => {
  winston.info('Service Message | insert message for league :' + leagueId);
  message.datetime = new Date();
  await MessageBucket
  .update({
    league: leagueId,
    count: {
      $lt: 20
    }
  }, {
    $inc: {
      count: 1
    },
    $push: {
      messages: message
    },
    $set: {
      updatedAt: new Date()
    }
  }, {
    upsert: true
  }).exec();
  return { status: 200, message: 'Message successfully updated' };
};
