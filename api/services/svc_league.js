const winston = require('winston');

const APIException = require('../routes/APIException.js');

const User = require('../models/users.js');
const League = require('../models/leagues.js');
const Message = require('../models/messages.js');

const SvcForecast = require('../services/svc_forecast.js');

const iterator = require('../tools/iterator.js');

// todo: add description
const encryptArray = [
  3, 7, 10, 16, 21, 8, 13, 11,
  4, 18, 17, 14, 22, 1, 12, 6,
  9, 15, 19, 5, 23, 0, 20, 2
];

const encrypt = (key) => {
  if (key.length === 24) {
    // We only consider 24-char key, corresponding to 12-byte mongo object id
    let newKey = '';
    encryptArray.forEach((char) => {
      newKey += key.charAt(char);
    });
    return newKey;
  }
  return key;
};

/**
* Provide League name availability details from the DB
* test ok
*/
const isLeagueNameAvailable = async (wantedLeagueName) => {
  winston.info('Service League | league name availability checking for league name :' + wantedLeagueName);
  // perform some sanity check on league_name
  if (wantedLeagueName === '' || wantedLeagueName === '..') {
    return false;
  }
  const wantedLeagueUrl = wantedLeagueName.trim().toLowerCase().replace(/ /g, '.');
  const league = await League.findOne({ url: wantedLeagueUrl }).exec();
  if (!league) {
    winston.info('Service League | the wantedLeagueName is available');
    return true;
  }
  winston.info('Service League | the wanted_league_name is already taken');
  // return 200 here is normal
  return false;
};

exports.isLeagueNameAvailable = isLeagueNameAvailable;

/**
* Create a new league on a given competition
* Callback(saved_league)
*/
exports.createLeague = async (leagueName, urlLeagueName, userId, competitionId, { sponsor }) => {
  winston.info('Service League | starting new league creation for league :' + leagueName);
  // 1. Check the availability
  if (!await isLeagueNameAvailable(leagueName)) {
    winston.info('Service League | league name is not available for league name: ' + leagueName);
    // we may override a 200 from isLeagueNameAvailable by a 400
    // because it become an error for createleague.
    return { status: 400, message: 'league name not available' };
  }
  winston.info('Service League | league name is available for :' + leagueName);
  // 2. Create forecast
  const forecast = await SvcForecast.createForecast(competitionId, userId);
  // 3.a Create a new league
  const newLeague = new League({
    createdAt: new Date(),
    lastMessageAt: new Date(),
    name: leagueName,
    url: urlLeagueName,
    captains: [userId],
    competition: competitionId,
    members: [{
      user: userId,
      points: forecast.points,
      rank: 1,
      lastConnectedAt: new Date()
    }],
    sponsor
  });
  // 3.b encrypt the token
  newLeague.token = encrypt(newLeague.id);
  // 3.c save the league
  const savedLeague = await newLeague.save();
  winston.info('Service League | New league has been saved :' + leagueName);

  // 4. Add league reference to user account
  await User.findByIdAndUpdate(userId, {
    $push: {
      leagues: {
        league: savedLeague.id,
        ranking: 1
      }
    }
  }, { upsert: true }).exec();
  return { status: 200, message: 'The league has been created', league: savedLeague };
};

/**
* Provide League details from the DB
*/
exports.getLeague = async (leagueId) => {
  winston.info('Service League | league details searched for league :' + leagueId);
  const league = await League
  .findById(leagueId)
  .exec();
  if (league) {
    return league;
  }
  winston.info('Service League | no league found for id ' + leagueId);
  throw new APIException('league not found', 200);
};


/**
* Get league members
*/
exports.getLeagueMembers = async (league, forPremium) => {
  winston.info('Service League | retrieving members in league ' + league._id);
  if (!league.members) {
    winston.error('Service League | league ' + league._id + ' does\'nt have members');
    throw new Error();
  }
  const userIds = [];
  league.members.forEach((member) => {
    // filter league members
    if (member.status === 'banned') {
      // ignore this member
      return;
    }
    userIds.push(member.user);
  });
  const users = await User
  .find({ _id: { $in: userIds } })
  .exec();
  return users.map((user) => {
    const doc = {
      _id: user._id,
      displayName: user.displayName(),
      pictureUrl: user.pictureURL()
    };
    if (forPremium) {
      doc.flag = user.flag;
      doc.banned = user.banned;
      doc.displayEmail = user.displayEmail();
    }
    return doc;
  });
};

/**
 * Update the last connection date for each given members
 */
exports.updateLastConnectionDate = async (leagueId/* , userIds*/) => {
  winston.info('Service League | Update last connection date for ' + leagueId);
};

/**
* Add a user to a given league
* todo: update ranking ?
*/
exports.addUserToLeague = async (leagueId, userId) => {
  // 1. Check if user is already a member
  const leagueWithMember = await League.findOne({ _id: leagueId, 'members.user': userId }).exec();
  if (leagueWithMember) {
    // the user is already member of the league
    return {
      status: 400,
      code: 40010,
      message: 'user already member of the league'
    };
  }

  // 2. Check if the league exists
  const league = await League.findById(leagueId).exec();
  if (!league) {
    winston.info('Service League | league id ' + leagueId + ' doesn\'t exist');
    return { status: 404, message: 'no league found for id ' + leagueId };
  }
  // 3. Check if the league is full
  if (!league.isPremium() && league.members && league.members.length >= 30) {
    winston.info('Service League | league ' + league.name + '(' + leagueId + ') reach the members limit');
    return { status: 400, code: 40011, message: 'league is full' };
  }

  // 4. Create a new forecast for the user if needed
  const forecast = await SvcForecast.createForecast(league.competition, userId);
  if (!league.members) {
    league.members = [];
  }
  // 5. add user to league and rank the league
  // get the index to insert the user
  let indexToInsert = -1;
  let userRank = 1;
  league.members.forEach((member, index) => {
    if (member.points < forecast.points) {
      member.rank += 1;
      if (indexToInsert < 0) {
        indexToInsert = index;
      }
    }
    if (member.points === forecast.points) {
      userRank = member.rank;
      indexToInsert = index + 1;
    }
    if (member.points > forecast.points) {
      userRank = index + 2;
    }
  });
  // User to be inserted at the end
  if (indexToInsert < 0 && userRank > 1) {
    indexToInsert = league.members.length;
  }
  // Insert user in members array
  league.members.splice(indexToInsert, 0, {
    user: userId,
    points: forecast.points,
    rank: userRank
  });
  await league.save();
  // 6. Add a reference to the league in the user object
  await User.findByIdAndUpdate(userId, {
    $push: {
      leagues: {
        league: leagueId,
        ranking: userRank
      }
    }
  }, {
    upsert: false
  }).exec();

  // 8. Update member rank
  // we do it only from indexToInsert to length
  for (let i = league.members.length - 1; i > indexToInsert; i -= 1) {
    User.findOneAndUpdate({
      _id: league.members[i].user,
      'leagues.league': leagueId
    }, {
      $set: {
        'leagues.$.rank': league.members[i].rank
      }
    }, { upsert: false })
    .exec()
    .catch((err) => {
      winston.error('Service league | error when updating rank for user ' + league.members[i].user + ' : ' + err);
    });
  }

  // 8. Reply
  return {
    status: 200,
    message: 'The user has been successfully added to the league'
  };
};

/**
* Remove user from a given league and update the league ranking
* Callback(err, user, league_name)
*/
const removeUserFromLeague = async (leagueId, userId) => {
  winston.info('Service League | remove user ' + userId + 'from league ' + leagueId);
  if (!userId) {
    winston.error('Service League | error in removeUserFromLeague : userId must be given');
    throw new Error();
  }
  // 1. find league
  const league = await League.findById(leagueId).exec();
  if (!league) {
    winston.info('Service League | leagueId ' + leagueId + ' seems incorrect');
    throw new APIException('no league found', 400);
  }
  // 2. Remove the user reference in the league
  let userRank;
  let userPoints;
  let indexToRemove;
  league.members.forEach((member, index) => {
    if (member.user.toString() === userId) {
      userRank = member.rank;
      userPoints = member.points;
      indexToRemove = index;
    } else if (userRank && userPoints > member.points) {
      member.rank -= 1;
    }
  });
  league.members.splice(indexToRemove, 1);
  await league.save();
  // 3. Remove the league reference in the user object
  await User
  .findByIdAndUpdate(userId, {
    $pull: { leagues: { league: league.id } }
  })
  .exec();
  // 4. Update member rank
  // we do it only from indexToRemove to length
  for (let i = league.members.length - 1; i >= indexToRemove; i -= 1) {
    User.findOneAndUpdate({
      _id: league.members[i].user,
      'leagues.league': leagueId
    }, {
      $set: {
        'leagues.$.rank': league.members[i].rank
      }
    }, { upsert: false })
    .exec()
    .catch((err) => {
      winston.error('Service league | error when updating rank for user ' + league.members[i].user + ' : ' + err);
    });
  }
  winston.info('Service League | removeUserFromLeague successful');
  return { status: 200, code: 20002, message: 'You successfully left the league ' + league.name, league: league.name };
};

exports.removeUserFromLeague = removeUserFromLeague;

/**
* Remove the league, all the messages, the assocation between the league and its members
*/
exports.removeLeague = async (league) => {
  winston.info('Service League | removing league :' + league.id);
  await Message
  .remove({ league: league.id })
  .exec();
  await iterator.pmap(league.members, async (member) => {
    await removeUserFromLeague(league.id, member.user);
  });
  await League.findByIdAndRemove(league.id).exec();
  return { status: 200, message: 'The league has been deleted' };
};

/**
* Provide leagues details from the DB
*/
exports.getLeagues = async (options) => {
  winston.info('Service League | league detail retrieved for leagues matching ' + options.search);
  const selector = {};
  if (options.search && options.search !== '') {
    selector.name = new RegExp(options.search, 'i');
  }
  if (options.competition) {
    selector.competition = options.competition;
  }
  if (options.minsize) {
    selector.members = { $exists: true };
    selector['members.' + options.minsize] = { $exists: true };
  }
  const leagues = await League
  .find(selector)
  .skip(options.page * options.limit)
  .limit(options.limit)
  .sort({ _id: 1 })
  .exec();
  if (options.count !== 'true') {
    return leagues;
  }
  const count = await League.count(selector).exec();
  return { status: 200, leagues, count };
};

/**
 * Update league status
 */
exports.updateLeagueStatus = async (leagueId, status) => {
  winston.info('Service League | update status to ' + status + ' for league ' + leagueId);
  const update = {
    'premium.validated': status !== 'standard'
  };
  await League.update({ _id: leagueId }, { $set: update }).exec();
  return { status: 200, message: 'league status successfully updated' };
};

/**
 * Update league sponsor
 */
exports.updateLeagueSponsor = async (leagueId, sponsor = '') => {
  winston.info('Service League | update sponsor to ' + sponsor + ' for league ' + leagueId);
  const update = sponsor !== '' ? { $set: { sponsor } } : { $unset: { sponsor: 1 } };
  await League.update({ _id: leagueId }, update).exec();
  return { status: 200, message: 'league sponsor successfully updated' };
};

/**
 * Get league users
 */
exports.getLeagueUsers = async (leagueId) => {
  winston.info('Service League | users retrieved for league ' + leagueId);
  const league = await League
  .findOne({ _id: leagueId })
  .populate('members.user')
  .exec();
  if (!league) {
    winston.info('SvcLeague | League ' + leagueId + ' does not exist');
    throw new APIException('League not found', 400);
  }
  const members = [];
  league.members.forEach((item) => {
    members.push(item.user);
  });
  return members;
};

/**
* Provide leagues details from the DB by token
*/
exports.getLeagueByToken = async (token) => {
  winston.info('Service League | league detail retrieved for leagues matching token' + token);
  const league = await League
  .findOne({ token })
  .exec();
  if (!league) {
    winston.info('SvcLeague | League token' + token + ' does not exist');
    throw new APIException('League not found', 400);
  }
  return league;
};
