const winston = require('winston');

const SvcMatch = require('./svc_match.js');

const LequipeMatchScrapper = require('../../scheduler/algos/scrappers_match/lequipe.js');
const EurosportMatchScrapper = require('../../scheduler/algos/scrappers_match/eurosport.js');
const EspnMatchScrapper = require('../../scheduler/algos/scrappers_match/espn.js');

/**
* Scrap matches info from the source provided
*/
exports.scrapMatchDate = async (matchId, source) => {
  winston.info('Service Match Automation | scrap date for match ' + matchId);
  const match = await SvcMatch.getMatchRecord(matchId);
  if (match.automation && match.automation[source] && match.automation[source].url) {
    // Iterate the sources
    // lequipe.fr
    if (source === 'lequipe') {
      return LequipeMatchScrapper.scrapMatchDate(match.automation[source].url, match.sport);
    }
    if (source === 'eurosport') {
      return EurosportMatchScrapper.scrapMatchDate(match.automation[source].url, match.sport);
    }
    if (source === 'espn') {
      return EspnMatchScrapper.scrapMatchDate(match.automation[source].url, match.sport);
    }
  }
  return { status: 400, message: 'source not supported for date scrapping' };
};

/**
* Scrap matches info from the source provided
*/
exports.scrapMatchResult = async (matchId, source) => {
	return { status: 400, message: 'not implemented' };
};
