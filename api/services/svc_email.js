const winston = require('winston');

const cfg = require('../../config');
const sendgrid = require('sendgrid')(cfg.sendgrid.apiKey);

const SvcUser = require('../models/users.js');

let res;
let i18n;

/**
* Init Svc_email
*  - app: used to build emails from template
*  - i18n: used to translate emails
*/
exports.init = (app, i18next) => {
  res = app;
  i18n = i18next;
};

const getTranslator = (lng) => {
  return new Promise((resolve, reject) => {
    i18n.changeLanguage(lng, (err, translator) => {
      if (err) {
        winston.error('Svc Email | error when changing the language to ' + lng + ' : ' + err);
        reject();
        return;
      }
      resolve(translator);
    });
  });
};

const renderTemplate = (templateName, options) => {
  return new Promise((resolve, reject) => {
    res.render(templateName, options, (err2, html2) => {
      if (err2) {
        winston.error('Svc Email | error when rendering ' + templateName + ' : ' + err2);
        reject();
        return;
      }
      resolve(html2);
    });
  });
};

/**
* Send an email with Sendgrid
*/
const sendEmail = async (message) => {
  if (process.env.NODE_ENV === 'dev' || process.env.NODE_ENV === 'test') {
    return;
  }
  const email = new sendgrid.Email({
    to: message.to[0].email,
    from: 'contact@scoragora.com',
    fromname: message.fromName || 'Scoragora',
    subject: message.subject,
    html: message.html
  });

  await new Promise((resolve) => {
    sendgrid.send(email, (err, json) => {
      if (err) {
        winston.error(err);
      }
      winston.debug('API | Svc email | sendgrid feedback on successful sending: ' + json);
      return resolve();
    });
  });
};


/**
* Send an invitation to emails
*/
exports.sendInvitation = function(req, res, email_list, league_name, link, next) {
  /*res.render('email/invitation/index', {league_name: league_name, link: link}, function(err, html) {
  if (err) {
  return next(err);
}
var emails = [];
for (i = 0; i < email_list.length; i++) {
emails.push({
email: email_list[i],
type: 'bcc'
});
}
var message = {
html: html,
subject: req.i18n.t('email.invitation.subject', {league_name: league_name}),
fromName: req.i18n.t('common.brand_team'),
to: emails
};
return sendEmail(message, next);
});*/
};


/**
* Send a registration validation email
*/
exports.sendRegistrationValidation = async (user, link, options) => {
  winston.info('SvcEmail | sending registration validation to ' + user.email + ' in ' + options.lng);
  const t = await getTranslator(options.lng || 'en');
  const html = await renderTemplate('emails/registration_validation', { pseudo: user.pseudo, link, t });
  const message = {
    html,
    subject: t('email.register_validate.subject'),
    fromName: t('common.brand_team'),
    to: [{
      email: user.email,
      type: 'to'
    }]
  };
  sendEmail(message);
  return { status: 200, message: 'registration validation email successfully sent' };
};

/**
* Send a reset password email
*/
exports.sendResetPassword = async (email, link, options) => {
  winston.info('SvcEmail | sending reset password to ' + email + ' in ' + options.lng);
  const t = await getTranslator(options.lng || 'en');
  const html = await renderTemplate('emails/forgotten_password', { link, t });
  const message = {
    html,
    subject: t('email.forgotten_password.subject'),
    fromName: t('common.brand_team'),
    to: [{
      email,
      type: 'to'
    }]
  };
  sendEmail(message);
  return { status: 200, message: 'reset password email successfully sent' };
};

/**
* Send a email for league creation if the email exists
*/
exports.sendNewLeague = async (userId, leagueName, competition, link) => {
  if (!userId) {
    return { status: 400, message: 'missing user' };
  }
  if (!leagueName) {
    return { status: 400, message: 'missing league name' };
  }
  const user = await SvcUser.getUser(userId);
  const t = await getTranslator(user.lng || 'en');
  const html = await renderTemplate('email/new_league/index', {
    competition,
    link,
    t,
    league_name: leagueName,
    pseudo: user.displayName()
  });
  const message = {
    html,
    subject: t('email.new_league.subject', { league_name: leagueName }),
    fromName: t('common.brand_team'),
    to: [
      { email: user.displayEmail(), type: 'to' }
    ]
  };
  sendEmail(message);
  return { status: 200, message: 'new league email successfully sent' };
};
