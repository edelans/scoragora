const winston = require('winston');

const Forecast = require('../models/forecasts.js');
const Match = require('../models/matches.js');
const User = require('../models/users.js');

const SvcCompetition = require('../services/svc_competition.js');
const SvcMatch = require('../services/svc_match.js');

const APIException = require('../routes/APIException.js');

const checkAndFormatMatchForecast = (matchForecast, sport) => {
  if (!matchForecast || !matchForecast.forecast) return false;
  if (sport === 'football') {
    if (!matchForecast.forecast.hometeam || !matchForecast.forecast.awayteam) return false;
    // build forecast
    const direction = matchForecast.forecast.direction;
    matchForecast.forecast = {
      hometeam: matchForecast.forecast.hometeam,
      awayteam: matchForecast.forecast.awayteam
    };
    if (!!direction) {
      matchForecast.forecast.direction = direction;
    }
    return true;
  }
  if (sport === 'rugby') {
    const differenceSlice = matchForecast.forecast.difference_slice;
    if (isNaN(differenceSlice)) {
      return false;
    }
    // build forecast
    const direction = matchForecast.forecast.direction;
    matchForecast.forecast = {
      difference_slice: differenceSlice
    };
    if (!!direction) {
      matchForecast.forecast.direction = direction;
    }
    return true;
  }
  return false;
};

exports.getForecast = async (forecastId, userId) => {
  winston.info('Service Forecast | forecast detail retrieved for forecast : ' + forecastId);
  const forecast = await Forecast
  .findById(forecastId)
  .exec();
  if (!forecast) {
    winston.info('Service Forecast | no forecast found for id ' + forecastId);
    throw new APIException('forecast not found', 400);
  }
  // forecast details can only be retrieved by its user
  console.log(userId, forecast.user.toString());
  if (userId === forecast.user.toString()) {
    return forecast;
  }
  throw new APIException('Unauthorized access', 403);
};

exports.getForecasts = async (options) => {
  winston.info('Service Forecast | forecast search');
  const selector = {};
  if (options.competition) {
    selector.competition = options.competition;
  }
  if (options.users) {
    selector.user = { $in: options.users };
  }
  return Forecast
  .find(selector, { user: 1, matches: 1 })
  .exec();
};

/**
 * Get forecast for a league
 */
exports.getForecastsFromLeague = async (league) => {
  winston.info('Service Forecast | retrieve forecasts for league ' + league._id);
  // get the id of the validated members
  const membersIDs = [];
  const filterUnacceptedLeagueMembers = league.isPremium();
  winston.debug('Service Forecast | league ' + league._id + ' is premium : ' + filterUnacceptedLeagueMembers);
  league.members.forEach((member) => {
    if (filterUnacceptedLeagueMembers && member.status === 'banned') {
      return;
    }
    membersIDs.push(member.user);
  });

  const matches = await SvcCompetition.getCompetitionMatches(league.competition, { status: 'closed' });
  if (!matches || matches.length === 0) {
    winston.info('Service Forecast | there seems to be no closed match yet on this league of competition : ' + league.competition);
    return [];
  }
  const closedMatchIDs = matches.map(m => m.id);
  winston.info('Service Forecast | closedMatchIDs : ' + closedMatchIDs);

  const forecasts = await exports.getForecasts({
    competition: league.competition,
    users: membersIDs
  });
  const membersforecasts = [];
  forecasts.forEach((forecast) => {
    // winston.info('Service Forecast | forecast of the moment : ' + forecast);
    forecast.matches.forEach((match) => {
      if (closedMatchIDs.indexOf(match.match.toString()) > -1) {
        membersforecasts.push({
          user: forecast.user,
          match: match.match,
          points: (match.points || 0),
          bonus: (match.bonus || []),
          forecast: match.forecast
        });
      }
    });
  });
  return membersforecasts;
};

/**
 * Get match forecast for a league
 */
exports.getMatchForecastsFromLeague = async (league, matchId) => {
  winston.info('Service Forecast | retrieve match ' + matchId + ' forecasts for league ' + league._id);
  const match = await SvcMatch.getMatch(matchId);
  // check sur la date du match (doit etre passé)
  if (Date.now() < match.datetime) {
    // match has not started yet
    return { status: 200, message: 'this match has not started yet : no forecasts available' };
  }
  // match has already started, we can provide the forecasts
  // get the id of the validated members
  const membersIDs = [];
  const filterUnacceptedLeagueMembers = league.isPremium();
  winston.debug('Service Forecast | league ' + league._id + ' is premium : ' + filterUnacceptedLeagueMembers);
  league.members.forEach((member) => {
    if (filterUnacceptedLeagueMembers && member.status === 'banned') {
      return;
    }
    membersIDs.push(member.user);
  });

  const forecasts = await Forecast.find({ user: { $in: membersIDs }, 'matches.match': matchId }, {
    user: 1,
    matches: {
      $elemMatch: { match: matchId }
    }
  }).exec();
  const result = [];
  forecasts.forEach((forecast) => {
    // réorganiser qlq infos avant de renvoyer
    result.push({
      user: forecast.user,
      points: forecast.matches[0].points,
      bonus: forecast.matches[0].bonus,
      forecast: forecast.matches[0].forecast
    });
  });
  // winston.info('forecasts: %j',result);
  return result;
};

exports.updateMatchForecast = async (forecastId, userId, matches) => {
  winston.info('Service Forecast | forecast matches updated for forecast : ' + forecastId);
  const forecast = await exports.getForecast(forecastId, userId);
  if (forecast.matches.length < matches.length) {
    return { status: 400, message: 'Invalid input : too many matches to update' };
  }
  // Get matches
  const matchDetails = await Match
  .find({ competition: forecast.competition, datetime: { $gte: new Date() } })
  .exec();
  const impossibleUpdates = [];
  const invalidMatches = [];

  // process matches
  const matchDetailDico = {};
  matchDetails.forEach((matchDetail) => {
    matchDetailDico[matchDetail.id] = matchDetail;
  });

  const matchDico = {};
  matches.forEach((match) => {
    if (matchDetailDico[match.match]) {
      // check match
      if (checkAndFormatMatchForecast(match, matchDetailDico[match.match].sport)) {
        matchDico[match.match] = match.forecast;
      } else {
        winston.info('Service Forecast | match forecast invalid ' + JSON.stringify(match.forecast));
        invalidMatches.push(match);
      }
    } else {
      impossibleUpdates.push(match);
    }
  });
  // Update match forecast
  forecast.matches.forEach((match) => {
    if (matchDico[match.match]) {
      match.forecast = matchDico[match.match];
    }
  });
  const savedForecast = await forecast.save();
  return { status: 200, message: 'Forecast has been updated', forecast: savedForecast };
};


/**
* Create a new forecast for the user **if needed** and retrieve it
*/
exports.createForecast = async (competitionId, userId) => {
  winston.info('Service Forecast | create forecast for user ' + userId + ' on competition ' + competitionId);
  const forecast = await Forecast.findOne({
    competition: competitionId,
    user: userId
  }).exec();
  if (forecast) {
    // no forecast need to be created
    return forecast;
  }
  // no forecast existed before, let's create one
  const newForecast = new Forecast({
    createdAt: new Date(),
    competition: competitionId,
    user: userId
  });
  const savedForecast = await newForecast.save();
  User
  .update({ _id: userId }, {
    $push: {
      forecasts: { forecast: savedForecast.id }
    }
  })
  .exec();
  // the forecast object id is saved in the user document
  // now we have to add all the corresponding matches to the forecast
  const matches = await Match
  .find({ competition: competitionId })
  .exec();
  const forecastMatches = [];
  matches.forEach((match) => {
    forecastMatches.push({
      match: match.id,
      point: 0,
      bonus: [],
      forecast: { hometeam: {}, awayteam: {} }
    });
  });
  savedForecast.update({
    matches: forecastMatches
  }).exec();
  return savedForecast;
};

/**
* Removes a forecast for a user and a competition
*/
exports.removeForecast = async (competitionId, userId) => {
  // todo
  // is it really needed ?
};
