const winston = require('winston');
const moment = require('moment');

const Match = require('../models/matches.js');

const APIException = require('../routes/APIException.js');

const SvcCompetitionAdmin = require('./svc_competition_admin.js');

/**
* Provide match details from the DB
*/
exports.getMatch = async (matchId) => {
  winston.info('Service Match | match details searched for match : ' + matchId);
  const match = await Match
  .findById(matchId)
  .select({ automation: 0, forecast: 0 })
  .exec();
  if (match) {
    return match;
  }
  throw new APIException('No match found', 400);
};

/**
* Provide match details from the DB with no alteration
*/
const getMatchRecord = async (matchId) => {
  winston.info('Service Match | match record retrieved for match :' + matchId);
  const match = await Match
  .findById(matchId)
  .exec();
  if (match) {
    return match;
  }
  throw new APIException('No match found', 400);
};
exports.getMatchRecord = getMatchRecord;

/**
* Provide match details from the DB
*/
exports.getMatchWithTeams = async (matchId) => {
  winston.info('Service Match | match details with teams retrieved for match :' + matchId);
  const match = await Match
  .findById(matchId)
  .populate('awayteam hometeam')
  .exec();
  if (match) {
    return match;
  }
  throw new APIException('No match found', 400);
};

exports.getMatchScore = async (matchId) => {
  winston.info('Service Match | match details searched for match :' + matchId);
  const match = await getMatchRecord(matchId);
  return match.result;
};

/**
* Get the matches for a specific day
*/
exports.getMatchesWithTeams = async (options) => {
  winston.info('Service Match | match details searched');
  const selector = {};
  let datetimeSorting = -1;
  if (options.date) {
    const date = options.date.replace(' ', '+'); // hack to make the date valid as + from the url is replaced by a blank space
    const current = moment(date);
    // Check if the date given is valid
    if (!current.isValid()) {
      return { status: 400, message: 'invalid date' };
    }
    const start = new Date(current.startOf('day').format());
    const end = new Date(current.endOf('day').format());
    selector.datetime = {
      $lte: end,
      $gt: start
    };
  }
  if (options.status) {
    if (options.status === 'active') {
      selector.status = 'active';
      selector.datetime = { $exists: true };
      datetimeSorting = 1;
    } else if (options.status === 'started') {
      selector.status = 'active';
      selector.datetime = {
        $lte: new Date()
      }; // Override datetime from date option
    } else if (options.status === 'to_be_computed' || options.status === 'to_be_computed_back') {
      selector.status = options.status;
    } else {
      return { status: 400, message: 'invalid status' };
    }
  }
  const matches = await Match
  .find(selector)
  .sort({ datetime: datetimeSorting })
  .skip(options.page * options.limit)
  .limit(options.limit)
  .populate('awayteam hometeam')
  .exec();
  if (options.count) {
    const count = await Match.count(selector).exec();
    return { status: 200, matches, count };
  }
  return matches;
};

/**
* Update match details
*/
exports.updateMatchInfo = async (matchId, info) => {
  winston.info('Service Match | match info updated to + ' + JSON.stringify(info) + ' for match : ' + matchId);
  const match = await Match
  .findByIdAndUpdate(matchId, { $set: info })
  .exec();
  if (match) {
    if (info.rule) {
      SvcCompetitionAdmin.computeCompetitionRules(match.competition);
    }
    return { status: 200, message: 'Match successfully updated' };
  }
  return { status: 200, message: 'No match found' };
};

/**
* Update match result
*/
exports.updateMatchResult = async (matchId, result, sport) => {
  winston.info('Service Match | match result updated for match ' + matchId);
  if (!result || result === {}) {
    return { status: 400, message: 'invalid match result' };
  }
  // check if match result is valid
  let isValid = false;
  // Football or rugby (tries are optional)
  if (sport === 'football' || sport === 'rugby') {
    if (result.hometeam && result.awayteam && result.hometeam.score !== null
      && result.awayteam.score !== null) {
      isValid = true;
    }
  } else {
    return { status: 400, message: 'invalid sport' };
  }

  if (!isValid) {
    return { status: 400, message: 'invalid match result' };
  }
  const updateReport = await Match
  .update({
    _id: matchId,
    sport: sport // ,
    // status: 'active'
  }, {
    $set: {
      result: result,
      status: 'to_be_computed'
    }
  })
  .exec();
  if (updateReport) {
    return { status: 200, message: 'Match result successfully updated' };
  }
  return { status: 200, message: 'No match found to be updated' };
};

/**
* Provide match details from the DB
*/
exports.hasStarted = async (matchId) => {
  winston.info('Service Match | match details searched for match :' + matchId);
  const match = await getMatchRecord(matchId);
  return match.datetime >= Date.now();
};
