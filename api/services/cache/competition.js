const cache = require('./cache.js');

/**
* Get value from the key
*/
exports.get = (key) => {
  return cache.get('competition' + key);
};

/**
*  Store value for a given key with an expiry limit of 5mins
*/
exports.store = (key, competition) => {
  return cache.set('competition' + key, competition, 60 * 5);
};

/**
* Delete value from the cache
*/
exports.flush = (key) => {
  cache.del('competition' + key);
};
