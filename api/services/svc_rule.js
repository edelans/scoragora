const winston = require('winston');

const APIException = require('../routes/APIException.js');

const Match = require('../models/matches.js');
const Rule = require('../models/rules.js');

/**
* Get rule detail from the DB
*/
exports.getRule = async (ruleId) => {
  winston.info('Service Rule | rule retrieved for id: ' + ruleId);
  const rule = await Rule
  .findById(ruleId)
  .exec();
  if (rule) {
    return rule;
  }
  winston.info('Service Match | no rule found for id ' + ruleId);
  throw new APIException('No rule found', 400);
};

/**
* Provide rules from the DB
*/
exports.getRules = async (options) => {
  winston.info('Service Rule | rules retrieved');
  const selector = {};
  if (options.search) {
    selector.name = new RegExp(options.search, 'i');
  }
  if (options.sport) {
    selector.sport = options.sport;
  }
  const rules = await Rule
  .find(selector)
  .skip(options.page * options.limit)
  .limit(options.limit)
  .sort({ name: 1 })
  .exec();
  if (!options.count) {
    return rules;
  }
  const count = await Rule.count(selector).exec();
  return { status: 200, rules, count };
};

/**
* Insert a new rule in the DB
*/
exports.createRule = async (rule) => {
  winston.info('Service Rule | rules creation');
  if (!rule) return { status: 400, message: 'no rule given' };
  if (!rule.sport) return { status: 400, message: 'sport is missing' };
  if (!rule.name) return { status: 400, message: 'name is missing' };
  if (!rule.criterias) return { status: 400, message: 'at least one criteria is required' };
  const document = new Rule({
    name: rule.name,
    sport: rule.sport,
    criterias: []
  });
  rule.criterias.forEach((criteria) => {
    if (criteria.points > 0) {
      document.criterias.push({
        name: criteria.name,
        points: criteria.points
      });
    }
  });
  if (document.criterias.length === 0) {
    return { status: 400, message: 'at least one criteria is required' };
  }
  await document.save();
  return { status: 200, message: 'rule successfully created' };
};

/**
* Delete a given rule in the DB
*/
exports.deleteRule = async (ruleId) => {
  winston.info('Service Rule | rules deletion');
  // Check if deletion is possible
  const match = await Match
  .findOne({ rule: ruleId })
  .exec();
  if (match) {
    return { status: 400, message: 'Deletion not allowed' };
  }
  await Rule
  .findByIdAndRemove(ruleId)
  .exec();
  return { status: 200, message: 'rule successfully deleted' };
};
