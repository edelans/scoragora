const winston = require('winston');
const moment = require('moment');

const User = require('../models/users.js');

/**
* Provide rules from the DB
*/
exports.getUserLanguages = async () => {
  winston.info('Service Stats | User languages retrieved');
  return User
  .aggregate([{
    $group: {
      _id: { lng: '$lng' },
      count: { $sum: 1 }
    }
  }, {
    $project: {
      _id: 0,
      value: '$_id.lng',
      count: '$count'
    }
  }])
  .exec();
};

exports.getUserCreations = async (options) => {
  winston.info('Service User | user creations');
  if (!options.period || options.period !== 'day') {
    return { status: 400, message: 'invalid period' };
  }
  if (!options.start || !options.end) {
    return { status: 400, message: 'missing date' };
  }
  const start = parseInt(options.start, 10);
  const end = parseInt(options.end, 10);
  if (isNaN(start) || isNaN(end)) {
    return { status: 400, message: 'invalid date' };
  }
  let group;
  if (options.period === 'day') {
    group = { dayOfYear: { $dayOfYear: '$createdAt' }, year: { $year: '$createdAt' } };
  }
  const results = await User
  .aggregate([{
    $match: {
      createdAt: {
        $lte: new Date(end * 1000),
        $gt: new Date(start * 1000)
      }
    }
  }, {
    $group: {
      _id: group,
      count: { $sum: 1 }
    }
  }])
  .exec();
  if (results && options.period === 'day') {
    const dayResults = [];
    results.forEach((item) => {
      dayResults.push({
        date: moment().startOf('day').dayOfYear(item._id.dayOfYear).year(item._id.year)
      .format('DD/MM/YYYY'),
        count: item.count
      });
    });
    return dayResults;
  }
  return results;
};
