const winston = require('winston');

const pdf = require('html-pdf');
const i18next = require('i18next');
const path = require('path');
const moment = require('moment');

const League = require('../models/leagues.js');
const User = require('../models/users.js');
const SvcLeague = require('./svc_league.js');
const SvcUser = require('./svc_user.js');

const PDF_OPTIONS = {
  format: 'A4',
  orientation: 'portrait',
  type: 'pdf',
  localToRemoteUrlAccessEnabled: true,

	border: {
		top: '10mm',
		bottom: '10mm',
		left: '15mm',
		right: '15mm'
	},
  footer: {
  	height: '10mm',
  	contents: '<div style="text-align: right;"><span style="color: #444;">{{page}}</span>/<span>{{pages}}</span></div>'
  },
  timeout: 30000
};

const staticRoot = 'file://' + path.resolve(__dirname, '../../server/public');

/**
 * Generate ranking pdf and pipe it
 */
exports.getRankingPDF = async (league, dateInput, req, res) => {
  winston.info('Service League Enterprise | generate ranking pdf for league ' + league._id);
  const date = (!dateInput || dateInput === '') ? moment().format('DD/MM/YYYY') : dateInput;
  const user = await SvcUser.getUser(req.user);
  const lng = user.lng || 'en';
  const members = await SvcLeague.getLeagueMembers(league);
	// populate members.user
	const membersMap = {};
	members.forEach((member) => {
		// set a img link for phantomjs
		if (member.pictureUrl.match(/^\//)) {
			member.pictureUrl = staticRoot + member.pictureUrl;
		}
		members[member._id] = member;
	});

	i18next.changeLanguage(lng || 'en', function(err, t) {
		if (err) {
			winston.info('Svc League Enterprise | error when setting the language for pdf ranking : ' + err);
			return res.status(500).send({status: 500, message: 'internal server error'});
		}
		res.render('pdfs/ranking.jade', {league: league, members: members, date: date, t: t, staticRoot: staticRoot}, function(err, html) {
			if (err) {
				winston.info('Svc League Enterprise | error when generating html for pdf ranking : ' + err);
				return res.status(500).send({status: 500, message: 'internal server error'});
			}
			winston.debug(html);
			pdf.create(html, PDF_OPTIONS).toStream(function(err, stream) {
				if (err) {
					winston.info('Svc League Enterprise | error when generating pdf ranking : ' + err);
					return res.status(500).send({status: 500, message: 'internal server error'});
				}
				res.setHeader('Content-disposition', 'attachment; filename="' + league.url + '_' + t('ranking.title') + '.pdf"');
				res.setHeader('Content-type', 'application/pdf');
				stream.pipe(res);
				//stream.pipe(fs.createWriteStream('./foo.pdf'));
			});
		});
	});
};


/**
 * Update member status
 */
exports.updateLeagueMemberStatus = async (league, userId, status) => {
	winston.info('Service League Enterprise | update status to ' + status + ' for member ' + userId + ' in league ' + league._id);
	if (!league.members) {
		winston.error('Service League | league ' + league._id + ' does\'nt have members');
		return { status: 500, code: 50000, message: 'internal server error' };
	}
	// Prevent captain banning
	const isCaptain = league.captains.find(captain => captain.equals(userId));
	if (isCaptain) {
		winston.info('Service League | user ' + userId + ' is captain in league ' + league._id);
		return { status: 400, message: 'user not allowed' };
	}
	const user = league.members.find(member => member.user.equals(userId));
	if (!user) {
		winston.info('Service League | user ' + userId + ' not found in league ' + league._id);
		return { status: 400, message: 'user doesnt belong to the league' };
	}
	await League.update({
		_id: league._id,
		'members.$.user' : userId,
	}, {
		'members.$.status': status,
	}).exec();
	return { status: 200, message: 'member status successfully updated' };
};

/**
 * Update member flag
 */
exports.updateLeagueMemberFlag = async (league, userId, flag) => {
	winston.info('Service League Enterprise | update flag to ' + flag + ' for member ' + userId + ' in league ' + league._id);
  if (!league.members) {
		winston.error('Service League | league ' + league._id + ' does\'nt have members');
		return { status: 500, code: 50000, message: 'internal server error' };
	}
	const user = league.members.find(member => member.user.equals(userId));
	if (!user) {
		winston.info('Service League | user ' + userId + ' not found in league ' + league._id);
		return { status: 400, message: 'user doesnt belong to the league' };
	}
	const update = {};
	if (flag) {
		update.$set = {
			'members.$.flag': flag.value
		};
	}	else {
		update.$unset = {
			'members.$.flag': 1
		};
	}
	await League.update({
		_id: league._id,
		'members.user': userId
	}, {
		$set: {
			'members.$.flag': flag
		}
	}).exec();
	return { status: 200, message: 'member flag successfully updated' };
};
