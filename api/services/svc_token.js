const winston = require('winston');
const crypto = require('crypto');

const User = require('../models/users.js');
const Token = require('../models/tokens.js');

const SvcEmail = require('./svc_email.js');

exports.getToken = async (token) => {
  winston.info('Service Token | retrieve token ' + token);
  return Token.findOne({ token });
};

exports.createRecoveryToken = async (email) => {
  winston.info('Service Token | create recovery token for ' + email);
  // Ensure email exists and is a string
  if (!email || typeof email !== 'string') {
    return { status: 400, message: 'invalid email' };
  }
  const user = await User.findOne({
    'login.local.email': email
  }).exec();
  if (!user) {
    return { status: 400, message: 'no user found for email ' + email };
  }
  const token = await new Promise((resolve) => {
    crypto.randomBytes(48, (ex, buf) => {
      resolve(buf.toString('hex'));
    });
  });
  await Token.update({
    user: user._id
  }, {
    token: token,
    datetime: new Date(),
    user: user._id
  }, {
    upsert: true
  }).exec();
  const link = 'http://www.scoragora.com/#/passwordreset?email=' + email + '&token=' + token;
  return SvcEmail.sendResetPassword(email, link, { lng: user.lng });
};
