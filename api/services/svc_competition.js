const winston = require('winston');

const APIException = require('../routes/APIException.js');

const Competition = require('../models/competitions.js');
const Match = require('../models/matches.js');
const Forecast = require('../models/forecasts.js');

const CompetitionCache = require('./cache/competition.js');

/**
* Provide competition details from the DB
* teams and automation fields should be excluded
* As this service is highly requested from the client interface, resources are cached
*/
const getCompetition = async (competitionId) => {
  winston.info('Service Competition | Competition details retrieved for competition :' + competitionId);
  // first try to get it from the cache
  const cachedCompetition = CompetitionCache.get(competitionId);
  if (cachedCompetition) {
    winston.info('Service Competition | competition ' + competitionId + ' found in the cache');
    return cachedCompetition;
  }

  // then target the db
  winston.info('Service Competition | competition ' + competitionId + ' not in the cache');
  const competition = await Competition
  .findById(competitionId)
  .select({ teams: 0, automation: 0 })
  .exec();
  if (!competition) {
    winston.info('Service Competition | no competition found for id ' + competitionId);
    throw new APIException('competition not found', 400);
  }
  // cache the resource
  CompetitionCache.store(competition._id, competition.toObject());
  return competition;
};
exports.getCompetition = getCompetition;

/**
* Provide competition teams details from the DB
*/
exports.getCompetitionTeams = async (competitionId) => {
  winston.info('Service Competition | Teams retrieved for competition :' + competitionId);
  const competition = await Competition
  .findById(competitionId)
  .populate('teams.team')
  .exec();
  if (!competition) {
    winston.info('Service Competition | no competition found for id ' + competitionId);
    throw new APIException('competition not found', 400);
  }
  return competition.teams;
};


/**
* Provide competition matches details from the DB
*/
exports.getCompetitionMatches = async (competitionId, options) => {
  winston.info('Service Competition | Matches retrieved for competition :' + competitionId);
  const limit = options.limit || 1000000;
  const selector = { competition: competitionId };
  const order = {};
  if (options.past) {
    selector.datetime = {
      $lte: new Date()
    };
    order.datetime = -1;
  }
  if (options.status) {
    selector.status = options.status;
  }
  const matches = await Match
  .find(selector)
  .limit(limit)
  .sort(order)
  .select({ automation: 0, forecast: 0 })
  .exec();
  if (matches.length === 0) {
    // check if the competition exists
    await getCompetition(competitionId);
  }
  return matches;
};

/**
* Provide competition teams details from the DB
* Todo to be merge with getCompetitions
*/
exports.getPublicOrReadyCompetitions = async (options) => {
  winston.info('Service Competition | Ready and public competitions details retrieved');
  return Competition
  .find({ $or: [{ status: 'public' }, { status: 'ready' }] })
  .skip(options.page * options.limit)
  .limit(options.limit)
  .select({ name: 1, year: 1, sport: 1, 'config.type': 1 })
  .exec();
};

/**
* Provide competition teams details from the DB
*/
exports.getSummarizedCompetitions = async (options) => {
  winston.info('Service Competition | Summarized competitions retrieved');
  const selector = {};
  if (options.search) {
    selector.name = new RegExp(options.search, 'i');
  }
  if (options.sport) {
    if (!options.sport.match(/[football|rugby|tennis]/i)) {
      return { status: 404, message: 'invalid sport' };
    }
    selector.sport = options.sport;
  }
  if (options.status) {
    selector.status = options.status;
  }
  const competitions = await Competition
  .find(selector)
  .skip(options.page * options.limit)
  .limit(options.limit)
  .sort({ status: -1 })
  .select('name year sport status')
  .exec();
  if (options.count) {
    const count = await Competition.count(selector).exec();
    return { status: 200, competitions, count };
  }
  return competitions;
};

exports.getCompetitionRanking = async (competitionId, limit) => {
  winston.info('Service Competition | Get ranking for competition ' + competitionId);
  const forecasts = await Forecast
  .find({ competition: competitionId })
  .sort({ points: -1 })
  .limit(limit)
  .populate('user')
  .exec();
  const users = [];
  if (forecasts) {
    let rank = 0;
    let points = -1;
    forecasts.forEach((forecast, index) => {
      if (points !== forecast.points) {
        rank = index + 1;
        points = forecast.points;
      }
      users.push({
        points: forecast.points,
        rank: rank,
        pictureUrl: forecast.user.pictureURL(),
        displayName: forecast.user.displayName(),
        user: forecast.user._id,
        forecast: forecast._id
      });
    });
  }
  return users;
};
