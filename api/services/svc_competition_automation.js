const winston = require('winston');

const Competition = require('../models/competitions.js');
const Match = require('../../api/models/matches.js');
const Team = require('../models/teams.js');

const LequipeCompetitionScrapper = require('../../scheduler/algos/scrappers_competition/lequipe.js');
const EurosportCompetitionScrapper = require('../../scheduler/algos/scrappers_competition/eurosport.js');
const EspnCompetitionScrapper = require('../../scheduler/algos/scrappers_competition/espn.js');

const SCRAPPER = {
  lequipe: LequipeCompetitionScrapper,
  eurosport: EurosportCompetitionScrapper,
  espn: EspnCompetitionScrapper
};

class MatchUpdate {
  constructor(info, manager) {
    this.info = info;
    this.manager = manager;
  }

  async process() {
    await this.identifyMatch();
    if (this.match) {
      this.updateMatch();
      return;
    }
    if (this.hometeam && this.awayteam) {
      this.initMatch();
    }
  }

  /**
  * Identify match
  */
  async identifyMatch() {
    if (this.info.url && this.info.date) {
      // Identify match based on url
      winston.debug('Service Competition Automation | get match from automation url ' + this.info.url);
      const selector = {
        competition: this.manager.competition._id
      };
      selector['automation.' + this.manager.source + '.url'] = this.info.url;
      const match = await Match.findOne(selector).exec();
      if (match) {
        this.match = match;
        return;
      }
      winston.debug('Service Competition Automation | no match found for automation url ' + this.info.url);
      await this.identifyMatchFromTeams();
      return;
    }

    // In case of date or url update
    if (this.info.hometeam && this.info.awayteam && (this.info.date || this.info.url)) {
      await this.identifyMatchFromTeams();
      return;
    }

    winston.debug('Service Competition Automation | nothing to do for match ' + this.info);
  }

  /**
  * Update match info
  */
  async updateMatch() {
    winston.info('Service Competition Automation | update match ' + this.match._id);
    const update = {};
    if (this.info.url) {
      update['automation.' + this.manager.source + '.url'] = this.info.url;
    }
    // TODO date
    if (this.info.date) {
      update.datetime = this.info.date;
    }
    await Match.update({ _id: this.match._id }, { $set: update }).exec().catch((err) => {
      winston.error('Service Competition Automation | error when updating match ' + this.match._id + ' : ' + err);
    });
  }


  /**
  * Initialised match based on info
  */
  async initMatch() {
    winston.info('Service Competition Automation | initialise match between ' + this.hometeam.name + ' and ' + this.awayteam.name);
    const matchId = this.manager.getAvailableMatchId(this.info.day);
    if (!matchId) {
      return;
    }
    winston.info('Service Competition Automation | match ' + matchId + ' picked to be initialised');
    const update = {
      hometeam: this.hometeam._id,
      awayteam: this.awayteam._id,
      hometeamname: this.hometeam.name,
      awayteamname: this.awayteam.name
    };
    if (this.info.url) {
      update['automation.' + this.manager.source + '.url'] = this.info.url;
    }
    // TODO date
    if (this.info.date) {
      update.datetime = this.info.date;
    }
    await Match.update({ _id: matchId }, { $set: update }).exec().catch((err) => {
      winston.error('Service Competition Automation | error when initialising match ' + matchId + ' : ' + err);
    });
  }

  /**
  * Identify match from teams
  */
  async identifyMatchFromTeams() {
    if (this.info.hometeam && this.info.awayteam) {
      const hometeam = await this.manager.identifyTeam(this.info.hometeam.url, this.info.hometeam.name);
      const awayteam = await this.manager.identifyTeam(this.info.awayteam.url, this.info.awayteam.name);
      if (hometeam) {
        this.hometeam = hometeam;
      }
      if (awayteam) {
        this.awayteam = awayteam;
      }
      if (awayteam && hometeam) {
        await this.findMatchFromTeams();
      }
    }
  }

  async findMatchFromTeams() {
    // Try to identify a match from the teams we found
    const match = await Match.findOne({
      hometeam: this.hometeam._id,
      awayteam: this.awayteam._id,
      competition: this.manager.competition._id
    }).exec();
    if (match) {
      this.match = match;
    } else {
      winston.debug('Service Competition Automation | no match found for team ' + this.hometeam._id + ' and ' + this.awayteam._id);
    }
  }
}

/**
* Object handling the match update
*/
class MatchUpdateManager {
  constructor(competition, source) {
    this.competition = competition;
    this.source = source;
    this.matchesToBeCreated = [];
    this.teamsFromUrl = {};
    this.teamsFromName = {};

    // bind async methods
    this.init = this.init.bind(this);
    this.identifyTeam = this.identifyTeam.bind(this);
  }

  adaptInfo(info) {
    if (this.source === 'lequipe') {
      const year = parseInt(this.competition.year, 10);
      info.url = info.url.replace('/d/', `/${year - 1}-${year}/`);
    }
    return info;
  }

  processMatch(info) {
    const matchInfo = this.adaptInfo(info);
    const matchUpdate = new MatchUpdate(matchInfo, this);
    matchUpdate.process();
  }

  /**
  * Initialised the data for the manager
  */
  async init() {
    if (this.matchesToBeCreated.length) {
      return;
    }
    winston.debug('Service Competition Automation | initialising update manager');
    if (!this.competition.matches) {
      winston.debug('Service Competition Automation | update manager can\'t be updated without competition matches');
      return;
    }
    const matchIds = [];
    this.competition.matches.forEach((item) => {
      matchIds.push(item.match);
    });
    const unsetMatchIds = await Match.distinct('_id', {
      _id: {
        $in: matchIds
      },
      hometeam: null,
      awayteam: null
    }).exec() || [];
    const ids = [];
    unsetMatchIds.forEach((item) => {
      ids.push(item.toString());
    });

    this.matchesToBeCreated = [];
    this.competition.matches.forEach((item) => {
      if (ids.indexOf(item.match.toString()) > -1) {
        this.matchesToBeCreated.push({
          match: item.match,
          day: item.day
        });
      }
    });
  }

  /**
  * Get and remove match id
  */
  getAvailableMatchId(day) {
    for (let i = 0; i < this.matchesToBeCreated.length; i += 1) {
      if (this.matchesToBeCreated[i].day === day) {
        const matchId = this.matchesToBeCreated[i].match;
        this.matchesToBeCreated.splice(i, 1);
        winston.debug('Service Competition Automation | get match id ' + matchId + ' for day ' + day);
        return matchId;
      }
    }
    return null;
  }

  /**
  * Identify team
  */
  async identifyTeam(url, name) {
    if (!url && !name) {
      return null;
    }
    // Try to get the team from the cache
    if (url && this.teamsFromUrl[url]) {
      return this.teamsFromUrl[url].team;
    }
    if (name && this.teamsFromName[name]) {
      return this.teamsFromName[name].team;
    }
    const selector = {
      sport: this.competition.sport
    };
    if (url) {
      selector['automation.' + this.source + '.url'] = url;
    } else if (name) {
      selector['automation.' + this.source + '.name'] = name;
    }
    const team = await Team.findOne(selector).exec();
    if (!team) {
      winston.error('Service Competition Automation | no team found for info ', (url || name));
    }
    // Cache the team
    if (url) {
      this.teamsFromUrl[url] = { team };
    } else if (name) {
      this.teamsFromName[name] = { team };
    }
    return team;
  }
}

/**
* Scrap matches info from the source provided
*/
exports.scrapCompetitionMatches = async (competitionId, source) => {
  winston.info('Service Competition Automation | compute match details from scrapping for competition ' + competitionId);
  const competition = await Competition.findOne({ _id: competitionId }).exec();
  if (competition.automation && competition.automation[source]
    && competition.automation[source].url) {
    if (competition.config && competition.config.type === 'championship') {
      // Iterate the sources
      // lequipe.fr
      if (source === 'lequipe' || source === 'eurosport' || source === 'espn') {
        const response = await SCRAPPER[source].scrapChampionshipMatches(competition.automation[source].url);
        if (response.status !== 200) {
          return response;
        }
        const manager = new MatchUpdateManager(competition, source);
        await manager.init();
        response.matches.forEach(item => manager.processMatch(item));
        return { status: 200, message: 'Competition scrapped successfully' };
      }
    }
  }
  return { status: 400, message: 'source not supported for competition scrapping' };
};
