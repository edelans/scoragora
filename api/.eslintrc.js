module.exports = {
    "plugins": ["jasmine"],
    "extends": "airbnb-base/legacy",
    "env": {
      "jasmine": true
    },
    "parserOptions": {
      "ecmaVersion": 8
    },
    "rules": {
      "no-underscore-dangle": ["error", { "allow": ["_id", "__dirname"] }]
    }
};
