require('pmx').init();
const express = require('express');
const morgan = require('morgan');
const bodyParser = require('body-parser');
const cookieParser = require('cookie-parser');
const methodOverride = require('method-override');
const https = require('https');
const http = require('http');
const winston = require('winston');
const mongoose = require('mongoose');
const i18n = require('i18next');
const FilesystemBackend = require('i18next-node-fs-backend');

const app = exports.app = express();

const cfg = require('../config');
const SvcEmail = require('./services/svc_email.js');

const competitionRouter = require('./routes/competition.js').router;
const competitionAdminRouter = require('./routes/competition_admin.js').router;
const forecastRouter = require('./routes/forecast.js').router;
const leagueRouter = require('./routes/league.js').router;
const leagueAdminRouter = require('./routes/league_admin.js').router;
const loginRouter = require('./routes/login.js').router;
const loginAdminRouter = require('./routes/login_admin.js').router;
const matchRouter = require('./routes/match.js').router;
const matchAdminRouter = require('./routes/match_admin.js').router;
const premiumLeagueRouter = require('./routes/league_enterprise.js').router;
const ruleAdminRouter = require('./routes/rule_admin.js').router;
const statAdminRouter = require('./routes/stat_admin.js').router;
const teamAdminRouter = require('./routes/team_admin.js').router;
const tokenRouter = require('./routes/token.js').router;
const userRouter = require('./routes/user.js').router;
const userAdminRouter = require('./routes/user_admin.js').router;

// configurate winston
winston.remove(winston.transports.Console);

if (process.env.NODE_ENV === 'production') {
  /**
  * Requiring 'winston-mongodb' will expose winston.transports.MongoDB
  */
  require('winston-mongodb').MongoDB;
  cfg.winstonLogs.label = 'api';
  winston.add(winston.transports.MongoDB, cfg.winstonLogs);
} else {
  winston.add(winston.transports.Console, {
    colorize: true,
    prettyPrint: true,
    timestamp: true
  });
}

app.set('views', __dirname + '/views');
app.set('view engine', 'jade');
app.disable('x-powered-by'); // security measure

// i18n config
// must be after app definition
i18n
.use(FilesystemBackend)
.init({
  ignoreRoutes: ['public/'],
  debug: false,
  supportedLngs: ['fr', 'en'],
  fallbackLng: 'en',
  backend: {
    loadPath: __dirname + '/locales/{{lng}}/{{ns}}.json'
  }
});

SvcEmail.init(app, i18n);

app.use(express.static(__dirname + '/public')); // set the static files location /public/img will be /img for users
app.use(morgan('dev')); // log every request to the console
app.use(bodyParser.urlencoded({
  extended: true
}));
app.use(bodyParser.json()); // pull information from html in POST
app.use(methodOverride()); // simulate DELETE and PUT

const connectToDB = async () => {
  // Do not connect to database on test, it is done elsewhere
  if (process.env.NODE_ENV !== 'test') {
    await mongoose.connect(cfg.mongo.uri, { useMongoClient: true });
    winston.log('App | Successfully connected to database');
  }
};

connectToDB().then(() => {
  app.use(cookieParser()); // Parser cookie if needed (especially for auth)

  const hasAllowedHostname = (req) => {
    const hostname = req.hostname;
    if (!hostname) {
      return false;
    }
    if (hostname.match(/scoragora.com$/)) {
      return true;
    }
    if (process.env.NODE_ENV === 'production' || process.env.NODE_ENV === 'staging') {
      return false;
    }
    if (hostname === 'localhost') {
      return true;
    }
    if (hostname.match(/\/\/192\.168(\.[0-9]{1,3}){2}$/)) {
      return true;
    }
    // return false; TODO : fix after first cdm18 fixture
    return true;
  };

  // Add headers
  app.use((req, res, next) => {
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, OPTIONS');
    if (hasAllowedHostname(req)) {
      let origin = (req.headers.origin == null) ? '' : req.headers.origin;
      res.setHeader('Access-Control-Allow-Origin', origin);
      res.setHeader('Access-Control-Allow-Headers', 'Authorization, Origin, X-Requested-With, Content-Type, Accept');
      res.setHeader('Access-Control-Allow-Credentials', true);
    }

    if (req.method === 'OPTIONS') {
      res.status(200).end();
    } else {
      next();
    }
  });

  const router = express.Router(); // get an instance of the express Router

  router.use('/login', loginRouter);
  router.use('/competitions', competitionRouter);
  router.use('/forecasts', forecastRouter);
  router.use('/leagues', leagueRouter);
  router.use('/premium/leagues', premiumLeagueRouter);
  router.use('/matches', matchRouter);
  router.use('/tokens', tokenRouter);
  router.use('/users', userRouter);
  router.use('/admin/competitions', competitionAdminRouter);
  router.use('/admin/login', loginAdminRouter);
  router.use('/admin/leagues', leagueAdminRouter);
  router.use('/admin/matches', matchAdminRouter);
  router.use('/admin/rules', ruleAdminRouter);
  router.use('/admin/stats', statAdminRouter);
  router.use('/admin/teams', teamAdminRouter);
  router.use('/admin/users', userAdminRouter);

  // Declare the API base route
  app.use('/api', router);

  // var options = {
  //   key: fs.readFileSync('../config/https/server.key'),
  //   cert: fs.readFileSync('../config/https/server.crt')
  // };
  const httpInstance = http.createServer(app);
  //https.createServer(options, app).listen(cfg.scheduler_port);
  require('./sockets/index.js').initSocketIO(httpInstance);

  httpInstance.listen(cfg.api_port);
  winston.info('Creating a server on port ' + cfg.api_port + ' in ' + cfg.env + ' mode');
});
