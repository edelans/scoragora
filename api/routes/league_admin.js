const express = require('express');

const checker = require('./checker.js');
const handleRes = require('./utils.js').handleRes;

const SvcLeague = require('../services/svc_league.js');

// utility function needed for parsing string args in request
const getAsInt = n => Math.ceil(Number(n));

const app = express.Router();

app.route('/')

  // Get all the leagues
  .get(checker.checkAdmin, (req, res) => {
    const options = {
      competition: req.query.competition,
      count: req.query.count || 'false',
      limit: getAsInt(req.query.limit) || 10,
      minsize: req.query.minsize,
      page: getAsInt(req.query.page) || 0,
      search: req.query.search || ''
    };
    handleRes(SvcLeague.getLeagues(options), res, 'leagues');
  });

app.route('/:id')

  /**
   * Get league details
   */
  .get(checker.checkAdmin, (req, res) => {
    handleRes(SvcLeague.getLeague(req.params.id), res, 'league');
  })

  /**
   * Update league
   */
  .put(checker.checkAdmin, (req, res) => {
    if (req.body.status) {
      handleRes(SvcLeague.updateLeagueStatus(req.params.id, req.body.status), res);
      return;
    }
    if (req.body.sponsor) {
      handleRes(SvcLeague.updateLeagueSponsor(req.params.id, req.body.sponsor), res);
      return;
    }
    res.status(400).send({ status: 400, message: 'not implemented' });
  });

app.route('/:id/users')

  /**
   * Get league users
   */
  .get(checker.checkAdmin, (req, res) => {
    handleRes(SvcLeague.getLeagueUsers(req.params.id), res, 'users');
  });

exports.router = app;
