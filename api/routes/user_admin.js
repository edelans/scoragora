const express = require('express');

const checker = require('./checker.js');
const handleRes = require('./utils.js').handleRes;

const SvcUser = require('../services/svc_user.js');

// utility function needed for parsing string args in request
const getAsInt = n => Math.ceil(Number(n));

const app = express.Router();

app.route('/')

  // Get all the users
  .get(checker.checkAdmin, (req, res) => {
    const limit = getAsInt(req.query.limit) || 10;
    const page = getAsInt(req.query.page) || 0;
    const search = req.query.search || '';
    const count = req.query.count || 'false';
    handleRes(SvcUser.getUsers({ search, page, limit, count }), res);
  });

app.route('/:id')

  /* Get user details
  */
  .get(checker.checkAdmin, (req, res) => {
    const userId = req.params.id;
    handleRes(SvcUser.getUserRecord(userId), res, 'user');
  });

  app.route('/:id/forecasts')

    /* Get user details
    */
    .get(checker.checkAdmin, (req, res) => {
      const userId = req.params.id;
      const options = {
        limit: getAsInt(req.query.limit) || 5,
        page: getAsInt(req.query.page) || 0
      };
      console.log(req.query, options);
      handleRes(SvcUser.getUserForecastSummaries(userId, options), res, 'forecasts');
    });

exports.router = app;
