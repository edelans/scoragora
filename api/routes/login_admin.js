const express = require('express');
const jwt = require('jwt-simple');
const moment = require('moment');

const cfg = require('../../config');

const checker = require('./checker.js');

/**
* Generate a token for admin user
*/
const createAdminJWT = () => {
  const payload = {
    admin: true,
    iat: moment().unix(),
    exp: moment().add(1, 'hours').unix()
  };
  return jwt.encode(payload, cfg.secret);
};

const app = express.Router();

app.route('/')

  .get(async (req, res) => {
    const payload = checker.extractDataFromToken(req);
    if (payload && payload.sub && payload.admin) {
      return res.send({
        status: 200,
        login: true
      });
    }
    return res.send({
      status: 200,
      login: false,
      message: 'Unauthorized access'
    });
  })
  .post((req, res) => {
    if (req.body.login === cfg.admin.user && req.body.password === cfg.admin.password) {
      return res.send({
        status: 200,
        login: true,
        token: createAdminJWT()
      });
    }
    return res.status(401).send({ status: 401 });
  });

exports.router = app;
