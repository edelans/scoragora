const express = require('express');

const checker = require('./checker.js');
const handleRes = require('./utils.js').handleRes;

const SvcStat = require('../services/svc_stat.js');

const app = express.Router();

app.route('/users/language')
  .get(checker.checkAdmin, (req, res) => {
    handleRes(SvcStat.getUserLanguages(), res, 'languages');
  });

app.route('/users/creation')
  .get(checker.checkAdmin, (req, res) => {
    handleRes(SvcStat.getUserCreations({
      period: req.query.period,
      start: req.query.start,
      end: req.query.end
    }), res, 'creations');
  });

exports.router = app;
