const express = require('express');

const checker = require('./checker.js');
const handleRes = require('./utils.js').handleRes;

const SvcRule = require('../services/svc_rule.js');

// utility function needed for parsing string args in request
const getAsInt = n => Math.ceil(Number(n));

const app = express.Router();

app.route('/')

  // Get all the users
  .get(checker.checkAdmin, (req, res) => {
    const options = {
      limit: getAsInt(req.query.limit) || 10,
      page: getAsInt(req.query.page) || 0,
      search: req.query.search,
      count: req.query.count === 'true',
      sport: req.query.sport
    };
    handleRes(SvcRule.getRules(options), res, 'rules');
  })

  .post(checker.checkAdmin, (req, res) => {
    handleRes(SvcRule.createRule(req.body.rule), res);
  });

app.route('/:id')

  .get(checker.checkAdmin, (req, res) => {
    handleRes(SvcRule.getRule(req.params.id), res, 'rule');
  })

  .delete(checker.checkAdmin, (req, res) => {
    handleRes(SvcRule.deleteRule(req.params.id), res);
  });

exports.router = app;
