var winston = require('winston');
var jwt = require('jwt-simple');
var moment = require('moment');

var cfg = require('../../config');

var League = require('../models/leagues.js');

/**
* Extract data from the token and retrieve a valid payload
*/
exports.extractDataFromToken = (req) => {
  if (!req.headers || !req.headers.authorization) {
    winston.debug('no authorization header');
    return null;
  }
  const token = req.headers.authorization.split(' ')[1];
  let payload = null;
  try {
    payload = jwt.decode(token, cfg.secret);
  } catch (err) {
    winston.error('Checker | error catched when decoding token : ' + err);
    return null;
  }

  if (payload.exp <= moment().unix()) {
    return null;
  }
  return payload;
};

/**
* Check if the user is admin.
*/
exports.checkAdmin = (req, res, next) => {
  const payload = exports.extractDataFromToken(req);
  if (!payload || !payload.admin) {
    return res.status(401).send({ status: 401, message: 'Unauthorized access' });
  }
  req.user = payload.sub;
  return next();
};

/**
* Check if the user is logged in.
*/
exports.checkLogin = (req, res, next) => {
  const payload = exports.extractDataFromToken(req);
  if (!payload) {
    return res.status(401).send({ status: 401, message: 'Unauthorized access' });
  }
  req.user = payload.sub;
  return next();
};

/**
* Check if the user is logged in.
* Check if the user belongs to the league
* If it is the case return the corresponding league.
* Leverage on mongo process for calculus
*/
exports.checkLeagueMembership = async (req, res, next) => {
  const selector = {
    'members.user': req.user
  };
  const leagueId = req.params.leagueId;
  if (leagueId) {
    winston.info('Checker | attempting to retrieve league by id: ' + leagueId);
    selector._id = leagueId;
  } else if (req.query.url) {
    winston.info('Checker | attempting to retrieve league by url: ' + req.query.url);
    selector.url = req.query.url.toLowerCase();
  } else {
    winston.info('Checker | missing league identifier');
    res.status(404).send({ status: 404, message: 'League not found' });
    return;
  }
  const league = await League.findOne(selector).exec();
  if (league) {
    req.league = league;
    next();
    return;
  }
  delete selector['members.user'];
  const leagueById = await League.findOne(selector).exec();
  if (!leagueById) {
    // league does not exist
    winston.info('Checker | League ' + (leagueId || req.query.url) + ' does not exist');
    res.status(404).send({ status: 404, code: 40012, message: 'League not found' });
    return;
  }
  // there is a league but user is not member
  winston.info('Checker | User ' + req.user + ' does not belong to league ' + (req.params.leagueId || req.query.url));
  res.status(403).send({ status: 403, code: 40013, message: 'Unauthorized access' });
};

/**
* Check if the user is logged in.
* Check if the user belongs to the league
* Check if the league is an enterprise league
* If it is the case return the corresponding league.
*/
exports.checkPremiumLeague = (req, res, cb) => {
  if (!req.league) {
    throw Error('checkLeagueMemberShip need to be called first');
  }
  // check if the league is an premium one
  if (req.league.premium && req.league.premium.validated) {
    cb();
    return;
  }
  winston.info('Checker | league ' + req.league._id + ' is not a premium league');
  res.status(403).send({ status: 403, code: 40017, message: 'Unauthorized access' });
};

/**
* Check if the user is a captain
* If it is the case return the corresponding league.
*/
exports.checkLeagueCaptaincy = (req, res, cb) => {
  if (!req.league) {
    throw Error('checkLeagueMemberShip need to be called first');
  }
  let isCaptain = false;
  req.league.captains.forEach((captain) => {
    if (req.user === captain.toString()) {
      isCaptain = true;
    }
  });
  if (isCaptain) {
    cb();
    return;
  }
  // there is a league but user is not captain
  winston.info('Checker | User ' + req.user + ' is not captain of league ' + req.league._id);
  res.status(403).send({ status: 403, code: 40014, message: 'Unauthorized access' });
};

/**
* Check if the socket is logged in.
* If not there is nothing to do.
*/
exports.checkSocketLeagueMembership = (socket, leagueInfo, next) => {
  if (socket && socket.request && socket.request.user) {
    const user = socket.request.user;
    const selector = {
      'members.user': user
    };
    if (leagueInfo.id) {
      selector._id = leagueInfo.id;
    } else if (leagueInfo.url) {
      selector.url = leagueInfo.url;
    }
    League.findOne(selector, (err, league) => {
      if (err) {
        winston.error('Checker | Error when retrieving league ' + (leagueInfo.id || leagueInfo.url) + ' with user ' + user + ' as member : ' + err);
        return;
      }
      if (league) {
        next(user, league);
      }
    });
  }
};
