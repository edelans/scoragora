const express = require('express');

const checker = require('./checker.js');
const handleRes = require('./utils.js').handleRes;

const SvcLeagueEnterprise = require('../services/svc_league_enterprise.js');
const SvcLeague = require('../services/svc_league.js');

const app = express.Router();

app.route('/:leagueId/ranking')
  // get league ranking
  .get(checker.checkLogin, checker.checkLeagueMembership,
    checker.checkPremiumLeague, (req, res) => {
      SvcLeagueEnterprise.getRankingPDF(req.league, req.params.date, req, res);
    });

app.route('/:leagueId/members')

  .get(checker.checkLogin, checker.checkLeagueMembership,
    checker.checkLeagueCaptaincy, checker.checkPremiumLeague, (req, res) => {
      handleRes(SvcLeague.getLeagueMembers(req.league, true), res, 'members');
    });

app.route('/:leagueId/members/:userId')

  .put(checker.checkLogin, checker.checkLeagueMembership,
    checker.checkLeagueCaptaincy, checker.checkPremiumLeague, (req, res) => {
			if (req.body.status) {
				handleRes(SvcLeagueEnterprise.updateLeagueMemberStatus(req.league, req.params.userId, req.body.status), res);
				return;
			}
			if (req.body.flag) {
				handleRes(SvcLeagueEnterprise.updateLeagueMemberFlag(req.league, req.params.userId, req.body.flag.value), res);
				return;
			}
			res.status(400).send({ status: 400, message: 'invalid input' });
		});

exports.router = app;
