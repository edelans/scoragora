const express = require('express');

const checker = require('./checker.js');
const handleRes = require('./utils.js').handleRes;

const SvcCompetition = require('../services/svc_competition.js');
const SvcCompetitionAdmin = require('../services/svc_competition_admin.js');
const SvcCompetitionAutomation = require('../services/svc_competition_automation.js');

// utility function needed for parsing string args in request
const getAsInt = n => Math.ceil(Number(n));

const app = express.Router();

app.route('/')

  // Get all the competitions
  .get(checker.checkAdmin, (req, res) => {
    const options = {
      limit: getAsInt(req.query.limit) || 10,
      page: getAsInt(req.query.page) || 0,
      search: req.query.search,
      sport: req.query.sport,
      count: req.query.count === 'true',
      status: req.query.status
    };
    handleRes(SvcCompetition.getSummarizedCompetitions(options), res, 'competitions');
  })

  // Create a competition
  .post(checker.checkAdmin, (req, res) => {
    if (req.body.competition) {
      handleRes(SvcCompetitionAdmin.createCompetition(req.body.competition), res);
      return;
    }
    res.status(400).send({ status: 400, message: 'competition missing' });
  });

app.route('/:id')

  /*
  * Get competition details
  */
  .get(checker.checkAdmin, (req, res) => {
    handleRes(SvcCompetitionAdmin.getCompetitionRecord(req.params.id), res, 'competition');
  })

  /*
  * Update competition
  */
  .put(checker.checkAdmin, (req, res) => {
    if (req.body.status) {
      handleRes(SvcCompetitionAdmin.updateCompetitionStatus(req.params.id, req.body.status), res);
      return;
    }
    if (req.body.automation) {
      handleRes(
        SvcCompetitionAdmin.updateCompetitionAutomation(req.params.id, req.body.automation), res);
      return;
    }
    res.status(400).send({ status: 400, message: 'invalid input' });
  })

  /*
  * Delete competition
  */
  .delete(checker.checkAdmin, (req, res) => {
    handleRes(SvcCompetitionAdmin.deleteCompetition(req.params.id), res);
  });

app.route('/:id/matches')

  /*
  * Get competition details
  */
  .get(checker.checkAdmin, (req, res) => {
    handleRes(SvcCompetitionAdmin.getCompetitionMatchRecords(req.params.id), res, 'matches');
  });

app.route('/:id/teams')

  /*
  * Get competition details
  */
  .get(checker.checkAdmin, (req, res) => {
    handleRes(SvcCompetitionAdmin.getCompetitionTeamRecords(req.params.id), res, 'teams');
  })

  .post(checker.checkAdmin, (req, res) => {
    if (req.body.team) {
      handleRes(SvcCompetitionAdmin.addCompetitionTeam(req.params.id, req.body.team), res);
      return;
    }
    res.status(404).send({ status: 404, message: 'team missing' });
  });

app.route('/:id/forecasts')

  /**
  * Get competition forecasts
  */
  .get(checker.checkAdmin, (req, res) => {
    if (req.query.count === 'true') {
      handleRes(SvcCompetitionAdmin.countCompetitionForecasts(req.params.id), res, 'count');
      return;
    }
    if (req.query.creations) {
      handleRes(SvcCompetitionAdmin.getCompetitionForecastCreations(req.params.id), res, 'count');
      return;
    }
    const top = getAsInt(req.query.top) || 0;
    if (top) {
      handleRes(SvcCompetition.getCompetitionRanking(req.params.id, top), res, 'users');
      return;
    }
    res.status(404).send({ status: 404, message: 'not implemented' });
  });

app.route('/:id/leagues')

  /**
  * Get competition leagues
  */
  .get(checker.checkAdmin, (req, res) => {
    if (req.query.count === 'true') {
      handleRes(SvcCompetitionAdmin.countCompetitionLeagues(req.params.id), res, 'leagues');
      return;
    }
    if (req.query.creations) {
      handleRes(SvcCompetitionAdmin.getCompetitionLeagueCreations(req.params.id), res, 'count');
      return;
    }
    res.status(404).send({ status: 404, message: 'not implemented' });
  });

app.route('/:id/rules')

  /**
  * Update competition rules
  */
  .put(checker.checkAdmin, (req, res) => {
    handleRes(SvcCompetitionAdmin.updateCompetitionRules(req.params.id, req.body), res);
  });

app.route('/:id/emails')

  /**
  * Get competition emails
  */
  .get(checker.checkAdmin, (req, res) => {
    const lng = req.query.lng;
    if (lng) {
      const reminder = req.query.reminder === 'true';
      handleRes(SvcCompetitionAdmin.getCompetitionEmails(req.params.id, {
        lng,
        reminder
      }), res, 'emails');
      return;
    }
    res.status(404).send({ status: 404, message: 'not implemented' });
  });

app.route('/:id/automation')

  /**
  * Get competition matches
  */
  .get(checker.checkAdmin, (req, res) => {
    const source = req.query.source;
    handleRes(SvcCompetitionAutomation.scrapCompetitionMatches(req.params.id, source), res);
  });

exports.router = app;
