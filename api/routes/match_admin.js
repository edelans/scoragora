const express = require('express');

const checker = require('./checker.js');
const handleRes = require('./utils.js').handleRes;

const SvcMatch = require('../services/svc_match.js');
var SvcMatchAutomation = require('../services/svc_match_automation.js');

// utility function needed for parsing string args in request
const getAsInt = n => Math.ceil(Number(n));

const app = express.Router();

app.route('/')

  .get(checker.checkAdmin, (req, res) => {
    const options = {
      limit: getAsInt(req.query.limit) || 100,
      page: getAsInt(req.query.page) || 0,
      count: req.query.count === 'true'
    };
    if (req.query.status) {
      options.status = req.query.status;
    }
    if (req.query.date) {
      options.date = req.query.date;
    }
    handleRes(SvcMatch.getMatchesWithTeams(options), res, 'matches');
  });

app.route('/:id')

  .get(checker.checkAdmin, (req, res) => {
    if (req.query.teams === 'true') {
      handleRes(SvcMatch.getMatchWithTeams(req.params.id), res, 'match');
      return;
    }
    handleRes(SvcMatch.getMatchRecord(req.params.id), res, 'match');
  })

  // update a match
  .put(checker.checkAdmin, (req, res) => {
    if (req.body.result && req.body.sport) {
      handleRes(SvcMatch.updateMatchResult(req.params.id, req.body.result, req.body.sport), res);
      return;
    }
    // Update basic info: datetime, live, hometeam, awayteam, rule
    const info = {};
    ['automation', 'awayteam', 'awayteamname', 'datetime', 'hometeam',
      'hometeamname', 'rule', 'sport'].forEach((key) => {
        if (req.body[key]) {
          info[key] = req.body[key];
        }
      });
    if (req.body.live !== null) {
      info.live = req.body.live;
    }
    if (Object.keys(info).length === 0) {
      res.send({ status: 200, message: 'nothing to update' });
      return;
    }
    handleRes(SvcMatch.updateMatchInfo(req.params.id, info), res);
  });

app.route('/:id/automation')

  /**
  * Get matches date/score
  */
  .get(checker.checkAdmin, (req, res) => {
    const matchId = req.params.id;
    const source = req.query.source;
    if (req.query.date === 'true') {
      handleRes(SvcMatchAutomation.scrapMatchDate(matchId, source), res, 'date');
      return;
    }
    if (req.query.result === 'true') {
      handleRes(SvcMatchAutomation.scrapMatchResult(matchId, source), res, 'result');
      return;
    }
    res.status(400).send({ status: 400, message: 'invalid input' });
  });

exports.router = app;
