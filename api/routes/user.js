const express = require('express');

const checker = require('./checker.js');
const handleRes = require('./utils.js').handleRes;

const SvcUser = require('../services/svc_user.js');
const SvcUserLogin = require('../services/svc_userlogin.js');

// utility function needed for parsing string args in request
const getAsInt = n => Math.ceil(Number(n));

const app = express.Router();

app.route('/')
  .get((req, res) => {
    if (req.query.token) {
      handleRes(SvcUserLogin.getUserAndValidateRegistration(req.query.token), res);
      return;
    }
    res.send({ status: 200 });
  })
  /* Create user
  */
  .post((req, res) => {
    if (req.body.user) {
      handleRes(SvcUserLogin.createUser(req.body.user), res);
      return;
    }
    res.status(400).send({ status: 400, code: 40000, message: 'invalid data' });
  });

app.route('/:id')

  /* Get user details
  * @querystring:
  * - profile : [full, summary] default = full
  */
  .get(checker.checkLogin, (req, res) => {
    const userId = req.params.id;
    if (req.query.profile === 'summary') {
      handleRes(SvcUser.getUserSummary(userId), res, 'user');
      return;
    }
    if (userId !== req.user) {
      res.status(403).send({ status: 403, message: 'Unauthorized access' });
      return;
    }
    handleRes(SvcUser.getUser(userId), res, 'user');
  })

  /* Update user details
  * @data:
  * - pictureMode: [local, twitter, facebook, google]
  * - pictureURL: String
  * - pseudo: String
  * - lng: String
  * - passwordForm: {
  *    password: String,
  *    oldPassword: String,
  *    passwordConfirmation: String
  *   }
  *  - emailSettings: {
  *   ranking_updates: 'no_updates' || 'daily' || 'weekly',
  *   forecast_management: {
  *      reminder_24h: Boolean [true],
  *      new_matches: Boolean [true]
  *    }
  *  }
  */
  .put(checker.checkLogin, (req, res) => {
    const userId = req.params.id;
    if (userId !== req.user) {
      res.status(403).send({ status: 403, message: 'Unauthorized access' });
      return;
    }
    if (req.body.pictureMode) {
      handleRes(SvcUser.updateUserPictureMode(userId, {
        pictureMode: req.body.pictureMode,
        pictureURL: req.body.pictureURL
      }), res);
      return;
    }
    if (req.body.pictureURL) {
      handleRes(SvcUser.updateUserPictureURL(userId, req.body.pictureURL), res);
      return;
    }
    if (req.body.pseudo) {
      handleRes(SvcUser.updateUserPseudo(userId, req.body.pseudo), res);
      return;
    }
    if (req.body.passwordForm) {
      handleRes(SvcUser.updateUserPassword(userId, req.body.passwordForm), res);
      return;
    }
    if (req.body.emailSettings) {
      handleRes(SvcUser.updateUserMailSettings(userId, req.body.emailSettings), res);
      return;
    }
    if (req.body.lng) {
      handleRes(SvcUser.updateUserLng(userId, req.body.lng), res);
      return;
    }
    res.send({ status: 200, message: 'no update done' });
  })

  .delete(checker.checkLogin, (req, res) => {
    const userId = req.params.id;
    if (userId !== req.user) {
      res.status(403).send({ status: 403, message: 'Unauthorized access' });
      return;
    }
    handleRes(SvcUser.removeUser(req.params.id), res);
  });

app.route('/:id/leagues')

  /* Get user league list
  * @querystring:
  * - limit: Number of items
  * - page: Number of the page
  * - order: [activity]
  * - unreadmsg: Boolean
  */
  .get(checker.checkLogin, (req, res) => {
    const userId = req.params.id;
    if (userId !== req.user) {
      res.status(403).send({ status: 403, message: 'Unauthorized access' });
      return;
    }
    // we limit the number of leagues to 100
    const option = {
      limit: getAsInt(req.query.limit) ? Math.min(req.query.limit, 100) : 100,
      page: getAsInt(req.query.page) || 0,
      order: req.query.order
    };
    if (req.query.unreadmsg && req.query.unreadmsg === 'true') {
      handleRes(SvcUser.getUserLeagueSummariesWithUnreadMessages(userId, option), res, 'leagues');
      return;
    }
    handleRes(SvcUser.getUserLeagueSummaries(userId, option), res, 'leagues');
  });

app.route('/:id/matches')

  /* Get user match list
  * @querystring:
  * - limit: Number of items
  * - page: Number of the page
  * - status: [past]
  */
  .get(checker.checkLogin, (req, res) => {
    const userId = req.params.id;
    if (userId !== req.user) {
      res.status(403).send({ status: 403, message: 'Unauthorized access' });
      return;
    }
    const options = {
      limit: getAsInt(req.query.limit) || 5,
      page: getAsInt(req.query.page) || 0
    };
    if (req.query.status === 'past') {
      handleRes(SvcUser.getPastUserMatches(userId, options), res, 'matches');
      return;
    }
    res.send({ status: 200, matches: [] });
  });

app.route('/:id/forecasts')

  /* Get user forecast list sorted on forecast status and _id(creation date)
  * @querystring:
  * - limit: Number of items
  * - page: Number of the page
  */
  .get(checker.checkLogin, (req, res) => {
    const userId = req.params.id;
    if (userId !== req.user) {
      res.status(403).send({ status: 403, message: 'Unauthorized access' });
      return;
    }
    const options = {
      limit: getAsInt(req.query.limit) || 5,
      page: getAsInt(req.query.page) || 0
    };
    handleRes(SvcUser.getUserForecastSummaries(userId, options), res, 'forecasts');
  });

app.route('/:id/matchforecasts')

  /* Get user match forecast list
  * @querystring:
  * - limit: Number of items
  * - page: Number of the page
  * - status: [past]
  */
  .get(checker.checkLogin, (req, res) => {
    const userId = req.params.id;
    if (userId !== req.user) {
      res.status(403).send({ status: 403, message: 'Unauthorized access' });
      return;
    }
    const options = {
      limit: getAsInt(req.query.limit) || 5,
      page: getAsInt(req.query.page) || 0
    };
    handleRes(SvcUser.getUserMatchForecasts(userId, options), res, 'matches');
  });

exports.router = app;
