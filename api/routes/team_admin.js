const express = require('express');

const checker = require('./checker.js');
const handleRes = require('./utils.js').handleRes;

const SvcTeam = require('../services/svc_team.js');
const SvcTeamAutomation = require('../services/svc_team_automation.js');

// utility function needed for parsing string args in request
function getAsInt(n) {
  return Math.ceil(Number(n));
}

const app = express.Router();

app.route('/')

  // Get all the users
  .get(checker.checkAdmin, (req, res) => {
    const options = {
      limit: getAsInt(req.query.limit) || 10,
      page: getAsInt(req.query.page) || 0,
      search: req.query.search || '',
      count: req.query.count === 'true',
      sport: req.query.sport
    };
    handleRes(SvcTeam.getTeams(options), res, 'teams');
  })

  .post(checker.checkAdmin, (req, res) => {
    if (req.body.team) {
      handleRes(SvcTeam.createTeam(req.body.team), res);
      return;
    }
    res.statsu(400).send({ status: 400, message: 'team missing' });
  });

app.route('/:id')

  /*
  * Get team details
  */
  .get(checker.checkAdmin, (req, res) => {
    handleRes(SvcTeam.getTeam(req.params.id), res, 'team');
  })

  /*
  * Update team
  */
  .put(checker.checkAdmin, (req, res) => {
    if (req.body.automation) {
      handleRes(SvcTeam.updateTeamAutomation(req.params.id, req.body.automation), res);
      return;
    }
    const team = {
      name: req.body.name,
      label: req.body.label,
      sport: req.body.sport
    };
    handleRes(SvcTeam.updateTeamInfo(req.params.id, team), res);
  });

app.route('/:id/matches')

  /*
  * Get team matches
  */
  .get(checker.checkAdmin, (req, res) => {
    const options = {
      limit: getAsInt(req.query.limit) || 10,
      page: getAsInt(req.query.page) || 0,
      count: req.query.count === 'true'
    };
    handleRes(SvcTeam.getTeamMatches(req.params.id, options), res, 'matches');
  });

app.route('/:id/automation')

  /*
  * Get team details
  */
  .get(checker.checkAdmin, (req, res) => {
    const source = req.query.source;
    handleRes(SvcTeamAutomation.computeName(req.params.id, source), res, 'automation');
  });

exports.router = app;
