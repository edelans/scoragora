const express = require('express');

const checker = require('./checker.js');
const handleRes = require('./utils.js').handleRes;

var SvcForecast = require('../services/svc_forecast.js');

const app = express.Router();

app.route('/:id')

  /**
  * Get forecast detail
  */
  .get(checker.checkLogin, (req, res) => {
    handleRes(SvcForecast.getForecast(req.params.id, req.user), res, 'forecast');
  })

  /**
  * Update forecast bet
  * @data:
  * - matches: [{match: String, forecast: Object}]
  *   - bonus: [{name: String, value: String}]
  */
  .put(checker.checkLogin, (req, res) => {
    const matches = req.body.matches;
    if (matches && matches instanceof Array) {
      handleRes(SvcForecast.updateMatchForecast(req.params.id, req.user, matches), res);
      return;
    }
    const bonus = req.body.bonus;
    if (bonus && bonus instanceof Array) {
      // Update the bonus
    }
    res.status(400).send({ status: 400, message: 'Invalid input : no matches' });
  });

exports.router = app;
