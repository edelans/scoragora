const express = require('express');

const handleRes = require('./utils.js').handleRes;

const SvcToken = require('../services/svc_token.js');

const app = express.Router();

app.route('/')

  .post((req, res) => {
    if (req.body.email) {
      handleRes(SvcToken.createRecoveryToken(req.body.email), res);
      return;
    }
    res.status(400).send({ status: 400, message: 'email missing' });
  });

exports.router = app;
