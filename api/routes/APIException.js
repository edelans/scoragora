class APIException extends Error {
  constructor(message, status, code) {
    super(message);
    Error.captureStackTrace(this);
    if (isNaN(status)) {
      throw new Error('status must be an number');
    }
    this.status = status;
    this.code = code;
  }

  toJson() {
    const json = {
      status: this.status,
      message: this.message
    };
    if (this.code) {
      json.code = this.code;
    }
    return json;
  }
}

module.exports = APIException;
