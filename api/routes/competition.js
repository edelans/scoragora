const express = require('express');

const checker = require('./checker.js');
const handleRes = require('./utils.js').handleRes;

var SvcCompetition = require('../services/svc_competition.js');

// utility function needed for parsing string args in request
const getAsInt = n => Math.ceil(Number(n));

const app = express.Router();

app.route('/')

  .get(checker.checkLogin, (req, res) => {
    const options = {
      limit: getAsInt(req.query.limit) || 100,
      page: getAsInt(req.query.page) || 0
    };
    if (req.query.status === 'public') {
      handleRes(SvcCompetition.getPublicOrReadyCompetitions(options), res, 'competitions');
      return;
    }
    res.send({ status: 200 });
  });

app.route('/comingsoon')

    .get((req, res) => {
      handleRes(SvcCompetition.getPublicOrReadyCompetitions(), res);
    });

app.route('/:id')

  .get(checker.checkLogin, (req, res) => {
    handleRes(SvcCompetition.getCompetition(req.params.id), res, 'competition');
  });

app.route('/:id/matches')

  .get(checker.checkLogin, (req, res) => {
    // Todo : enable filtering by day to prevent sending around 400 matches
    // var day = req.param('day');
    //   if (day != null) {
    //   return SvcCompetition.getCompetitionMatchesByDay(req.param('id'), day, req, res);
    // }
    const options = {};
    if (req.query.limit) {
      options.limit = getAsInt(req.query.limit);
    }
    if (req.query.status === 'past') {
      options.past = true;
    }
    handleRes(SvcCompetition.getCompetitionMatches(req.params.id, options), res, 'matches');
  });


exports.router = app;
