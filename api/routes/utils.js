const winston = require('winston');

const APIException = require('./APIException.js');

exports.handleRes = async (func, res, key) => {
  try {
    const data = await (() => func)();
    if (!isNaN(data.status)) {
      return res.status(data.status).send(data);
    }
    return res.send({ status: 200, [key || 'message']: data });
  } catch (err) {
    if (err instanceof APIException) {
      return res.status(err.status).send(err.toJson());
    }
    winston.error(err);
    return res.status(500).send({ status: 500, code: 50000, message: 'internal server error' });
  }
};
