const express = require('express');
const jwt = require('jwt-simple');
const moment = require('moment');
const request = require('request-promise-native');
const winston = require('winston');

const cfg = require('../../config');

const checker = require('./checker.js');
const handleRes = require('./utils.js').handleRes;

const User = require('../models/users.js');
const SvcUser = require('../services/svc_user.js');

const FACEBOOK_TOKEN_URL = 'https://graph.facebook.com/v2.3/oauth/access_token';
const GRAPH_API_URL = 'https://graph.facebook.com/v2.3/me';

const GOOGLE_TOKEN_URL = 'https://accounts.google.com/o/oauth2/token';
const PEOPLE_API_URL = 'https://www.googleapis.com/plus/v1/people/me/openIdConnect';

/**
* Generate a token for user
*/
const createJWT = (userId) => {
  const payload = {
    sub: userId,
    iat: moment().unix(),
    exp: moment().add(14, 'days').unix()
  };
  return jwt.encode(payload, cfg.secret);
};

const authenticateUser = async (body) => {
  // Simple check on provided fields
  if (!body) {
    return { status: 400, code: 40001, message: 'Missing credentials' };
  }
  if (!body.email) {
    return { status: 400, code: 40005, message: 'Missing credentials' };
  }
  if (!body.password) {
    return { status: 400, code: 40006, message: 'Missing credentials' };
  }
  const user = await User.findOne({
    'login.local.email': body.email.toLowerCase(),
    expiry: { $exists: false }
  }, { 'login.local.password': 1 }).exec();
  if (!user) {
    return { status: 400, code: 40002, message: 'Incorrect email' };
  }
  if (!user.validPassword(body.password)) {
    return { status: 400, code: 40003, message: 'Incorrect password' };
  }
  return {
    status: 200,
    loggedIn: true,
    userId: user.id,
    token: createJWT(user._id)
  };
};

const app = express.Router();

app.route('/')

  // Check if the user is logged in
  .get((req, res) => {
    const payload = checker.extractDataFromToken(req);
    const response = { status: 200, loggedIn: false };
    if (payload && payload.sub) {
      response.loggedIn = true;
      response.userId = payload.sub;
    }
    return res.send(response);
  });

app.route('/local')

  .post((req, res) => {
    handleRes(authenticateUser(req.body), res);
  })

  .put((req, res) => {
    if (req.body.email && req.body.password) {
      handleRes(SvcUser.updateUserPasswordFromEmail({
        email: req.body.email,
        token: req.body.token,
        password: req.body.password,
        password_confirmation: req.body.password_confirmation
      }), res);
      return;
    }
    res.status(400).send({ status: 400, message: 'invalid data' });
  });

  // app.get('/login/facebook', passport.authenticate('facebook'));

  // app.get('/login/twitter', passport.authenticate('twitter'));

  // app.get('/login/twitter/callback', passport.authenticate('twitter'), login_success);

/**
 * Google
 */
const googleAuthenticate = async (req) => {
  winston.info('Login | authentication from google for client ' + req.body.clientId);
  if (!req.body || !req.body.clientId) {
    return { status: 401, message: 'missing input' };
  }

  const params = {
    code: req.body.code,
    client_id: req.body.clientId,
    client_secret: cfg.google.secret,
    redirect_uri: req.body.redirectUri,
    grant_type: 'authorization_code'
  };

  // Step 1. Exchange authorization code for access token.
  const token = await request.post(GOOGLE_TOKEN_URL, { json: true, form: params });
  const accessToken = token.access_token;
  const headers = { Authorization: 'Bearer ' + accessToken };

  // Step 2. Retrieve profile information about the current user.
  const profile = await request.get({ url: PEOPLE_API_URL, headers: headers, json: true });
  if (profile.error) {
    winston.info('Login | google api return an error message : ' + JSON.stringify(profile.error));
    return { status: 401, message: 'incorrect input' };
  }
  // Step 3a. Link user accounts.
  if (req.headers.authorization) {
    const existingUser = await User.findOne({ 'login.google.id': profile.sub }).exec();
    if (existingUser) {
      return { status: 409, message: 'There is already a Google account that belongs to you' };
    }
    const reqToken = req.headers.authorization.split(' ')[1];
    const payload = jwt.decode(reqToken, cfg.secret);
    const user = await User.findById(payload.sub);
    if (!user) {
      return { status: 400, message: 'User not found' };
    }
    user.login.google = {
      id: profile.sub,
      pictureURL: profile.picture.replace('sz=50', 'sz=200'),
      displayName: profile.name,
      email: profile.email
    };
    await user.save();
    return { status: 200, message: 'successfully linked to google user' };
  }
  // Step 3b. Create a new user account or return an existing one.
  const existingUser = await User.findOne({ 'login.google.id': profile.sub }).exec();
  if (existingUser) {
    winston.info('Login | existing user ' + existingUser.id + ' for google id ' + profile.sub);
    return { status: 200, token: createJWT(existingUser.id) };
  }
  const user = new User({
    createdAt: new Date(),
    pictureMode: 'google'
  });
  user.login = {
    google: {
      id: profile.sub,
      pictureURL: profile.picture.replace('sz=50', 'sz=200'),
      displayName: profile.name,
      email: profile.email
    }
  };
  await user.save();
  return { status: 200, token: createJWT(user.id) };
};

app.route('/google')

  .post(async (req, res) => {
    handleRes(googleAuthenticate(req), res);
  });

/**
 * Facebook
 */
const facebookAuthenticate = async (req) => {
  const params = {
    code: req.body.code,
    client_id: req.body.clientId,
    client_secret: cfg.facebook.secret,
    redirect_uri: req.body.redirectUri
  };

  // Step 1. Exchange authorization code for access token.
  const accessToken = await request.get({ url: FACEBOOK_TOKEN_URL, qs: params, json: true });

  // Step 2. Retrieve profile information about the current user.
  const profile = await request.get({ url: GRAPH_API_URL, qs: accessToken, json: true });
  if (req.headers.authorization) {
    const existingUser = await User.findOne({ 'login.facebook.id': profile.id }).exec();
    if (existingUser) {
      return { status: 409, message: 'There is already a Facebook account that belongs to you' };
    }
    const token = req.headers.authorization.split(' ')[1];
    const payload = jwt.decode(token, cfg.secret);
    const user = await User.findById(payload.sub).exec();
    if (!user) {
      return { status: 400, message: 'User not found' };
    }
    user.login.facebook = {
      id: profile.id,
      displayName: profile.name,
      name: {
        givenName: profile.first_name,
        familyName: profile.last_name
      },
      pictureURL: `https://graph.facebook.com/v2.3/${profile.id}/picture?type=large`,
      email: profile.email
    };
    winston.info(`Login | linking user ${payload.sub} with facebook account ${profile.name}`);
    await user.save();
    return { status: 200, token: createJWT(user._id) };
  }
  // Step 3b. Create a new user account or return an existing one.
  const existingUser = await User.findOne({ 'login.facebook.id': profile.id }).exec();
  const facebook = {
    id: profile.id,
    displayName: profile.name,
    name: {
      givenName: profile.first_name,
      familyName: profile.last_name
    },
    pictureURL: `https://graph.facebook.com/v2.3/${profile.id}/picture?type=large`,
    email: profile.email
  };
  if (existingUser) {
    winston.info('Login | updating facebook detail for existing user ' + existingUser._id);
    existingUser.login.facebook = facebook;
    await existingUser.save();
    return { status: 200, token: createJWT(existingUser._id) };
  }
  const user = new User({
    createdAt: new Date(),
    pictureMode: 'facebook'
  });
  user.login = {
    facebook: facebook
  };
  winston.info('Login | creating a new user based on facebook account ' + profile.name);
  await user.save();
  return { status: 200, token: createJWT(user._id) };
};

app.route('/facebook')

  .post(async (req, res) => {
    handleRes(facebookAuthenticate(req), res);
  });

    /**
     *
     */
    /*app.route('/login/twitter').post(function(req, res) {
        var requestTokenUrl = 'https://api.twitter.com/oauth/request_token';
        var accessTokenUrl = 'https://api.twitter.com/oauth/access_token';
        var authenticateUrl = 'https://api.twitter.com/oauth/authenticate';

        if (!req.query.oauth_token || !req.query.oauth_verifier) {
            var requestTokenOauth = {
                consumer_key: cfg.twitter.consumerKey,
                consumer_secret: cfg.twitter.secret,
                callback: cfg.twitter.callbackURL
            };

            // Step 1. Obtain request token for the authorization popup.
            request.post({ url: requestTokenUrl, oauth: requestTokenOauth }, function(err, response, body) {
                var oauthToken = qs.parse(body);
                var params = qs.stringify({ oauth_token: oauthToken.oauth_token });

                // Step 2. Redirect to the authorization screen.
                res.send(oauthToken);
            });
        } else {
            var accessTokenOauth = {
                consumer_key: cfg.twitter.consumerKey,
                consumer_secret: cfg.twitter.secret,
                token: req.query.oauth_token,
                verifier: req.query.oauth_verifier
            };

            // Step 3. Exchange oauth token and oauth verifier for access token.
            request.post({ url: accessTokenUrl, oauth: accessTokenOauth }, function(err, response, profile) {
                profile = qs.parse(profile);

                // Step 4a. Link user accounts.
                if (req.headers.authorization) {
                    User.findOne({ 'login.twitter.id': profile.user_id }, function(err, existingUser) {
                        if (existingUser) {
                            return res.status(409).send({ message: 'There is already a Twitter account that belongs to you' });
                        }
                        var token = req.headers.authorization.split(' ')[1];
                        var payload = jwt.decode(token, cfg.secret_token);
                        User.findById(payload.sub, function(err, user) {
                            if (!user) {
                                return res.status(400).send({ message: 'User not found' });
                            }
                            user.login.twitter.id = profile.user_id;
                            user.login.displayName = profile.screen_name;
                            user.save(function(err) {
                                res.send({ status: 200, token: createJWT(user._id) });
                            });
                        });
                    });
                } else {
                    // Step 4b. Create a new user account or return an existing one.
                    User.findOne({ 'login.twitter.id': profile.user_id }, function(err, existingUser) {
                        if (existingUser) {
                            return res.send({ token: createJWT(existingUser._id) });
                        }
                        var user = new User();
                        user.twitter.id = profile.user_id;
                        user.twitter.displayName = profile.screen_name;
                        user.save(function(err) {
                            res.send({ token: createJWT(user._id) });
                        });
                    });
                }
            });
        }
    });

  app.get('/login/twitter/link', function(req, res) {
      // set linking_account to true in order to know to redirect to /user/settings when provider perform his GET to callback url (auth callback)
      req.session.linking_account = true;
      res.redirect('/login/twitter');
  });

  // unlinking
  app.delete('/login/twitter/link', function(req, res) {
      var user = req.user;
      // console.log(user);
      if (user.login.facebook.token || user.login.google.token ||  user.login.local.password) {
          // ok for unlinking account: user will still have a way to login
          // console.log('twitter data to be deleted');
          user.login.twitter = undefined;
          if (user.pictureMode == 'twitter') user.pictureMode = 'local';
          user.save(function(err) {
              res.redirect('/user/settings');
          });
      } else {
          // console.log('cannot delete twitter data');
          req.flash('error', req.i18n.t('server.alert.cannot_unlink'));
          res.redirect('/user/settings');
      }
  });*/

exports.router = app;
