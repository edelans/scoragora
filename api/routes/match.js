const express = require('express');

const checker = require('./checker.js');
const handleRes = require('./utils.js').handleRes;

const SvcMatch = require('../services/svc_match.js');

const app = express.Router();

app.route('/:id')

  .get(checker.checkLogin, (req, res) => {
    if (req.query.filter === 'score') {
      handleRes(SvcMatch.getMatchScore(req.params.id), res, 'score');
      return;
    }
    handleRes(SvcMatch.getMatch(req.params.id), res, 'match');
  });

exports.router = app;
