const express = require('express');
const winston = require('winston');

const checker = require('./checker.js');
const handleRes = require('./utils.js').handleRes;

const cfg = require('../../config');

const SvcLeague = require('../services/svc_league.js');
const SvcMessage = require('../services/svc_message.js');
const SvcForecast = require('../services/svc_forecast.js');

const stripe = require('stripe')(cfg.stripe.secretKey);

// utility function needed for parsing string args in request
const getAsInt = n => Math.ceil(Number(n));

const app = express.Router();

app.route('/')
  // create a new ligue
  .post(checker.checkLogin, (req, res) => {
    if (!req.body.competition || req.body.competition === '' || !req.body.name || req.body.name === '') {
      winston.info('invalid parameters');
      res.status(400).send({ status: 400, message: 'missing arguments' });
      return;
    }
    if (req.body.name.match(/[$&+,/:;=?@'<>#%{}|\\^~[\]`]/)) {
      res.status(400).send({
        status: 400,
        message: 'You should provide a valid league name, these characters are not allowed: $&+,/:;=?@\'<>#%{}|\\^~[]`',
        available: 'false'
      });
      return;
    }
    const competitionId = req.body.competition;
    const name = req.body.name;
    const urlLeagueName = req.body.name.toLowerCase().replace(/ /g, '.');
    handleRes(SvcLeague.createLeague(name, urlLeagueName, req.user, competitionId, {
      sponsor: req.body.sponsor
    }), res);
  })

  // check availability of a league name
  // (actually we check the availability for the league url =
  // league name with spaces replaced by dots)
  .get(checker.checkLogin, (req, res) => {
    if (req.query.url) {
      checker.checkLeagueMembership(req, res, () => {
        res.send({ status: 200, leagues: [req.league] });
      });
      return;
    }
    if (req.query.token) {
      handleRes(SvcLeague.getLeagueByToken(req.query.token).then(league => [league]), res, 'leagues');
      return;
    }
    if (req.query.checkavailability === 'true') {
      winston.info('logged user wants to check league name availability for ' + req.query.name);
      // perform some sanity check on parameter
      if (!req.query.name || req.query.name.match(/[$&+,/:;=?@'<>#%{}|\\^~[\]`]/)) {
        res.status(400).send({
          status: 400,
          message: 'You should provide a valid league name, these characters are not allowed: $&+,/:;=?@\'<>#%{}|\\^~[]`',
          available: 'false'
        });
        return;
      }
      winston.info('logged user wants to check league name availability');
      // proceed to availablility check
      handleRes(SvcLeague.isLeagueNameAvailable(req.query.name), res, 'available');
      return;
    }
    res.status(404).send({ status: 404, message: 'No ressource here' });
  });

app.route('/:leagueId')
  // get league details
  .get(checker.checkLogin, checker.checkLeagueMembership, (req, res) => {
    return res.send({ status: 200, league: req.league });
  })

  // delete the league object and clear the association with this league
  // from other users of the league
  .delete(checker.checkLogin, checker.checkLeagueMembership,
    checker.checkLeagueCaptaincy, (req, res) => {
      handleRes(SvcLeague.removeLeague(req.league), res);
    });


app.route('/:leagueId/members')
  // get the users objects of the league, unpopulated
  .get(checker.checkLogin, checker.checkLeagueMembership, (req, res) => {
    handleRes(SvcLeague.getLeagueMembers(req.league), res, 'members');
  })

  // join a league
  .put(checker.checkLogin, (req, res) => {
    handleRes(SvcLeague.addUserToLeague(req.params.leagueId, req.user), res);
  })

  // leave a league
  .delete(checker.checkLogin, checker.checkLeagueMembership, (req, res) => {
    handleRes(SvcLeague.removeUserFromLeague(req.league._id, req.user), res);
  });


app.route('/:leagueId/messages')
  // get the chat messages of a league
  .get(checker.checkLogin, checker.checkLeagueMembership, (req, res) => {
    const options = {
      limit: getAsInt(req.query.limit) || 2,
      timestamp: getAsInt(req.query.timestamp) || ((new Date()).getTime() + 10000)
    };
    // todo prendre en compte timestamp
    handleRes(SvcMessage.getMessageBuckets(req.params.leagueId, options), res, 'messages');
  });


app.route('/:leagueId/forecasts')
  // get all *past* forecasts of all the users of that league for all
  // the completed matches of that league
  .get(checker.checkLogin, checker.checkLeagueMembership, (req, res) => {
    handleRes(SvcForecast.getForecastsFromLeague(req.league), res, 'forecasts');
  });

app.route('/:leagueId/matches/:matchId/forecasts')
  // TODO: test + move code to service
  // get the forecasts for that match for all the users of the league
  .get(checker.checkLogin, checker.checkLeagueMembership, (req, res) => {
    winston.info('entering get /leagues/:leagueId/matches/:matchId/forecasts');
    handleRes(SvcForecast.getMatchForecastsFromLeague(req.league, req.params.matchId), res, 'forecasts');
  });


app.route('/:leagueId/stripe')
  // TODO: test + move code to service
  // TODO: add products to be ore flexible (see stripe docs)
  .post(checker.checkLogin, checker.checkLeagueMembership, (req, res) => {
    winston.info('entering post /leagues/:leagueId/stripe');
    const stripeToken = req.body.stripeToken;
    winston.info('stripeToken is : ' + stripeToken.id);
    // TODO put this code in service
    // Check if not premium
    if (req.league && req.league.premium && req.league.premium.validated === true) {
      winston.info(' | league ' + req.league._id + ' is already premium, no payment required');
      res.status(400).send({ status: 400, message: 'league already premium' });
      return;
    }

    // Create Stripe Charge
    const charge = {
      amount: 5000,
      currency: 'eur',
      source: stripeToken.id,
      description: 'Payment for league ID : ' + req.params.leagueId
    };
    winston.info('charge is : ' + charge);
    // TODO: add customer before charging, see stripe docs
    stripe.charges.create(charge, (err) => {
      if (err) {
        winston.error('error when creating a payment with stripe : ' + err);
        res.status(500).send({ status: 500, message: 'internal server error' });
        return;
      }
      handleRes(SvcLeague.updateLeagueStatus(req.params.leagueId, 'premium'), res);
    });
  });

exports.router = app;
