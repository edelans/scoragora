// ensure the NODE_ENV is set to 'test'
if (process.env.NODE_ENV !== 'test') {
  throw new Error('NODE_ENV has to be set to test before running test!!!!');
}

const mongoose = require('mongoose');
const exec = require('child_process').exec;

const config = require('../../config');

before(function(done) {
  this.timeout(10000);
  console.log('initDB');

  function clearDB() {
    exec('mongo localhost:27017/stevetaylortest init_test.js', function(err, stdout, stderr) {
      if (stderr) console.log('stderr ' + stderr);
      if (stdout) console.log(stdout);
      if (err) console.log(err);
      return done();
    });
  }

  const reconnect = async () => {
    console.log(config.mongo.uri_test);
    await mongoose.connect(config.mongo.uri, {
      useMongoClient: true
    });
    console.log('connected to test database');
    clearDB();
  };

  const checkState = () => {
    switch (mongoose.connection.readyState) {
      case 0:
        reconnect();
        break;
      case 1:
        clearDB();
        break;
      default:
        process.nextTick(checkState);
    }
  };

  checkState();
});

after(() => {
  console.log('disconnected');
  mongoose.disconnect();
});
