/**
 * Test model translation
 */

var mongoose = require("mongoose");
var mocha = require("mocha");
var expect = require('expect.js');

var translator = require("../tools/translator.js");

describe("translate", function() {
	
	it("should do nothing when null", function(done) {
		var object = null;
		var schema = translator.convertRec(mongoose, object);
		expect(schema).to.be(null);
		return done();
	});
	
	it("should replace \"objectid\"", function(done) {
		var object = {
			key: "objectid"
		};
		var schema = translator.convertRec(mongoose, object);
		expect(schema.key).to.be(mongoose.Schema.Types.ObjectId);
		return done();
	});
	
	it("should replace \"mixed\"", function(done) {
		var object = {
			key: "mixed"
		};
		var schema = translator.convertRec(mongoose, object);
		expect(schema.key).to.be(mongoose.Schema.Types.Mixed);
		return done();
	});
	
	it("should not replace regexp", function(done) {
		var object = {
			key: /regex/i
		};
		var schema = translator.convertRec(mongoose, object);
		expect(schema.key).to.be.a(RegExp);
		return done();
	});
	
	it("should not replace number", function(done) {
		var object = {
			key: 30
		};
		var schema = translator.convertRec(mongoose, object);
		expect(schema.key).to.be(30);
		return done();
	});
	
	it("should not replace string", function(done) {
		var object = {
			key: "phrase"
		};
		var schema = translator.convertRec(mongoose, object);
		expect(schema.key).to.be("phrase");
		return done();
	});
	
	it("should support array nesting", function(done) {
		var object = {
			key: [
				{key1: "mixed"},
				{key2: "objectid"},
				{key3: "string"},
				{key4: 3}
			]
		};
		var schema = translator.convertRec(mongoose, object);
		expect(schema.key).not.to.be(null);
		expect(schema.key[0].key1).to.be(mongoose.Schema.Types.Mixed);
		expect(schema.key[1].key2).to.be(mongoose.Schema.Types.ObjectId);
		expect(schema.key[2].key3).to.be("string");
		expect(schema.key[3].key4).to.be(3);
		return done();
	});
	
	it("should support object nesting", function(done) {
		var object = {
			key: {
				key1: "mixed",
				key2: "objectid",
				key3: "string",
				key4: 3
			}
		};
		var schema = translator.convertRec(mongoose, object);
		expect(schema.key).not.to.be(null);
		expect(schema.key.key1).to.be(mongoose.Schema.Types.Mixed);
		expect(schema.key.key2).to.be(mongoose.Schema.Types.ObjectId);
		expect(schema.key.key3).to.be("string");
		expect(schema.key.key4).to.be(3);
		return done();
	});
});