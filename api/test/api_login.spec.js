// force process to test
process.env.NODE_ENV = 'test';

const expect = require('expect.js');
const request = require('supertest-as-promised');

const app = require('../app.js').app;

describe('#routes/login.js', () => {
  before(async () => { });

  describe('GET /api/login', () => {
    describe('When the user is logged in', () => {
      let token;

      // Create an user and log him
      before(async () => {
        const res = await request(app)
        .post('/api/login/local')
        .send({
          email: 'etienne@scoragora.com',
          password: 'pwd'
        });
        token = 'Bearer ' + res.body.token;
      });

      it('should return true when the user is loggged in', async () => {
        const res = await request(app)
        .get('/api/login')
        .set('Authorization', token)
        .expect(200);
        expect(res.body).to.have.property('loggedIn', true);
      });
    });

    it('should return false when the user is not logged in', async () => {
      const res = await request(app)
      .get('/api/login')
      .expect(200);
      expect(res.body).to.have.property('loggedIn', false);
    });
  });

  describe('DELETE /api/login', function() {

  });

  describe('GET /api/login/local', function() {

  });

  describe('POST /api/login/local', () => {
    describe('When user succeed to log in', () => {
      it('should return the token', async () => {
        const res = await request(app)
        .post('/api/login/local')
        .send({
          email: 'etienne@scoragora.com',
          password: 'pwd'
        });
        expect(res.status).to.be(200);
        expect(res.body).to.have.property('loggedIn', true);
        expect(res.body).to.have.property('token');
      });
    });

    describe('When user fail to log in', () => {
      it('should return code 40002', async () => {
        const res = await request(app)
        .post('/api/login/local')
        .send({
          email: 'albert@scoragora.com',
          password: 'pwd'
        });
        expect(res.status).to.be(400);
        expect(res.body.code).to.be(40002);
      });

      it('should return code 40003', async () => {
        const res = await request(app)
        .post('/api/login/local')
        .send({
          email: 'etienne@scoragora.com',
          password: 'pwds'
        })
        .expect(400);
        expect(res.body.code).to.be(40003);
      });

      it('should return code 40006', async () => {
        const res = await request(app)
        .post('/api/login/local')
        .send({
          email: 'albert@scoragora.com'
        })
        .expect(400);
        expect(res.body.code).to.be(40006);
      });

      it('should return code 40005', async () => {
        const res = await request(app)
        .post('/api/login/local')
        .send({})
        .expect(400);
        expect(res.body.code).to.be(40005);
      });
    });
  });

  describe.skip('POST /api/login/google', function() {

    describe('When user succeed to authenticate', function() {
      it('should return token', function(done) {
        request(app)
          .post('/api/login/google')
          .send({
            code: '4/hvhLZDoxthXRC9vloJyEx_aiKmugbqwZASo1vg9DX7U',
            clientId: '976002710436-ao6u737kdftmd67kc66vr34mr3c8bbnj.apps.googleusercontent.com',
            redirectUri: 'http://localhost:3000'
          })
          .end(function(err, res) {
            expect(res.body.status).to.be(200);
            expect(res.body).to.have.property("token");
            return done();
          });
      });
    });

    describe('When user fail to authenticate', function() {
      it('should return status 401', function(done) {
        request(app)
          .post('/api/login/google')
          .send()
          .end(function(err, res) {
            expect(res.body.status).to.be(401);
            return done();
          });
      });

      it('should return status 401', function(done) {
        request(app)
          .post('/api/login/google')
          .send({
            clientId: "fakeid"
          })
          .end(function(err, res) {
            expect(res.body.status).to.be(401);
            return done();
          });
      });
    });
  });

});
