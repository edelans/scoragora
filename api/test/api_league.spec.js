// force process to test
process.env.NODE_ENV = 'test';

const expect = require('expect.js');
const request = require('supertest-as-promised');
const mongoose = require('mongoose');

const app = require('../app.js').app;

describe('#routes/leagues.js', function () {
  let token;
  this.timeout(5000);

  before(async function () {
    this.timeout(10000);
    // log it
    const res = await request(app)
    .post('/api/login/local')
    .send({ email: 'etienne@scoragora.com', password: 'pwd' });
    token = 'Bearer ' + res.body.token;
  });


  describe('GET /leagues/:leagueID', () => {
    describe('not logged in', () => {
      it('should return an error message', async () => {
        const res = await request(app)
        .get('/api/leagues/5300a28935b003712ba1adac')
        .expect(401);
        expect(res.body).not.to.have.property('league');
        expect(res.body.message).to.be('Unauthorized access');
      });
    });

    describe('no matching league', () => {
      it('should return an error message', async () => {
        const res = await request(app)
        .get('/api/leagues/5300a28935b003712ba1aaaa')
        .set('Authorization', token)
        .expect(404);
        expect(res.body.code).to.be(40012);
        expect(res.body).not.to.have.property('league');
      });
    });

    describe('there is a matching league and the user is part of the league', () => {
      it('should return the league object, unpopulated', async () => {
        const res = await request(app)
        .get('/api/leagues/5300a28935b003712ba1adac')
        .set('Authorization', token)
        .expect(200);
        expect(res.body).to.have.property('league');
      });
    });

    describe('there is a matching league but the user is NOT part of it', () => {
      it('should return an error message: unauthorized access for this user', async () => {
        const res = await request(app)
        .get('/api/leagues/53bcf56ab3263bdf3a79daeb')
        .set('Authorization', token)
        .expect(403);
        expect(res.body.code).to.be(40013);
        expect(res.body).not.to.have.property('league');
      });
    });
  });


  describe('GET /leagues/:leagueID/members', () => {
    describe('not logged in', () => {
      it('should return an error message', async () => {
        await request(app)
        .get('/api/leagues/5300a28935b003712ba1adac/members')
        .expect(401);
      });
    });

    describe('no matching league', () => {
      it('should return an error message', async () => {
        const res = await request(app)
        .get('/api/leagues/5300a28935b003712ba1aaaa/members')
        .set('Authorization', token)
        .expect(404);
        expect(res.body.code).to.be(40012);
      });
    });

    describe('there is a matching league and the user is part of the league', () => {
      it('should return the users objects of the league, unpopulated', async () => {
        // check if there is only one object,
        // and the id of the returned object is the id of the query string
        const res = await request(app)
        .get('/api/leagues/5300a28935b003712ba1adac/members')
        .set('Authorization', token)
        .expect(200);
        expect(res.body).to.have.property('members');
        expect(res.body.members[0]).to.be.eql({
          _id: '53668055a5746578545df2d8',
          displayName: 'etienne',
          pictureUrl: '/images/profile_picture/trix_flix08.jpg'
        });
      });
    });

    describe('there is a matching league but the user is NOT part of it', () => {
      it('should return an error message: unauthorized access for this user', async () => {
        const res = await request(app)
        .get('/api/leagues/53bcf56ab3263bdf3a79daeb/members')
        .set('Authorization', token)
        .expect(403);
        expect(res.body).not.to.have.property('members');
        expect(res.body.code).to.be(40013);
      });
    });
  });

  describe('PUT /leagues/:leagueID/members', () => {
    describe('not logged in', () => {
      it('should return an error message', async () => {
        await request(app)
        .put('/api/leagues/5300a28935b003712ba1adac/members', {})
        .expect(401);
      });
    });

    describe('no matching league', () => {
      it('should return an error message', async () => {
        const res = await request(app)
        .put('/api/leagues/5300a28935b003712ba1aaaa/members', {})
        .set('Authorization', token)
        .expect(404);
        expect(res.body).to.be.eql({
          status: 404,
          message: 'no league found for id 5300a28935b003712ba1aaaa'
        });
      });
    });

    describe('there is a matching league and the user is part of the league', () => {
      it('should return an error message', async () => {
        // check if there is only one object,
        // and the id of the returned object is the id of the query string
        const res = await request(app)
        .put('/api/leagues/5300a28935b003712ba1adac/members', {})
        .set('Authorization', token)
        .expect(400);
        expect(res.body).to.be.eql({
          status: 400,
          code: 40010,
          message: 'user already member of the league'
        });
      });
    });

    describe('there is a matching league and the user is not part of it', () => {
      it('should return a success message', async () => {
        const res = await request(app)
        .put('/api/leagues/53bcf56ab3263bdf3a79da34/members', {})
        .set('Authorization', token)
        .expect(200);
        expect(res.body).to.be.eql({
          status: 200,
          message: 'The user has been successfully added to the league'
        });
      });
    });
  });


  describe('GET /leagues/:leagueID/forecasts', () => {
    describe('the user doesn\'t belong to the league', () => {
      it('should return an error message: unauthorized access for this user', async () => {
        const res = await request(app)
        .get('/api/leagues/53bcf56ab3263bdf3a79daeb/forecasts')
        .set('Authorization', token)
        .expect(403);
        expect(res.body.code).to.be(40013);
        expect(res.body.message).to.be('Unauthorized access');
      });
    });

    describe('the user belongs to the league', () => {
      it('should return all *past* forecasts of all the users of that league for all the completed matches of that league', async () => {
        const res = await request(app)
        .get('/api/leagues/5300a28935b003712ba1adac/forecasts')
        .set('Authorization', token)
        .expect(200);
        expect(res.body).to.have.property('forecasts');
        expect(res.body.forecasts).to.have.length(4);
      });
    });
  });

  describe('GET /leagues/:leagueID/matches/:matchID/forecasts', () => {
    describe('the user doesn\'t belong to the league', () => {
      it('should return an error message: unauthorized access for this user', async () => {
        const res = await request(app)
        .get('/api/leagues/53bcf56ab3263bdf3a79daeb/matches/52fd4eda6b891e6702ea3490/forecasts')
        .set('Authorization', token)
        .expect(403);
        expect(res.body.code).to.be(40013);
        expect(res.body).to.not.have.property('forecasts');
        expect(res.body.message).to.be.eql('Unauthorized access');
      });
    });

    describe('the matchID is not in the league', () => {
      it('should return an error message', async () => {
        const randomId = '52fd4eda6b891e6702ea3491';
        await request(app)
        .get('/api/leagues/5300a28935b003712ba1adac/matches/' + randomId + '/forecasts')
        .set('Authorization', token)
        .expect(400);
      });
    });
  });

  describe('before the starttime of the match', () => {
    it('it should return no forecast', async () => {
      const res = await request(app)
      .get('/api/leagues/5300a28935b003712ba1adac/matches/53025e2e632b0d1402a3cb15/forecasts')
      .set('Authorization', token)
      .expect(200);
      expect(res.body.message).to.be.eql('this match has not started yet : no forecasts available');
      expect(res.body).to.not.have.property('forecasts');
    });
  });

  describe('after the starttime of the match', () => {
    it('it should return the forecasts for that match for all the users of the league', async () => {
      const res = await request(app)
      .get('/api/leagues/5300a28935b003712ba1adac/matches/52fd4eda6b891e6702ea3490/forecasts')
      .set('Authorization', token)
      .expect(200);
      expect(res.body).to.have.property('forecasts');
      expect(res.body.forecasts).to.eql([{
        user: '53668055a5746578545df2d8',
        points: 0,
        bonus: [],
        forecast: {
          hometeam: { score: 1 },
          awayteam: { score: 2 }
        }
      }, {
        user: '536e20d65ce44a661fde090a',
        points: 0,
        bonus: [],
        forecast: {
          hometeam: { score: 1 },
          awayteam: { score: 2 }
        }
      }]);
    });
  });

  describe('POST /leagues', () => {
    describe('add a new league via html post', () => {
      it('should have saved a new league in db', async () => {
        const res = await request(app)
        .post('/api/leagues')
        .set('Authorization', token)
        .send({ name: 'new league', competition: '52ea654cf7073b5b2f42bc27' })
        .expect(200);
        expect(res.body.message).to.be.eql('The league has been created');
        const league = await mongoose
        .model('League')
        .findOne({ name: 'new league' })
        .exec();
        expect(league.name).to.be('new league');
        expect(league.members.length).to.be(1);
        expect(league.members[0].rank).to.be(1);
      });
    });

    describe('add a league with a name already used', () => {
      it('should return an error', async () => {
        const res = await request(app)
        .post('/api/leagues')
        .set('Authorization', token)
        .send({ name: 'the other league', competition: '52ea654cf7073b5b2f42bc27' })
        .expect(400);
        expect(res.body).to.be.eql({
          status: 400,
          message: 'league name not available'
        });
      });
    });


    describe('add a league with insufficiant parameters', () => {
      it('should return an error', async () => {
        const res = await request(app)
        .post('/api/leagues')
        .set('Authorization', token)
        .send({ name: 'new league 2' })
        .expect(400);
        expect(res.body).to.be.eql({
          status: 400,
          message: 'missing arguments'
        });
      });
    });
  });


  describe('DELETE /leagues/:leagueID', () => {
    describe('not logged in', () => {
      it('should return an error message', async () => {
        const res = await request(app)
        .del('/api/leagues/5300a28935b003712ba1adac')
        .expect(401);
        expect(res.body.message).to.be('Unauthorized access');
      });
    });

    describe('no matching league', () => {
      it('should return an error message', async () => {
        const res = await request(app)
        .del('/api/leagues/5300a28935b003712ba1aaaa')
        .set('Authorization', token)
        .expect(404);
        expect(res.body.code).to.be(40012);
        expect(res.body.message).to.be('League not found');
      });
    });

    describe('there is a matching league', () => {
      describe('the user is NOT part of it', () => {
        it('should return an error message: unauthorized', async () => {
          const res = await request(app)
          .del('/api/leagues/53bcf56ab3263bdf3a79daeb')
          .set('Authorization', token)
          .expect(403);
          expect(res.body.code).to.be(40013);
          expect(res.body.message).to.be('Unauthorized access');
        });
      });

      describe('the user is part of the league', () => {
        describe('the user is NOT its captain', () => {
          it('should return an error message', async () => {
            const res = await request(app)
            .del('/api/leagues/5300a28935b003712ba1adac')
            .set('Authorization', token)
            .expect(403);
            expect(res.body.code).to.be(40014);
            expect(res.body.message).to.be('Unauthorized access');
            const league = await mongoose
            .model('League')
            .findById('5300a28935b003712ba1adac')
            .exec();
            expect(league.name).to.be('my funny league'); // check that the league still exists !
          });
        });

        describe('the user IS its captain', () => {
          it('should delete the league object and clear the association with this league from other users of the league', async () => {
            const res = await request(app)
            .del('/api/leagues/7777a28935b003712ba1bbbb')
            .set('Authorization', token)
            .expect(200);
            expect(res.body.message).to.be('The league has been deleted');
            const league = await mongoose
            .model('League')
            .findById('7777a28935b003712ba1bbbb')
            .exec();
            expect(league).to.be(null);
            const bucket = await mongoose
            .model('MessageBucket')
            .findOne({ league: '7777a28935b003712ba1bbbb' })
            .exec();
            expect(bucket).to.be(null);
            const user = await mongoose
            .model('User')
            .findOne({ 'leagues.league': '7777a28935b003712ba1bbbb' })
            .exec();
            expect(user).to.be(null);
          });


          // revert changes
          after(async () => {
            await mongoose.model('League')
            .update({
              _id: '7777a28935b003712ba1bbbb'
            }, { // todo: check that update is ok for a creation
              _id: '7777a28935b003712ba1bbbb',
              name: 'my even funnier funny league',
              url: 'my.even.funnier.league',
              competition: '52ea654cf7073b5b2f42bc27',
              createdAt: '2014-02-16T11:35:37.000Z',
              captains: ['53668055a5746578545df2d8'],
              members: [
                {
                  user: '536e20d65ce44a661fde090a',
                  points: 810,
                  rank: 1
                },
                {
                  user: '53668055a5746578545df2d8',
                  points: 727,
                  rank: 2
                }
              ]
            }, { upsert: true })
            .exec();
          });
        });
      });
    });
  });

  // GET avec des options
  describe('GET /leagues?checkavailability=true&name=<name>', () => {
    describe('no matching league with that name', () => {
      it('should return ok', async () => {
        const res = await request(app)
        .get('/api/leagues?checkavailability=true&name=my%20exotic%20league')
        .set('Authorization', token)
        .expect(200);
        expect(res.body.available).to.be.eql(true);
      });
    });

    describe('there is already a league with that name', () => {
      it('should return nok', async () => {
        const res = await request(app)
        .get('/api/leagues?checkavailability=true&name=my%20funny%20league')
        .set('Authorization', token)
        .expect(200); // check code
        expect(res.body.available).to.be.eql(false);
      });
    });
  });

  describe('GET /leagues/leagueID/messages', () => {
    describe('the user doesn\'t belong to the league', () => {
      it('should return an error', async () => {
        const res = await request(app)
        .get('/api/leagues/53bcf56ab3263bdf3a79daeb/messages')
        .set('Authorization', token)
        .expect(403);
        expect(res.body).to.be.eql({
          status: 403,
          code: 40013,
          message: 'Unauthorized access'
        });
      });
    });

    describe('the user belongs to the league', () => {
      it('should return the last buckets of messages for that league, according to the parameters', async () => {
        const res = await request(app)
        .get('/api/leagues/5300a28935b003712ba1adac/messages?limit=5')
        .set('Authorization', token)
        .expect(200);
        expect(res.body.messages[0]).to.be.eql({
          content: 'Hello world !',
          datetime: '2014-02-16T10:35:37.000Z',
          sender: '536e20d65ce44a661fde090a'
        });
      });

      it('should return return only 1 message (1bucket*1message) if the limit is set to 1 bucket', async () => {
        const res = await request(app)
        .get('/api/leagues/5300a28935b003712ba1adac/messages?limit=1')
        .set('Authorization', token)
        .expect(200);
        expect(res.body.messages.length).to.be.lessThan(2);
        expect(res.body.messages[0]).to.be.eql({
          sender: '53668055a5746578545df2d8',
          datetime: '2014-02-16T11:38:37.000Z',
          content: 'Hell Yeah ! Lorem ipsum dolor sit amet jsqd qd azeazpe'
        });
      });

      it('should should skip bucket when asked for it', async () => {
        const res = await request(app)
        .get('/api/leagues/5300a28935b003712ba1adac/messages?timestamp=' + new Date('2014-02-16T11:37:00.000Z').getTime())
        .set('Authorization', token)
        .expect(200);
        expect(res.body).to.be.eql({
          status: 200,
          messages: [{
            sender: '536e20d65ce44a661fde090a',
            datetime: '2014-02-16T10:35:37.000Z',
            content: 'Hello world !'
          }, {
            sender: '53668055a5746578545df2d8',
            datetime: '2014-02-16T10:36:37.000Z',
            content: 'Hello world too !'
          }]
        });
      });
      // todo: ajouter un check sur une league avec plusieurs bucket
      // de messages pour tester la bucketistion ?
      // todo: ajouter test de la prise en compte timestamp
    });
  });


  describe.skip('GET /leagues/:leagueID/history?type=[ranking|points]&abscissa=[date|phase|day]&limit=[date|days]', () => {
    describe('the user doesn\'t belong to the league', () => {
      it('should return an error', async () => {
        const res = await request(app)
        .get('/api/leagues/53bcf56ab3263bdf3a79daeb/history?type=ranking&abscissa=date')
        .set('Authorization', token)
        .expect(403);
        expect(res.body).to.be.eql({
          status: 403,
          code: 40013,
          message: 'Unauthorized access'
        });
      });
    });

    describe('the user belongs to the league', () => {
      it('should return all *past* forecasts of all the users of that league for all the completed matches of that league with a format in accordance with parameters', async () => {
        const res = await request(app)
        .get('/api/leagues/5300a28935b003712ba1adac/history?type=ranking&abscissa=date')
        .set('Authorization', token)
        .expect(200);
        expect(res.body).to.be.eql({
          // todo: to be determined, à voir avec Etienne
        });
      });
    });
  });

/* describe.only('GET /enterprise/leagues/:id/ranking', () => {
it('should download the ranking', async () => {
  request(app)
    .get('/api/enterprise/leagues/5300a28935b003712ba1adac/ranking')
    .set('Authorization', token)
    .end(function(err, res) {
      expect(res.status).to.be(200);
      expect(res.body).to.be.eql({
        //todo: to be determined, à voir avec Etienne
      })
      return done();
    })
});*/
});
