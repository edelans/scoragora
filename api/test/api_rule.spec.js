//force process to test
process.env.NODE_ENV = 'test';

var mocha = require('mocha');
var should = require('should');
var request = require('supertest');

var SvcUser = require('../services/svc_user.js');
var app = require('../app.js').app;

var utils = require('./utils');
var mongoose = require('mongoose');
var Rule = require('../models/rules.js');


describe('#routes/rules.js', function() {

  describe('GET /rules/:ruleID', function() {

    describe('no matching rule', function() {
      it('should return an error message');
    });

    describe('there is a matching rule', function() {
      it('should return the rule object');
    });
  });
});
