// force process to test
process.env.NODE_ENV = 'test';

const expect = require('expect.js');
const request = require('supertest-as-promised');

const app = require('../app.js').app;

const mongoose = require('mongoose');

describe('#routes/user.js', function () {
  let token;
  this.timeout(5000);

  before(async () => {
    const res = await request(app)
    .post('/api/login/local')
    .send({
      email: 'etienne@scoragora.com',
      password: 'pwd'
    });
    token = 'Bearer ' + res.body.token;
  });

  describe('POST /api/users', () => {
    describe('No data', () => {
      it('should return code 40000', async () => {
        const res = await request(app)
        .post('/api/users')
        .send({});
        expect(res.status).to.be(400);
        expect(res.body.code).to.be(40000);
      });
    });

    describe('Empty user', () => {
      it('should return code 40004', async () => {
        const res = await request(app)
        .post('/api/users')
        .send({
          user: {}
        });
        expect(res.status).to.be(400);
        expect(res.body.code).to.be(40004);
      });
    });

    describe('No pseudo', () => {
      it('should return code 40004', async () => {
        const res = await request(app)
        .post('/api/users')
        .send({
          user: {
            email: 'email'
          }
        });
        expect(res.status).to.be(400);
        expect(res.body.code).to.be(40004);
      });
    });

    describe('No email', () => {
      it('should return code 40005', async () => {
        const res = await request(app)
        .post('/api/users')
        .send({
          user: {
            pseudo: 'pseudo'
          }
        });
        expect(res.status).to.be(400);
        expect(res.body.code).to.be(40005);
      });
    });

    describe('No password', () => {
      it('should return code 40006', async () => {
        const res = await request(app)
        .post('/api/users')
        .send({
          user: {
            pseudo: 'pseudo',
            email: 'email'
          }
        });
        expect(res.status).to.be(400);
        expect(res.body.code).to.be(40006);
      });
    });

    describe('Invalid email', () => {
      it('should return code 40007', async () => {
        const res = await request(app)
        .post('/api/users')
        .send({
          user: {
            pseudo: 'pseudo',
            email: 'email',
            password: 'pwd'
          }
        });
        expect(res.status).to.be(400);
        expect(res.body.code).to.be(40007);
      });
    });

    describe('Email already used', () => {
      it('should return code 40009', async () => {
        const res = await request(app)
        .post('/api/users')
        .send({
          user: {
            pseudo: 'pseudo',
            email: 'etienne@scoragora.com',
            password: 'pwd'
          }
        });
        expect(res.status).to.be(400);
        expect(res.body.code).to.be(40009);
      });
    });
  });

  describe('GET /users/:id', () => {
    describe('not logged in', () => {
      it('should return an error message', async () => {
        const res = await request(app)
        .get('/api/users/53668055a5746578545df2d8');
        expect(res.status).to.be(401);
      });
    });

    describe('no matching user', () => {
      it('should return an error message', async () => {
        const res = await request(app)
        .get('/api/users/53668055a5746578545df2d9?profile=summary')
        .set('Authorization', token);
        expect(res.status).to.be(400);
        expect(res.body).to.be.eql({
          status: 400,
          message: 'no user found'
        });
      });
    });

    describe('matching user', () => {
      describe('accessing same user', () => {
        it('should return a summary', async () => {
          const res = await request(app)
          .get('/api/users/53668055a5746578545df2d8?profile=summary')
          .set('Authorization', token);
          expect(res.status).to.be(200);
          expect(res.body.user).to.be.eql({
            _id: '53668055a5746578545df2d8',
            displayName: 'etienne',
            pictureUrl: '/images/profile_picture/trix_flix08.jpg'
          });
        });

        it('should return full user details', async () => {
          const res = await request(app)
          .get('/api/users/53668055a5746578545df2d8')
          .set('Authorization', token);
          expect(res.status).to.be(200);
          // remove __v
          delete res.body.user.__v;
          expect(res.body.user).to.be.eql({
            _id: '53668055a5746578545df2d8',
            createdAt: '2014-05-04T18:00:53.000Z',
            lng: 'en',
            login: {
              local: {
                email: 'etienne@scoragora.com',
                pictureURL: '/images/profile_picture/trix_flix08.jpg',
                pseudo: 'etienne'
              },
              google: {
                id: '111372465408075365270',
                token: 'ya29.LgA7DvfgDJNiqB8AAAD9lPVaI7ZL0u5nBrHpCvKWWaCIzuFNIh0Mgu2FRfJosQ',
                displayName: 'Fabrisio K',
                email: 'koutcho.fabrice@gmail.com',
                pictureURL: 'https://lh5.googleusercontent.com/-sstGsFFvOZw/AAAAAAAAAAI/AAAAAAAAAfI/KTAwJX5G7Ds/photo.jpg'
              }
            },
            pictureMode: 'local',
            points: 0,
            email_settings: {
              forecast_management: {
                new_matches: true,
                reminder_24h: true
              }
            }
          });
          expect(res.body.user.login.local).not.to.have.property('passport');
          expect(res.body.user).not.to.have.property('forecasts');
          expect(res.body.user).not.to.have.property('leagues');
        });
      });

      describe('accessing different user', () => {
        it('should return an error message when requiring full details', async () => {
          const res = await request(app)
          .get('/api/users/536e20d65ce44a661fde090a')
          .set('Authorization', token);
          expect(res.status).to.be(403);
        });

        it('should return a summary', async () => {
          const res = await request(app)
          .get('/api/users/536e20d65ce44a661fde090a?profile=summary')
          .set('Authorization', token);
          expect(res.status).to.be(200);
          expect(res.body.user).to.be.eql({
            _id: '536e20d65ce44a661fde090a',
            displayName: 'edouard',
            pictureUrl: '/images/profile_picture/trix_flix08.jpg'
          });
        });
      });
    });
  });

  describe('GET /users/:id/matches', () => {
    describe('not logged in', () => {
      it('should return an error message', async () => {
        const res = await request(app)
        .get('/api/users/53668055a5746578545df2d8/matches');
        expect(res.status).to.be(401);
      });
    });

    describe('accessing different user', () => {
      it('should return an error message when requiring full details', async () => {
        const res = await request(app)
        .get('/api/users/536e20d65ce44a661fde090a/matches')
        .set('Authorization', token);
        expect(res.status).to.be(403);
      });
    });

    describe('accessing same user', () => {
      it('should return as many matches as the given limit', async () => {
        const res = await request(app)
        .get('/api/users/53668055a5746578545df2d8/matches?limit=2&status=past')
        .set('Authorization', token);
        expect(res.status).to.be(200);
        expect(res.body.matches).to.be.an(Array);
        expect(res.body.matches.length).to.be.lessThan(3);
      });

      it('should return past matches', async () => {
        const res = await request(app)
        .get('/api/users/53668055a5746578545df2d8/matches?limit=3&status=past')
        .set('Authorization', token);
        expect(res.status).to.be(200);
        expect(res.body.matches).to.be.an(Array);
        res.body.matches.forEach((match) => {
          expect((new Date(match.datetime)).getTime()).to.be.lessThan((new Date()).getTime());
        });
      });

      it('should return no match', async () => {
        const res = await request(app)
        .get('/api/users/53668055a5746578545df2d8/matches')
        .set('Authorization', token);
        expect(res.status).to.be(200);
        expect(res.body.matches).to.be.an(Array);
        expect(res.body.matches.length).to.be(0);
      });
    });
  });

  describe('GET /users/:id/leagues', () => {
    describe('not logged in', () => {
      it('should return an error message', async () => {
        const res = await request(app)
        .get('/api/users/53668055a5746578545df2d8/leagues');
        expect(res.status).to.be.equal(401);
      });
    });

    describe('accessing different user', () => {
      it('should return an error message when requiring full details', async () => {
        const res = await request(app)
        .get('/api/users/536e20d65ce44a661fde090a/leagues')
        .set('Authorization', token);
        expect(res.status).to.be(403);
      });
    });

    describe('matching user', () => {
      it('should return as many leagues as the given limit', async () => {
        const res = await request(app)
        .get('/api/users/53668055a5746578545df2d8/leagues?limit=1')
        .set('Authorization', token);
        expect(res.status).to.be(200);
        expect(res.body.leagues.length).to.be.lessThan(2);
      });

      it('should return last active leagues');

      it('should return unread messages');
    });
  });

  describe('GET /users/:id/forecasts', () => {
    describe('not logged in', () => {
      it('should return an error message', function(done) {
        request(app)
        .get('/api/users/53668055a5746578545df2d8/forecasts')
        .end(function(err, res) {
          expect(res.status).to.be.equal(401);
          return done();
        });
      });
    });

    describe('accessing different user', () => {
      it('should return an error message when requiring full details', function(done) {
        request(app)
        .get('/api/users/536e20d65ce44a661fde090a/forecasts')
        .set('Authorization', token)
        .end(function(err, res) {
          expect(res.status).to.be(403);
          return done();
        });
      });
    });

    describe('matching user', function() {
      it('should return as many matches as the given limit', function(done) {
        request(app)
        .get('/api/users/53668055a5746578545df2d8/forecasts?limit=1')
        .set('Authorization', token)
        .end(function(err, res) {
          expect(res.status).to.be(200);
          expect(res.body.forecasts.length).to.be.lessThan(2);
          expect(res.body.forecasts).to.be.eql([{
            _id: '53000da4d94a6de820acf005',
            competition: {
              _id: '52ea654cf7073b5b2f42bc27',
              config: {
	            type: 'cup'
              },
              name: 'world_cup',
              year: '2014'
            }
          }]);
          return done();
        });
      });
    });
  });

  describe('GET /users/:id/matchforecasts', function() {
    describe('not logged in', function() {
      it('should return an error message', function(done) {
        request(app)
        .get('/api/users/53668055a5746578545df2d8/forecasts')
        .end(function(err, res) {
          expect(res.status).to.be.equal(401);
          return done();
        });
      });
    });

    describe('accessing different user', function() {
      it('should return an error message when requiring full details', function(done) {
        request(app)
        .get('/api/users/536e20d65ce44a661fde090a/forecasts')
        .set('Authorization', token)
        .end(function(err, res) {
          expect(res.status).to.be(403);
          return done();
        });
      });
    });

    describe('matching user', () => {
      it('should return as many matches as the given limit', async () => {
        const res = await request(app)
        .get('/api/users/53668055a5746578545df2d8/matchforecasts?limit=1')
        .set('Authorization', token);
        expect(res.status).to.be(200);
        expect(res.body.matches.length).to.be.lessThan(2);
        res.body.matches.forEach((match) => {
          expect((new Date(match.datetime)).getTime()).to.be.greaterThan((new Date()).getTime());
          expect(match).to.have.property('matchForecast');
          expect(match).to.have.property('competition');
          expect(match).to.have.property('match');
        });
      });
    });
  });

  describe('PUT /users/:id', () => {
    describe('not logged in', () => {
      it('should return an error message', async () => {
        const res = await request(app)
        .put('/api/users/53668055a5746578545df2d8');
        expect(res.status).to.be.equal(401);
      });
    });

    describe('accessing different user', () => {
      it('should return an error message when requiring full details', async () => {
        const res = await request(app)
        .put('/api/users/536e20d65ce44a661fde090a')
        .set('Authorization', token);
        expect(res.status).to.be(403);
      });
    });

    describe('matching user', () => {
      describe('updating pictureMode', () => {
        describe('invalid mode', () => {
          it('should return an error message', async () => {
            const res = await request(app)
            .put('/api/users/53668055a5746578545df2d8')
            .send({
              pictureMode: 'wrongmode'
            })
            .set('Authorization', token);
            expect(res.status).to.be(400);
            expect(res.body.message).to.be('Invalid input');
          });
        });

        describe('unavailable mode', () => {
          it('should return an error message', async () => {
            const res = await request(app)
            .put('/api/users/53668055a5746578545df2d8')
            .send({
              pictureMode: 'twitter'
            })
            .set('Authorization', token);
            expect(res.status).to.be(400);
            expect(res.body.message).to.be('User can\'t be updated');
          });
        });

        describe('available mode', () => {
          it('should update picture mode', async () => {
            const res = await request(app)
            .put('/api/users/53668055a5746578545df2d8')
            .send({
              pictureMode: 'google'
            })
            .set('Authorization', token);
            expect(res.status).to.be(200);
            expect(res.body.message).to.be('User picture mode has been updated');
            const user = await mongoose.model('User').findById('53668055a5746578545df2d8', {
              pictureMode: 1
            }).exec();
            expect(user).to.have.property('pictureMode');
            expect(user.pictureMode).to.be.equal('google');
          });

          // Revert change
          after(async () => {
            await mongoose.model('User').update({
              _id: '53668055a5746578545df2d8'
            }, {
              pictureMode: 'local'
            }).exec();
          });
        });
      });

      describe('updating pseudo', () => {
        describe('invalid pseudo', () => {
          it('should return an error message', async () => {
            const res = await request(app)
            .put('/api/users/53668055a5746578545df2d8')
            .send({
              pseudo: 'atoolongpseudoatoolongpseudoatoolongpseudoatoolongpseudo'
            })
            .set('Authorization', token)
            .expect(400);
            expect(res.body.message).to.be('Invalid input : pseudo exceeding 50 chars');
          });
        });

        describe('valid pseudo', () => {
          it('should update pseudo', async () => {
            const res = await request(app)
            .put('/api/users/53668055a5746578545df2d8')
            .send({
              pseudo: 'tintin'
            })
            .set('Authorization', token);
            expect(res.status).to.be(200);
            expect(res.body.message).to.be('User pseudo has been updated');
            const user = await mongoose
            .model('User')
            .findById('53668055a5746578545df2d8')
            .select({
              'login.local.pseudo': 1,
              _id: 0
            })
            .exec();
            expect(user).to.have.property('login');
            expect(user.login).to.have.property('local');
            expect(user.login.local).to.have.property('pseudo');
            expect(user.login.local.pseudo).to.be.equal('tintin');
          });

          // Revert change
          after(async () => {
            await mongoose.model('User').update({
              _id: '53668055a5746578545df2d8'
            }, {
              'login.local.pseudo': 'etienne'
            }).exec();
          });
        });

        describe('no local account', () => {
          // remove login.local
          before(async () => {
            await mongoose.model('User').update({
              _id: '53668055a5746578545df2d8'
            }, {
              $unset: {
                'login.local': 1
              }
            }).exec();
          });

          it('should update pseudo', async () => {
            const res = await request(app)
            .put('/api/users/53668055a5746578545df2d8')
            .send({
              pseudo: 'tintin'
            })
            .set('Authorization', token);
            expect(res.status).to.be(200);
            expect(res.body.message).to.be('User pseudo has been updated');
            const user = await mongoose
            .model('User')
            .findById('53668055a5746578545df2d8')
            .exec();
            expect(user).to.have.property('login');
            expect(user.login).to.have.property('local');
            expect(user.login.local).to.have.property('pseudo');
            expect(user.login.local.pseudo).to.be.equal('tintin');
          });

          // Revert change
          after(async () => {
            await mongoose.model('User').update({
              _id: '53668055a5746578545df2d8'
            }, {
              'login.local': {
                email: 'etienne@scoragora.com',
                password: '$2a$10$d4GMqC6TDfEiY2eJB2ffv.JfOMINOuVZKu9nKxVToxhkQ3pgtIpzS',
                pictureURL: '/images/profile_picture/trix_flix08.jpg',
                pseudo: 'etienne'
              }
            }).exec();
          });
        });
      });

      describe('updating password', () => {
        describe('invalid input', () => {
          describe('missing new password', () => {
            it('should return an error message', async () => {
              const res = await request(app)
              .put('/api/users/53668055a5746578545df2d8')
              .send({
                passwordForm: {
                  oldPassword: 'pwd',
                  passwordConfirmation: 'new_pwd'
                }
              })
              .set('Authorization', token);
              expect(res.status).to.be(400);
              expect(res.body.message).to.be('password missing');
            });
          });

          describe('missing old password', () => {
            it('should return an error message', async () => {
              const res = await request(app)
              .put('/api/users/53668055a5746578545df2d8')
              .send({
                passwordForm: {
                  password: 'new_pwd',
                  passwordConfirmation: 'new_pwd'
                }
              })
              .set('Authorization', token);
              expect(res.status).to.be(400);
              expect(res.body.message).to.be('old password missing');
            });
          });

          describe('missing password confirmation', () => {
            it('should return an error message', async () => {
              const res = await request(app)
              .put('/api/users/53668055a5746578545df2d8')
              .send({
                passwordForm: {
                  password: 'new_pwd',
                  oldPassword: 'pwd'
                }
              })
              .set('Authorization', token);
              expect(res.status).to.be(400);
              expect(res.body.message).to.be('password confirmation missing');
            });
          });

          describe('mismatching passwords', () => {
            it('should return an error message', async () => {
              const res = await request(app)
              .put('/api/users/53668055a5746578545df2d8')
              .send({
                passwordForm: {
                  password: 'new_pwd',
                  oldPassword: 'pwd',
                  passwordConfirmation: 'different_pwd'
                }
              })
              .set('Authorization', token);
              expect(res.status).to.be(400);
              expect(res.body.message).to.be('password and password confirmation dont match');
            });
          });

          describe('wrong old password', () => {
            it('should return an error message', async () => {
              const res = await request(app)
              .put('/api/users/53668055a5746578545df2d8')
              .send({
                passwordForm: {
                  password: 'new_pwd',
                  oldPassword: 'wrong_pwd',
                  passwordConfirmation: 'new_pwd'
                }
              })
              .set('Authorization', token);
              expect(res.status).to.be(400);
              expect(res.body.message).to.be('wrong old password');
            });
          });
        });

        describe('valid password form', () => {
          it('should update password', async () => {
            const res = await request(app)
            .put('/api/users/53668055a5746578545df2d8')
            .send({
              passwordForm: {
                password: 'new_pwd',
                oldPassword: 'pwd',
                passwordConfirmation: 'new_pwd'
              }
            })
            .set('Authorization', token);

            expect(res.status).to.be(200);
            expect(res.body.message).to.be('User password has been updated');
            const user = await mongoose.model('User')
            .findById('53668055a5746578545df2d8')
            .exec();
            const isValid = user.validPassword('new_pwd');
            expect(isValid).to.be(true);
            const isValid2 = user.validPassword('pwd');
            expect(isValid2).to.be(false);
          });

          // Revert change
          after(async () => {
            await mongoose.model('User').update({
              _id: '53668055a5746578545df2d8'
            }, {
              'login.local.password': '$2a$10$d4GMqC6TDfEiY2eJB2ffv.JfOMINOuVZKu9nKxVToxhkQ3pgtIpzS'
            }).exec();
          });
        });

        describe('no local account', () => {
          // remove login.local
          before(async () => {
            await mongoose.model('User').update({
              _id: '53668055a5746578545df2d8'
            }, {
              $unset: {
                'login.local': 1
              }
            }).exec();
          });

          it('should not update password', async () => {
            const res = await request(app)
            .put('/api/users/53668055a5746578545df2d8')
            .send({
              passwordForm: {
                password: 'new_pwd',
                oldPassword: 'pwd',
                passwordConfirmation: 'new_pwd'
              }
            })
            .set('Authorization', token);
            expect(res.status).to.be(400);
            expect(res.body.message).to.be('wrong old password');
          });

          it('should not update password', async () => {
            const res = await request(app)
            .put('/api/users/53668055a5746578545df2d8')
            .send({
              passwordForm: {
                password: 'new_pwd',
                oldPassword: '',
                passwordConfirmation: 'new_pwd'
              }
            })
            .set('Authorization', token);
            expect(res.status).to.be(400);
            expect(res.body.message).to.be('old password missing');
          });

          // Revert change
          after(async () => {
            await mongoose.model('User').update({
              _id: '53668055a5746578545df2d8'
            }, {
              'login.local': {
                email: 'etienne@scoragora.com',
                password: '$2a$10$d4GMqC6TDfEiY2eJB2ffv.JfOMINOuVZKu9nKxVToxhkQ3pgtIpzS',
                pictureURL: '/images/profile_picture/trix_flix08.jpg',
                pseudo: 'etienne'
              }
            }).exec();
          });
        });
      });

      describe('updating local picture', () => {
        describe('valid picture', () => {
          it('should update picture', async () => {
            const res = await request(app)
            .put('/api/users/53668055a5746578545df2d8')
            .send({
              pictureURL: 'url'
            })
            .set('Authorization', token);
            expect(res.status).to.be(200);
            expect(res.body.message).to.be('User picture has been updated');
            const user = await mongoose
            .model('User')
            .findById('53668055a5746578545df2d8')
            .exec();
            expect(user).to.have.property('login');
            expect(user.login).to.have.property('local');
            expect(user.login.local).to.have.property('pictureURL');
            expect(user.login.local.pictureURL).to.be.equal('url');
          });

          // Revert change
          after(async () => {
            await mongoose.model('User').update({
              _id: '53668055a5746578545df2d8'
            }, {
              'login.local.pictureURL': '/images/profile_picture/trix_flix08.jpg'
            }).exec();
          });
        });

        describe('no local account', function() {
          // remove login.local
          before(async () => {
            await mongoose.model('User').update({
              _id: '53668055a5746578545df2d8'
            }, {
              $unset: {
                'login.local': 1
              }
            }).exec();
          });

          it('should not update password', async () => {
            const res = await request(app)
            .put('/api/users/53668055a5746578545df2d8')
            .send({
              pictureURL: 'url'
            })
            .set('Authorization', token);
            expect(res.status).to.be(200);
            expect(res.body.message).to.be('User picture has been updated');
          });

          // Revert change
          after(async () => {
            await mongoose.model('User').update({
              _id: '53668055a5746578545df2d8'
            }, {
              'login.local': {
                email: 'etienne@scoragora.com',
                password: '$2a$10$d4GMqC6TDfEiY2eJB2ffv.JfOMINOuVZKu9nKxVToxhkQ3pgtIpzS',
                pictureURL: '/images/profile_picture/trix_flix08.jpg',
                pseudo: 'etienne'
              }
            }).exec();
          });
        });
      });

      describe('updating language', () => {
        describe('valid lng', () => {
          it('should update picture', async () => {
            const res = await request(app)
            .put('/api/users/53668055a5746578545df2d8')
            .send({
              lng: 'fr'
            })
            .set('Authorization', token);
            expect(res.status).to.be(200);
            expect(res.body.message).to.be('User lng has been updated');
            const user = await mongoose
            .model('User')
            .findById('53668055a5746578545df2d8')
            .exec();
            expect(user.lng).to.be('fr');
          });

          // Revert change
          after(async () => {
            await mongoose.model('User').update({
              _id: '53668055a5746578545df2d8'
            }, {
              lng: 'en'
            }).exec();
          });
        });

        describe('unvalid lng', () => {
          it('should not update lng', async () => {
            const res = await request(app)
            .put('/api/users/53668055a5746578545df2d8')
            .send({
              lng: 'zc'
            })
            .set('Authorization', token);
            expect(res.status).to.be(400);
            expect(res.body.message).to.be('Invalid language');
          });
        });
      });
    });
  });
});
