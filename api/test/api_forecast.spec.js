// force process to test
process.env.NODE_ENV = 'test';

const expect = require('expect.js');
const request = require('supertest-as-promised');
const mongoose = require('mongoose');

const app = require('../app.js').app;

require('./utils');

describe('#routes/forecasts.js', function () {
  let token;
  this.timeout(5000);

  before(async () => {
    // log it
    const res = await request(app)
    .post('/api/login/local')
    .send({ email: 'etienne@scoragora.com', password: 'pwd' });
    token = 'Bearer ' + res.body.token;
  });

  describe('PUT /forecasts/:forecastID', () => {
    describe('not logged in', () => {
      it('should return an error message', async () => {
        const res = await request(app)
        .put('/api/forecasts/53000da4d94a6de820acf005')
        .send({
          matches: []
        });
        expect(res.status).to.be(401);
      });
    });

    describe('no matching forecasts', () => {
      it('should return an error message', async () => {
        const res = await request(app)
        .put('/api/forecasts/53000da4d94a6de820acf004')
        .send({
          matches: [{}]
        })
        .set('Authorization', token);
        expect(res.status).to.be(400);
        expect(res.body).to.be.eql({
          status: 400,
          message: 'forecast not found'
        });
      });
    });

    describe('forecast not from the user', () => {
      it('should return an error message', async () => {
        const res = await request(app)
        .put('/api/forecasts/535c563330ecdaa03335e361')
        .send({
          matches: [{}]
        })
        .set('Authorization', token);
        expect(res.status).to.be(403);
        expect(res.body).to.be.eql({
          status: 403,
          message: 'Unauthorized access'
        });
      });
    });

    describe('forecast from the user', () => {
      describe('update a match from the past', () => {
        it('should not update the match', async () => {
          const res = await request(app)
          .put('/api/forecasts/53000da4d94a6de820acf005')
          .send({
            matches: [{
              match: '52fd4eda6b891e6702ea3490',
              forecast: {
                hometeam: {
                  score: 0
                },
                awayteam: {
                  score: 1
                }
              }
            }]
          })
          .set('Authorization', token);
          expect(res.status).to.be(200);
          expect(res.body.message).to.be('Forecast has been updated');
          const forecast = await mongoose.model('Forecast').findById('53000da4d94a6de820acf005').exec();
          forecast.matches.forEach((match) => {
            if (match.match === '52fd4eda6b891e6702ea3490') {
              expect(match.forecast).to.be.eql({
                hometeam: {
                  score: 1
                },
                awayteam: {
                  score: 2
                }
              });
            }
          });
        });
      });

      describe('update matches from the past and the future', () => {
        it('should update only the future match', async () => {
          const res = await request(app)
          .put('/api/forecasts/53000da4d94a6de820acf005')
          .send({
            matches: [{
              match: '53025e2e632b0d1402a3cb15',
              forecast: {
                hometeam: {
                  score: 0
                },
                awayteam: {
                  score: 1
                }
              }
            }, {
              match: '52fd4eda6b891e6702ea3490',
              forecast: {
                hometeam: {
                  score: 4
                },
                awayteam: {
                  score: 6
                }
              }
            }]
          })
          .set('Authorization', token);
          expect(res.status).to.be(200);
          expect(res.body.message).to.be('Forecast has been updated');
          const forecast = await mongoose.model('Forecast').findById('53000da4d94a6de820acf005').exec();
          forecast.matches.forEach((match) => {
            if (match.match === '53025e2e632b0d1402a3cb15') {
              expect(match.forecast).to.be.eql({
                hometeam: {
                  score: 0
                },
                awayteam: {
                  score: 1
                }
              });
            }
            if (match.match === '52fd4eda6b891e6702ea3490') {
              expect(match.forecast).to.be.eql({
                hometeam: {
                  score: 1
                },
                awayteam: {
                  score: 2
                }
              });
            }
          });
        });

        after(async () => {
          await mongoose.model('Forecast')
          .update({
            _id: '53000da4d94a6de820acf005',
            'matches.match': '53025e2e632b0d1402a3cb15'
          }, {
            'matches.$.forecast': {
              hometeam: {
                score: 2
              },
              awayteam: {
                score: 3
              }
            }
          }).exec();
        });
      });

      describe('update matches', () => {
        it('should update the forecast', async () => {
          const res = await request(app)
          .put('/api/forecasts/53000da4d94a6de820acf005')
          .send({
            matches: [{
              match: '53025e2e632b0d1402a3cb15',
              forecast: {
                hometeam: {
                  score: 0
                },
                awayteam: {
                  score: 1
                }
              }
            }]
          })
          .set('Authorization', token);
          expect(res.status).to.be(200);
          expect(res.body.message).to.be('Forecast has been updated');
          const forecast = await mongoose.model('Forecast').findById('53000da4d94a6de820acf005').exec();
          forecast.matches.forEach((match) => {
            if (match.match === '53025e2e632b0d1402a3cb15') {
              expect(match.forecast).to.be.eql({
                hometeam: {
                  score: 0
                },
                awayteam: {
                  score: 1
                }
              });
            }
          });
        });

        after(async () => {
          await mongoose.model('Forecast')
          .update({
            _id: '53000da4d94a6de820acf005',
            'matches.match': '53025e2e632b0d1402a3cb15'
          }, {
            'matches.$.forecast': {
              hometeam: {
                score: 2
              },
              awayteam: {
                score: 3
              }
            }
          })
          .exec();
        });
      });

      describe('update too many matches', () => {
        it('should return an error', async () => {
          const matches = [];
          for (let i = 1; i < 1000; i += 1) {
            matches.push({
              match: i
            });
          }
          const res = await request(app)
          .put('/api/forecasts/53000da4d94a6de820acf005')
          .send({
            matches: matches
          })
          .set('Authorization', token);
          expect(res.status).to.be(400);
          expect(res.body).to.be.eql({
            message: 'Invalid input : too many matches to update',
            status: 400
          });
        });
      });
    });
  });

  describe('GET /forecasts/:forecastID', () => {
    describe('not logged in', () => {
      it('should return an error message', async () => {
        const res = await request(app)
        .get('/api/forecasts/53000da4d94a6de820acf005');
        expect(res.status).to.be(401);
      });
    });

    describe('no matching forecasts', () => {
      it('should return an error message', async () => {
        const res = await request(app)
        .get('/api/forecasts/53000da4d94a6de820acf004')
        .set('Authorization', token);
        expect(res.status).to.be(400);
        expect(res.body).to.be.eql({
          status: 400,
          message: 'forecast not found'
        });
      });
    });

    describe('forecast from the user', () => {
      it('should return the forecasts object, unpopulated', async () => {
        const res = await request(app)
        .get('/api/forecasts/53000da4d94a6de820acf005')
        .set('Authorization', token);
        expect(res.status).to.be(200);
        expect(res.body).to.have.property('forecast');
      });
    });

    describe('forecast not from the user', () => {
      it('should return an error message', async () => {
        const res = await request(app)
        .get('/api/forecasts/535c563330ecdaa03335e361')
        .set('Authorization', token);
        expect(res.status).to.be(403);
        expect(res.body).to.be.eql({
          status: 403,
          message: 'Unauthorized access'
        });
      });
    });
  });
});
