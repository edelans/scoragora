var mocha = require("mocha");
var expect = require('expect.js');

var utils = require("../../scheduler/algos/utils.js");

describe("areEqual", function() {
	
	it("should compare null", function(done) {
		var result = utils.areEqual(null, null);
		expect(result).to.be(true);
		return done();
	});
	
	it("should compare null (2)", function(done) {
		var result = utils.areEqual({a: 1}, null);
		expect(result).to.be(false);
		return done();
	});
	
	it("should compare null (3)", function(done) {
		var result = utils.areEqual(null, {a: 1});
		expect(result).to.be(false);
		return done();
	});
	
	it("should compare values", function(done) {
		var result = utils.areEqual({a: 2}, {a: 1});
		expect(result).to.be(false);
		return done();
	});
	
	it("should compare keys", function(done) {
		var result = utils.areEqual({b: 1}, {a: 1});
		expect(result).to.be(false);
		return done();
	});
	
	it("should not take the order into account", function(done) {
		var result = utils.areEqual({a: 2, b: 1}, {b: 1, a: 2});
		expect(result).to.be(true);
		return done();
	});
});
