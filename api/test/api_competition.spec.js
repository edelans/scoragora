// force process to test
process.env.NODE_ENV = 'test';

const expect = require('expect.js');
const request = require('supertest-as-promised');

const app = require('../app.js').app;

require('./utils.js');

describe('#routes/competitions.js', function () {
  let token;
  this.timeout(5000);

  before(async () => {
    // log it
    const res = await request(app)
    .post('/api/login/local')
    .send({ email: 'etienne@scoragora.com', password: 'pwd' });
    token = 'Bearer ' + res.body.token;
  });

  describe('GET /competitions', () => {
    describe('no parameter', () => {
      it('should return the list of all public competitions', async () => {
        const res = await request(app)
        .get('/api/competitions')
        .set('Authorization', token);
        expect(res.status).to.be(200);
        expect(res.body).to.be.eql({
          status: 200
        });
      });
    });

    describe('status set as public in parameter', () => {
      it('should return the list of all public competitions', async () => {
        const res = await request(app)
        .get('/api/competitions?status=public')
        .set('Authorization', token);
        expect(res.status).to.be(200);
        expect(res.body).to.be.eql({
          status: 200,
          competitions: [{
            _id: '52ea654cf7073b5b2f42bc27',
            config: {
              type: 'cup'
            },
            name: 'world_cup',
            year: '2014',
            sport: 'football'
          }]
        });
      });
    });
  });

  describe('GET /competitions/:competitionID', () => {
    describe('no matching competition', () => {
      it('should return an error message', async () => {
        const res = await request(app)
        .get('/api/competitions/52ea654cf7073b5b2f42bc28')
        .set('Authorization', token);
        expect(res.status).to.be(400);
        expect(res.body).to.be.eql({
          status: 400,
          message: 'competition not found'
        });
      });
    });

    describe('there is a matching competition', () => {
      it('should return the competition object, unpopulated', async () => {
        const res = await request(app)
        .get('/api/competitions/52ea654cf7073b5b2f42bc27')
        .set('Authorization', token);
        expect(res.status).to.be(200);
        expect(res.body.competition).not.to.be(null);
        // check dates
        expect(res.body.competition.start_date).not.to.be(null);
        delete res.body.competition.start_date;
        expect(res.body.competition).to.be.eql({
          _id: '52ea654cf7073b5b2f42bc27',
          config: {
            type: 'cup',
            cup: {
              elimination: {
                round_max: 8,
                two_legged: false,
                third_place: true
              },
              group: {
                number: 8,
                capacity: 4,
                two_legged: false,
                qualified_number: 2
              }
            }
          },
          matches: [{
            match: '52ea654cf7073b5b2f42bc50',
            group: 'A',
            day: 1
          }, {
            match: '53025e2e632b0d1402a3cb15'
          }, {
            match: '52fd4eda6b891e6702ea3490'
          }, {
            match: '53025e2e632b0d1402a3caf7'
          }],
          name: 'world_cup',
          progress: {
            now: 700,
            total: 2590
          },
          status: 'public',
          sport: 'football',
          year: '2014'
        });
      });
    });
  });

  describe('GET /competitions/competitionID/matches', () => {
    describe('no matching competition', () => {
      it('should return an error message', async () => {
        const res = await request(app)
        .get('/api/competitions/52ea654cf7073b5b2f42bc28/matches')
        .set('Authorization', token);
        expect(res.status).to.be(400);
        expect(res.body).to.be.eql({
          status: 400,
          message: 'competition not found'
        });
      });
    });

    describe('there is a matching competition', () => {
      describe('there is a limit filter', () => {
        it('should return the all the matches of the competition, including past, present and future', async () => {
          const res = await request(app)
          .get('/api/competitions/52ea654cf7073b5b2f42bc27/matches')
          .set('Authorization', token);
          expect(res.status).to.be(200);
          expect(res.body.matches).not.to.be(null);
          expect(res.body.matches.length).to.be.greaterThan(2);
          expect(res.body.matches[0]).to.be.eql({
            _id: '52fd4eda6b891e6702ea3490',
            awayteam: '5499a23357465b41c09d4982',
            competition: '52ea654cf7073b5b2f42bc27',
            datetime: '2014-04-29T18:45:00.000Z',
            hometeam: '5499a23357465b41c09d497d',
            result: {
              hometeam_score: 0,
              awayteam_score: 1
            },
            status: 'closed'
          });
        });
      });
    });
  });
});
