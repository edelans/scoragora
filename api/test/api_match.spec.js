// force process to test
process.env.NODE_ENV = 'test';

const expect = require('expect.js');
const request = require('supertest-as-promised');

const app = require('../app.js').app;

require('./utils');

describe('#routes/matches.js', function () {
  let token;
  this.timeout(5000);

  before(async () => {
    // log it
    const res = await request(app)
    .post('/api/login/local')
    .send({ email: 'etienne@scoragora.com', password: 'pwd' });
    token = 'Bearer ' + res.body.token;
  });

  describe('GET /matches/:matchID', () => {
    describe('not logged in', () => {
      it('should return an error message', async () => {
        const res = await request(app)
        .get('/api/matches/52fd4eda6b891e6702ea3490');
        expect(res.status).to.equal(401);
        expect(res.body).to.be.eql({
          status: 401,
          message: 'Unauthorized access'
        });
      });
    });

    describe('logged in', () => {
      describe('match details', () => {
        describe('no matching match', () => {
          it('should return an empty object', async () => {
            const res = await request(app)
            .get('/api/matches/52fd4eda6b891e6702ea3491')
            .set('Authorization', token);
            expect(res.status).to.equal(400);
            expect(res.body).to.be.eql({
              status: 400,
              message: 'No match found'
            });
          });
        });

        describe('there is a matching match', () => {
          it('should return the match object, without automation and forecast', async () => {
            const res = await request(app)
            .get('/api/matches/52fd4eda6b891e6702ea3490')
            .set('Authorization', token);
            expect(res.status).to.equal(200);
            expect(res.body).to.be.eql({
              status: 200,
              match: {
                _id: '52fd4eda6b891e6702ea3490',
                awayteam: '5499a23357465b41c09d4982',
                competition: '52ea654cf7073b5b2f42bc27',
                datetime: '2014-04-29T18:45:00.000Z',
                hometeam: '5499a23357465b41c09d497d',
                result: {
                  hometeam_score: 0,
                  awayteam_score: 1
                },
                status: 'closed'
              }
            });
            expect(res.body.match.automation).to.be(undefined);
            expect(res.body.match.forecast).to.be(undefined);
          });
        });
      });

      describe('with filter = score', () => {
        describe('no matching match', () => {
          it('should return an empty object', async () => {
            const res = await request(app)
            .get('/api/matches/52fd4eda6b891e6702ea3491?filter=score')
            .set('Authorization', token);
            expect(res.status).to.equal(400);
            expect(res.body).to.be.eql({
              status: 400,
              message: 'No match found'
            });
          });
        });

        describe('matching match', () => {
          it('should return only the score', async () => {
            const res = await request(app)
            .get('/api/matches/52fd4eda6b891e6702ea3490?filter=score')
            .set('Authorization', token);
            expect(res.status).to.equal(200);
            expect(res.body).to.be.eql({
              status: 200,
              score: {
                hometeam_score: 0,
                awayteam_score: 1
              }
            });
          });
        });
      });
    });
  });
});
