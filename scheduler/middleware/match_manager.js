var moment  = require('moment');
var winston = require('winston');
var cp      = require('child_process');
var path    = require('path');

var Match   = require("../models/matches.js");
var Team    = require("../models/teams.js");
var SvcTask = require('../services/svc_task.js');

exports.execute = function() {
  winston.info('Match Manager | execute');
  // load match list
  loadMatchList(function(list) {
    winston.info('Match Manager | ' + list.length + ' matches retrieved');
    // filter relevant matches
    list = filterMatchList(list);
    winston.info('Match Manager | ' + list.length + ' matches to handle');
    list.forEach(function(match) {
      return retrieveMatchResult(match);
    });
  });
};

/**
* Find all the already started matches without result
*/
function loadMatchList(next) {
  Match
  .find({
    $and: [{ status: { $ne: 'closed' } }, { status: { $ne: 'to_be_computed' } }],
    datetime: { $lte: new Date() }
  })
  .populate('awayteam hometeam')
  .exec(function(err, matches) {
    if (err) {
      winston.error('Match Manager | error when retrieving match list : ' + err);
      return;
    }
    return next(matches);
  });
}

/**
* Filter matches
*/
function filterMatchList(list) {
  var filteredList = [];
  list.forEach(function(match) {
    if (match.live === true) {
      return filteredList.push(match);
    }
    if (match.sport == 'football' && moment().isAfter(moment(match.datetime).add(110, 'm'))) {
      return filteredList.push(match);
    }
    if (match.sport == 'rugby' && moment().isAfter(moment(match.datetime).add(105, 'm'))) {
      return filteredList.push(match);
    }
    if (match.sport == 'tennis' && moment().isAfter(moment(match.datetime).add(20, 'm'))) {
      return filteredList.push(match);
    }
  });
  return filteredList;
}

/**
*
*/
function retrieveMatchResult(match) {
  var result;
  var end = true;
  if (match.live) end = false;
  var match_stringified = JSON.stringify(match);
  var child = cp.fork(path.resolve(__dirname, '../algos/get_match_score.js'));
  child.send({match: match});
  child.on('message', function(data) {
    console.log(data);
    if (data) {
      result = data.result;
      if (data.end) end = data.end;
    }
  });
  child.on('exit', function() {
    winston.info('Match Manager | Exit received for match ' + match._id);
    if (result) {
      Match.update({
        _id: match._id
      }, {
        $set: {
          result: result,
          status: 'to_be_computed'
        }
      }, function(err) {
        if (err) {
          return winston.error('Match Manager | error when updating match ' + match._id + " : " + err);
        }
        if (end) {
          winston.info('Match Manager | end of the match ' + match._id);
          SvcTask.computePoints({match: match._id, type: "auto"}, 8, function(data) {});
        }
      });
    }
    else winston.info('Match Manager | no result found for match ' + match._id);
  });
}
