var moment = require("moment");
var winston = require("winston");

var Match = require("../models/matches.js");

exports.executeNightlyReminder = function() {
  winston.info('Email Manager | executeNightlyRemind');
  // Get match of the day
  var start = moment().startOf("day").add(3, "hour");
  var end = moment().startOf("day").add(27, "hour");
  Match.find({status: 'active', datetime: {$lte: end, $gt: start}}, function(err, matches) {
    if (err) {
      winston.error("Email Manager | error when retrieving matches for nightly reminder : " + err);
      return;
    }
    // group matches by competition
    var competition = {};
    var competition_keys = [];
    matches.forEach(function(match) {
      if (!competition[match.competition]) {
        competition[match.competition] = [match];
        competition_keys.push(match.competition);
      }
      else {
        competition[match.competition].push(match);
      }
    });

    // iterate the competition
    competition_keys.forEach(function(competition_id) {
      sendNightlyReminder(competition_id, competition[competition_id]);
    });
  });
}

/**
* Send emails to all the user who havent completed their bet on the given matches
*/
function sendNightlyReminder(competition, matches) {
  /*var match_stringified = JSON.stringify(match);
  var child = cp.fork(path.resolve(__dirname, '../algos/send_nightly_reminder.js'));
  child.send({competition: competition, matches: matches});
  child.on('exit', function() {
  winston.info('Email Manager | Exit received for competition ' + competition);
});*/
}
