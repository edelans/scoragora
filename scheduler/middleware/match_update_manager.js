const winston = require('winston');
const moment  = require('moment');

const Match = require('../models/matches.js');

const ElectionManager = require('./ElectionManager.js');

const ScrapperLequipe = require('../algos/scrappers_match/lequipe.js');
const ScrapperEspn = require('../algos/scrappers_match/espn.js');
const ScrapperEurosport = require('../algos/scrappers_match/eurosport.js');

const SCRAPPERS = {
	lequipe: ScrapperLequipe,
	espn: ScrapperEspn,
	eurosport: ScrapperEurosport
};

/**
 * Find all the future matches coming soon
 * @params next: callback containing the match list
 */
const loadComingSoonMatchList = async () => {
  const limit = moment().add(7, 'days').toDate();
  const matches = await Match
  .find({
    $and: [{
      datetime: { $gte: new Date() }
    }, {
      datetime: { $lte: limit }
    }]
  }).exec();
  return matches;
};

/**
 * Find all the future matches
 * @params next: callback containing the match list
 */
const loadFutureMatchList = async () => {
	const untrackedMatches = await Match
  .find({
    status: { $ne: 'closed' },
    automation: { $exists: 1 },
		datetime: { $exists: false }
  })
  .limit(100)
	.exec();
	winston.info(untrackedMatches);
	const nextMatches = await Match
  .find({
    status: { $ne: 'closed' },
    automation: { $exists: 1 }
  })
  .limit(100)
  .sort({ datetime: 1 })
	.exec();
	return [...untrackedMatches, ...nextMatches];
}

/**
 * Retrieve the date from the differents sources
 * @param match
 */
const getDate = async (match) => {
	winston.info('Match Update Manager | retrieve date for match : ' + match._id + ' ' + match.datetime);
	if (!match.automation) {
		winston.info('Algo GetMatchDate | no automation set up');
		throw new Error('no automation set up');
	}

	const manager = new ElectionManager();

	// Iterate the sources
	['espn', 'eurosport'/*, 'lequipe'*/].forEach((source) => {
		if (match.automation[source] && match.automation[source].url) {
			manager.add(SCRAPPERS[source].scrapMatchDate(match.automation[source].url, match.sport));
		}
	});

	return manager.elect();
};

/**
 * Update the match date
 * Only apply to the matches of the week
 */
exports.executeDailyUpdate = async () => {
  winston.info('Match Update Manager | executeDailyUpdate');
  // load all the future matches coming soon
  const matches = await loadComingSoonMatchList();
	updateDate(matches);
	return { status: 200 };
};

/**
 * Update the match date
 * Apply to all the future matches
 */
exports.executeWeeklyUpdate = async () => {
  winston.info('Match Update Manager | executeWeeklyUpdate');
  // load matches of the week
  const matches = await loadFutureMatchList();
	updateDate(matches);
	return { status: 200 };
};

function updateDate(matches) {
  winston.info('Match Update Manager | ' + matches.length + ' matches to be updated');
  matches.forEach(async (match) => {
    if (!match.automation) {
    	winston.info('Match Update Manager | no automation set up');
    	return;
    }
    try {
			const res = await getDate(match);
			const date = new Date(1000*res.result);
			winston.info('Match Update Manager | update match date to ' + date);
			await Match.update({
	      _id: match._id
	    }, {
	      $set: {
	        datetime: date
	      }
	    }).exec();
		} catch(err) {
			winston.error('Match Update Manager | error when updating the date ' + err);
		}
  });
}
