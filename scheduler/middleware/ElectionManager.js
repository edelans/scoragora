const winston = require("winston");

const utils = require("../algos/utils.js");

class ElectionManager {

	constructor() {
 		this.scrappers = [];
	}

	/**
	 * Add a source
	 */
	add(p) {
		this.scrappers.push(p);
	}

	async elect() {
		const results = await Promise.all(this.scrappers);
		const resultTable = [];
		const sources = results.length;
		for (let i = 0; i < sources; i++) {
			const res = results[i];
			if (res.status === 200) {
				winston.debug('Election Manager | result found : ' + res);
				if (sources <= 2) {
					winston.debug('Election Manager | result elected from 1 source : ' + res);
					return res;
				}
				const match = resultTable.find((item) => {
					return utils.areEqual(item.value, res);
				});
				if (match) {
					match.count++;
					// Threshold to reach to consider a result found as valid
					if (match.count >= sources / 2) {
						// item.value can be sent as the result
						winston.debug('Election Manager | result elected from multiple sources : ' + match.value);
						return match.value;
					}
				}
				if (!match) {
					resultTable.push({
						value: res,
						count: 1
					});
				}
			}
		}
		winston.debug('Election Manager | no result elected');
		throw new Error('no result elected');
	}
}

module.exports = ElectionManager;
