/**
*  Schedule all the tasks
*/

var later        = require('later');
var winston      = require('winston');

var MatchManager = require("./match_manager.js");
var MatchUpdateManager = require("./match_update_manager.js");
var EmailManager = require("./email_manager.js");
var SvcTask      = require("../services/svc_task.js");

// Automated score update
var matchSchedule = {schedules: [{m: [0, 5, 10, 15, 20, 25, 30, 35, 40, 45, 50, 55], s: [30]}]};
later.setInterval(MatchManager.execute, matchSchedule);

// Automated match date update
var weeklyMatchUpdate =  {schedules: [{dw: [4], h: [2], m: [0]}]};
later.setInterval(MatchUpdateManager.executeWeeklyUpdate, weeklyMatchUpdate);

var dailyMatchUpdate =  {schedules: [{dw: [1, 2, 3, 5, 6, 7], h: [2], m: [0]}]};
later.setInterval(function() {
  winston.info("Scheduler | running daily match update");
  MatchUpdateManager.executeDailyUpdate()
}, dailyMatchUpdate);

// Nightly reminder emails
var nightlyReminder =  {schedules: [{h: [3], m: [30]}]};
later.setInterval(EmailManager.executeNightlyReminder, nightlyReminder);
// Nightly emails status

// Weekly email status

// Automated crowdscoring computing
var crowdscoringSchedule = later.parse.recur().every(6).hour();
later.setInterval(function() {
  winston.info("Scheduler | running crowdscoring");
  SvcTask.computeCrowdscoring({
    type: "auto"
  }, 8, function() {});
}, crowdscoringSchedule);
