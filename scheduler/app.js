const express	= require('express');
const morgan = require('morgan');
const bodyParser = require('body-parser');
const cookieParser = require('cookie-parser');
const methodOverride = require('method-override');
const mongoose = require('mongoose');
const https	= require('https');
const http = require('http');
const fs = require('fs');
const winston = require('winston');

const app = exports.app = express();

const cfg = require('../config');

const defaultRouter = require('./routes/index.js').router;

// configurate winston
winston.remove(winston.transports.Console);

if (process.env.NODE_ENV === 'production') {
  /**
  * Requiring 'winston-mongodb' will expose winston.transports.MongoDB
  */
  require('winston-mongodb').MongoDB;
  cfg.winstonLogs.label = 'scheduler';
  winston.add(winston.transports.MongoDB, cfg.winstonLogs);
} else {
  winston.add(winston.transports.Console, {
    colorize: true,
    prettyPrint: true,
    timestamp: true
  });
}

app.disable('x-powered-by'); // security measure
app.use(express.static(__dirname + '/public')); // set the static files location /public/img will be /img for users
app.use(morgan('dev')); // log every request to the console
app.use(bodyParser()); // pull information from html in POST
app.use(methodOverride()); // simulate DELETE and PUT

const connectToDB = async () => {
  // Do not connect to database on test, it is done elsewhere
  if (process.env.NODE_ENV !== 'test') {
    await mongoose.connect(cfg.mongo.uri, { });
    winston.log('App | Successfully connected to database');
  }
};

connectToDB().then(() => {
  // required for passport
  app.use(cookieParser()); // Parser cookie if needed (especially for auth)

  // Add headers
  app.use(function (req, res, next) {

    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, OPTIONS');

    var origin = req.headers.origin;
    if (origin &&
      (!!origin.match(/admin.scoragora.com$/) ||
        (process.env.NODE_ENV !== 'production' &&
          (origin === 'http://localhost:3002' ||
          !!origin.match(/http:\/\/192\.168\..*\..*:3002/))))) {
      res.setHeader('Access-Control-Allow-Origin', origin);
      res.setHeader('Access-Control-Allow-Headers', 'Authorization, Origin, X-Requested-With, Content-Type, Accept');
      res.setHeader('Access-Control-Allow-Credentials', true);
    } else {
      winston.info('No access control given for origin : ' + origin);
    }

    if (req.method === 'OPTIONS') {
      res.status(200).end();
    } else {
      next();
    }
  });

  // schedule
  require('./middleware/scheduler.js');

  const router = express.Router(); // get an instance of the express Router

  // Define the API routes
  router.use('/', defaultRouter);

  // Declare the API base route
  app.use('/', router);

  // var options = {
  //   key: fs.readFileSync('../config/https/server.key'),
  //   cert: fs.readFileSync('../config/https/server.crt')
  // }
  const httpInstance = http.createServer(app);
  //https.createServer(options, app).listen(cfg.scheduler_port);
  require('./sockets/index.js').initSocketIO(httpInstance);

  httpInstance.listen(cfg.scheduler_port);
  winston.info(`Creating a server on port ${cfg.scheduler_port} in ${cfg.env} mode`);
});
