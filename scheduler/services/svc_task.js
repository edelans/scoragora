const cfg = require('../../config');

const winston = require('winston');
const path = require('path');
const PythonShell = require('python-shell');

const socketIO = require('../sockets/index.js');

const Task = require('../models/tasks.js');

const COMPUTER_OPTIONS = {
  mode: 'json',
  pythonOptions: ['-u'],
  scriptPath: path.resolve(__dirname, '../../computer/services'),
  args: []
};

exports.getTasks = async (options) => {
  winston.info('Service Task | retrieving tasks details');
  return Task
  .find()
  .limit(options.limit)
  .sort({ datetime: -1 })
  .exec();
};

exports.getTaskStats = async (algo, parameters) => {
  winston.info('Service Task | retrieving tasks details');
  const selector = {
    algo,
    status: 'succeeded'
  };
  if (parameters.competition) {
    selector['parameters.competition'] = parameters.competition;
  }
  if (parameters.match) {
    selector['parameters.match'] = parameters.match;
  }
  const result = await Task
  .aggregate([
    {
      $match: selector
    }, {
      $group: {
        _id: { processes: '$processes' },
        count: { $sum: 1 },
        time: { $avg: '$execution_time' }
      }
    }, {
      $project: {
        _id: 0,
        processes: '$_id.processes',
        count: '$count',
        time: '$time'
      }
    }
  ]);
  return result;
};

/**
* Exctract the execution time from the result
*/
const handleResult = async (results, taskId) => {
  if (!results || !results.length) {
    await Task
    .update({
      _id: taskId
    }, { status: 'failed' })
    .exec();
    socketIO.notifyTaskUpdate({
      id: taskId,
      status: 'failed'
    });
    return;
  }
  winston.info(results);
  const res = results[results.length - 1];
  if (res.status === 200) {
    const update = {
      status: 'succeeded'
    };
    update.execution_time = Math.ceil((res.execution_time || 0) * 1000);
    if (res.processes) {
      update.processes = res.processes;
    }
    await Task
    .update({
      _id: taskId
    }, update)
    .exec();
    socketIO.notifyTaskUpdate({
      id: taskId,
      execution_time: update.execution_time,
      processes: update.processes,
      status: 'succeeded'
    });
    return;
  }
  if (res.message) {
    winston.error('Error when computing task ' + taskId + ', message : ' + res.message);
  }
  winston.error('Error when computing task ' + taskId + ', results : ' + JSON.stringify(results));
  await Task
  .update({ _id: taskId }, { status: 'failed' })
  .exec();
  socketIO.notifyTaskUpdate({
    id: taskId,
    status: 'failed'
  });
};

exports.computePoints = async (parameters, processes) => {
  winston.info('Service Task | creating task compute points');
  if (!parameters && !parameters.match) {
    return { status: 400, message: 'invalid parameters' };
  }
  const task = new Task({
    type: parameters.type || 'manual',
    parameters: {
      match: parameters.match,
      revert: parameters.revert || false
    },
    datetime: new Date(),
    algo: 'compute_points',
    status: 'running'
  });
  if (processes !== 0) {
    task.processes = processes;
  }
  await task.save();
  const options = Object.assign({}, COMPUTER_OPTIONS, {
    args: [parameters.match, parameters.revert || false, processes || 8]
  });
  PythonShell.run('compute_points.py', options, (err, results) => {
    if (err) {
      winston.error(err);
    }
    handleResult(results, task._id);
  });
  return { status: 200, message: 'task succesfully started' };
};

exports.computePointsFromScratch = async (parameters, processes) => {
  winston.info('Service Task | creating task compute points from scratch');
  if (!parameters && !parameters.competition) {
    return { status: 400, message: 'invalid parameters' };
  }
  const task = new Task({
    type: 'manual',
    parameters: {
      competition: parameters.competition
    },
    datetime: new Date(),
    algo: 'compute_points_from_scratch',
    status: 'running'
  });
  await task.save();
  const options = Object.assign({}, COMPUTER_OPTIONS, {
    args: [parameters.competition, processes || 8]
  });
  PythonShell.run('compute_points_from_scratch.py', options, (err, results) => {
    if (err) {
      winston.error('Service Task | error when running compute from scratch : ' + err);
    }
    handleResult(results, task._id);
  });
  return { status: 200, message: 'task succesfully started' };
};

exports.computeCrowdscoring = async (parameters, processes) => {
  winston.info('Service Task | creating task crowdscoring');
  if (!parameters) {
    return { status: 400, message: 'invalid parameters' };
  }
  const task = new Task({
    type: parameters.type || 'manual',
    parameters: {},
    datetime: new Date(),
    algo: 'compute_crowdscoring',
    status: 'running'
  });
  if (parameters.competition) {
    task.competition = parameters.competition;
  }
  await task.save();
  const args = [processes || 8];
  if (parameters.competition && parameters.sport) {
    args.push(parameters.competition);
    args.push(parameters.sport);
  }
  const options = Object.assign({}, COMPUTER_OPTIONS, { args });
  PythonShell.run('compute_crowdscoring.py', options, (err, results) => {
    if (err) {
      winston.error(err);
    }
    handleResult(results, task._id);
  });
  return { status: 200, message: 'task succesfully started' };
};

exports.computeRanking = async (parameters, processes) => {
  winston.info('Service Task | creating task ranking');
  if (!parameters && !parameters.competition) {
    return { status: 400, message: 'invalid parameters' };
  }
  const task = new Task({
    type: 'manual',
    parameters: {
      match: parameters.competition
    },
    datetime: new Date(),
    algo: 'compute_ranking',
    status: 'running'
  });
  await task.save();
  const options = Object.assign({}, COMPUTER_OPTIONS, {
    args: [parameters.competition, processes || 8]
  });
  PythonShell.run('compute_ranking.py', options, (err, results) => {
    if (err) {
      winston.error(err);
    }
    handleResult(results, task._id);
  });
  return { status: 200, message: 'task succesfully started' };
};
