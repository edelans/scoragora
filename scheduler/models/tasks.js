var mongoose = require("mongoose");

var translator = require("../../api/tools/translator.js");
var schema = require("../../config/models/tasks.js");

var TaskSchema = translator.translate(schema, mongoose);

module.exports = mongoose.model('Task', TaskSchema, 'task');
