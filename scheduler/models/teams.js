var mongoose = require("mongoose");

var translator = require("../../api/tools/translator.js");
var schema = require("../../config/models/teams.js");

var TeamSchema = translator.translate(schema, mongoose);

module.exports = mongoose.model('Team', TeamSchema, 'team');