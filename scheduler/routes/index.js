const winston = require('winston');
const express = require('express');

const checker = require('../../api/routes/checker.js');
const handleRes = require('../../api/routes/utils.js').handleRes;

const MatchUpdateManager = require('../middleware/match_update_manager');
const SvcTask = require('../services/svc_task.js');

const app = express.Router();

app.route('/logs')

  /**
  * Query options:
  * @param from : Date,
  * @param until: Date,
  * @param limit: Number,
  * @param start: Number,
  * @param order: Enum ('desc'),
  * @param fields: String
  */
  .get(checker.checkAdmin, (req, res) => {
    const options = {
      start: req.query.start || 0,
      limit: req.query.limit || 2000,
      order: req.query.order || 'desc'
    };
    if (req.query.from) {
      const from = parseInt(req.query.from, 10) || 0;
      options.from = new Date(from);
    } else {
      options.from = new Date(0);
    }
    if (req.query.until) {
      const until = parseInt(req.query.until, 10);
      // If until is a valid date, lets define a strict comparison
      if (!isNaN(until)) {
        options.until = new Date(until - 1);
      }
    }
    winston.query(options, (err, results) => {
      if (err) {
        res.status(500).send({ status: 500, message: 'internal server error' });
        throw err;
      }
      res.send({ status: 200, logs: results.mongodb });
    });
  });

app.route('/tasks')
  .get(checker.checkAdmin, (req, res) => {
    const options = {
      limit: req.query.limit || 10
    };
    handleRes(SvcTask.getTasks(options), res, 'tasks');
  })

  /*
  * Create task
  */
  .post(checker.checkAdmin, (req, res) => {
    if (!req.body || !req.body.task) {
      return res.status(400).send({ status: 400, message: 'task missing' });
    }
    if (req.body.task.algo === 'compute_points' && req.body.task.parameters) {
      return handleRes(SvcTask.computePoints(req.body.task.parameters, req.body.processes), res);
    }
    if (req.body.task.algo === 'compute_points_from_scratch' && req.body.task.parameters) {
      return handleRes(SvcTask.computePointsFromScratch(req.body.task.parameters, req.body.task.processes), res);
    }
    if (req.body.task.algo === 'compute_ranking' && req.body.task.parameters) {
      return handleRes(SvcTask.computeRanking(req.body.task.parameters, req.body.processes), res);
    }
    if (req.body.task.algo === 'compute_crowdscoring' && req.body.task.parameters) {
      return handleRes(SvcTask.computeCrowdscoring(req.body.task.parameters, req.body.task.processes), res);
    }
    if (req.body.task.algo === 'scrap_match_date') {
      return handleRes(MatchUpdateManager.executeWeeklyUpdate(), res);
    }
    return res.status(400).send({ status: 400, message: 'invalid algorithm' });
  });

app.route('/tasks/stats')

  .get(checker.checkAdmin, (req, res) => {
    if (req.query.algo) {
      const parameters = {};
      if (req.query.competition) {
        parameters.competition = req.query.competition;
      }
      if (req.query.match) {
        parameters.match = req.query.match;
      }
      return handleRes(SvcTask.getTaskStats(req.query.algo, parameters), res, 'stats');
    }
    return res.status(400).send({ status: 400, message: 'invalid input' });
  });

exports.router = app;
