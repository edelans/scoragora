const utils = require('../utils.js');
const iterator = require('../../../api/tools/iterator.js');

const scrapDayMatches = async (url, day) => {
	const $ = await utils.loadParsedDOM(url);
	const result = [];

	$('.ligne.bb-color').each((i, item) => {
		// Parse all the useful information
		const line = $(item);
		const hometeam = line.find('.equipeDom').clone().find('span').remove().end().text().trim();
		const hometeamUrl = `www.lequipe.fr${line.find('.equipeDom a').attr('href')}`;

		const awayteam = line.find('.equipeExt').clone().find('span').remove().end().text().trim();
		const awayteamUrl = `www.lequipe.fr${line.find('.equipeExt a').attr('href')}`;

		const idMatch = line.attr('idmatch');

		if (!idMatch) {
			console.log(`Scrapper Competition Lequipe | unable to get match url for ${url} with ${line.html()}`);
			return;
		}

		result.push({
			url: 'www.lequipe.fr/Football/match-direct/c/d/m/' + idMatch,
			hometeam: {
				name: hometeam,
				url: hometeamUrl
			},
			awayteam: {
				name: awayteam,
				url: awayteamUrl,
			},
			day,
		});
	});
	return result;
};

/**
 * Get days schedule on the competition page
 */
const scrapCompetitionDays = async ($) => {
	let response;
	const dayUrls = [];
	try {
		let result = [];
		// Get the day pages
		const days = $('.data .onglet-select select > option');
		const limit = days.length;
		let count = 0;
		await iterator.pmap(days, async (item, i) => {
			const matches = await scrapDayMatches('www.lequipe.fr' + $(item).val(), i);
			result = result.concat(matches);
		});
		//
		return { status: 200, matches: result };
	} catch(err) {
		return { status: 400, message: 'unable to get result' };
	}
};

/*
 * Scrap competition matches
 */
exports.scrapChampionshipMatches = async (url) => {
  const $ = await utils.loadParsedDOM(url);
  return scrapCompetitionDays($);
};
