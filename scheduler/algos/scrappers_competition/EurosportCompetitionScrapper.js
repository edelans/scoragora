const APIException = require('../../../api/routes/APIException');
const CompetitionScrapperAbstract = require('./CompetitionScrapperAbstract');
const iterator = require('../../../api/tools/iterator');
const utils = require('../utils');


const computeTeamListUrl = (url) => {
  // translate the url into the team list url
  if (url.match(/html$/i)) {
    url = url.replace(/\/([a-zA-Z-]*).shtml$/i, '/');
  }
  if (!url.match(/\/$/i)) {
    url += '/';
  }
  url += 'standing.shtml'; // teamlist.shtml before the start of the season ?
  return url;
};

const getCompetitionInfos = async (url) => {
  const teamlistUrl = computeTeamListUrl(url);
  const $ = await utils.loadParsedDOM(teamlistUrl);
  return {
    competitionId: scrapCompetitionId($),
    teamUrls: scrapTeamUrls($)
  };
};

const scrapCompetitionId = ($) => {
	let competitionId;
	try {
		const competitionData = $("#content-top .ad").attr("data-adtech-key-value");
		return JSON.parse(competitionData).recurring_event;
	} catch (e) {
		throw new APIException('competition id can\'t be extracted', 400);
	}
};

const scrapTeamUrls = ($) => {
	try {
		const urls = [];
		// Get the teams element
		const teamsDOM = $('.standing-table__team-link');
		teamsDOM.each((i, item) => {
			const teamDOM = $(item);
			urls.push('www.eurosport.fr' + teamDOM.attr('href'));
		});
    return urls;
	}	catch(err) {
		console.log('Scrapper Competition Eurosport | error when scrapping html : ' + err);
		throw new APIException('unable to get team urls', 400);
	}
};

const scrapFootballTeamSchedule = async (url, competitionId) => {
	let $ = await utils.loadParsedDOM(url);

	const teamName = $('.team-header__team-name').text();
	const scheduleUrl = 'www.eurosport.fr' + $('.teamresults_bis_v8_5 div').data('ajax-url') + '&recurringeventid=' + competitionId;

	$ = await utils.loadParsedDOM(scheduleUrl);
	const result = [];

	$('.teamresults__container.row .teamresults__resultlist-next-match').each((i, item) => {
		const matchDOM = $(item);
		const awayteamDOM = matchDOM.find('.scoreboard-match-team').last();

		// We only consider home game
		if (awayteamDOM.find('> a').length == 0) {
			return;
		}

		const link = matchDOM.find('.scoreboard-match-score .scoreboard-match-scoring a');
		const dayMatch = matchDOM.find('.scoreboard-match-score .scoreboard-match-round').text().match(/^([0-9]*)e/i);
		let day;
		if (dayMatch && dayMatch.length > 1) {
			day = dayMatch[1] - 1;
		} else {
			return;
		}
		result.push({
			url: 'www.eurosport.fr' + link.attr('href'),
			hometeam: {
				name: teamName,
				url,
			},
			awayteam: {
				url: 'www.eurosport.fr' + awayteamDOM.find('> a').attr('href'),
				name: awayteamDOM.find('> a').text()
			},
			day,
		});
	});

	return { status: 200, matches: result };
};

class EurosportCompetitionScrapper extends CompetitionScrapperAbstract {

  async scrapChampionshipMatches() {
  	const { competitionId, teamUrls } = await getCompetitionInfos(this.url);
    let matches = [];
    await iterator.pmap(teamUrls, async (teamUrl) => {
      const response = await scrapFootballTeamSchedule(teamUrl, competitionId);
      if (response.status === 200 && response.matches) {
        matches = matches.concat(response.matches);
      }
    });
    return { status: 200, matches };
  }
}

module.exports = EurosportCompetitionScrapper;
