class CompetitionScrapperAbstract {

  constructor(url) {
    this.url = url;

    this.scrapChampionshipMatches = this.scrapChampionshipMatches.bind(this);
  }

  scrapChampionshipMatches() {
    throw new Error('scrapChampionshipMatches not implemented');
  }
}

module.exports = CompetitionScrapperAbstract;
