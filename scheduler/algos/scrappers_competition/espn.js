const cheerio = require('cheerio');
const moment = require('moment');

const utils = require('../utils.js');
const iterator = require('../../../api/tools/iterator.js');

/**
 *
 */
const scrapMatchesFromTeam = async (url) => {
	const json = await utils.loadParsedJSON(url);
	if (!json || !json.content || !json.content.html) {
		console.log('Scrapper Competition Espn | unable to get html from json');
		return [];
	}
	const result = [];
	const $ = cheerio.load(json.content.html.replace('"', '\'').replace('\\/', '/'));
	// get the name of the team
	const teamDOM = $('[data-section="feed"] > a').text().split('Home');
	if (teamDOM.length < 2) {
		console.log('Scrapper Competition Espn | unable to get team name');
		return [];
	}
	const teamName = teamDOM[0].trim();
	const matches = $('.games-container > a[data-gameid]');
	matches.each((i, match) => {
		if (i === 0) {
			return; // skip the first one;
		}
		const matchUrl = $(match).attr('href');
		const hometeam = $(match).find('.score-home-team .team-logo img').attr('alt').trim();
		if (hometeam !== teamName) {
			return; // skip the away games
		}
		const awayteam = $(match).find('.score-away-team .team-logo img').attr('alt').trim();

		const date = moment($(match).find('.date-container .time').attr('data-time')).toDate();
		result.push({
			url: matchUrl,
			hometeam: { name: hometeam },
			awayteam: { name: awayteam },
			date: date,
			day: i - 1
		});
	});
	return result;
};

/**
 * Get teams fixtures url from the competition page
 */
const scrapCompetitionTeams = async ($, competitionId) => {
	let response;
	const dayUrls = [];
	const result = [];
	try {
		// Get the teams page
		const teams = $('.has-sub.teams .teams ul > li.team > a');
		const limit = teams.length;
		let count = 0;
		await iterator.pmap(teams, async (i, item) => {
			const teamUrl = $(item).attr('href');
			const splits = teamUrl.split('/');
			if (splits.length < 6) {
				return;
			}
			const teamFixturesUrl = 'www.espnfc.co.uk/team/' + splits[4] + '/'
				+ splits[5] + '/fixtures?leagueId=' + competitionId + '&season=&xhr=1';
			const matches = await scrapMatchesFromTeam(teamFixturesUrl);
			result = result.concat(matches);
		});
		return { status: 200, matches: result };
	} catch(err) {
		console.log('Scrapper Competition Espn | error when scrapping html : ' + err);
		throw { status: 400, message: 'unable to get result' };
	}
}

/*
 * Scrap competition matches
 */
exports.scrapChampionshipMatches = async (url) => {
	// Get the competition id from the url
	// www.espnfc.co.uk/french-ligue-1/9/index -> 9
	const splits = url.split('/');
	if (splits.length < 3) {
		return { status: 400, message: 'unable to get the competition id from the url provided' };
	}
	const competitionId = url.split('/')[2];
	if (isNaN(competitionId)) {
		return { status: 400, message: 'the competition id from the url is not an number' };
	}
	// Load the url to get the teams
	const $ = await utils.loadParsedDOM(url);
	return scrapCompetitionTeams($, competitionId);
};
