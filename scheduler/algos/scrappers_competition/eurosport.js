const EurosportCompetitionScrapper = require('./EurosportCompetitionScrapper');
const utils = require('../utils.js');

const scrapFootballTeamSchedule = async (url, competitionId) => {
	let $ = await utils.loadParsedDOM(url);

	const teamName = $('.team-header__team-name').text();
	const scheduleUrl = 'www.eurosport.fr' + $('.teamresults_v8_1 div').data('ajax-src') + '&recurringeventid=' + competitionId;

	$ = await utils.loadParsedDOM(scheduleUrl);
	const result = [];

	$('.all-results').each((i, item) => {
		const matchDOM = $(item);
		const awayteamDOM = matchDOM.find('.scoreboard-match-team').last();

		// We only consider home game
		if (awayteamDOM.find('> a').length == 0) {
			return;
		}

		const link = matchDOM.find('.scoreboard-match-score .scoreboard-match-scoring a');
		const dayMatch = matchDOM.find('.scoreboard-match-score .scoreboard-match-round').text().match(/^([0-9]*)e/i);
		let day;
		if (dayMatch && dayMatch.length > 1) {
			day = dayMatch[1] - 1;
		} else {
			return;
		}
		result.push({
			url: 'http://www.eurosport.fr' + link.attr('href'),
			hometeam: {
				name: teamName,
				url,
			},
			awayteam: {
				url: 'http://www.eurosport.fr' + awayteamDOM.find('> a').attr('href'),
				name: awayteamDOM.find('> a').text()
			},
			day,
		});
	});

	return { status: 200, matches: result };
};

const scrapTeamUrls = async ($) => {
	try {
		let result = [];

		// Get the competition name
		const competitionName = $('.standing-header__event-name').text();
		let competitionId;
		try {
			const competitionData = $("#content-top .ad").attr("data-adtech-key-value");
			competitionId = JSON.parse(competitionData).recurring_event;
		} catch (e) {
			return { status: 400, message: 'competition id can\'t be extracted' };
		}
		console.log('competition', competitionId);
		// Get the teams element
		const teamsDOM = $('.standing-table__team-link');
		const limit = teamsDOM.length;
		let count = 0;
		if (limit === 0) {
			return { status: 200, matches: [] };
		}
		teamsDOM.each((i, item) => {
			const teamDOM = $(item);
			const teamUrl = 'www.eurosport.fr' + teamDOM.attr('href');
		});
	}	catch(err) {
		console.log('Scrapper Competition Eurosport | error when scrapping html : ' + err);
		throw new { status: 400, message: 'unable to get team urls' };
	}
};

/**
 * Get days schedule on the competition page
 */
const scrapCompetitionTeams = async ($) => {
	try {
		let result = [];

		// Get the competition name
		const competitionName = $('.standing-header__event-name').text();
		let competitionId;
		try {
			const competitionData = $("#content-top .ad").attr("data-adtech-key-value");
			competitionId = JSON.parse(competitionData).recurring_event;
		} catch (e) {
			return { status: 400, message: 'competition id can\'t be extracted' };
		}
		console.log('competition', competitionId);
		// Get the teams element
		const teamsDOM = $('.standing-table__team-link');
		const limit = teamsDOM.length;
		let count = 0;
		if (limit === 0) {
			return { status: 200, matches: [] };
		}
		await new Promise((resolve) => {
			teamsDOM.each(async (i, item) => {
				const teamDOM = $(item);
				const teamUrl = 'www.eurosport.fr' + teamDOM.attr('href');

				const response = await scrapFootballTeamSchedule(teamUrl, competitionId);
				count++;
				if (response.status === 200 && response.matches) {
					result = result.concat(response.matches);
				}
				if (count === limit - 1) {
					resolve({ status: 200, matches: result });
				}
			});
		});
	}	catch(err) {
		console.log('Scrapper Competition Eurosport | error when scrapping html : ' + err);
		return { status: 400, message: 'unable to get result' };
	}
}

/*
 * Scrap competition matches
 */
exports.scrapChampionshipMatches = async (url) => {
	const scrapper = new EurosportCompetitionScrapper(url);
	return scrapper.scrapChampionshipMatches();
	/*

	// translate the url into the team list url
	if (url.match(/html$/i)) {
		url = url.replace(/\/([a-zA-Z-]*).shtml$/i, '/');
	}
	if (!url.match(/\/$/i)) {
		url += '/';
	}
	url += 'calendar-result.shtml'; // teamlist.shtml before the start of the season ?
	const $ = await utils.loadParsedDOM(url);
	return scrapCompetitionTeams($);*/
};
