const cheerio = require('cheerio');

const loader = require('bottleneck-loader');
const winston = require('winston');

const loadUrl = (url) => {
	if (! url.startsWith('http')) {
		url = 'https://' + url;
	}
	return new Promise((resolve, reject) => {
		loader.loadUrl(url, function (err, response, html) {
			if (err || response.statusCode != 200) {
				winston.error('Utils | error when loading url ' + url + ' with err : ' + err);
				reject({ status: 400, message: 'Unable to load html' });
				return;
			}
			resolve(html);
		});
	});
};

/*
 * Load html page and return a parsed DOM
 */
exports.loadParsedDOM = async (url) => {
  const html = await loadUrl(url);
  const $ = cheerio.load(html);
  winston.info(`Utils | ${url} loaded`);
  return $;
};

exports.loadParsedJSON = async (url) => {
	const html = await loadUrl(url);
	try {
		const json = JSON.parse(html);
		winston.info(`Utils | ${url} loaded`);
		return json;
	}
	catch(e) {
		throw { status: 400, message: 'Unable to parse JSON' };
	}
};

/**
 * Check if the two object contain the same data
 * @param x1
 * @param x2
 * @return bool
 */
var areEqual = exports.areEqual = function(x1, x2)
{
	if (x1 === null) return x2 === null;
	if (x2 === null) return x1 === null;
	var p;
	for(p in x1) {
		if(typeof(x2[p]) == 'undefined') { return false; }
	}
	for(p in x1) {
		if (x1[p]) {
			switch(typeof(x1[p])) {
				case 'object':
					if (!areEqual(x1[p], x2[p])) { return false; } break;
				case 'function':
					if (typeof(x2[p]) == 'undefined' || (x1[p].toString() != x2[p].toString())) return false;
					break;
				default:
					if (x1[p] != x2[p]) { return false; }
			}
		}
		else {
			if (x2[p]) return false;
		}
	}
	for(p in x2) {
		if(typeof(x1[p]) == 'undefined') { return false; }
	}
	return true;
};

/* test
for (i = 0; i < 11; i++) {
	exports.loadParsedDOM("https://www.scoragora.com" + i + "/less/import.less" + i, function() {
	  console.log(Date.now() + " next ");
	}, function() {
		console.log(Date.now() + " success ");
	});
}*/
