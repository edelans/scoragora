const moment = require('moment');
const utils = require('../utils');

/*
 * Scrap footaball match result
 */
const scrapFootballResult = ($, isLive) => {
	try {
		const homeScore = parseInt($('[data-bind="text: scoreHome"]').text());
		const awayScore = parseInt($('[data-bind="text: scoreAway"]').text());
		const isOver = !!$('.heromatch__status').text().match(/Term/i);
		if (isOver || isLive) {
			const result = {
				hometeam: {
					score: homeScore
				},
				awayteam: {
					score: awayScore
				}
			};
			return { status: 200, result, end: isOver };
		}
		return { status: 400, message: 'result not ready' };
	} catch(err) {
		return { status: 400, message: 'unable to get result' };
	}
};

/*
 * Scrap rugby match result
 */
const scrapRugbyResult = ($, isLive) => {
	try {
		const homeScore = parseInt($('.score-home').text());
		const awayScore = parseInt($('.score-away').text());
		const isOver = $('.live-hero.past .match-finished').length > 0;
		if (isOver || isLive) {
			const result = {
				hometeam: {
					score: homeScore
					//tries: home_tries
				},
				awayteam: {
					score: awayScore
					//tries: away_tries
				}
			};
			return { status: 200, result, end: isOver };
		}
	 	return { status: 400, message: 'result not ready' };
	}	catch(err) {
		return { status: 400, message: 'unable to get result' };
	}
};

/*
 * Scrap match result
 */
exports.scrapMatchResult = async (url, sport, isLive) => {
	const $ = await utils.loadParsedDOM(url);
	if (sport === 'football') {
		return scrapFootballResult($, isLive);
	}
	if (sport === 'rugby') {
		return scrapRugbyResult($, isLive);
	}
	//if (sport == 'tennis') return scrapTennisResult($, is_live, next);
	console.error(`Scrapper Eurosport | sport ${sport} not supported`);
	return { status: 400, message: 'sport not implemented yet' };
};

const scrapDate = ($) => {
	try {
		const strDate = $('meta[name="date"]').attr('content');
		const parsedDate = moment(strDate);
		return { status: 200, result: parsedDate.unix() };
	} catch(e) {
		console.log(e);
		return { status: 400, message: 'unable to get date' };
	}
};

exports.scrapMatchDate = async (url, sport) => {
	if (sport === 'football') {
		const $ = await utils.loadParsedDOM(url);
		return scrapDate($);
	}
	console.error('Scrapper Lequipe | sport ' + sport + ' not supported');
	return { status: 400, message: 'sport not implemented yet' };
};
