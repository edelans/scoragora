const moment = require('moment');

const utils = require('../utils.js');

/**
 * Compute if there is an inversion in the teams
 */
const computeInversion = (hometeamRef, awayteamRef, hometeam, awayteam) => (
  hometeamRef.toLowerCase() === awayteam.toLowerCase()
  || awayteamRef.toLowerCase() === hometeam.toLowerCase()
);

/*
 * Scrap football match result
 */
const scrapFootballResult = ($, hometeam, awayteam, isLive) => {
	try {
		const homeScore = parseInt($('.team.home .score').text().trim(), 10);
		const awayScore = parseInt($('.team.away .score').text().trim(), 10);
		const isOver = $('.game-status > .game-time').text().match(/^FT/);
		if (isOver || isLive) {
			// Try to detects an inversion
			const hometeamname = $('.team.home .long-name').text().trim();
			const awayteamname = $('.team.away .long-name').text().trim();
			var result;
			if (computeInversion(hometeam, awayteam, hometeamname, awayteamname)) {
				console.log('Inversion!!');
				result = {
					hometeam: {
						score: awayScore,
					},
					awayteam: {
						score: homeScore,
					}
				};
			} else {
				result = {
					hometeam: {
						score: homeScore
					},
					awayteam: {
						score: awayScore
					}
				};
			}
			return { status: 200, result, end: isOver };
		}
		return { status: 400, message: 'result not ready' };
	} catch(err) {
		return { status: 400, message: 'unable to get result' };
	}
};

/*
 * Scrap match result from the url provided
 * @param url : automation match url
 * @param sport
 * @param hometeam
 * @param awayteam
 * @param isLive : tells if the match is live
 * @param next : callback
 */
exports.scrapMatchResult = async (url, sport, hometeam, awayteam, isLive) => {
	// transform the url for espn
	if (sport === 'rugby') {
		const queries = url.split(/[?&=]/);
		let gameId, league;
		queries.forEach((query, index) => {
			if (query === 'gameId') {
				gameId = queries[index + 1];
			}
			if (query === 'league') {
				league = queries[index + 1];
			}
		});
		const newUrl = `http://site.api.espn.com/apis/site/v2/sports/rugby/${league}/summary?event=${gameId}`;
		const json = await utils.loadParsedJSON(newUrl);
		return scrapRugbyResult(json, hometeam, awayteam, isLive);
	}
	if (sport === 'football') {
		const $ = await utils.loadParsedDOM(url);
		return scrapFootballResult($, hometeam, awayteam, isLive);
	}
	console.error(`Scrapper Espn | sport ${sport} not supported`);
	return { status: 400, message: 'sport not implemented yet' };
};

/**
 * Scrap the date and convert it in unix
 * @param $ : parsed DOM
 * @param next : callback
 */
const scrapDate = ($) => {
	try {
		const date = $('.subdued span[data-date]').attr('data-date');
		if (date) {
			const parsedDate = moment(date);
			return { status: 200, result: parsedDate.unix() };
		}
		return { status: 400, message: 'unable to get date' };
	}	catch (e) {
		return { status: 400, message: 'unable to get date' };
	}
};

/**
 * Scrap match date from the url given
 * @param url
 * @param sport
 * @param next : callback
 */
exports.scrapMatchDate = async (url, sport) => {
  if (sport === 'football') {
		const $ = await utils.loadParsedDOM(url);
		return scrapDate($);
	}
	console.error(`Scrapper Espn | sport ${sport} not supported`);
	return { status: 400, message: 'sport not implemented yet' };
};

/*
 * Scrap rugby match result from JSON
 * example : http://www.espn.co.uk/rugby/match?gameId=181992&league=164205
 */
function scrapRugbyResult(html, hometeam, awayteam, isLive) {
	var response;
	try {
		data = JSON.parse(html);
		if (data && data.header && data.header.competitions && data.header.competitions[0] && data.header.competitions[0].status && data.header.competitions[0].status.type) {
			var status = data.header.competitions[0].status.type.state;
			if (status != 'pre') {
				var teams = data.header.competitions[0].competitors;
				var home_score, away_score, homeId, awayId;
				var inversion = false;
				teams.forEach(function(team) {
					if (team.homeAway == 'home') {
						homeId = team.id;
						home_score = parseInt(team.score, 10);

						// Check if there is an inversion
						if (awayteam && team.team && team.team.name && awayteam == team.team.name.toLowerCase()) inversion = true;
					}
					if (team.homeAway == 'away') {
						awayId = team.id;
						away_score = parseInt(team.score);

						// Check if there is an inversion
						if (hometeam && team.team && team.team.name && hometeam == team.team.name.toLowerCase()) inversion = true;
					}
				});
				if (awayId && homeId && data.boxscore && data.boxscore.teams && data.boxscore.teams.length > 1) {
					var teamStats = data.boxscore.teams;
					var homeTries;
					teamStats.forEach(function(stat) {
						if (stat.statistics && stat.statistics.length > 0) {
							if (stat.team && stat.team.id == homeId) {
								homeStats = stat.statistics[0].stats;
								homeStats.forEach(function(stat) {
									if (stat.name == 'tries') {
										homeTries = parseInt(stat.value);
									}
								});
							}
							if (stat.team && stat.team.id == awayId) {
								awayStats = stat.statistics[0].stats;
								awayStats.forEach(function(stat) {
									if (stat.name == 'tries') {
										awayTries = parseInt(stat.value);
									}
								});
							}
						}
					});
				}
				if (status === 'post' || isLive) {
					var result = {
						hometeam: {
							score: home_score
							//tries: homeTries
						},
						awayteam: {
							score: away_score
							//tries: awayTries
						}
					};
					// Invert the score if an inversion has been detected
					if (inversion) {
						console.log('Inversion !!');
						result = {
							awayteam: {
								score: home_score
								//tries: homeTries
							},
							hometeam: {
								score: away_score
								//tries: awayTries
							}
						};
					}
					return { status: 200, result: result, end: status === 'post' };
				}
				return { status: 400, message: 'result not ready' };
			}
			return { status: 400, message: 'result not ready' };
		}
		return { status: 400, message: 'wrong data structure' };
	} catch(err) {
		console.log(err);
		return { status: 400, message: 'unable to get result' };
	}
	return response;
}
