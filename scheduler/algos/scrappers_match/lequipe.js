const moment = require('moment');
const utils = require('../utils');

/*
 * Scrap footaball match result
 */
const scrapFootballResult = async ($, hometeam, awayteam, isLive) => {
	try {
		const root = $('.scoreboard__scoring');
		const score = root.find('.scoreboard__gamescore').text().split('-').map(i => i.replace(/\r?\n|\r/g, '').trim());
		const awayScore = parseInt(score[1]);
		const homeScore = parseInt(score[0]);
		const awayName = root.find('.scoreboard__team.scoreboard__team--away').text().trim();
		const homeName = root.find('.scoreboard__team.scoreboard__team--home').text().trim();
		const isCompleted = root.find('.scoreboard__match').attr('data-statut') === 'termine';
		if (isCompleted || isLive) {
			let result = {
				hometeam: {
					score: homeScore
				},
				awayteam: {
					score: awayScore
				}
			};
			if ((hometeam && awayName && hometeam === awayName.toLowerCase())
				|| (awayteam && homeName && awayteam === homeName.toLowerCase())) {
				console.log('Inversion !!!');
				result = {
					hometeam: {
						score: awayScore
					},
					awayteam: {
						score: homeScore
					}
				};
			}
			return { status: 200, result, end: isCompleted };
		}
		return { status: 400, message: 'result not ready' };
	} catch(err) {
		return { status: 400, message: 'unable to get result' };
	}
};

/*
 * Scrap rugby match result
 */
const scrapRugbyResult = ($, hometeam, awayteam, isLive) => {
	try {
		const homeScore = parseInt($('#scoDom').html());
		const awayScore = parseInt($('#scoExt').html());
		const homeTries = parseInt($('#EqDom #pES > a > span').html());
		const homeName = $('#EqDom > .equipe > .ablok').text().trim();
		const awayTries = parseInt($('#EqExt #pES > a > span').html());
		const awayName = $('#EqExt > .equipe > .ablok').html().trim();
		var status = $('.sco > .min').attr('statut');
		if (status == 'T' || is_live) {
			let result = {
				hometeam: {
					score: homeScore
					//tries: home_tries
				},
				awayteam: {
					score: awayScore
					//tries: away_tries
				}
			};
			if ((hometeam && awayName && hometeam === awayName.toLowerCase())
				|| (awayteam && homeName && awayteam === homeName.toLowerCase())) {
				console.log('Inversion !!!');
				result = {
					hometeam: {
						score: awayScore
						//tries: awayTries
					},
					awayteam: {
						score: homeScore
						//tries: homeTries
					}
				};
			}
			return { status: 200, result, end: status === 'T' };
		}
		return { status: 400, message: 'result not ready' };
	} catch(err) {
		return { status: 400, message: 'unable to get result' };
	}
};

/*
 * Scrap tennis match result
 */
const scrapTennisResult = ($, is_live) => {
	try {
		const homeSets = [];
		const awaySets = [];
		$('.res-j1 .set1.score').each(function(i, item) {
			const set = $(item).clone().children().remove().end().text();
			if (set === ' ') {
				return;
			}
			homeSets.push(parseInt(set));
		});
		$('.res-j2 .set1.score').each(function(i, item) {
			const set = $(item).clone().children().remove().end().text();
			if (set === ' ') return;
			awaySets.push(parseInt(set));
		});
		const isOver = $('.tennis_victoire').length > 0;
		if (isOver || isLive) {
			const result = {
				hometeam: {
					sets: homeSets
				},
				awayteam: {
					sets: awaySets
				},
			};
			return { status: 200, result };
		}
		return { status: 400, message: 'result not ready' };
	} catch(err) {
		return { status: 400, message: 'unable to get result' };
	}
};

/*
 * Scrap match result
 */
exports.scrapMatchResult = async (url, sport, hometeam, awayteam, isLive) => {
	const $ = await utils.loadParsedDOM(url);
	if (sport === 'football') {
		return scrapFootballResult($, hometeam, awayteam, isLive);
	}
	if (sport === 'rugby') {
		return scrapRugbyResult($, hometeam, awayteam, isLive);
	}
	if (sport === 'tennis') {
		return scrapTennisResult($, isLive);
	}
	console.error(`Scrapper Lequipe | sport ${sport} not supported`);
	return { status: 400, message: 'sport not implemented yet' };
};

/**
 * Scrap the date and convert it in unix
 * @param $ : parsed DOM
 */
const scrapDate = ($) => {
	try {
		const json = JSON.parse($('script[type="application/ld+json"]').get()[0].children[0].data);
		const strDate = json.coverageStartTime.replace(/ /g, '');
		const parsedDate = moment(strDate);
		return { status: 200, result: parsedDate.unix() };
	} catch (e) {
		return { status: 400, message: 'unable to get date' };
	}
};

/**
 * Scrap match date from the url given
 * @param url
 * @param sport
 * @param next : callback
 */
exports.scrapMatchDate = async (url, sport) => {
	if (sport === 'football') {
		const $ = await utils.loadParsedDOM(url);
		return scrapDate($);
	}
	console.error('Scrapper Lequipe | sport ' + sport + ' not supported');
	return { status: 400, message: 'sport not implemented yet' };
};
