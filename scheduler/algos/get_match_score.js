const ElectionManager = require('../middleware/ElectionManager.js');

const ScrapperLequipe = require('./scrappers_match/lequipe.js');
const ScrapperEspn = require('./scrappers_match/espn.js');
const ScrapperEurosport = require('./scrappers_match/eurosport.js');

// TEST
/*var url = 'http://www.lequipe.fr/Rugby/match/19012';
ScrapperLequipe.scrapMatchResult(url, 'rugby', null, null, false, function(data) {
	console.log(data);
});*/

// Listen to data coming from the parent process
process.on('message', async (data) => {
	// Check if match exists
	if (!data.match) {
		console.error('Algo GetMatchScore | no match given');
		process.exit();
		return;
	}

  const match = data.match;
  // TODO need to get team name from sources
  const hometeam = match.hometeam.name ? match.hometeam.name.replace('_', ' ') : null;
  const awayteam = match.awayteam.name ? match.awayteam.name.replace('_', ' ') : null;
  if (!match.automation) {
    console.error('Algo GetMatchScore | no automation set up');
    process.exit();
    return;
  }

  const manager = new ElectionManager();

	// Iterate the sources
	// lequipe.fr
	if (match.automation.lequipe && match.automation.lequipe.url) {
		manager.add(ScrapperLequipe.scrapMatchResult(match.automation.lequipe.url, match.sport, null, null, match.live));
	}
	// espn
	if (match.automation.espn && match.automation.espn.url) {
		const espnHometeam = (match.hometeam.automation && match.hometeam.automation.espn && match.hometeam.automation.espn.name && match.hometeam.automation.espn.name.trim() !== '') ? match.hometeam.automation.espn.name : hometeam;
		const espnAwayteam = (match.awayteam.automation && match.awayteam.automation.espn && match.awayteam.automation.espn.name && match.awayteam.automation.espn.name.trim() !== '') ? match.awayteam.automation.espn.name : awayteam;
		manager.add(ScrapperEspn.scrapMatchResult(match.automation.espn.url, match.sport, espnHometeam, espnAwayteam, match.live));
	}

	// eurosport
	if (match.automation.eurosport && match.automation.eurosport.url) {
		manager.add(ScrapperEurosport.scrapMatchResult(match.automation.eurosport.url, match.sport, match.live));
	}

	// Check if a response can be sent to the scheduler
	try {
		const res = await manager.elect();
		console.log('Algo GetMatchScore | result found');
		process.send(res);
	} catch (e) {
		// no result found. empty response to sent
		console.log('Algo GetMatchScore | no result found');
	}
	process.exit();
});
