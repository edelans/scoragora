const utils = require('../utils.js');

/**
 * Get the information on the team page
 * Get the team name
 */
const extractTeamName = ($) => {
	try {
		const name = $('section.fichetitle > h1').text();
		return name;
	} catch(err) {
		console.log('Scrapper Team Lequipe | error when scrapping html : ' + err);
		return { status: 400, message: 'unable to get result' };
	}
};

/*
 * Scrap team
 */
exports.scrapTeamName = async (url) => {
	const $ = await utils.loadParsedDOM(url);
	return extractTeamName($);
}
