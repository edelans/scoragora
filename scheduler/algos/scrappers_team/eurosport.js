const utils = require('../utils.js');

/**
 * Get the information on the team page
 * Get the team name
 */
const scrapFootballTeamName = ($) => {
	try {
		const name = $('.team-header__team-name').text();
		return name;
	} catch(err) {
		console.log('Scrapper Team Eurosport | error when scrapping html : ' + err);
		return { status: 400, message: 'unable to get result' };
	}
};

/**
 * Get the information on the team page
 * Get the team name
 */
const scrapRugbyTeamName = ($) => {
	try {
		const title = $('.page-header h1');
		const name = title.clone().find('span').remove().end()
		.text();
		return name;
	} catch(err) {
		console.log('Scrapper Team Eurosport | error when scrapping html : ' + err);
		return { status: 400, message: 'unable to get result' };
	}
};

/*
 * Scrap team
 */
exports.scrapTeamName = async (url, sport) => {
	const $ = await utils.loadParsedDOM(url);
	if (sport === 'football') {
		return scrapFootballTeamName($);
	}
	if (sport === 'rugby') {
		return scrapRugbyTeamName($);
	}
	return { status: 400, message: 'sport not supported' };
};
