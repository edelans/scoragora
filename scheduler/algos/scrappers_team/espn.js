const utils = require('../utils');
/**
 * Get the information on the team page
 * Get the team name
 */
const extractTeamName = ($) => {
	let response;
	try {
		const name = $('.tertiary-mobile .team-name > .link-text').text().trim();
		return name;
	} catch(err) {
		console.log('Scrapper Team Eurosport | error when scrapping html : ' + err);
		return { status: 400, message: 'unable to get result' };
	}
};

/*
 * Scrap team
 */
exports.scrapTeamName = async (url/* , sport*/) => {
  const $ = await utils.loadParsedDOM(url);
  return extractTeamName($);
};
