var winston = require('winston');
var jwt = require('jwt-simple');
var moment = require('moment');

var cfg = require('../../config');

var io;

/*
* Important notice:
* This socket is not secured.
* It should only be used to send data from server to client.
* Not the opposite way
*/
const authorize = () => {
  return (socket, next) => {
    if (!socket || !socket.request) {
      winston.error('Socket IO | socket request should be provided');
      return;
    }
    const req = socket.request;
    if (!req._query || !req._query.token) {
      winston.error('Socket IO | socket request should contain a query');
      return;
    }
    const token = req._query.token;

    let payload = null;
    try {
      payload = jwt.decode(token, cfg.secret);
    } catch (err) {
      winston.error('Socket IO | error catched when decoding token : ' + err);
      return;
    }

    if (payload.exp <= moment().unix() || !payload.admin) {
      winston.error('Socket IO | unvalid payload');
      return;
    }
    req.user = payload.sub;
    next();
  };
};

exports.initSocketIO = function(httpInstance) {
  io = require('socket.io').listen(httpInstance).of('/scheduler');

  process.env.DEBUG = 'socket.io:socket';

  io.use(authorize());

  io.on('connection', function (socket) {
    winston.info('Socket IO | socket connection');

    socket.on('hello', function(data) {
      winston.info('Socket IO | receiving hello');
      socket.room = 'admin'
      socket.join(socket.room);
    });

    socket.on('disconnect', function(data) {
      winston.info('Socket IO | Disconnecting');
    });
  });
};

exports.notifyTaskUpdate = (data) => {
  if (!io) {
    winston.info('socket not open');
    return;
  }
  winston.info('Socket IO | Sending task update');
  io.to('admin').emit('task_update', data);
};
